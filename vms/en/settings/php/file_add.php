<?php

//Start session
session_start();

// Prevent caching.
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 01 Jan 1996 00:00:00 GMT');
// The JSON standard MIME header.
header('Content-type: application/json');

//Default Data
$error			= false;
$error_msg		= null;
$error_level	= null;
$error_code		= 0;
$hash			= null; //Default, no password generated
$verified		= false;

$request		= null;
$io_agency		= null;
$io_decr		= null;

$data_file		= null;

//Access check
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
if(!grant_access([3004]))
{
	error_response_json(
		"Access denied - Distributor creation is not allowed.",
		1,
		__LINE__,
		null,
		null);
}

// No required data received in request?
if(!isset($_POST['token']))
{
	error_response_json(
		"Please login before continuing.",
		1,
		__LINE__,
		null,
		null);
}

if(!isset($_POST['request']))
{
	error_response_json(
		"Request type not available.",
		1,
		__LINE__,
		null,
		null);
}

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session_token.php';
if (!verify_token($_POST['token']))
{
	error_response_json(
		"Please login before continuing.",
		1,
		__LINE__,
		null,
		null);
}

if(!isset($_POST['data']))
{
	error_response_json(
		"No request data was received.",
		1,
		__LINE__,
		null,
		null);
}

$data_rec = json_decode($_POST['data'], true);

//Open DB connection
include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/db_vmsx.php';

// Protect from MySQL injection
$io_req	= mysqli_real_escape_string($mysqli, stripslashes($_POST['request']));
$io_file_name	= mysqli_real_escape_string($mysqli, stripslashes($data_rec['file']['fname']));

//Required fields check
if($io_req != "file_add")
{
	error_response_json(
		"Invalid request type.",
		1,
		__LINE__,
		null,
		$mysqli);
}


//$conn=new PDO('mysql:host=localhost; dbname=demo', 'root', '') or die(mysql_error());
//if(isset($_POST['submit'])!=""){
  $name=$_FILES['file']['name'];
  $size=$_FILES['file']['size'];
  $type=$_FILES['file']['type'];
  $temp=$_FILES['file']['tmp_name'];
  // $caption1=$_POST['caption'];
  // $link=$_POST['link'];
  $fname = date("YmdHis").'_'.$name;
  
   
$sqli_query =  "SELECT * FROM  upload where name = ? ";
$stmt = $mysqli->prepare($sqli_query);
$stmt->bind_param("s",$name);
$stmt->execute();

if(!($result = $stmt->get_result()))
{
	error_response_json(
		"Could not connect to the database.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}
$num_rows = $result->num_rows;


  if($num_rows > 0){
    $i = 1;
    $c = 0;
	while($c == 0){
    	$i++;
    	$reversedParts = explode('.', strrev($name), 2);
    	$tname = (strrev($reversedParts[1]))."_".($i).'.'.(strrev($reversedParts[0]));
    // var_dump($tname);exit;
    
    	//$sqli_query =  "SELECT * FROM  upload where name = ? ";
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param("s",$tname);
	$stmt->execute();
	$num_rows2 = $result->num_rows;
    	//$chk2 = $conn->query("SELECT * FROM  upload where name = '$tname' ")->rowCount();
    	if(num_rows2 == 0){
    		$c = 1;
    		$name = $tname;
    	}
    }
}
 $move =  move_uploaded_file($temp,"upload/".$fname);
 

 if($move){
 	$query=$conn->query("insert into upload(name,fname)values('$name','$fname')");
	if($query){
	header("location:index.php");
	}
	else{
	die(mysql_error());
	}
 }
//}
?>
