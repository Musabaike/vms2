<div class="row">
	<div class="input-field col s1 m1 right">
		<a id="btn_order_items_search" class="btn-floating waves-effect waves-light telkom-blue tooltipped right" data-position="right" data-tooltip="Search the list of order items"><i class="material-icons">search</i></a>
	</div>
	<div class="input-field col s5 m2 l3 xl2 right">
		<select multiple id="order_items_filter_sel">
			<option value="" disabled selected>Choose Columns</option>
		</select>
		<label>Filter by columns</label>
	</div>
	<div class="input-field col  s6 m3 right">
		<i class="material-icons prefix">filter_list</i>
		<input id="order_items_filter" type="text" class="validate">
		<label for="order_items_filter">Filter</label>
	</div>
</div>

<div class="divider"></div>		
<div class="row">
	<div class="col s12 center">
		<table id="order_items_tbl" class="responsive-table highlight striped">
			<thead>
				<tr>
					<th class="vms-sort">Item <i class="material-icons tiny vms-sort"></th>
					<th class="vms-sort">Voucher <i class="material-icons tiny vms-sort"></th>
					<th class="vms-sort">Qty <i class="material-icons tiny vms-sort"></th>
					<th class="vms-sort">Start Serial <i class="material-icons tiny vms-sort"></th>
					<th class="vms-sort">End Serial <i class="material-icons tiny vms-sort"></th>
					<th class="vms-sort">Status <i class="material-icons tiny vms-sort"></th>
					<th class="vms-sort">Last Updated On <i class="material-icons tiny vms-sort"></th>
					<th class="vms-sort">Last Updated By <i class="material-icons tiny vms-sort"></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
<div class="row">
	<div class="input-field col s12 m10 l10">
		<ul id="page_order_items" class="pagination">
			<li class="disabled"><a data-page="null" href="#!"><i class="material-icons">chevron_left</i></a></li>
			<li class="disabled right"><a data-page="null" href="#!"><i class="material-icons">chevron_right</i></a></li>
		</ul>
	</div>
	<div class="input-field col s3 m2 l2 right">
		<select id="page_limit_order_items" class="right">
			<option value="2">2</option>
			<option value="5">5</option>
			<option value="10" selected>10</option>
			<option value="20">20</option>
			<option value="50">50</option>
			<option value="100">100</option>
		</select>
		<label>No. of rows</label>
	</div>
</div>
