<div class="row">
	<h6>Order Progress: 	
	</h6>
</div>

<div id="order_inf_status" class="row red-text text-accent-4">
</div>

<div class="row">
	<div class="card-panel col s12 m6 l3">
		<h6>SAP <i class="tiny material-icons">arrow_forward</i> VMS</h6>
		<p>
			<label>
				<input id="order_prog_1" type="checkbox" disabled="disabled" checked/>
				<span id="order_prog_1_sp">Order Received</span>
			</label>
		</p>
		<p>
			<label>
				<input id="order_prog_2" type="checkbox" disabled="disabled" checked/>
				<span id="order_prog_2_sp">Order Loaded</span>
			</label>
		</p>
	</div>
	<div class="card-panel col s12 m6 l3">
		<h6>VMS <i class="tiny material-icons">arrow_forward</i> ESP</h6>
		<p>
			<label>
				<input id="order_prog_3" type="checkbox" disabled="disabled" checked/>
				<span id="order_prog_3_sp">ESP file created</span>
			</label>
		</p>
		<p>
			<label>
				<input id="order_prog_4" type="checkbox" disabled="disabled" checked/>
				<span id="order_prog_4_sp">Sent to ESP</span>
			</label>
		</p>
	</div>
	<div class="card-panel col s12 m6 l3">
		<h6>ESP <i class="tiny material-icons">arrow_forward</i> VMS</h6>
		<p>
			<label>
				<input id="order_prog_5" type="checkbox" disabled="disabled" checked/>
				<span id="order_prog_5_sp">Received from ESP</span>
			</label>
		</p>
		<p>
			<label>
				<input id="order_prog_6" type="checkbox" disabled="disabled" />
				<span id="order_prog_6_sp">Verified ESP load</span>
			</label>
		</p>
	</div>
	<div class="card-panel col s12 m6 l3">
		<h6>VMS <i class="tiny material-icons">arrow_forward</i> SFG</h6>
		<p>
			<label>
				<input id="order_prog_7" type="checkbox" disabled="disabled" />
				<span id="order_prog_7_sp">Distributor file created</span>
			</label>
		</p>
		<p>
			<label>
				<input id="order_prog_8" type="checkbox" disabled="disabled" />
				<span id="order_prog_8_sp">Sent to distributor </span>
			</label>
		</p>
	</div>
</div>

<div class="row">
	<h6>Order Details:</h6>
</div>
<div class="row">
	<div class="collection">
		<a href="#!" class="collection-item"><b>SAP Information</b></a>
		<a href="#!" class="collection-item"><span id="order_inf_sap_order_no" class="badge"></span>Order No.</a>
		<a href="#!" class="collection-item"><span id="order_inf_sap_date_rec" class="badge"></span>Date Received</a>
		<a href="#!" class="collection-item"><span id="order_inf_sap_seq_no" class="badge"></span>Sequence No.</a>
	</div>
	
	<div class="collection">
		<a href="#!" class="collection-item"><b>System Information</b></a>
		<a href="#!" class="collection-item"><span id="order_inf_created_on" class="badge"></span>Created On</a>
		<a href="#!" class="collection-item"><span id="order_inf_created_by" class="badge"></span>Created By</a>
		<a href="#!" class="collection-item"><span id="order_inf_updated_on" class="badge"></span>Updated On</a>
		<a href="#!" class="collection-item"><span id="order_inf_updated_by" class="badge"></span>Updated By</a>
	</div>

	<div class="collection">
		<a href="#!" class="collection-item"><b>File Information</b></a>
		<a href="#!" class="collection-item"><span id="order_inf_sap_file_name" class="badge"></span>SAP File Name</a>
		<a href="#!" class="collection-item"><span id="order_inf_esp_file_input" class="badge"></span>ESP File Input</a>
		<a href="#!" class="collection-item"><span id="order_inf_esp_file_output" class="badge"></span>ESP File Output</a>
	</div>
</div>

<div class="row">
<div class="input-field col s12">
		<textarea placeholder="Reason to cancel the order" id="io_cancel_reason" data-length="255" maxlength="255" class="validate"></textarea>
		<label for="io_cancel_reason">Reason to cancel </label>
	</div>
</div>

<div class="row" <?php if(!grant_access([5000])){echo "hide";} ?> >
      <button class = "btn waves-effect waves-light pink" type = "submit" id="mod_order_edit_btn_cancel_order">Cancel Order
            <i class = "material-icons right" >cancel</i></button>
</div>
