<?php
//Filter results
if (isset($_POST['filter']['columns']) && isset($_POST['filter']['keyword']))
{
	if($_POST['filter']['keyword'] != "" && $_POST['filter']['keyword'] != "null" && $_POST['filter']['keyword'] != null)
	{
		$temp_counter = 0;
		$temp_length = count($_POST['filter']['columns']);
		$temp_multi_conditions = false;
		$temp_keyword = $_POST['filter']['keyword'];
		$filter = "WHERE ( ";
		foreach($_POST['filter']['columns'] as $key => $value)
		{
			//error_log($_POST['filter']['columns'][$key],0);
			switch($_POST['filter']['columns'][$key])
			{
				case 0:
					$temp_col = "e.order_number";
					break;
				case 1:
					$temp_col = "e.customer_name";
					break;
				case 2:
					$temp_col = "e.date";
					break;
				case 3:
					$temp_col = "e.item_description";
					break;
				case 4:
					$temp_col = "e.osid";
					break;
				case 5:
					$temp_col = "e.updated_on";
					break;
				case 6:
					$temp_col = "e.updated_by";
					break;
				default:
				$temp_col = "";
				$temp_counter++;
			}
			
			if($temp_col != "")
			{
				if($temp_multi_conditions)
				{
					$filter = $filter."OR $temp_col LIKE '%$temp_keyword%' ";
				}
				else
				{
					$filter = $filter."$temp_col LIKE '%$temp_keyword%' ";
					$temp_multi_conditions = true;
				}
			}
		}
		$filter = $filter.")";
		if ($temp_counter >= $temp_length)
		{	//No columns were selected
			$filter = "";
		}
	}
}


//error_log($filter,0);
?>