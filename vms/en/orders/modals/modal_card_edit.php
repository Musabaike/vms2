<div id="modal_card_edit" class="modal modal-fixed-footer">
	<div class="modal-content">
		<h4>Card: <span id="io_edit_user_uname"></span></h4>
		
		<div class="row">
			<div class="col s12">
				<div class="row">
					<div class="col s12">
						<ul id="tab_order_edit" class="tabs">
							<li class="tab col s4"><a id="order_inf_tab" class="active" href="#tab_users_info"><b>Information </b></a></li>
							<li class="tab col s4"><a id="order_cards_tab" href="#tab_order_cards"><b>Vouchers </b></a></li>
						</ul>
					</div>
					<div id="tab_order_info" class="col s12"><?php //include $_SERVER['DOCUMENT_ROOT'].'/en/orders/tabs/tab_card_inf.php'; ?></div>
					<div id="tab_order_cards" class="col s12"><?php //include $_SERVER['DOCUMENT_ROOT'].'/en/orders/tabs/tab_card_other.php'; ?></div>
				</div>
				
			</div>
		</div>
		<div class="row">
			<div class="col s12 m2 l3">
			</div>
			<div class="col s12 m8 l6">
			</div>
			<div class="col s12 m2 l3">
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<a href="#!" class="modal-close waves-effect waves-red btn-flat">Close</a>
	</div>
</div>