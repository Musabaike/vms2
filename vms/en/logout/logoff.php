<?php
//Start session
session_start();

// Prevent caching.
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 01 Jan 1996 00:00:00 GMT');
// The JSON standard MIME header.
header('Content-type: application/json');

//Default Data
$error			= false;
$error_msg		= null;
$error_level	= null;
$error_code		= 0;

$request		= null;
$logoff			= false;

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';

// Empty data received in request.
if(empty($_POST['token']))
{
	error_response_json(
		"Please login before continuing.",
		1,
		__LINE__,
		null,
		null);
}

// Data received in request.
if(empty($_POST['request']))
{
	error_response_json(
		"Request data not available.",
		1,
		__LINE__,
		null,
		null);
}


include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session_token.php';
if (!verify_token($_POST['token']))
{
	error_response_json(
		"Please login before continuing.",
		1,
		__LINE__,
		null,
		null);
}


// Protect from MySQL injection
$io_request	= stripslashes($_POST['request']);

if($io_request != "logoff")
{
	error_response_json(
		"Could not logoff account.",
		1,
		__LINE__,
		null,
		null);
}

if(!file_exists($_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/secure/session_destroy.php'))
{
	error_response_json(
		"Could not logoff account. Please try again or contact the site administrator.",
		1,
		__LINE__,
		null,
		null);
}

//Destroy session:
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/secure/session_destroy.php';

$logoff = true;

//create a JSON data structure.
$data_json = array (	
	'errors' 			=> $error,
	'error_msg'			=> $error_msg,
	'error_level'		=> $error_level,
	'error_code'		=> $error_code,
	'logoff'			=> $logoff
);

//Respond to request.
echo json_encode($data_json, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
?>