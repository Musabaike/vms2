<?php
//Start session
session_start();

// Prevent caching.
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 01 Jan 1996 00:00:00 GMT');
// The JSON standard MIME header.
header('Content-type: application/json');

//Default Data
$error			= false;
$error_msg		= null;
$error_level	= null;
$error_code		= 0;
$hash			= null; //Default, no password generated
$verified		= false;

$request		= null;
$io_uname		= null;
$io_name		= null;
$io_initials	= null;
$io_surname		= null;
$io_email		= null;
$io_salref		= null;
$io_pword		= null;
$io_group		= null;
$io_status		= null;
$io_enabled		= null;

$data_users		= null;

//Access check
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
if(!grant_access([4004]))
{
	error_response_json(
		"Access denied.",
		1,
		__LINE__,
		null,
		null);
}

// No required data received in request?
if(!isset($_POST['token']))
{
	error_response_json(
		"Please login before continuing.",
		1,
		__LINE__,
		null,
		null);
}

if(!isset($_POST['request']))
{
	error_response_json(
		"Request data not available.",
		1,
		__LINE__,
		null,
		null);
}

if(!isset($_POST['uname']))
{
	error_response_json(
		"No username was received.",
		1,
		__LINE__,
		null,
		null);
}

if(!isset($_POST['fname']))
{
	error_response_json(
		"No first name was received.",
		1,
		__LINE__,
		null,
		null);
}

if(!isset($_POST['lname']))
{
	error_response_json(
		"No surname was received.",
		1,
		__LINE__,
		null,
		null);
}

if(!isset($_POST['email']))
{
	error_response_json(
		"No email address was received.",
		1,
		__LINE__,
		null,
		null);
}

if(!isset($_POST['salref']))
{
	error_response_json(
		"No salary reference number was received.",
		1,
		__LINE__,
		null,
		null);
}

if(!isset($_POST['pword']))
{
	error_response_json(
		"No password was received.",
		1,
		__LINE__,
		null,
		null);
}

if(!isset($_POST['group']))
{
	error_response_json(
		"No access type was received.",
		1,
		__LINE__,
		null,
		null);
}

if(!isset($_POST['enabled']))
{
	error_response_json(
		"No enable indication was received.",
		1,
		__LINE__,
		null,
		null);
}

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session_token.php';
if (!verify_token($_POST['token']))
{
	error_response_json(
		"Please login before continuing.",
		1,
		__LINE__,
		null,
		null);
}

//Open DB connection
include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/db_vmsx.php';

// Protect from MySQL injection
$io_req			= mysqli_real_escape_string($mysqli, stripslashes($_POST['request']));
$io_uname		= mysqli_real_escape_string($mysqli, stripslashes($_POST['uname']));
$io_initials	= mysqli_real_escape_string($mysqli, stripslashes($_POST['initials']));
$io_name		= mysqli_real_escape_string($mysqli, stripslashes($_POST['fname']));
$io_surname		= mysqli_real_escape_string($mysqli, stripslashes($_POST['lname']));
$io_email		= mysqli_real_escape_string($mysqli, stripslashes($_POST['email']));
$io_salref		= mysqli_real_escape_string($mysqli, stripslashes($_POST['salref']));
$io_pword		= mysqli_real_escape_string($mysqli, stripslashes($_POST['pword']));
$io_group		= mysqli_real_escape_string($mysqli, stripslashes($_POST['group']));
$io_enabled		= mysqli_real_escape_string($mysqli, stripslashes($_POST['enabled']));


//Required fields check
if($io_req != "user_add")
{
	error_response_json(
		"Invalid request type.",
		1,
		__LINE__,
		null,
		$mysqli);
}

if($io_uname == "")
{
	error_response_json(
		"No username was specified.",
		1,
		__LINE__,
		null,
		$mysqli);
}

if($io_name == "")
{
	error_response_json(
		"No first name was specified.",
		1,
		__LINE__,
		null,
		$mysqli);
}

if($io_surname == "")
{
	error_response_json(
		"No surname was specified.",
		1,
		__LINE__,
		null,
		$mysqli);
}

if($io_email == "")
{
	error_response_json(
		"No email address was specified.",
		1,
		__LINE__,
		null,
		$mysqli);
}

if($io_salref == "")
{
	error_response_json(
		"No salary reference number was specified.",
		1,
		__LINE__,
		null,
		$mysqli);
}

if($io_pword == "")
{
	error_response_json(
		"No password was specified.",
		1,
		__LINE__,
		null,
		$mysqli);
}

if($io_group == "")
{
	error_response_json(
		"No access type was received.",
		1,
		__LINE__,
		null,
		$mysqli);
}

if($io_status == "")
{
	$io_status = 0;
}

if($io_enabled == "")
{
	$io_enabled = 0;
}

// SQL query: Username already taken?
$sql_uname_unique = "
	SELECT
		users.id
	FROM
		vmsx_db.users
	WHERE
		(users.username=?)
	UNION ALL
	SELECT
		users_req.id
	FROM
		vmsx_db.users_req
	WHERE
		(users_req.username=?)
	UNION ALL
	SELECT
		users_del.id
	FROM
		vmsx_db.users_del
	WHERE
		(users_del.username=?)
";

$stmt = $mysqli->prepare($sql_uname_unique);
$stmt->bind_param("sss",$io_uname, $io_uname, $io_uname);
$stmt->execute();
if(!($result = $stmt->get_result()))
{
	error_response_json(
		"Could not connect to database.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

$num_rows = $result->num_rows;
if($num_rows > 0)
{
	error_response_json(
		"The username is already taken.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

//Generate password for DB
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/secure/gen_password.php';
//include_once 'secure/random_string.php';
$db_pword = vms_password_enc($io_pword);


// SQL query: Add user to user_req table
$sql_uname_unique = "
	INSERT INTO
		vmsx_db.users
	(
		username,
		name,
		initials,
		surname,
		email,
		password,
		gid,
		enabled,
		salref,
		created_by
	)
	VALUES
	(
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?
	)
";

try
{
	$stmt = $mysqli->prepare($sql_uname_unique);
	$stmt->bind_param("ssssssiisi",
		$io_uname,
		$io_name,
		$io_initials,
		$io_surname,
		$io_email,
		$db_pword,
		$io_group,
		$io_enabled,
		$io_salref,
		$_SESSION['uid']);
	$stmt->execute();
}
catch (Exception $e)
{
	if ($mysqli->errno === 1062)
	{
		error_response_json(
		"The username is already taken.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
	}
	
	error_response_json(
		"Could not connect to database.".$e->getMessage(),
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

// SQL query to fetch user info
$sqli_users_info = "
	SELECT
		users.id,
		users.username,
		users.name,
		users.initials,
		users.surname,
		users.email,
		users.created_on,
		users.updated_by,
		users.updated_on
	FROM
		vmsx_db.users
	WHERE
		(users.username=?)
	";
$stmt = $mysqli->prepare($sqli_users_info);
$stmt->bind_param("s",$io_uname);
$stmt->execute();
if(!($result = $stmt->get_result()))
{
	error_response_json(
		"Could not connect to the database.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}
$num_rows = $result->num_rows;
if($num_rows === 0)
{
	error_response_json(
		"The created user could not be retrieved.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

while($row = $result->fetch_assoc())
{
	$data_users[]	= $row;
}

//Send emails to user and admin
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/email/email_req.php';
$emailed_user = email_user_approve($io_uname, $io_name, $io_surname, $io_email);
$submitted = true;

//Close statment and db connections
if(isset($stmt) && $stmt != null){$stmt->close();}
if(isset($mysqli) && $mysqli){$mysqli->close();}

//create a JSON data structure.
$data_json = array (	
	'errors' 			=> $error,
	'error_msg'			=> $error_msg,
	'error_level'		=> $error_level,
	'error_code'		=> $error_code,
	'session_timeout'	=> $_SESSION['timeout'],
	'submitted'			=> $submitted,
        'emailed_user'                  => $emailed_user,
	'user'				=> $data_users
);

//Respond to request.
echo json_encode($data_json, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
?>
