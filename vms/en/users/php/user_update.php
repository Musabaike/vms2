<?php
//Start session
session_start();

// Prevent caching.
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 01 Jan 1996 00:00:00 GMT');
// The JSON standard MIME header.
header('Content-type: application/json');

//Default Data
$error			= false;
$error_msg		= null;
$error_level	= null;
$error_code		= 0;
$verified		= null;

$stmt			= null;
$mysqli			= null;

$request		= null;
$io_uname		= null;
$io_pword		= null;
$io_temp		= null;

$data_user		= null;


include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
if(!grant_access([4005]))
{
	error_response_json(
		"Access denied.",
		1,
		__LINE__,
		null,
		null);
}

// Empty data received in request.
if(!isset($_POST['token']))
{
	error_response_json(
		"Please login before continuing.",
		1,
		__LINE__,
		null,
		null);
}

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session_token.php';
if (!verify_token($_POST['token']))
{
	error_response_json(
		"Please login again before continuing.",
		1,
		__LINE__,
		null,
		null);
}

// Data received in request.
if(!isset($_POST['request']))
{
	error_response_json(
		"Request type not available.",
		1,
		__LINE__,
		null,
		null);
}

if(!isset($_POST['json']))
{
	error_response_json(
		"Request data not available.",
		1,
		__LINE__,
		null,
		null);
}
$data_rec = json_decode($_POST['json'], true);

//Open DB connection
include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/db_vmsx.php';

$io_request	= mysqli_real_escape_string($mysqli, stripslashes($_POST['request']));
if($io_request == "" || $io_request != "a_user_update")
{
	error_response_json(
		"Invalid request.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

$io_uid	= mysqli_real_escape_string($mysqli, stripslashes($data_rec['user']['uid'][0]));
if($io_uid == "")
{
	error_response_json(
		"User identity was undefined.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($io_uid == 1)
{	//SYSTEM user account cannot be updated.
	error_response_json(
		"You cannot make changes to the SYSTEM user information.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

$io_fname = mysqli_real_escape_string($mysqli, stripslashes($data_rec['user']['fname'][0]));
if($io_fname == "" || $io_fname == null)
{
	error_response_json(
		"Please provide a First Name.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

$io_initials = mysqli_real_escape_string($mysqli, stripslashes($data_rec['user']['initials'][0]));

$io_lname = mysqli_real_escape_string($mysqli, stripslashes($data_rec['user']['lname'][0]));
if($io_lname == "" || $io_lname == null)
{
	error_response_json(
		"Please provide a Surname.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

$io_email = mysqli_real_escape_string($mysqli, stripslashes($data_rec['user']['email'][0]));
if($io_email == "" || $io_email == null)
{
	error_response_json(
		"Please provide an email address.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

$io_salref = mysqli_real_escape_string($mysqli, stripslashes($data_rec['user']['salref'][0]));
if($io_salref == "" || $io_salref == null)
{
	error_response_json(
		"Please provide a salary reference number.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

$io_group = mysqli_real_escape_string($mysqli, stripslashes($data_rec['user']['group'][0]));
	if($io_group == "" || $io_group == null)
	{
		error_response_json(
			"Please assign a group.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}

$io_enabled = mysqli_real_escape_string($mysqli, stripslashes($data_rec['user']['enabled'][0]));
if($io_enabled == "" || $io_enabled == null)
{
	error_response_json(
		"Please select Enabled option.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

$io_notify = mysqli_real_escape_string($mysqli, stripslashes($data_rec['user']['notify'][0]));
if($io_notify == "" || $io_notify == null)
{
	error_response_json(
		"Please select Enabled option.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

$io_pword = mysqli_real_escape_string($mysqli, stripslashes($data_rec['user']['pword'][0]));
$io_pword1 = mysqli_real_escape_string($mysqli, stripslashes($data_rec['user']['pword1'][0]));
if($data_rec['user']['pword'][1] || $data_rec['user']['pword1'][1])
{
	if($io_pword == "" || $io_pword == null || $io_pword != $io_pword1)
	{
		error_response_json(
			"Please ensure the passwords match and that they meet the password requirements ",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
}

if($data_rec['user']['fname'][1])
{
	
	// SQL update query
	$sqli_user_info = "
		UPDATE
			vmsx_db.users 
		SET 
			name = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			users.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_user_info);
	$stmt->bind_param( "sis", $io_fname, $_SESSION['uid'], $io_uid);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update First Name.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
	if(isset($stmt) && $stmt != null){$stmt->close();}
}

if($data_rec['user']['initials'][1])
{
	// SQL update query
	$sqli_user_info = "
		UPDATE
			vmsx_db.users 
		SET 
			initials = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			users.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_user_info);
	$stmt->bind_param( "sis", $io_initials, $_SESSION['uid'], $io_uid);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update Initials.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
	if(isset($stmt) && $stmt != null){$stmt->close();}
}

if($data_rec['user']['lname'][1])
{
	// SQL update query
	$sqli_user_info = "
		UPDATE
			vmsx_db.users 
		SET 
			surname = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			users.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_user_info);
	$stmt->bind_param( "sis", $io_lname, $_SESSION['uid'], $io_uid);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update Surname.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
	if(isset($stmt) && $stmt != null){$stmt->close();}
}

if($data_rec['user']['email'][1])
{
	// SQL update query
	$sqli_user_info = "
		UPDATE
			vmsx_db.users 
		SET 
			email = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			users.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_user_info);
	$stmt->bind_param( "sis", $io_email, $_SESSION['uid'], $io_uid);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update Email Address.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
	if(isset($stmt) && $stmt != null){$stmt->close();}
}

if($data_rec['user']['salref'][1])
{
	
	// SQL update query
	$sqli_user_info = "
		UPDATE
			vmsx_db.users 
		SET 
			salref = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			users.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_user_info);
	$stmt->bind_param( "sis", $io_salref, $_SESSION['uid'], $io_uid);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update Salary Ref.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
	if(isset($stmt) && $stmt != null){$stmt->close();}
}

if($data_rec['user']['group'][1])
{
	// SQL update query
	$sqli_user_info = "
		UPDATE
			vmsx_db.users 
		SET 
			gid = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			users.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_user_info);
	$stmt->bind_param( "iis", $io_group, $_SESSION['uid'], $io_uid);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update the assigned Group.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
	if(isset($stmt) && $stmt != null){$stmt->close();}
}

if($data_rec['user']['enabled'][1])
{
	// SQL update query
	$sqli_user_info = "
		UPDATE
			vmsx_db.users 
		SET 
			enabled = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			users.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_user_info);
	$stmt->bind_param( "iis", $io_enabled, $_SESSION['uid'], $io_uid);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update the enabled state.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
	if(isset($stmt) && $stmt != null){$stmt->close();}
}

if($data_rec['user']['notify'][1])
{
	// SQL update query
	$sqli_user_info = "
		UPDATE
			vmsx_db.users 
		SET 
			notify_support = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			users.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_user_info);
	$stmt->bind_param( "iis", $io_notify, $_SESSION['uid'], $io_uid);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update the Receive support emails state.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
	if(isset($stmt) && $stmt != null){$stmt->close();}
}

if($data_rec['user']['pword'][1])
{
	
	
	//Generate password for DB
	include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/secure/gen_password.php';
	//include_once 'secure/random_string.php';
	$db_pword = vms_password_enc($io_pword);
	
	// SQL update query
	$sqli_user_info = "
		UPDATE
			vmsx_db.users 
		SET 
			password = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			users.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_user_info);
	$stmt->bind_param( "sis", $db_pword, $_SESSION['uid'], $io_uid);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update the password.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
	if(isset($stmt) && $stmt != null){$stmt->close();}
}

// SQL query to fetch user info
$sqli_user_info = "
	SELECT
		e.id,
		e.username,
		e.name,
		e.initials,
		e.surname,
		e.email,
		e.gid,
		e.status,
		e.enabled,
		e.salref,
		e.created_by,
		c.username AS created_by_uname,
		e.created_on,
		e.updated_by,
		u.username AS updated_by_uname,
		e.updated_on,
		e.deleted_by,
		d.username AS deleted_by_uname,
		e.deleted_on
	FROM
		vmsx_db.users e
	LEFT JOIN
		vmsx_db.users c
	ON
		e.created_by=c.id
	LEFT JOIN
		vmsx_db.users u
	ON
		e.updated_by=u.id
	LEFT JOIN
		vmsx_db.users d
	ON
		e.deleted_by=d.id
	WHERE
		e.id=?
	";
$stmt = $mysqli->prepare($sqli_user_info);
$stmt->bind_param( "i", $io_uid);
$exe_result = $stmt->execute();
if(!$exe_result)
{
	error_response_json(
		"Could not update the password.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}
	
if(!($result = $stmt->get_result()))
{
	error_response_json(
		"Could not connect to the database.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}
$num_rows = $result->num_rows;
if($num_rows === 0)
{
	error_response_json(
		"Could not retrieve user information.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($num_rows > 1)
{
	error_response_json(
		"Conflicting user information",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

while($row = $result->fetch_assoc())
{
	$data_user		= $row;
}

//Close db connections
if(isset($stmt) && $stmt != null){$stmt->close();}
if(isset($mysqli) && $mysqli){$mysqli->close();}

//create a JSON data structure.
$data = array (	
	'errors' 			=> $error,
	'error_msg'			=> $error_msg,
	'error_level'		=> $error_level,
	'error_code'		=> $error_code,
	'session_timeout'	=> $_SESSION['timeout'],
	'user'				=> $data_user
);

//Respond to request.
echo json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
?>