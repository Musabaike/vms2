<?php
$data_users_req_count	= 0;
$user_req_count_error	= false;

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
if(grant_access([1,2]))
{
	$user_req_count_error = true;
}

//Open DB connection
include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/db_vmsx.php';
// SQL query to fetch user info
$sqli_users_req_info = "
	SELECT
		COUNT(id) AS num_request
	FROM
		vmsx_db.users_req
	";
$stmt = $mysqli->prepare($sqli_users_req_info);
$stmt->execute();
if(!($result = $stmt->get_result()))
{
	
	$user_req_count_error = true;
}


while($row = $result->fetch_assoc())
{
	$data_users_req_count	= $row['num_request'];
}

//Close statment and db connections
$stmt->close();
$mysqli->close();

if ($user_req_count_error)
{
	echo '<div class="chip telkom-red white-text">'.$data_users_req_count.'</div>';
}
else
{
	echo "";
}
?>