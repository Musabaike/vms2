var elems, options;
var tabs;
var btn_add, btn_add_tooltips;
var mod_user_add, mod_group_add;
var mod_user_edit, mod_users_req_edit, mod_group_edit;
var tt_sessions;

var users_page_no = 1;
var users_page_limit = 10;
var users_sort_order = "ASC";
var users_sort_column = 0;

var groups_page_no = 1;
var groups_page_limit = 10;
var groups_sort_order = "ASC";
var groups_sort_column = 0;

var users_reqs_page_no = 1;
var users_reqs_page_limit = 10;
var users_reqs_sort_order = "ASC";
var users_reqs_sort_column = 0;

var users_hist_page_no = 1;
var users_hist_page_limit = 10;
var users_hist_sort_order = "ASC";
var users_hist_sort_column = 0;

$(document).ready(function()
{	//Main
	prepareData();
	initialise_Mcss();
	initialise_Btns();
	initialise_Forms();
	
	//Default data: Fetch all users
	users_get($("body").attr('data-token'), null);
});


//INITIALIZATION
function prepareData()
{	//Prepare DOM elements for initialising
	getColumnsToFilterBy('users_tbl',		'users_filter_sel');
	getColumnsToFilterBy('groups_tbl',		'groups_filter_sel');
	getColumnsToFilterBy('users_reqs_tbl',	'users_reqs_filter_sel');
	getColumnsToFilterBy('users_hist_tbl',		'users_hist_filter_sel');	
}

function initialise_Mcss()
{	//MaterializeCSS initializations

	//Tabs:
	elems  = document.querySelectorAll('.tabs');
	options = {
		duration: 300
	};
	tabs = M.Tabs.init(elems, options);
	
	//Button Tooltips:
	options = {
		enterDelay: 500
	};
	elems = document.querySelectorAll('.tooltipped');
    M.Tooltip.init(elems, options);
	//Tab Tooltips:
	options = {
		enterDelay: 500
	};
	elems = document.querySelectorAll('.tabs .tooltipped');
    tabs_tooltips = M.Tooltip.init(elems, options);

	elems = document.querySelectorAll('.btn-floating.tooltipped');
    btn_add_tooltips = M.Tooltip.init(elems, options);
	
	//Btn Add User
	elems = document.querySelectorAll('.fixed-action-btn');
	options = {hoverEnabled: false};
    btn_add = M.FloatingActionButton.init(elems, options);
	
	//Modals
    mod_user_add = M.Modal.init(document.querySelector('#modal_user_add'), {});
	mod_user_edit = M.Modal.init(document.querySelector('#modal_user_edit'), {});
	mod_dist_add = M.Modal.init(document.querySelector('#modal_group_add'), {});
	mod_users_req_edit = M.Modal.init(document.querySelector('#modal_users_req_edit'), {});
	mod_group_add = M.Modal.init(document.querySelector('#modal_group_add'), {});
	mod_group_edit = M.Modal.init(document.querySelector('#modal_group_edit'), {});
	
	//Collapsables
	M.Collapsible.init(document.querySelectorAll('.collapsible'), {});

	//Character Counters
	$('input#io_grp_name').characterCounter();
}

function initialise_Btns()
{	//Initialise Button listeners
	//Search
	$('#btn_users_search').click(function(){
		event.preventDefault();
		users_page_no = 1;
		users_get($("body").attr('data-token'), null);
	});
	$('#btn_groups_search').click(function(){
		event.preventDefault();
		groups_page_no = 1;
		groups_get($("body").attr('data-token'), null);
	});
	$('#btn_reqs_search').click(function(){
		event.preventDefault();
		users_reqs_page_no = 1;
		users_reqs_get($("body").attr('data-token'), null);
	});
	$('#btn_users_hist_search').click(function(){
		event.preventDefault();
		users_hist_page_no = 1;
		users_hist_get($("body").attr('data-token'), null);
	});	

	//Add
	$('#btn_users_modal_add').click(function(){
		event.preventDefault();
		groups_list_get($("body").attr('data-token'), '#io_user_group');
		mod_user_add.open();
	});	
	$('#btn_user_add').click(function(){
		event.preventDefault();
		user_add($("body").attr('data-token'));
	});	
	$('#btn_groups_add').click(function(){
		event.preventDefault();
		//groups_list_get(token, '#io_user_group');
		mod_group_add.open();
	});
	$('#mod_group_btn_add').click(function(){
		event.preventDefault();
		group_add($("body").attr('data-token'));
	});
	
	//Update
	$('#btn_users_edit_update').click(function(){
		event.preventDefault();
		if($('#tab_users_info').hasClass('active'))
		{
			user_set($("body").attr('data-token'), $('#io_edit_user_uname').attr('data-uid'));
		}
		if($('#tab_users_priv').hasClass('active'))
		{
			user_priv_set($("body").attr('data-token'), $('#io_edit_user_uname').attr('data-uid'))
		}
	});
	$('#btn_groups_edit_update').click(function(){
		event.preventDefault();
		if($('#tab_group_info').hasClass('active'))
		{
			//user_set($("body").attr('data-token'), $('#io_edit_group_name').attr('data-uid'));
			group_set($("body").attr('data-token'), $('#modal_group_edit').attr('data-gid'));
			//console.log('INFO Tab Groups');
		}
		if($('#tab_group_priv').hasClass('active'))
		{
			group_priv_set($("body").attr('data-token'), $('.vms-grp-editing').attr('data-gid'))
			//console.log('PRIV Tab Groups - UPDATE');
		}
	});
	
	//Delete
	$('#btn_users_modal_rem').click(function(){
		event.preventDefault();
		user_rem($("body").attr('data-token'), $('#io_edit_user_uname').attr('data-uid'));
		
	});
	$('#btn_groups_modal_rem').click(function(){
		event.preventDefault();
		group_rem($("body").attr('data-token'), $('.vms-grp-editing').attr('data-gid'));
		
	});
	
	//Destroy all user sessions
	$('#btn_users_edit_ses_rem').click(function(){
		event.preventDefault();
		user_ses_rem_all($("body").attr('data-token'), $('#modal_user_edit').attr('data-uid'));
	});	

	//Fetch all data on Tab select
	$("#users_tab").click(function (){
		event.preventDefault();
		sort_clear('#users_tbl');
		document.getElementById("users_filter").value="";
		users_get($("body").attr('data-token'), this);
	});
	$("#groups_tab").click(function (){
		event.preventDefault();
		sort_clear('#groups_tbl');
		document.getElementById("groups_filter").value="";
		groups_get($("body").attr('data-token'), this);
	});
	$("#users_reqs_tab").click(function (){
		event.preventDefault();
		sort_clear('#users_reqs_tbl');
		document.getElementById("users_reqs_filter").value="";
		users_reqs_get($("body").attr('data-token'), this);
	});
	$("#user_ses_tab").click(function (){
		event.preventDefault();
		sort_clear('#users_ses_tbl');
		user_ses_get($("body").attr('data-token'),$('#modal_user_edit').attr('data-uid'));
	});
	$("#user_priv_tab").click(function (){
		event.preventDefault();
		user_priv_get($('body').attr('data-token'), $('#modal_user_edit').attr('data-uid'));
	});
	$("#users_hist_tab").click(function (){
		event.preventDefault();
		sort_clear('#users_hist_tbl');
		document.getElementById("users_hist_filter").value="";
		users_hist_get($("body").attr('data-token'), this);
	});	

	//Sort Button: i.e. Header cell
	$('#users_tbl > thead > tr > th').click(function(){
		event.preventDefault();
		sortByColumn("#users_tbl", this);
	});
	$('#users_reqs_tbl > thead > tr > th').click(function(){
		event.preventDefault();
		sortByColumn("#users_reqs_tbl", this);
	});
	$('#groups_tbl > thead > tr > th').click(function(){
		event.preventDefault();
		sortByColumn("#groups_tbl", this);
	});
	$('#users_hist_tbl > thead > tr > th').click(function(){
		event.preventDefault();
		sortByColumn("#users_hist_tbl", this);
	});
	
	//Approve/Reject Buttons
	$('#btn_users_req_approve').click(function(){
		event.preventDefault();
		//console.log($('.vms-users-req-editing').attr('data-urid'));
		users_req_approve($(this).attr('data-token'), $('.vms-users-req-editing').attr('data-urid'));
	});
	$('#btn_users_req_reject').click(function(){
		event.preventDefault();
		console.log("Rejected");
	});
	
	//Row Double Click => EDIT
	$('#users_tbl > tbody').on("dblclick", "tr", function(){
		event.preventDefault();
		let token = $('body').attr('data-token');
		$('#modal_user_edit').attr('data-uid', $(this).attr("data-uid"));
		user_get(token, $(this).attr("data-uid"));
		$(this).siblings().removeClass('vms-user-editing');
		$(this).addClass('vms-user-editing');
		mod_user_edit.open();
	});
	$('#users_reqs_tbl > tbody').on("dblclick", "tr", function(){
		event.preventDefault();
		$('#modal_user_req_edit').attr('data-urid', $(this).attr("data-urid"));
		users_req_get($('body').attr('data-token'), $(this).attr("data-urid"))
		$(this).siblings().removeClass('vms-users-req-editing');
		$(this).addClass('vms-users-req-editing');
		mod_users_req_edit.open();
	});
	$('#groups_tbl > tbody').on("dblclick", "tr", function(){
		event.preventDefault();
		$('#modal_group_edit').attr('data-gid', $(this).attr("data-gid"));
		group_get($('body').attr('data-token'), $(this).attr("data-gid"));
		$(this).siblings().removeClass('vms-grp-editing');
		$(this).addClass('vms-grp-editing');
		mod_group_edit.open();
	});
}

function initialise_Forms()
{	//Initialise Form listeners
	M.updateTextFields();
	
	//Select dropdown
	M.FormSelect.init(document.querySelectorAll('select'), {});

	//Paging
	$("#page_limit_users").change(function (){
		users_page_no = 1;
		users_get($("body").attr('data-token'), null);
	});
	$("#page_limit_groups").change(function (){
		groups_page_no = 1;
		groups_get($("body").attr('data-token'), null);
	});
	$("#page_limit_users_reqs").change(function (){
		users_reqs_page_no = 1;
		users_reqs_get($("body").attr('data-token'), null);
	});
	$("#page_limit_users_hist").change(function (){
		users_hist_page_no = 1;
		users_hist_get($("body").attr('data-token'), null);
	});
}


//USERS TAB
function users_get(token, caller)
{	//Fetch users in the database
	filter_data = {
		columns:	$('#users_filter_sel').val(),
		keyword:	document.getElementById("users_filter").value
	};
	if(caller != null)
	{
		if(caller.hasAttribute("data-page"))
		{	//Check if page change requested
			let temp_page_no = caller.getAttribute("data-page");
			if (temp_page_no != null && temp_page_no != "null")
			{	//Set new page by selection
				users_page_no = temp_page_no;
			}
			else
			{
				return;
			}
		}
	}
	//#page_limit
	users_page_limit = $('#page_limit_users').val();

	$('#users_tbl > tbody').text(""); //sort_clear('#users_tbl');
	$('.progress').remove();
	$('#users_tbl').parent().append('<div class="progress"><div class="indeterminate"></div></div>');
	$.post('/en/users/php/users.php',
		{
			request: 'all_users',
			token: token,
			page_no: users_page_no,
			page_limit: users_page_limit,
			sort_order: users_sort_order,
			sort_column: users_sort_column,
			filter: filter_data
		},
		function(json)
		{
			paging_update('page_users', json['page_no'], json['page_total']);
			//Paging click event
			$('#page_users li').on("click", "a", function(){
				event.preventDefault();
				users_get($("body").attr('data-token'), this);
			});	
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			users_append('#users_tbl', json['users']);
		},
		'json'
	).done(function()
	{
		$('.progress').remove();
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	
	return null;
}

function users_append(table_id, users_array)
{	//Add users to a specified table
	if (users_array == null) return false;
	var html = "";
	users_array.forEach(function(item, index, data){
		html = '<tr data-uid="'+	data[index]['id'] + 
			'"><td>'+ 			data[index]['username'] +
			'</td><td>' +		data[index]['name'] + 
			'</td><td>' +		data[index]['initials'] + 
			'</td><td>' +		data[index]['surname'] + 
			'</td><td>' +		data[index]['group_name'] + 
			'</td><td>' +		data[index]['created_on'] + 
			'</td><td>' +		(data[index]['updated_on']!=null?data[index]['updated_on']:'') + 
			'</td></tr>';
			
			$(table_id + ' > tbody').append(html);
	});
	return true;
}

function user_add(token)
{	//Add a new user as an administrator
	var uname	= $('#io_user_uname').val();
	var fname	= $('#io_user_fname').val();
	var initials = $('#io_user_initials').val();
	var lname	= $('#io_user_lname').val();
	var email	= $('#io_user_email').val();
	var salref	= $('#io_user_salref').val();
	var pword	= $('#io_user_password').val();
	var pass1	= $('#io_user_password1').val();
	var group	= $('#io_user_group').val();
	var status	= $('#io_user_status').val();
	var enabled	= $('#io_user_enabled').val();
	
	if (uname=="")
	{
		M.toast({html: "<span>Please enter a Username.</span>"});
		var elmnt = document.getElementById("io_req_uname");
		elmnt.scrollIntoView();
		$("#io_req_uname").removeClass('valid').focus();
		return null;
	}
	
	if (fname=="")
	{
		M.toast({html: "<span>Please enter a First Name.</span>"});
		var elmnt = document.getElementById("io_req_fname");
		elmnt.scrollIntoView();
		$("#io_req_fname").removeClass('valid').focus();
		return null;
	}
	
	if (lname=="")
	{
		M.toast({html: "<span>Please enter a Surname.</span>"});
		var elmnt = document.getElementById("io_req_lname");
		elmnt.scrollIntoView();
		$("#io_req_lname").removeClass('valid').focus();
		return null;
	}
	
	if (email=="")
	{
		M.toast({html: "<span>Please enter an email address.</span>"});
		var elmnt = document.getElementById("io_req_email");
		elmnt.scrollIntoView();
		$("#io_req_email").removeClass('valid').focus();
		return null;
	}
	
	if (salref=="")
	{
		M.toast({html: "<span>Please enter a Salary Reference Number.</span>"});
		var elmnt = document.getElementById("io_req_salref");
		elmnt.scrollIntoView();
		$("#io_req_salref").removeClass('valid').focus();
		return null;
	}
	
	if (pword=="")
	{
		M.toast({html: "<span>Please enter a Password.</span>"});
		var elmnt = document.getElementById("io_req_password");
		elmnt.scrollIntoView();
		$("#io_req_password").removeClass('valid').focus();
		return null;
	}
	
	if (pass1=="")
	{
		M.toast({html: "<span>Please confirm your password.</span>"});
		var elmnt = document.getElementById("io_req_password1");
		elmnt.scrollIntoView();
		$("#io_req_password1").removeClass('valid').focus();
		return null;
	}
	
	if (pass1!=pword)
	{
		M.toast({html: "<span>The passwords do not match.</span>"});
		var elmnt = document.getElementById("io_req_password");
		elmnt.scrollIntoView();
		$("#io_req_password").removeClass('valid').focus();
		return null;
	}
	
	$.post('/en/users/php/user_add.php',
		{
			request: 'user_add',
			token: token,
			uname: uname,
			fname: fname,
			initials: initials,
			lname: lname,
			email: email,
			salref: salref,
			pword: pword,
			group: group,
			enabled: enabled
		},
		function(json)
		{
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			users_append('#users_tbl', json['user']);
			
			mod_user_add.close();
			var toastHTML = '<span>The user has been created.</span>';
			M.toast({html: toastHTML});
		},
		'json'
	).done(function()
	{
		
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	return null;
}

function user_get(token, uid)
{	//Fetch a user in the database
	groups_list_get(token, '#io_edit_user_group');
	//Clear previous data:
	//Tab: User info
	$('#io_edit_user_uname').html("Unknown");
	$('#io_edit_user_fname').val("");
	$('#io_edit_user_initials').val("");
	$('#io_edit_user_lname').val("");
	$('#io_edit_user_email').val("");
	$('#io_edit_user_salref').val("");
	$('#io_edit_user_group').val(0);
	$('#io_edit_user_enabled').val(-1);
	$('#io_edit_user_notify').val(-1);
	$('#io_edit_user_password').val("");
	$('#io_edit_user_password1').val("");
	
	$('#io_edit_user_group').formSelect();
	$('#io_edit_user_enabled').formSelect();
	$('#io_edit_user_notify').formSelect();
	
	document.getElementById('user_inf_username').innerText = "";
	document.getElementById('user_inf_created_on').innerText = "";
	document.getElementById('user_inf_created_by').innerText = "";
	document.getElementById('user_inf_updated_on').innerText = "";
	document.getElementById('user_inf_updated_by').innerText = "";
	
	//Tab: Sessions
	$('#users_ses_tbl > tbody').html("");
	
	//Tab: Privileges
	
	M.updateTextFields();
	M.FormSelect.init(document.querySelectorAll('select'), {});
	//Retrieve new data
	$.post('/en/users/php/user.php',
		{
			request: 'a_user',
			token: token,
			uid: uid
		},
		function(json)
		{
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			
			user_ses_get(token, uid);
			user_priv_get(token, uid);
			
			//Vanilla
			document.getElementById('modal_user_edit').setAttribute('data-uid', json['user']['id']);
			document.getElementById('io_edit_user_uname').setAttribute('data-uid', uid);
			document.getElementById('io_edit_user_uname').setAttribute('data-value', json['user']['username']);
			document.getElementById('io_edit_user_uname').innerText = json['user']['username'];
			document.getElementById('io_edit_user_fname').setAttribute('data-value', json['user']['name']);
			document.getElementById('io_edit_user_fname').value = json['user']['name'];
			document.getElementById('io_edit_user_initials').setAttribute('data-value', json['user']['initials']);
			document.getElementById('io_edit_user_initials').value = json['user']['initials'];
			document.getElementById('io_edit_user_lname').setAttribute('data-value', json['user']['surname']);
			document.getElementById('io_edit_user_lname').value = json['user']['surname'];
			document.getElementById('io_edit_user_email').setAttribute('data-value', json['user']['email']);
			document.getElementById('io_edit_user_email').value = json['user']['email'];
			document.getElementById('io_edit_user_salref').setAttribute('data-value', json['user']['salref']);
			document.getElementById('io_edit_user_salref').value = json['user']['salref'];
			
			document.getElementById('user_inf_username').innerText = json['user']['username'];
			document.getElementById('user_inf_created_on').innerText = json['user']['created_on'];
			document.getElementById('user_inf_created_by').innerText = json['user']['created_by_uname'];
			document.getElementById('user_inf_updated_on').innerText = json['user']['updated_on'];
			document.getElementById('user_inf_updated_by').innerText = json['user']['updated_by_uname'];
			
			document.getElementById('io_edit_user_enabled').setAttribute('data-value', json['user']['enabled']);
			document.getElementById('io_edit_user_enabled').value = json['user']['enabled'];
			
			document.getElementById('io_edit_user_notify').setAttribute('data-value', json['user']['notify_support']);
			document.getElementById('io_edit_user_notify').value = json['user']['notify_support'];
			
			
			document.getElementById('io_edit_user_group').setAttribute('data-value', json['user']['gid']);
			document.getElementById('io_edit_user_group').value = json['user']['gid'];
			
			
			M.updateTextFields();
			$('#io_edit_user_enabled').formSelect();
			$('#io_edit_user_notify').formSelect();
			$('#io_edit_user_group').formSelect();
		},
		'json'
	).done(function()
	{
		$('.progress').remove();
	}).fail(function(jqXHR,status,err)
	{
		$('#io_edit_user_uname').html("Error");
	}).always(function()
	{
		
	});
	
	return null;
}

function user_set(token, uid)
{	//Set/update user info in the database
	
	//Fetch input info:
	let io_uname	= $('#io_edit_user_uname').html();
	let io_fname	= $('#io_edit_user_fname').val();
	let io_initials	= $('#io_edit_user_initials').val();
	let io_lname	= $('#io_edit_user_lname').val();
	let io_email	= $('#io_edit_user_email').val();
	let io_salref	= $('#io_edit_user_salref').val();
	let io_group	= $('#io_edit_user_group').val()
	let io_enabled	= $('#io_edit_user_enabled').val()
	let io_notify	= $('#io_edit_user_notify').val()
	let io_pword	= $('#io_edit_user_password').val();
	let io_pword1	= $('#io_edit_user_password1').val();
	
	json_data = {
		"user":{
			"uid":		[uid, false],
			"uname":	[io_uname, (io_uname==$('#io_edit_user_uname').attr('data-value')?false:true)],
			"fname":	[io_fname, (io_fname==$('#io_edit_user_fname').attr('data-value')?false:true)],
			"initials":	[io_initials, (io_initials==$('#io_edit_user_initials').attr('data-value')?false:true)],
			"lname":	[io_lname, (io_lname==$('#io_edit_user_lname').attr('data-value')?false:true)],
			"email":	[io_email, (io_email==$('#io_edit_user_email').attr('data-value')?false:true)],
			"salref":	[io_salref, (io_salref==$('#io_edit_user_salref').attr('data-value')?false:true)],
			"group":	[io_group, (io_group==$('#io_edit_user_group').attr('data-value')?false:true)],
			"enabled":	[io_enabled, (io_enabled==$('#io_edit_user_enabled').attr('data-value')?false:true)],
			"notify":	[io_notify, (io_notify==$('#io_edit_user_notify').attr('data-value')?false:true)],
			"pword":	[io_pword, (io_pword==""?false:true)],
			"pword1":	[io_pword1, (io_pword1==""?false:true)]
		}
	};
	let hasChange = false;
	
	for (var key in json_data["user"]) {
		if (json_data["user"].hasOwnProperty(key))
		{
			if (json_data["user"][key][1])
			{
				hasChange = true;
			}
		}
	}
	
	if(!hasChange) //Nothing has changed
	{
		M.toast({html: "<span>No changes to save.</span>"});
		return null;
	}
	
	if(json_data["user"]["pword"][0] != json_data["user"]["pword1"][0])
	{
		M.toast({html: "<span>Passwords do not match.</span>"});
		return null;
	}
	
	$.post('/en/users/php/user_update.php',
		{
			"request": 'a_user_update',
			"token": token,
			"json": JSON.stringify(json_data)
		},
		function(json)
		{
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			//Update table row
			let html = '<tr class="vms-user-editing" data-uid="'+	uid + 
			'"><td>'+ 			io_uname +
			'</td><td>' +		io_fname + 
			'</td><td>' +		io_initials + 
			'</td><td>' +		io_lname + 
			'</td><td>' +		(json['user']['created_on']!=null?json['user']['created_on']:'') + 
			'</td><td>' +		(json['user']['updated_on']!=null?json['user']['updated_on']:'') + 
			'</td><td>' +		(json['user']['updated_by_uname']!=null?json['user']['updated_by_uname']:'') + 
			'</td></tr>';
			
			$('.vms-user-editing').replaceWith(html);
			
			//Update modal data-value attribute:
			user_get(token, uid);
			
			M.toast({html: '<span>User updated.</span>'});
			
		},
		'json'
	).done(function()
	{
		
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	
	return null;
}

function user_rem(token, uid)
{	//Set/update user info in the database
	
	var result = confirm("Are you sure you want to delete the user account?");
	if (result)
	{	//Continue to delete the user account

		$.post('/en/users/php/user_del.php',
			{
				"request": 'a_user_remove',
				"token": token,
				"uid": uid
			},
			function(json)
			{
				if(json['error']){
					let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
					M.toast({html: toastHTML});
					if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
					}
					return null;
				}
				
				//Successful response
				//Update table row
				$('.vms-user-editing').remove();
				mod_user_edit.close();
				M.toast({html: json['user']});
			},
			'json'
		).done(function()
		{
			
		}).fail(function(jqXHR,status,err)
		{
			
		}).always(function()
		{
			
		});
	}
	
	return null;
}

function user_ses_get(token, uid)
{	//Fetch all users in the database
	$('#users_ses_tbl > tbody').html("");
	$.post('/en/users/php/user_ses.php',
		{
			request: 'all_user_ses',
			token: token,
			uid: uid
		},
		function(json)
		{
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			user_ses_append('#users_ses_tbl', json['user_sessions'], json['my_session_id']);
		},
		'json'
	).done(function()
	{
		
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	
	return null;
}

function user_ses_append(table_id, ses_array, my_ses_id)
{	//Add user sessions to a specified table
	if (ses_array == null) { return false; }
	
	var html = "";
	ses_array.forEach(function(item, index, data){
		html = '<tr data-id="' +	data[index]['id'] + 
			'" data-sid="' +		data[index]['sid'] +
			'"><td>'+ 			data[index]['ip_addr'] +
			'</td><td>' +		data[index]['start'] +
			'</td><td>' + 
			((my_ses_id != data[index]['sid'])?'<a class="btn-flat red-text tooltipped vms_rem_ses" data-position="left" data-tooltip="Remove session"><i class="material-icons">close</i></a>':'<a class="btn-flat blue-text tooltipped" data-position="left" data-tooltip="Current session"><i class="material-icons">person</i></a>') +
			'</td></tr>';
			
			$(table_id + ' > tbody').append(html);
	});
	
	//Remove a specific session
	$('tbody > tr').on("click", '.vms_rem_ses', function(){
		event.preventDefault();
		user_ses_rem($('body').attr('data-token'), $(this).closest('tr').attr("data-id"), this);
	});
	$('.tooltipped').tooltip();
	return true;
}

function user_ses_rem_all(token, uid)
{	//Destroy all sessions for specified user
	$.post('/en/users/php/user_ses_rem_all.php',
		{
			request: 'all_user_ses_rem',
			token: token,
			uid: uid
		},
		function(json)
		{
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				user_ses_get(token, uid);	//Get current session information.
				return null;
			}
			
			//Successful response
			//$('#users_ses_tbl > tbody').html("");
			if(json['user_sessions']==null) return null;
			user_ses_get(token, json['user_sessions'][0]['uid']);	//Get current session information.
		},
		'json'
	).done(function()
	{
		
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	
	return null;
}

function user_ses_rem(token, ses_id, row_btn)
{	//Destroy user session specified
	$.post('/en/users/php/user_ses_rem.php',
		{
			request: 'user_ses_rem',
			token: token,
			id: ses_id
		},
		function(json)
		{
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			//Clear the tootltip that was opened before deleting row
			tt_sessions = M.Tooltip.getInstance(row_btn);
			tt_sessions.close();
			
			//Successful response
			user_ses_get(token, $('#modal_user_edit').attr('data-uid'));	//Get current session information.
		},
		'json'
	).done(function()
	{
		
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	
	return null;
}

function user_priv_get(token, uid)
{	//Fetch all users in the database
	//Clear current privilege selection
	//HOME/DASH
	document.getElementById("priv_1000").checked = false;
	document.getElementById("priv_1001").checked = false;
	document.getElementById("priv_1002").checked = false;
	document.getElementById("priv_1003").checked = false;
	document.getElementById("priv_1004").checked = false;
	document.getElementById("priv_1005").checked = false;
	document.getElementById("priv_1006").checked = false;
	document.getElementById("priv_1000").disabled = false;
	document.getElementById("priv_1001").disabled = false;
	document.getElementById("priv_1002").disabled = false;
	document.getElementById("priv_1003").disabled = false;
	document.getElementById("priv_1004").disabled = false;
	document.getElementById("priv_1005").disabled = false;
	document.getElementById("priv_1006").disabled = false;
	//ORDERS
	document.getElementById("priv_2000").checked = false;
	document.getElementById("priv_2001").checked = false;
	document.getElementById("priv_2002").checked = false;
	document.getElementById("priv_2003").checked = false;
	document.getElementById("priv_2004").checked = false;
	document.getElementById("priv_2005").checked = false;
	document.getElementById("priv_2000").disabled = false;
	document.getElementById("priv_2001").disabled = false;
	document.getElementById("priv_2002").disabled = false;
	document.getElementById("priv_2003").disabled = false;
	document.getElementById("priv_2004").disabled = false;
	document.getElementById("priv_2005").disabled = false;
	//ADMIN
	document.getElementById("priv_3000").checked = false;
	document.getElementById("priv_3001").checked = false;
	document.getElementById("priv_3002").checked = false;
	document.getElementById("priv_3003").checked = false;
	document.getElementById("priv_3004").checked = false;
	document.getElementById("priv_3005").checked = false;
	document.getElementById("priv_3006").checked = false;
	document.getElementById("priv_3000").disabled = false;
	document.getElementById("priv_3001").disabled = false;
	document.getElementById("priv_3002").disabled = false;
	document.getElementById("priv_3003").disabled = false;
	document.getElementById("priv_3004").disabled = false;
	document.getElementById("priv_3005").disabled = false;
	document.getElementById("priv_3006").disabled = false;
	//USERS
	document.getElementById("priv_4000").checked = false;
	document.getElementById("priv_4001").checked = false;
	document.getElementById("priv_4002").checked = false;
	document.getElementById("priv_4003").checked = false;
	document.getElementById("priv_4004").checked = false;
	document.getElementById("priv_4005").checked = false;
	document.getElementById("priv_4006").checked = false;
	document.getElementById("priv_4007").checked = false;
	document.getElementById("priv_4008").checked = false;
	document.getElementById("priv_4009").checked = false;
	document.getElementById("priv_4010").checked = false;
	document.getElementById("priv_4011").checked = false;
	document.getElementById("priv_4012").checked = false;
	document.getElementById("priv_4013").checked = false;
	document.getElementById("priv_4014").checked = false;
	document.getElementById("priv_4015").checked = false;
	document.getElementById("priv_4016").checked = false;
	document.getElementById("priv_4017").checked = false;
	document.getElementById("priv_4018").checked = false;
	document.getElementById("priv_4019").checked = false;
	document.getElementById("priv_4000").disabled = false;
	document.getElementById("priv_4001").disabled = false;
	document.getElementById("priv_4002").disabled = false;
	document.getElementById("priv_4003").disabled = false;
	document.getElementById("priv_4004").disabled = false;
	document.getElementById("priv_4005").disabled = false;
	document.getElementById("priv_4006").disabled = false;
	document.getElementById("priv_4007").disabled = false;
	document.getElementById("priv_4008").disabled = false;
	document.getElementById("priv_4009").disabled = false;
	document.getElementById("priv_4010").disabled = false;
	document.getElementById("priv_4011").disabled = false;
	document.getElementById("priv_4012").disabled = false;
	document.getElementById("priv_4013").disabled = false;
	document.getElementById("priv_4014").disabled = false;
	document.getElementById("priv_4015").disabled = false;
	document.getElementById("priv_4016").disabled = false;
	document.getElementById("priv_4017").disabled = false;
	document.getElementById("priv_4018").disabled = false;
	document.getElementById("priv_4019").disabled = false;
	//SETTINGS
	document.getElementById("priv_5000").checked = false;
	document.getElementById("priv_5000").disabled = false;
	
	$.post('/en/users/php/user_priv.php',
		{
			request: 'user_priv',
			token: token,
			uid: uid
		},
		function(json)
		{
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			if(json['group_levels']!=null)
			{
				user_priv_def(json['group_levels'],false);
			}
			
			if(json['user_levels']!=null)
			{
				user_priv_def(json['user_levels'],true);
			}
		},
		'json'
	).done(function()
	{
		
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	
	return null;
}

function user_priv_def(obj_levels, priv_type)	//priv_type = GROUP/false or USER/true
{	//Define, checkboxes, the User level access
	obj_levels.forEach(function(item, index, data)
	{
		switch(data[index]['level'])
		{
			//DB Type UBigInt
			case 1000:
				//$("#priv_pg_orders").attr("checked", true); //jQuery
				document.getElementById("priv_1000").checked = true; //Vanilla
				(priv_type?true:document.getElementById("priv_1000").disabled = true);
				$("#priv_1000").attr("data-value", true);
			break;
			case 1001:
				document.getElementById("priv_1001").checked = true;
				(priv_type?true:document.getElementById("priv_1001").disabled = true);
				$("#priv_1001").attr("data-value", true);
			break;
			case 1002:
				document.getElementById("priv_1002").checked = true;
				(priv_type?true:document.getElementById("priv_1002").disabled = true);
				$("#priv_1002").attr("data-value", true);
			break;
			case 1003:
				document.getElementById("priv_1003").checked = true;
				(priv_type?true:document.getElementById("priv_1003").disabled = true);
				$("#priv_1003").attr("data-value", true);
			break;
			case 1004:
				document.getElementById("priv_1004").checked = true;
				(priv_type?true:document.getElementById("priv_1004").disabled = true);
				$("#priv_1004").attr("data-value", true);
			break;
			case 1005:
				document.getElementById("priv_1005").checked = true;
				(priv_type?true:document.getElementById("priv_1005").disabled = true);
				$("#priv_1005").attr("data-value", true);
			break;
			case 1006:
				document.getElementById("priv_1006").checked = true;
				(priv_type?true:document.getElementById("priv_1006").disabled = true);
				$("#priv_1006").attr("data-value", true);
			break;
			case 2000:
				//$("#priv_pg_orders").attr("checked", true); //jQuery
				document.getElementById("priv_2000").checked = true;
				(priv_type?true:document.getElementById("priv_2000").disabled = true);
				$("#priv_2000").attr("data-value", true);
			break;
			case 2001:
				document.getElementById("priv_2001").checked = true;
				(priv_type?true:document.getElementById("priv_2001").disabled = true);
				$("#priv_2001").attr("data-value", true);
			break;
			case 2002:
				document.getElementById("priv_2002").checked = true;
				(priv_type?true:document.getElementById("priv_2002").disabled = true);
				$("#priv_2002").attr("data-value", true);
			break;
			case 2003:
				document.getElementById("priv_2003").checked = true;
				(priv_type?true:document.getElementById("priv_2003").disabled = true);
				$("#priv_2003").attr("data-value", true);
			break;
			case 2004:
				document.getElementById("priv_2004").checked = true;
				(priv_type?true:document.getElementById("priv_2004").disabled = true);
				$("#priv_2004").attr("data-value", true);
			break;
			case 2005:
				document.getElementById("priv_2005").checked = true;
				(priv_type?true:document.getElementById("priv_2005").disabled = true);
				$("#priv_2005").attr("data-value", true);
			break;
			case 3000:
				//$("#priv_pg_orders").attr("checked", true); //jQuery
				document.getElementById("priv_3000").checked = true;
				(priv_type?true:document.getElementById("priv_3000").disabled = true);
				$("#priv_3000").attr("data-value", true);
			break;
			case 3001:
				document.getElementById("priv_3001").checked = true;
				(priv_type?true:document.getElementById("priv_3001").disabled = true);
				$("#priv_3001").attr("data-value", true);
			break;
			case 3002:
				document.getElementById("priv_3002").checked = true;
				(priv_type?true:document.getElementById("priv_3002").disabled = true);
				$("#priv_3002").attr("data-value", true);
			break;
			case 3003:
				document.getElementById("priv_3003").checked = true;
				(priv_type?true:document.getElementById("priv_3003").disabled = true);
				$("#priv_3003").attr("data-value", true);
			break;
			case 3004:
				document.getElementById("priv_3004").checked = true;
				(priv_type?true:document.getElementById("priv_3004").disabled = true);
				$("#priv_3004").attr("data-value", true);
			break;
			case 3005:
				document.getElementById("priv_3005").checked = true;
				(priv_type?true:document.getElementById("priv_3005").disabled = true);
				$("#priv_3005").attr("data-value", true);
			break;
			case 3006:
				document.getElementById("priv_3006").checked = true;
				(priv_type?true:document.getElementById("priv_3006").disabled = true);
				$("#priv_3006").attr("data-value", true);
			break;
			case 4000:
				document.getElementById("priv_4000").checked = true;
				(priv_type?true:document.getElementById("priv_4000").disabled = true);
				$("#priv_4000").attr("data-value", true);
			break;
			case 4001:
				document.getElementById("priv_4001").checked = true;
				(priv_type?true:document.getElementById("priv_4001").disabled = true);
				$("#priv_4001").attr("data-value", true);
			break;
			case 4002:
				document.getElementById("priv_4002").checked = true;
				(priv_type?true:document.getElementById("priv_4002").disabled = true);
				$("#priv_4002").attr("data-value", true);
			break;
			case 4003:
				document.getElementById("priv_4003").checked = true;
				(priv_type?true:document.getElementById("priv_4003").disabled = true);
				$("#priv_4003").attr("data-value", true);
			break;
			case 4004:
				document.getElementById("priv_4004").checked = true;
				(priv_type?true:document.getElementById("priv_4004").disabled = true);
				$("#priv_4004").attr("data-value", true);
			break;
			case 4005:
				document.getElementById("priv_4005").checked = true;
				(priv_type?true:document.getElementById("priv_4005").disabled = true);
				$("#priv_4005").attr("data-value", true);
			break;
			case 4006:
				document.getElementById("priv_4006").checked = true;
				(priv_type?true:document.getElementById("priv_4006").disabled = true);
				$("#priv_4006").attr("data-value", true);
			break;
			case 4007:
				document.getElementById("priv_4007").checked = true;
				(priv_type?true:document.getElementById("priv_4007").disabled = true);
				$("#priv_4007").attr("data-value", true);
			break;
			case 4008:
				document.getElementById("priv_4008").checked = true;
				(priv_type?true:document.getElementById("priv_4008").disabled = true);
				$("#priv_4008").attr("data-value", true);
			break;
			case 4009:
				document.getElementById("priv_4009").checked = true;
				(priv_type?true:document.getElementById("priv_4009").disabled = true);
				$("#priv_4009").attr("data-value", true);
			break;
			case 4010:
				document.getElementById("priv_4010").checked = true;
				(priv_type?true:document.getElementById("priv_4010").disabled = true);
				$("#priv_4010").attr("data-value", true);
			break;
			case 4011:
				document.getElementById("priv_4011").checked = true;
				(priv_type?true:document.getElementById("priv_4011").disabled = true);
				$("#priv_4011").attr("data-value", true);
			break;
			case 4012:
				document.getElementById("priv_4012").checked = true;
				(priv_type?true:document.getElementById("priv_4012").disabled = true);
				$("#priv_4012").attr("data-value", true);
			break;
			case 4013:
				document.getElementById("priv_4013").checked = true;
				(priv_type?true:document.getElementById("priv_4013").disabled = true);
				$("#priv_4013").attr("data-value", true);
			break;
			case 4014:
				document.getElementById("priv_4014").checked = true;
				(priv_type?true:document.getElementById("priv_4014").disabled = true);
				$("#priv_4014").attr("data-value", true);
			break;
			case 4015:
				document.getElementById("priv_4015").checked = true;
				(priv_type?true:document.getElementById("priv_4015").disabled = true);
				$("#priv_4015").attr("data-value", true);
			break;
			case 4016:
				document.getElementById("priv_4016").checked = true;
				(priv_type?true:document.getElementById("priv_4016").disabled = true);
				$("#priv_4016").attr("data-value", true);
			break;
			case 4017:
				document.getElementById("priv_4017").checked = true;
				(priv_type?true:document.getElementById("priv_4017").disabled = true);
				$("#priv_4017").attr("data-value", true);
			break;
			case 4018:
				document.getElementById("priv_4018").checked = true;
				(priv_type?true:document.getElementById("priv_4018").disabled = true);
				$("#priv_4018").attr("data-value", true);
			break;
			case 4019:
				document.getElementById("priv_4019").checked = true;
				(priv_type?true:document.getElementById("priv_4019").disabled = true);
				$("#priv_4019").attr("data-value", true);
			break;
			case 5000:
				document.getElementById("priv_5000").checked = true;
				(priv_type?true:document.getElementById("priv_5000").disabled = true);
				$("#priv_5000").attr("data-value", true);
			break;
			default:
			//nada
			break;
		}
	});
}

function user_priv_set(token, uid)
{	//Set the user privileges.
	//Fetch input info:
	let priv_1000 = (document.getElementById("priv_1000").getAttribute("data-value")=="true"?true:false);
	let priv_1001 = (document.getElementById("priv_1001").getAttribute("data-value")=="true"?true:false);
	let priv_1002 = (document.getElementById("priv_1002").getAttribute("data-value")=="true"?true:false);
	let priv_1003 = (document.getElementById("priv_1003").getAttribute("data-value")=="true"?true:false);
	let priv_1004 = (document.getElementById("priv_1004").getAttribute("data-value")=="true"?true:false);
	let priv_1005 = (document.getElementById("priv_1005").getAttribute("data-value")=="true"?true:false);
	let priv_1006 = (document.getElementById("priv_1006").getAttribute("data-value")=="true"?true:false);
	let priv_2000 = (document.getElementById("priv_2000").getAttribute("data-value")=="true"?true:false);
	let priv_2001 = (document.getElementById("priv_2001").getAttribute("data-value")=="true"?true:false);
	let priv_2002 = (document.getElementById("priv_2002").getAttribute("data-value")=="true"?true:false);
	let priv_2003 = (document.getElementById("priv_2003").getAttribute("data-value")=="true"?true:false);
	let priv_2004 = (document.getElementById("priv_2004").getAttribute("data-value")=="true"?true:false);
	let priv_2005 = (document.getElementById("priv_2005").getAttribute("data-value")=="true"?true:false);
	let priv_3000 = (document.getElementById("priv_3000").getAttribute("data-value")=="true"?true:false);
	let priv_3001 = (document.getElementById("priv_3001").getAttribute("data-value")=="true"?true:false);
	let priv_3002 = (document.getElementById("priv_3002").getAttribute("data-value")=="true"?true:false);
	let priv_3003 = (document.getElementById("priv_3003").getAttribute("data-value")=="true"?true:false);
	let priv_3004 = (document.getElementById("priv_3004").getAttribute("data-value")=="true"?true:false);
	let priv_3005 = (document.getElementById("priv_3005").getAttribute("data-value")=="true"?true:false);
	let priv_3006 = (document.getElementById("priv_3006").getAttribute("data-value")=="true"?true:false);
	let priv_4000 = (document.getElementById("priv_4000").getAttribute("data-value")=="true"?true:false);
	let priv_4001 = (document.getElementById("priv_4001").getAttribute("data-value")=="true"?true:false);
	let priv_4002 = (document.getElementById("priv_4002").getAttribute("data-value")=="true"?true:false);
	let priv_4003 = (document.getElementById("priv_4003").getAttribute("data-value")=="true"?true:false);
	let priv_4004 = (document.getElementById("priv_4004").getAttribute("data-value")=="true"?true:false);
	let priv_4005 = (document.getElementById("priv_4005").getAttribute("data-value")=="true"?true:false);
	let priv_4006 = (document.getElementById("priv_4006").getAttribute("data-value")=="true"?true:false);
	let priv_4007 = (document.getElementById("priv_4007").getAttribute("data-value")=="true"?true:false);
	let priv_4008 = (document.getElementById("priv_4008").getAttribute("data-value")=="true"?true:false);
	let priv_4009 = (document.getElementById("priv_4009").getAttribute("data-value")=="true"?true:false);
	let priv_4010 = (document.getElementById("priv_4010").getAttribute("data-value")=="true"?true:false);
	let priv_4011 = (document.getElementById("priv_4011").getAttribute("data-value")=="true"?true:false);
	let priv_4012 = (document.getElementById("priv_4012").getAttribute("data-value")=="true"?true:false);
	let priv_4013 = (document.getElementById("priv_4013").getAttribute("data-value")=="true"?true:false);
	let priv_4014 = (document.getElementById("priv_4014").getAttribute("data-value")=="true"?true:false);
	let priv_4015 = (document.getElementById("priv_4015").getAttribute("data-value")=="true"?true:false);
	let priv_4016 = (document.getElementById("priv_4016").getAttribute("data-value")=="true"?true:false);
	let priv_4017 = (document.getElementById("priv_4017").getAttribute("data-value")=="true"?true:false);
	let priv_4018 = (document.getElementById("priv_4018").getAttribute("data-value")=="true"?true:false);
	let priv_4019 = (document.getElementById("priv_4019").getAttribute("data-value")=="true"?true:false);
	let priv_5000 = (document.getElementById("priv_5000").getAttribute("data-value")=="true"?true:false);

	json_data = {
		"user_priv":{
			1000: [document.getElementById("priv_1000").checked, (priv_1000==document.getElementById("priv_1000").checked?false:true)],
			1001: [document.getElementById("priv_1001").checked, (priv_1001==document.getElementById("priv_1001").checked?false:true)],
			1002: [document.getElementById("priv_1002").checked, (priv_1002==document.getElementById("priv_1002").checked?false:true)],
			1003: [document.getElementById("priv_1003").checked, (priv_1003==document.getElementById("priv_1003").checked?false:true)],
			1004: [document.getElementById("priv_1004").checked, (priv_1004==document.getElementById("priv_1004").checked?false:true)],
			1005: [document.getElementById("priv_1005").checked, (priv_1005==document.getElementById("priv_1005").checked?false:true)],
			1006: [document.getElementById("priv_1006").checked, (priv_1006==document.getElementById("priv_1006").checked?false:true)],
			2000: [document.getElementById("priv_2000").checked, (priv_2000==document.getElementById("priv_2000").checked?false:true)],
			2001: [document.getElementById("priv_2001").checked, (priv_2001==document.getElementById("priv_2001").checked?false:true)],
			2002: [document.getElementById("priv_2002").checked, (priv_2002==document.getElementById("priv_2002").checked?false:true)],
			2003: [document.getElementById("priv_2003").checked, (priv_2003==document.getElementById("priv_2003").checked?false:true)],
			2004: [document.getElementById("priv_2004").checked, (priv_2004==document.getElementById("priv_2004").checked?false:true)],
			2005: [document.getElementById("priv_2005").checked, (priv_2005==document.getElementById("priv_2005").checked?false:true)],
			3000: [document.getElementById("priv_3000").checked, (priv_3000==document.getElementById("priv_3000").checked?false:true)],
			3001: [document.getElementById("priv_3001").checked, (priv_3001==document.getElementById("priv_3001").checked?false:true)],
			3002: [document.getElementById("priv_3002").checked, (priv_3002==document.getElementById("priv_3002").checked?false:true)],
			3003: [document.getElementById("priv_3003").checked, (priv_3003==document.getElementById("priv_3003").checked?false:true)],
			3004: [document.getElementById("priv_3004").checked, (priv_3004==document.getElementById("priv_3004").checked?false:true)],
			3005: [document.getElementById("priv_3005").checked, (priv_3005==document.getElementById("priv_3005").checked?false:true)],
			3006: [document.getElementById("priv_3006").checked, (priv_3006==document.getElementById("priv_3006").checked?false:true)],
			4000: [document.getElementById("priv_4000").checked, (priv_4000==document.getElementById("priv_4000").checked?false:true)],
			4001: [document.getElementById("priv_4001").checked, (priv_4001==document.getElementById("priv_4001").checked?false:true)],
			4002: [document.getElementById("priv_4002").checked, (priv_4002==document.getElementById("priv_4002").checked?false:true)],
			4003: [document.getElementById("priv_4003").checked, (priv_4003==document.getElementById("priv_4003").checked?false:true)],
			4004: [document.getElementById("priv_4004").checked, (priv_4004==document.getElementById("priv_4004").checked?false:true)],
			4005: [document.getElementById("priv_4005").checked, (priv_4005==document.getElementById("priv_4005").checked?false:true)],
			4006: [document.getElementById("priv_4006").checked, (priv_4006==document.getElementById("priv_4006").checked?false:true)],
			4007: [document.getElementById("priv_4007").checked, (priv_4007==document.getElementById("priv_4007").checked?false:true)],
			4008: [document.getElementById("priv_4008").checked, (priv_4008==document.getElementById("priv_4008").checked?false:true)],
			4009: [document.getElementById("priv_4009").checked, (priv_4009==document.getElementById("priv_4009").checked?false:true)],
			4010: [document.getElementById("priv_4010").checked, (priv_4010==document.getElementById("priv_4010").checked?false:true)],
			4011: [document.getElementById("priv_4011").checked, (priv_4011==document.getElementById("priv_4011").checked?false:true)],
			4012: [document.getElementById("priv_4012").checked, (priv_4012==document.getElementById("priv_4012").checked?false:true)],
			4013: [document.getElementById("priv_4013").checked, (priv_4013==document.getElementById("priv_4013").checked?false:true)],
			4014: [document.getElementById("priv_4014").checked, (priv_4014==document.getElementById("priv_4014").checked?false:true)],
			4015: [document.getElementById("priv_4015").checked, (priv_4015==document.getElementById("priv_4015").checked?false:true)],
			4016: [document.getElementById("priv_4016").checked, (priv_4016==document.getElementById("priv_4016").checked?false:true)],
			4017: [document.getElementById("priv_4017").checked, (priv_4017==document.getElementById("priv_4017").checked?false:true)],
			4018: [document.getElementById("priv_4018").checked, (priv_4018==document.getElementById("priv_4018").checked?false:true)],
			4019: [document.getElementById("priv_4019").checked, (priv_4019==document.getElementById("priv_4019").checked?false:true)],
			5000: [document.getElementById("priv_5000").checked, (priv_5000==document.getElementById("priv_5000").checked?false:true)]
		}
	};
	let hasChange = false;
	
	for (var key in json_data["user_priv"]) {
		if (json_data["user_priv"].hasOwnProperty(key))
		{
			if (json_data["user_priv"][key][1])
			{
				hasChange = true;
			}
		}
	}
	
	if(!hasChange) //Nothing has changed
	{
		M.toast({html: "<span>No changes to save.</span>"});
		return null;
	}
	
	$.post('/en/users/php/user_priv_update.php',
		{
			"request": 'a_user_priv_update',
			"token": token,
			"uid":	uid,
			"json": JSON.stringify(json_data)
		},
		function(json)
		{
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Fetch changed Privileges:
			for (var key in json["user"])
			{
				document.getElementById("priv_"+json["user"][key]["level"]).checked = json["user"][key]["checked"];
				$("#priv_"+json["user"][key]["level"]).attr("data-value", json["user"][key]["checked"]);
			}
			M.toast({html: '<span>User privileges updated.</span>'});
			//user_priv_get(token, uid);
		},
		'json'
	).done(function()
	{
		
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	
	return null;
}


//GROUPS TAB
function group_get(token, gid)
{	//Fetch group in the database
	document.getElementById('io_edit_grp_name').value = "";
	document.getElementById('io_edit_grp_description').value = "";
	
	document.getElementById('group_inf_group_name').innerText = "";
	document.getElementById('group_inf_created_on').innerText = "";
	document.getElementById('group_inf_created_by').innerText = "";
	document.getElementById('group_inf_updated_on').innerText = "";
	document.getElementById('group_inf_updated_by').innerText = "";
	
	$.post('/en/users/php/group.php',
		{
			request: 'a_group',
			token: token,
			gid: gid
		},
		function(json)
		{
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			group_priv_get(token, gid);
			
			document.getElementById('io_edit_grp_name').value = json['group']['name'];
			document.getElementById('io_edit_grp_description').value = json['group']['description'];
			
			document.getElementById('io_edit_grp_name').setAttribute('data-value', json['group']['name']);
			document.getElementById('io_edit_grp_description').setAttribute('data-value', json['group']['description']);
			
			document.getElementById('group_inf_group_name').innerText = json['group']['name'];
			document.getElementById('group_inf_created_on').innerText = json['group']['created_on'];
			document.getElementById('group_inf_created_by').innerText = json['group']['created_by_uname'];
			document.getElementById('group_inf_updated_on').innerText = json['group']['updated_on'];
			document.getElementById('group_inf_updated_by').innerText = json['group']['updated_by_uname'];
		},
		'json'
	).done(function()
	{
		$('.progress').remove();
	}).fail(function(jqXHR,status,err)
	{
		$('#io_edit_user_uname').html("Error");
	}).always(function()
	{
		
	});
	
	return null;
}

function group_add(token)
{	//Add a new group
	let io_name	= document.getElementById("io_grp_name").value;
	let io_descr= document.getElementById("io_grp_description").value;
	
	json_data = {
		"group":{
			"name":			io_name,
			"description":	io_descr
		}
	};
	
	$.post('/en/users/php/group_add.php',
		{
			request: 'group_add',
			token: token,
			data: JSON.stringify(json_data)
		},
		function(json)
		{
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful
			groups_append('#groups_tbl', json['group']);
			M.toast({html: '<span>Group added.</span>'});
		},
		'json'
	).done(function()
	{
		
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	
	return null;
}

function group_set(token, gid)
{	//Update group information
	let io_name		= document.getElementById("io_edit_grp_name").value; //Vanilla
	let io_descr	= document.getElementById("io_edit_grp_description").value;
	
	json_data = {
		"group":{
			"gid":			[gid, false],
			"name":			[io_name, 	(io_name==document.getElementById('io_edit_grp_name').getAttribute('data-value')?false:true)],
			"description":	[io_descr, 	(io_descr==document.getElementById('io_edit_grp_description').getAttribute('data-value')?false:true)]
		}
	};
	let hasChange = false;
	
	for (var key in json_data["group"]) {
		if (json_data["group"].hasOwnProperty(key))
		{
			if (json_data["group"][key][1])
			{
				hasChange = true;
			}
		}
	}
	
	if(!hasChange) //Nothing has changed
	{
		M.toast({html: "<span>No changes to save.</span>"});
		return null;
	}
	
	$.post('/en/users/php/group_update.php',
		{
			request: 'group_update',
			token: token,
			data: JSON.stringify(json_data)
		},
		function(json)
		{
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful:
			//Replace the current data row with updated information.
			let html = '<tr data-gid="'+	json['group']['id'] + 
					'"><td>' +			(json['group']['name']!=null?json['group']['name']:'') + 
					'</td><td>' +		(json['group']['created_by_uname']!=null?json['group']['created_by_uname']:'') + 
					'</td><td>' +		(json['group']['created_on']!=null?json['group']['created_on']:'') + 
					'</td><td>' +		(json['group']['updated_by_uname']!=null?json['group']['updated_by_uname']:'') + 
					'</td><td>' +		(json['group']['updated_on']!=null?json['group']['updated_on']:'') + 
					'</td></tr>';
			
			$('.vms-grp-editing').replaceWith(html);
			
			var toastHTML = '<span>Group Information updated.</span>';
			M.toast({html: toastHTML});
		},
		'json'
	).done(function()
	{
		
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	
	return null;
}

function group_rem(token, gid)
{	//Set/update user info in the database
	
	var result = confirm("Are you sure you want to delete the group?");
	if (result)
	{	//Continue to delete the user account

		$.post('/en/users/php/group_del.php',
			{
				"request": 'a_group_remove',
				"token": token,
				"gid": gid
			},
			function(json)
			{
				if(json['error']){
					let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
					M.toast({html: toastHTML});
					if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
					}
					return null;
				}
				
				//Successful response
				//Update table row
				$('.vms-grp-editing').remove();
				mod_group_edit.close();
				M.toast({html: json['group']});
			},
			'json'
		).done(function()
		{
			
		}).fail(function(jqXHR,status,err)
		{
			
		}).always(function()
		{
			
		});
	}
	
	return null;
}

function group_priv_get(token, gid)
{	//Fetch all users in the database
	//Clear current privilege selection
	//HOME/DASH
	document.getElementById("priv_grp_1000").checked = false;
	document.getElementById("priv_grp_1001").checked = false;
	document.getElementById("priv_grp_1002").checked = false;
	document.getElementById("priv_grp_1003").checked = false;
	document.getElementById("priv_grp_1004").checked = false;
	document.getElementById("priv_grp_1005").checked = false;
	document.getElementById("priv_grp_1006").checked = false;
	//ORDERS
	document.getElementById("priv_grp_2000").checked = false;
	document.getElementById("priv_grp_2001").checked = false;
	document.getElementById("priv_grp_2002").checked = false;
	document.getElementById("priv_grp_2003").checked = false;
	document.getElementById("priv_grp_2004").checked = false;
	document.getElementById("priv_grp_2005").checked = false;
	//ADMIN
	document.getElementById("priv_grp_3000").checked = false;
	document.getElementById("priv_grp_3001").checked = false;
	document.getElementById("priv_grp_3002").checked = false;
	document.getElementById("priv_grp_3003").checked = false;
	document.getElementById("priv_grp_3004").checked = false;
	document.getElementById("priv_grp_3005").checked = false;
	document.getElementById("priv_grp_3006").checked = false;
	//USERS
	document.getElementById("priv_grp_4000").checked = false;
	document.getElementById("priv_grp_4001").checked = false;
	document.getElementById("priv_grp_4002").checked = false;
	document.getElementById("priv_grp_4003").checked = false;
	document.getElementById("priv_grp_4004").checked = false;
	document.getElementById("priv_grp_4005").checked = false;
	document.getElementById("priv_grp_4006").checked = false;
	document.getElementById("priv_grp_4007").checked = false;
	document.getElementById("priv_grp_4008").checked = false;
	document.getElementById("priv_grp_4009").checked = false;
	document.getElementById("priv_grp_4010").checked = false;
	document.getElementById("priv_grp_4011").checked = false;
	document.getElementById("priv_grp_4012").checked = false;
	document.getElementById("priv_grp_4013").checked = false;
	document.getElementById("priv_grp_4014").checked = false;
	document.getElementById("priv_grp_4015").checked = false;
	document.getElementById("priv_grp_4016").checked = false;
	document.getElementById("priv_grp_4017").checked = false;
	document.getElementById("priv_grp_4018").checked = false;
	document.getElementById("priv_grp_4019").checked = false;
	//SETTINGS
	document.getElementById("priv_grp_5000").checked = false;
	
	$.post('/en/users/php/group_priv.php',
		{
			request: 'group_priv',
			token: token,
			gid: gid
		},
		function(json)
		{
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			if(json['group_levels']!=null)
			{
				group_priv_def(json['group_levels']);
			}
		},
		'json'
	).done(function()
	{
		
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	
	return null;
}

function group_priv_set(token, gid)
{	//Set/update user info in the database
	
	//Fetch input info:
	let priv_grp_1000 = (document.getElementById("priv_grp_1000").getAttribute("data-value")=="true"?true:false);
	let priv_grp_1001 = (document.getElementById("priv_grp_1001").getAttribute("data-value")=="true"?true:false);
	let priv_grp_1002 = (document.getElementById("priv_grp_1002").getAttribute("data-value")=="true"?true:false);
	let priv_grp_1003 = (document.getElementById("priv_grp_1003").getAttribute("data-value")=="true"?true:false);
	let priv_grp_1004 = (document.getElementById("priv_grp_1004").getAttribute("data-value")=="true"?true:false);
	let priv_grp_1005 = (document.getElementById("priv_grp_1005").getAttribute("data-value")=="true"?true:false);
	let priv_grp_1006 = (document.getElementById("priv_grp_1006").getAttribute("data-value")=="true"?true:false);
	let priv_grp_2000 = (document.getElementById("priv_grp_2000").getAttribute("data-value")=="true"?true:false);
	let priv_grp_2001 = (document.getElementById("priv_grp_2001").getAttribute("data-value")=="true"?true:false);
	let priv_grp_2002 = (document.getElementById("priv_grp_2002").getAttribute("data-value")=="true"?true:false);
	let priv_grp_2003 = (document.getElementById("priv_grp_2003").getAttribute("data-value")=="true"?true:false);
	let priv_grp_2004 = (document.getElementById("priv_grp_2004").getAttribute("data-value")=="true"?true:false);
	let priv_grp_2005 = (document.getElementById("priv_grp_2005").getAttribute("data-value")=="true"?true:false);
	let priv_grp_3000 = (document.getElementById("priv_grp_3000").getAttribute("data-value")=="true"?true:false);
	let priv_grp_3001 = (document.getElementById("priv_grp_3001").getAttribute("data-value")=="true"?true:false);
	let priv_grp_3002 = (document.getElementById("priv_grp_3002").getAttribute("data-value")=="true"?true:false);
	let priv_grp_3003 = (document.getElementById("priv_grp_3003").getAttribute("data-value")=="true"?true:false);
	let priv_grp_3004 = (document.getElementById("priv_grp_3004").getAttribute("data-value")=="true"?true:false);
	let priv_grp_3005 = (document.getElementById("priv_grp_3005").getAttribute("data-value")=="true"?true:false);
	let priv_grp_3006 = (document.getElementById("priv_grp_3006").getAttribute("data-value")=="true"?true:false);
	let priv_grp_4000 = (document.getElementById("priv_grp_4000").getAttribute("data-value")=="true"?true:false);
	let priv_grp_4001 = (document.getElementById("priv_grp_4001").getAttribute("data-value")=="true"?true:false);
	let priv_grp_4002 = (document.getElementById("priv_grp_4002").getAttribute("data-value")=="true"?true:false);
	let priv_grp_4003 = (document.getElementById("priv_grp_4003").getAttribute("data-value")=="true"?true:false);
	let priv_grp_4004 = (document.getElementById("priv_grp_4004").getAttribute("data-value")=="true"?true:false);
	let priv_grp_4005 = (document.getElementById("priv_grp_4005").getAttribute("data-value")=="true"?true:false);
	let priv_grp_4006 = (document.getElementById("priv_grp_4006").getAttribute("data-value")=="true"?true:false);
	let priv_grp_4007 = (document.getElementById("priv_grp_4007").getAttribute("data-value")=="true"?true:false);
	let priv_grp_4008 = (document.getElementById("priv_grp_4008").getAttribute("data-value")=="true"?true:false);
	let priv_grp_4009 = (document.getElementById("priv_grp_4009").getAttribute("data-value")=="true"?true:false);
	let priv_grp_4010 = (document.getElementById("priv_grp_4010").getAttribute("data-value")=="true"?true:false);
	let priv_grp_4011 = (document.getElementById("priv_grp_4011").getAttribute("data-value")=="true"?true:false);
	let priv_grp_4012 = (document.getElementById("priv_grp_4012").getAttribute("data-value")=="true"?true:false);
	let priv_grp_4013 = (document.getElementById("priv_grp_4013").getAttribute("data-value")=="true"?true:false);
	let priv_grp_4014 = (document.getElementById("priv_grp_4014").getAttribute("data-value")=="true"?true:false);
	let priv_grp_4015 = (document.getElementById("priv_grp_4015").getAttribute("data-value")=="true"?true:false);
	let priv_grp_4016 = (document.getElementById("priv_grp_4016").getAttribute("data-value")=="true"?true:false);
	let priv_grp_4017 = (document.getElementById("priv_grp_4017").getAttribute("data-value")=="true"?true:false);
	let priv_grp_4018 = (document.getElementById("priv_grp_4018").getAttribute("data-value")=="true"?true:false);
	let priv_grp_4019 = (document.getElementById("priv_grp_4019").getAttribute("data-value")=="true"?true:false);
	let priv_grp_5000 = (document.getElementById("priv_grp_5000").getAttribute("data-value")=="true"?true:false);

	json_data = {
		"group_priv":{
			1000: [document.getElementById("priv_grp_1000").checked, (priv_grp_1000==document.getElementById("priv_grp_1000").checked?false:true)],
			1001: [document.getElementById("priv_grp_1001").checked, (priv_grp_1001==document.getElementById("priv_grp_1001").checked?false:true)],
			1002: [document.getElementById("priv_grp_1002").checked, (priv_grp_1002==document.getElementById("priv_grp_1002").checked?false:true)],
			1003: [document.getElementById("priv_grp_1003").checked, (priv_grp_1003==document.getElementById("priv_grp_1003").checked?false:true)],
			1004: [document.getElementById("priv_grp_1004").checked, (priv_grp_1004==document.getElementById("priv_grp_1004").checked?false:true)],
			1005: [document.getElementById("priv_grp_1005").checked, (priv_grp_1005==document.getElementById("priv_grp_1005").checked?false:true)],
			1006: [document.getElementById("priv_grp_1006").checked, (priv_grp_1006==document.getElementById("priv_grp_1006").checked?false:true)],
			2000: [document.getElementById("priv_grp_2000").checked, (priv_grp_2000==document.getElementById("priv_grp_2000").checked?false:true)],
			2001: [document.getElementById("priv_grp_2001").checked, (priv_grp_2001==document.getElementById("priv_grp_2001").checked?false:true)],
			2002: [document.getElementById("priv_grp_2002").checked, (priv_grp_2002==document.getElementById("priv_grp_2002").checked?false:true)],
			2003: [document.getElementById("priv_grp_2003").checked, (priv_grp_2003==document.getElementById("priv_grp_2003").checked?false:true)],
			2004: [document.getElementById("priv_grp_2004").checked, (priv_grp_2004==document.getElementById("priv_grp_2004").checked?false:true)],
			2005: [document.getElementById("priv_grp_2005").checked, (priv_grp_2005==document.getElementById("priv_grp_2005").checked?false:true)],
			3000: [document.getElementById("priv_grp_3000").checked, (priv_grp_3000==document.getElementById("priv_grp_3000").checked?false:true)],
			3001: [document.getElementById("priv_grp_3001").checked, (priv_grp_3001==document.getElementById("priv_grp_3001").checked?false:true)],
			3002: [document.getElementById("priv_grp_3002").checked, (priv_grp_3002==document.getElementById("priv_grp_3002").checked?false:true)],
			3003: [document.getElementById("priv_grp_3003").checked, (priv_grp_3003==document.getElementById("priv_grp_3003").checked?false:true)],
			3004: [document.getElementById("priv_grp_3004").checked, (priv_grp_3004==document.getElementById("priv_grp_3004").checked?false:true)],
			3005: [document.getElementById("priv_grp_3005").checked, (priv_grp_3005==document.getElementById("priv_grp_3005").checked?false:true)],
			3006: [document.getElementById("priv_grp_3006").checked, (priv_grp_3006==document.getElementById("priv_grp_3006").checked?false:true)],
			4000: [document.getElementById("priv_grp_4000").checked, (priv_grp_4000==document.getElementById("priv_grp_4000").checked?false:true)],
			4001: [document.getElementById("priv_grp_4001").checked, (priv_grp_4001==document.getElementById("priv_grp_4001").checked?false:true)],
			4002: [document.getElementById("priv_grp_4002").checked, (priv_grp_4002==document.getElementById("priv_grp_4002").checked?false:true)],
			4003: [document.getElementById("priv_grp_4003").checked, (priv_grp_4003==document.getElementById("priv_grp_4003").checked?false:true)],
			4004: [document.getElementById("priv_grp_4004").checked, (priv_grp_4004==document.getElementById("priv_grp_4004").checked?false:true)],
			4005: [document.getElementById("priv_grp_4005").checked, (priv_grp_4005==document.getElementById("priv_grp_4005").checked?false:true)],
			4006: [document.getElementById("priv_grp_4006").checked, (priv_grp_4006==document.getElementById("priv_grp_4006").checked?false:true)],
			4007: [document.getElementById("priv_grp_4007").checked, (priv_grp_4007==document.getElementById("priv_grp_4007").checked?false:true)],
			4008: [document.getElementById("priv_grp_4008").checked, (priv_grp_4008==document.getElementById("priv_grp_4008").checked?false:true)],
			4009: [document.getElementById("priv_grp_4009").checked, (priv_grp_4009==document.getElementById("priv_grp_4009").checked?false:true)],
			4010: [document.getElementById("priv_grp_4010").checked, (priv_grp_4010==document.getElementById("priv_grp_4010").checked?false:true)],
			4011: [document.getElementById("priv_grp_4011").checked, (priv_grp_4011==document.getElementById("priv_grp_4011").checked?false:true)],
			4012: [document.getElementById("priv_grp_4012").checked, (priv_grp_4012==document.getElementById("priv_grp_4012").checked?false:true)],
			4013: [document.getElementById("priv_grp_4013").checked, (priv_grp_4013==document.getElementById("priv_grp_4013").checked?false:true)],
			4014: [document.getElementById("priv_grp_4014").checked, (priv_grp_4014==document.getElementById("priv_grp_4014").checked?false:true)],
			4015: [document.getElementById("priv_grp_4015").checked, (priv_grp_4015==document.getElementById("priv_grp_4015").checked?false:true)],
			4016: [document.getElementById("priv_grp_4016").checked, (priv_grp_4016==document.getElementById("priv_grp_4016").checked?false:true)],
			4017: [document.getElementById("priv_grp_4017").checked, (priv_grp_4017==document.getElementById("priv_grp_4017").checked?false:true)],
			4018: [document.getElementById("priv_grp_4018").checked, (priv_grp_4018==document.getElementById("priv_grp_4018").checked?false:true)],
			4019: [document.getElementById("priv_grp_4019").checked, (priv_grp_4019==document.getElementById("priv_grp_4019").checked?false:true)],
			5000: [document.getElementById("priv_grp_5000").checked, (priv_grp_5000==document.getElementById("priv_grp_5000").checked?false:true)]
		}
	};
	let hasChange = false;
	
	for (var key in json_data["group_priv"]) {
		if (json_data["group_priv"].hasOwnProperty(key))
		{
			if (json_data["group_priv"][key][1])
			{
				hasChange = true;
			}
		}
	}
	
	if(!hasChange) //Nothing has changed
	{
		M.toast({html: "<span>No changes to save.</span>"});
		return null;
	}
	
	$.post('/en/users/php/group_priv_update.php',
		{
			"request": 'a_group_priv_update',
			"token": token,
			"gid":	gid,
			"json": JSON.stringify(json_data)
		},
		function(json)
		{
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			for (var key in json["group"])
			{
				document.getElementById("priv_grp_"+json["group"][key]["level"]).checked = json["group"][key]["checked"];
				$("#priv_grp_"+json["group"][key]["level"]).attr("data-value", json["group"][key]["checked"]);
			}
			M.toast({html: '<span>Group privileges updated.</span>'});
			
		},
		'json'
	).done(function()
	{
		
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	
	return null;
}

function group_priv_def(obj_levels)
{	//Define, checkboxes, the Group level access
	obj_levels.forEach(function(item, index, data)
	{
		switch(data[index]['level'])
		{
			//DB Type UBigInt
			case 1000:
				//$("#priv_pg_orders").attr("checked", true); //jQuery
				document.getElementById("priv_grp_1000").checked = true; //Vanilla
				$("#priv_grp_1000").attr("data-value", true);
			break;
			case 1001:
				document.getElementById("priv_grp_1001").checked = true;
				$("#priv_grp_1001").attr("data-value", true);
			break;
			case 1002:
				document.getElementById("priv_grp_1002").checked = true;
				$("#priv_grp_1002").attr("data-value", true);
			break;
			case 1003:
				document.getElementById("priv_grp_1003").checked = true;
				$("#priv_grp_1003").attr("data-value", true);
			break;
			case 1004:
				document.getElementById("priv_grp_1004").checked = true;
				$("#priv_grp_1004").attr("data-value", true);
			break;
			case 1005:
				document.getElementById("priv_grp_1005").checked = true;
				$("#priv_grp_1005").attr("data-value", true);
			break;
			case 1006:
				document.getElementById("priv_grp_1006").checked = true;
				$("#priv_grp_1006").attr("data-value", true);
			break;
			case 2000:
				//$("#priv_pg_orders").attr("checked", true); //jQuery
				document.getElementById("priv_grp_2000").checked = true; //Vanilla
				$("#priv_grp_2000").attr("data-value", true);
			break;
			case 2001:
				document.getElementById("priv_grp_2001").checked = true;
				$("#priv_grp_2001").attr("data-value", true);
			break;
			case 2002:
				document.getElementById("priv_grp_2002").checked = true;
				$("#priv_grp_2002").attr("data-value", true);
			break;
			case 2003:
				document.getElementById("priv_grp_2003").checked = true;
				$("#priv_grp_2003").attr("data-value", true);
			break;
			case 2004:
				document.getElementById("priv_grp_2004").checked = true;
				$("#priv_grp_2004").attr("data-value", true);
			break;
			case 2005:
				document.getElementById("priv_grp_2005").checked = true;
				$("#priv_grp_2005").attr("data-value", true);
			break;
			case 3000:
				//$("#priv_pg_orders").attr("checked", true); //jQuery
				document.getElementById("priv_grp_3000").checked = true; //Vanilla
				$("#priv_grp_3000").attr("data-value", true);
			break;
			case 3001:
				document.getElementById("priv_grp_3001").checked = true;
				$("#priv_grp_3001").attr("data-value", true);
			break;
			case 3002:
				document.getElementById("priv_grp_3002").checked = true;
				$("#priv_grp_3002").attr("data-value", true);
			break;
			case 3003:
				document.getElementById("priv_grp_3003").checked = true;
				$("#priv_grp_3003").attr("data-value", true);
			break;
			case 3004:
				document.getElementById("priv_grp_3004").checked = true;
				$("#priv_grp_3004").attr("data-value", true);
			break;
			case 3005:
				document.getElementById("priv_grp_3005").checked = true;
				$("#priv_grp_3005").attr("data-value", true);
			break;
			case 3006:
				document.getElementById("priv_grp_3006").checked = true;
				$("#priv_grp_3006").attr("data-value", true);
			break;
			case 4000:
				document.getElementById("priv_grp_4000").checked = true;
				$("#priv_grp_4000").attr("data-value", true);
			break;
			case 4001:
				document.getElementById("priv_grp_4001").checked = true;
				$("#priv_grp_4001").attr("data-value", true);
			break;
			case 4002:
				document.getElementById("priv_grp_4002").checked = true;
				$("#priv_grp_4002").attr("data-value", true);
			break;
			case 4003:
				document.getElementById("priv_grp_4003").checked = true;
				$("#priv_grp_4003").attr("data-value", true);
			break;
			case 4004:
				document.getElementById("priv_grp_4004").checked = true;
				$("#priv_grp_4004").attr("data-value", true);
			break;
			case 4005:
				document.getElementById("priv_grp_4005").checked = true;
				$("#priv_grp_4005").attr("data-value", true);
			break;
			case 4006:
				document.getElementById("priv_grp_4006").checked = true;
				$("#priv_grp_4006").attr("data-value", true);
			break;
			case 4007:
				document.getElementById("priv_grp_4007").checked = true;
				$("#priv_grp_4007").attr("data-value", true);
			break;
			case 4008:
				document.getElementById("priv_grp_4008").checked = true;
				$("#priv_grp_4008").attr("data-value", true);
			break;
			case 4009:
				document.getElementById("priv_grp_4009").checked = true;
				$("#priv_grp_4009").attr("data-value", true);
			break;
			case 4010:
				document.getElementById("priv_grp_4010").checked = true;
				$("#priv_grp_4010").attr("data-value", true);
			break;
			case 4011:
				document.getElementById("priv_grp_4011").checked = true;
				$("#priv_grp_4011").attr("data-value", true);
			break;
			case 4012:
				document.getElementById("priv_grp_4012").checked = true;
				$("#priv_grp_4012").attr("data-value", true);
			break;
			case 4013:
				document.getElementById("priv_grp_4013").checked = true;
				$("#priv_grp_4013").attr("data-value", true);
			break;
			case 4014:
				document.getElementById("priv_grp_4014").checked = true;
				$("#priv_grp_4014").attr("data-value", true);
			break;
			case 4015:
				document.getElementById("priv_grp_4015").checked = true;
				$("#priv_grp_4015").attr("data-value", true);
			break;
			case 4016:
				document.getElementById("priv_grp_4016").checked = true;
				$("#priv_grp_4016").attr("data-value", true);
			break;
			case 4017:
				document.getElementById("priv_grp_4017").checked = true;
				$("#priv_grp_4017").attr("data-value", true);
			break;
			case 4018:
				document.getElementById("priv_grp_4018").checked = true;
				$("#priv_grp_4018").attr("data-value", true);
			break;
			case 4019:
				document.getElementById("priv_grp_4019").checked = true;
				$("#priv_grp_4019").attr("data-value", true);
			break;
			case 5000:
				document.getElementById("priv_grp_5000").checked = true;
				$("#priv_grp_5000").attr("data-value", true);
			break;
			default:
			//nada
			break;
		}
	});
}

function groups_get(token, caller)
{	//Fetch groups in the database
	filter_data = {
		columns:	$('#groups_filter_sel').val(),
		keyword:	document.getElementById("groups_filter").value
	};
	if(caller != null)
	{
		if(caller.hasAttribute("data-page"))
		{	//Check if page change requested
			let temp_page_no = caller.getAttribute("data-page");
			if (temp_page_no != null && temp_page_no != "null")
			{	//Set new page by selection
				groups_page_no = temp_page_no;
			}
			else
			{
				return;
			}
		}
	}
	//#page_limit
	groups_page_limit = $('#page_limit_groups').val();
	
	$('#groups_tbl > tbody').text(""); //sort_clear('#groups_tbl');
	$('.progress').remove();
	$('#groups_tbl').parent().append('<div class="progress"><div class="indeterminate"></div></div>');
	$.post('/en/users/php/groups.php',
		{
			request: 'all_users_groups',
			token: token,
			page_no: groups_page_no,
			page_limit: groups_page_limit,
			sort_order: groups_sort_order,
			sort_column: groups_sort_column,
			filter: filter_data
		},
		function(json)
		{
			paging_update('page_groups', json['page_no'], json['page_total']);
			//Paging click event
			$('#page_groups li').on("click", "a", function(){
				event.preventDefault();
				groups_get($("body").attr('data-token'), this);
			});	
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			groups_append('#groups_tbl', json['users_groups']);
		},
		'json'
	).done(function()
	{
		$('.progress').remove();
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	
	return null;
}

function groups_append(table_id, groups_array)
{	//Add groups to a specified table
	if (groups_array == null) return false;
	var html = "";
	groups_array.forEach(function(item, index, data){
		html = html + 
			'<tr data-gid="'+	data[index]['id'] + 
			'"><td>' +			(data[index]['name']!=null?data[index]['name']:'') + 
			'</td><td>' +		(data[index]['created_by_uname']!=null?data[index]['created_by_uname']:'') + 
			'</td><td>' +		(data[index]['created_on']!=null?data[index]['created_on']:'') + 
			'</td><td>' +		(data[index]['updated_by_uname']!=null?data[index]['updated_by_uname']:'') + 
			'</td><td>' +		(data[index]['updated_on']!=null?data[index]['updated_on']:'') + 
			'</td></tr>';
	});
	$(table_id + ' > tbody').append(html);
	return true;
}

function groups_list_get(token, element_id)
{	//Fetch all groups in the database
	let result = false;
	$.post('/en/users/php/groups_list.php',
		{
			request: 'all_users_groups',
			token: token
		},
		function(json)
		{
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			groups_list_append(element_id, json['groups']);
		},
		'json'
	).done(function()
	{
		//Once the options are added, then initialize MCSS elements on the options
		$(element_id).formSelect();
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	
	return null;
}

function groups_list_append(element_id, groups_array)
{	//Add groups to a specified table
	if (groups_array == null) return false;
	$(element_id).html("");
	var html = '<option value="0" disabled selected></option>';
	$(element_id).append(html);
	groups_array.forEach(function(item, index, data){
		html = '<option value="' + groups_array[index]['id'] + '">' + groups_array[index]['name'] + '</option>';
		$(element_id).append(html);
	});
	return true;
}


//USER REQUESTS TAB
function users_reqs_get(token, caller)
{	//Fetch user requests in the database
	filter_data = {
		columns:	$('#users_reqs_filter_sel').val(),
		keyword:	document.getElementById("users_reqs_filter").value
	};
	if(caller != null)
	{
		if(caller.hasAttribute("data-page"))
		{	//Check if page change requested
			let temp_page_no = caller.getAttribute("data-page");
			if (temp_page_no != null && temp_page_no != "null")
			{	//Set new page by selection
				users_reqs_page_no = temp_page_no;
			}
			else
			{
				return;
			}
		}
	}
	//#page_limit
	users_reqs_page_limit = $('#page_limit_users_reqs').val();
	
	$('#users_reqs_tbl > tbody').text("");
	$('.progress').remove();
	$('#users_reqs_tbl').parent().append('<div class="progress"><div class="indeterminate"></div></div>');
	$.post('/en/users/php/users_reqs.php',
		{
			request: 'all_users_req',
			token: token,
			page_no: users_reqs_page_no,
			page_limit: users_reqs_page_limit,
			sort_order: users_reqs_sort_order,
			sort_column: users_reqs_sort_column,
			filter: filter_data
		},
		function(json)
		{
			paging_update('page_users_reqs', json['page_no'], json['page_total']);
			//Paging click event
			$('#page_users_reqs li').on("click", "a", function(){
				event.preventDefault();
				users_reqs_get($("body").attr('data-token'), this);
			});	
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			
			users_reqs_append('#users_reqs_tbl', json['users_req']);
		},
		'json'
	).done(function()
	{
		$('.progress').remove();
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	
	return null;
}

function users_reqs_append(table_id, users_reqs_array)
{	//Add users to a specified table
	if (users_reqs_array == null) return false;
	var html = "";
	users_reqs_array.forEach(function(item, index, data){
		html = '<tr data-urid="'+	data[index]['id'] + 
			'"><td>'+ 			data[index]['username'] +
			'</td><td>' +		(data[index]['name']!=null?data[index]['name']:'') + 
			'</td><td>' +		(data[index]['initials']!=null?data[index]['initials']:'') + 
			'</td><td>' +		(data[index]['surname']!=null?data[index]['surname']:'') + 
			'</td><td>' +		(data[index]['created_on']!=null?data[index]['created_on']:'') + 
			'</td><td>' +		(data[index]['updated_on']!=null?data[index]['updated_on']:'') + 
			'</td><td>' +		(data[index]['updated_by_uname']!=null?data[index]['updated_by_uname']:'') + 
			'</td></tr>';
			
			$(table_id + ' > tbody').append(html);
	});
	//$(table_id + ' > tbody').append(html);
	return true;
}

function users_req_get(token, urid)
{	//Fetch user request information
	//Fetch a user in the database
	groups_list_get(token, '#io_edit_users_req_group');
	//Clear previous data:
	//Tab: User info
	$('#io_edit_users_req_uname').html("Unknown");
	$('#io_edit_users_req_fname').val("");
	$('#io_edit_users_req_initials').val("");
	$('#io_edit_users_req_lname').val("");
	$('#io_edit_users_req_email').val("");
	$('#io_edit_users_req_salref').val("");
	$('#io_edit_users_req_group').val(0)
	$('#io_edit_users_req_enabled').val(-1)
	
	$('#io_edit_users_req_group').formSelect();
	$('#io_edit_users_req_enabled').formSelect();
	
	document.getElementById('users_req_inf_username').innerText = "";
	document.getElementById('users_req_inf_created_on').innerText = "";
	document.getElementById('users_req_inf_created_by').innerText = "";
	document.getElementById('users_req_inf_updated_on').innerText = "";
	document.getElementById('users_req_inf_updated_by').innerText = "";
	
	$.post('/en/users/php/users_req.php',
		{
			request: 'a_users_req',
			token: token,
			urid: urid
		},
		function(json)
		{
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			//Vanilla
			document.getElementById('modal_users_req_edit').setAttribute('data-urid', json['user']['id']);
			document.getElementById('io_edit_users_req_uname').setAttribute('data-urid', urid);
			document.getElementById('io_edit_users_req_uname').setAttribute('data-value', json['user']['username']);
			document.getElementById('io_edit_users_req_uname').innerText = json['user']['username'];
			document.getElementById('io_edit_users_req_fname').setAttribute('data-value', json['user']['name']);
			document.getElementById('io_edit_users_req_fname').value = json['user']['name'];
			document.getElementById('io_edit_users_req_initials').setAttribute('data-value', json['user']['initials']);
			document.getElementById('io_edit_users_req_initials').value = json['user']['initials'];
			document.getElementById('io_edit_users_req_lname').setAttribute('data-value', json['user']['surname']);
			document.getElementById('io_edit_users_req_lname').value = json['user']['surname'];
			document.getElementById('io_edit_users_req_email').setAttribute('data-value', json['user']['email']);
			document.getElementById('io_edit_users_req_email').value = json['user']['email'];
			document.getElementById('io_edit_users_req_salref').setAttribute('data-value', json['user']['salref']);
			document.getElementById('io_edit_users_req_salref').value = json['user']['salref'];
			
			document.getElementById('users_req_inf_username').innerText = json['user']['username'];
			document.getElementById('users_req_inf_created_on').innerText = json['user']['created_on'];
			document.getElementById('users_req_inf_created_by').innerText = json['user']['created_by_uname'];
			document.getElementById('users_req_inf_updated_on').innerText = json['user']['updated_on'];
			document.getElementById('users_req_inf_updated_by').innerText = json['user']['updated_by_uname'];
			
			//document.getElementById('io_edit_users_req_enabled').setAttribute('data-value', json['user']['enabled']);
			//document.getElementById('io_edit_users_req_enabled').value = json['user']['enabled'];
			
			document.getElementById('io_edit_users_req_group').setAttribute('data-value', json['user']['gid']);
			document.getElementById('io_edit_users_req_group').value = json['user']['gid'];
			
			
			M.updateTextFields();
			$('#io_edit_users_req_enabled').formSelect();
			$('#io_edit_users_req_group').formSelect();
		},
		'json'
	).done(function()
	{
		$('.progress').remove();
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	
	return null;
}

function users_req_approve(token, urid)
{	//Set/update user info in the database
	
	var result = confirm("Are you sure you want to Approve the user account?");
	if (result)
	{	//Continue to delete the user account

		$.post('/en/users/php/users_req_approve.php',
			{
				"request": 'a_users_req_added',
				"token": token,
				"urid": urid
			},
			function(json)
			{
				if(json['error']){
					let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
					M.toast({html: toastHTML});
					if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
					}
					return null;
				}
				
				//Successful response
				//Update table row
				$('.vms-users-req-editing').remove();
				mod_users_req_edit.close();
				M.toast({html: json['user']});
			},
			'json'
		).done(function()
		{
			
		}).fail(function(jqXHR,status,err)
		{
			
		}).always(function()
		{
			
		});
	}
	
	return null;
}

//USERS HIST TAB
function users_hist_get(token, caller)
{	//Fetch users in the database
	filter_data = {
		columns:	$('#users_hist_filter_sel').val(),
		keyword:	document.getElementById("users_hist_filter").value
	};
	if(caller != null)
	{
		if(caller.hasAttribute("data-page"))
		{	//Check if page change requested
			let temp_page_no = caller.getAttribute("data-page");
			if (temp_page_no != null && temp_page_no != "null")
			{	//Set new page by selection
				users_hist_page_no = temp_page_no;
			}
			else
			{
				return;
			}
		}
	}
	//#page_limit
	users_hist_page_limit = $('#page_limit_users_hist').val();

	$('#users_hist_tbl > tbody').text(""); //sort_clear('#users_hist_tbl');
	$('.progress').remove();
	$('#users_hist_tbl').parent().append('<div class="progress"><div class="indeterminate"></div></div>');
	$.post('/en/users/php/users_hist.php',
		{
			request: 'all_users_hist',
			token: token,
			page_no: users_hist_page_no,
			page_limit: users_hist_page_limit,
			sort_order: users_hist_sort_order,
			sort_column: users_hist_sort_column,
			filter: filter_data
		},
		function(json)
		{
			paging_update('page_users_hist', json['page_no'], json['page_total']);
			//Paging click event
			$('#page_users_hist li').on("click", "a", function(){
				event.preventDefault();
				users_hist_get($("body").attr('data-token'), this);
			});	
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			users_hist_append('#users_hist_tbl', json['users_hist']);
		},
		'json'
	).done(function()
	{
		$('.progress').remove();
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	
	return null;
}


function users_hist_append(table_id, users_array)
{	//Add users to a specified table
	if (users_array == null) return false;
	var html = "";
	users_array.forEach(function(item, index, data){
		html = '<tr data-uid="'+	data[index]['id'] + 
			'"><td>'+ 		data[index]['action'] +
			'</td><td>' +		data[index]['username'] + 
			'</td><td>' +		data[index]['name'] + 
			'</td><td>' +		data[index]['initials'] + 
			'</td><td>' +		data[index]['surname'] + 
			'</td><td>' +		data[index]['email'] + 
			'</td><td>' +		data[index]['group_name'] + 
			'</td><td>' +		data[index]['salref'] +
			'</td><td>' +		data[index]['updated_by_uname'] + 
			'</td><td>' +		(data[index]['updated_on']!=null?data[index]['updated_on']:'') + 
			'</td></tr>';
			
			$(table_id + ' > tbody').append(html);
	});
	return true;
}

//MISC
function getColumnsToFilterBy(table_id, filter_sel_id)
{	//Adds columns' indexes and names to to the drop down filter
	var testThisFunc = document.querySelectorAll('#'+table_id+'> thead > tr > th')
	testThisFunc.forEach(function(value,index,array){
		//console.log('index: '+ index + ', Value: ' + value.innerText);
		var html = '<option value="'+index+'" selected>'+value.innerText+'</option>';
		$('#'+filter_sel_id).append(html);
	});
	
}

function sortTable(table_id, column, order)
{	//Sorting Algorithm used by sortByColumn()
	switch(table_id)
	{
		case "#users_tbl":
			users_sort_order = order;
			users_sort_column = column;
			users_get($("body").attr('data-token'), null);
			break;
		case "#groups_tbl":
			groups_sort_order = order;
			groups_sort_column = column;
			groups_get($("body").attr('data-token'), null);
			break;
		case "#users_reqs_tbl":
			users_reqs_sort_order = order;
			users_reqs_sort_column = column;
			users_reqs_get($("body").attr('data-token'), null);
			break;
		case "#users_hist_tbl":
			users_hist_sort_order = order;
			users_hist_sort_column = column;
			users_hist_get($("body").attr('data-token'), null);
			break;
		default:
	}
}

function sortByColumn(table_id, table_th)
{	//Sort a table by a specified column
	if($(table_th).find("i").hasClass("vms-sort"))
	{
		sortTable(table_id, $(table_th).index(), "ASC");
		$(table_th).find("i").removeClass("vms-sort");
		$(table_th).find("i").addClass("vms-sort-asc").text("arrow_drop_up");
		$(table_th).siblings().find("i").addClass("vms-sort").text("");
		$(table_th).siblings().find("i").removeClass("vms-sort-asc");
		$(table_th).siblings().find("i").removeClass("vms-sort-desc");
	}
	else if($(table_th).find("i").hasClass("vms-sort-asc"))
	{
		sortTable(table_id, $(table_th).index(), "DESC");
		$(table_th).find("i").removeClass("vms-sort-asc");
		$(table_th).find("i").addClass("vms-sort-desc").text("arrow_drop_down");
		$(table_th).siblings().find("i").addClass("vms-sort").text("");
		$(table_th).siblings().find("i").removeClass("vms-sort-asc");
		$(table_th).siblings().find("i").removeClass("vms-sort-desc");
	}
	else if($(table_th).find("i").hasClass("vms-sort-desc"))
	{
		sortTable(table_id, $(table_th).index(), "ASC");
		$(table_th).find("i").removeClass("vms-sort-desc");
		$(table_th).find("i").addClass("vms-sort-asc").text("arrow_drop_up");
		$(table_th).siblings().find("i").addClass("vms-sort").text("");
		$(table_th).siblings().find("i").removeClass("vms-sort-asc");
		$(table_th).siblings().find("i").removeClass("vms-sort-desc");
	}
}

function sort_clear(table_id)
{	//Clearing current sort of selected tab
	$(table_id + ' > tbody').text("");
	$(table_id).find("i").removeClass("vms-sort-asc");
	$(table_id).find("i").removeClass("vms-sort-desc");
	$(table_id).find("i").addClass("vms-sort").text("");
}

function paging_update(element_id, new_page_no, new_page_total)
{
	(new_page_no==null)?new_page_no=1:(typeof new_page_no=="undefined")?new_page_no=1:false;
	(new_page_total==null)?new_page_total=1:(typeof new_page_total=="undefined")?new_page_total=1:false;
	//var html = "";
	document.getElementById(element_id).innerHTML = "";
	//Prev button
	(
		(new_page_no > 1) ?
			$('#' + element_id).append('<li class="waves-effect tooltipped" data-tooltip="Previous page" data-position="left"><a data-page="' + (new_page_no - 1) + '" href="#!"><i class="material-icons">chevron_left</i></a></li>') :
			$('#' + element_id).append('<li class="disabled"><a data-page="null" href="#!"><i class="material-icons">chevron_left</i></a></li>')
	);
	//First page
	(
		(new_page_no > 4) ?
			$('#' + element_id).append('<li class="waves-effect"><a data-page="1" href="#!">1</a></li>') :
			false
	);
	//Lower More = ...
	(
		(new_page_no > 5) ?
			((new_page_no == 6) ?
				$('#' + element_id).append('<li class="waves-effect"><a data-page="2" href="#!">2</a></li>') :
				$('#' + element_id).append('<li class="disabled"><a data-page="null" href="#!"><i class="material-icons">more_horiz</i></a></li>')
			) :
			false
	);
	//Current page - 3
	(
		(new_page_no > 3) ?
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no - 3) + '" href="#!">' + (new_page_no - 3) + '</a></li>') :
		false
	);
	//Current page - 2
	(
		(new_page_no > 2) ?
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no - 2) + '" href="#!">' + (new_page_no - 2) + '</a></li>') :
		false
	);
	//Current page - 1
	(
		(new_page_no > 1) ?
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no - 1) + '" href="#!">' + (new_page_no - 1) + '</a></li>') :
		false
	);
	//Current page
	(
		(new_page_no > 0) ?
		$('#' + element_id).append('<li class="active"><a data-page="null" href="#!">' + new_page_no + '</a></li>') :
		false
	);
	//Current page + 1
	(
		((new_page_total - new_page_no) > 1) ?
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no + 1) + '" href="#!">' + (new_page_no + 1) + '</a></li>') :
		false
	);
	//Current page + 2
	(
		((new_page_total - new_page_no) > 2) ?
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no + 2) + '" href="#!">' + (new_page_no + 2) + '</a></li>') :
		false
	);
	//Current page + 3
	(
		((new_page_total - new_page_no) > 3) ?
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no + 3) + '" href="#!">' + (new_page_no + 3) + '</a></li>') :
		false
	);
	//Upper More = ...
	(
		((new_page_total - new_page_no) > 4) ?
			(((new_page_total - new_page_no) == 5) ?
				$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_total - 1) + '" href="#!">' + (new_page_total - 1) + '</a></li>') :
				$('#' + element_id).append('<li class="disabled"><a data-page="null" href="#!"><i class="material-icons">more_horiz</i></a></li>')
			) :
			false
	);
	//Last page
	(
		(new_page_no >= new_page_total) ?
		true :
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_total) + '" href="#!">' + (new_page_total) + '</a></li>')
	);
	//Next button
	(
		((new_page_total - new_page_no) > 0) ?
		$('#'+element_id).append('<li class="waves-effect tooltipped right" data-tooltip="Next page" data-position="right"><a data-page="'+(new_page_no+1)+'" href="#!"><i class="material-icons">chevron_right</i></a></li>') :
		$('#'+element_id).append('<li class="disabled right"><a data-page="null" href="#!"><i class="material-icons">chevron_right</i></a></li>')
	);
	//Activate Tooltips:
	options = {
		enterDelay: 500
	};
	elems = document.querySelectorAll('ul .tooltipped');
    M.Tooltip.init(elems, options);
}
