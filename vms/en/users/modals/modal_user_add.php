<div id="modal_user_add" class="modal modal-fixed-footer">
	<div class="modal-content">
		<h4>Add a User</h4>
		<div class="row">
			<div class="col s12 m2 l3">
			</div>
			<div class="col s12 m8 l6">
				<form class="row">
					<div class="input-field col s12">
						<i class="material-icons prefix">domain</i>
						<input placeholder="Telkom username..." id="io_user_uname" type="text" class="validate">
						<label for="io_user_uname">Username</label>
					</div>
					<div class="input-field col s12">
						<i class="material-icons prefix">person</i>
						<input id="io_user_fname" type="text" class="validate">
						<label for="io_user_fname">First Name</label>
					</div>
					<div class="input-field col s12">
						<i class="material-icons prefix"></i>
						<input id="io_user_initials" type="text" class="validate">
						<label for="io_user_initials">Initials</label>
					</div>
					<div class="input-field col s12">
						<i class="material-icons prefix"></i>
						<input id="io_user_lname" type="text" class="validate">
						<label for="io_user_lname">Surname</label>
					</div>
					<div class="input-field col s12">
						<i class="material-icons prefix">email</i>
						<input id="io_user_email" type="text" class="validate">
						<label for="io_user_email">Email Address</label>
					</div>
					<div class="input-field col s12">
						<i class="material-icons prefix">local_offer</i>
						<input id="io_user_salref" type="text" class="validate">
						<label for="io_user_salref">Salary Ref.</label>
					</div>
					<div class="input-field col s12">
						<i class="material-icons prefix">supervisor_account</i>
						<select id="io_user_group">
							
						</select>
						<label>User Type</label>
					</div>
					<!--<div class="input-field col s12">
						<i class="material-icons prefix">verified_user</i>
						<select id="io_user_status">
							<option value="0" disabled selected></option>
							<option value="0">No Status</option>
							<option value="1">Locked</option>
						</select>
						<label>User Status</label>
					</div>-->
					<div class="input-field col s12">
						<i class="material-icons prefix">lock</i>
						<select id="io_user_enabled">
							<option value="0" disabled selected></option>
							<option value="0">Disabled</option>
							<option value="1">Enabled</option>
						</select>
						<label>User Enabled</label>
					</div>
					<div class="input-field col s12">
						<i class="material-icons prefix">vpn_key</i>
						<input id="io_user_password" type="password" class="validate">
						<label for="io_user_password">New Password</label>
					</div>
					<div class="input-field col s12">
						<i class="material-icons prefix"></i>
						<input id="io_user_password1" type="password" class="validate">
						<label for="io_user_password1">Confirm Password</label>
					</div>
				</form>
			</div>
			<div class="col s12 m2 l3">
			</div>
		</div>

		<!--<div class="row">
			<div class="col s12 m2 l3">
			</div>
			<div class="col s12 m8 l6">
				<p>Information: </p>
				<p>On submission of a valid request, an email will be sent to the VMS administrator for review and approval.</p>
				<p>A second email will be sent to the specified email address as confirmation of the request submission.</p>
				<p>On approval, the specified email address will recieve confirmation, at which point you may proceed by logging into VMS. </p>
			</div>
			<div class="col s12 m2 l3">
			</div>
		</div>-->
	</div>
	<div class="modal-footer">
		<a href="#!" class="modal-close waves-effect waves-red btn-flat">Cancel</a>
		<a id="btn_user_add" data-token="<?php echo create_token();?>" href="#!" class="waves-effect waves-green btn-flat <?php if(!grant_access([4004])){echo "hide";} ?>">Create</a>
	</div>
</div>