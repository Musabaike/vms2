<div id="modal_group_add" class="modal modal-fixed-footer">
	<div class="modal-content">
		<h4>Add a Group</h4>
		<div class="row">
			<div class="col s12 m2 l3">
			</div>
			<div class="col s12 m8 l6">
				<form class="row">
					<div class="input-field col s12">
						<i class="material-icons prefix">group</i>
						<input placeholder="New group name..." id="io_grp_name" type="text" data-length="50" maxlength="50" class="validate">
						<label for="io_grp_name">Group Name</label>
					</div>
					<div class="input-field col s12">
						<i class="material-icons prefix">description</i>
						<input placeholder="New group description..." id="io_grp_description" type="text" data-length="50" maxlength="50" class="validate">
						<label for="io_grp_description">Description</label>
					</div>
				</form>
			</div>
			<div class="col s12 m2 l3">
			</div>
		</div>

		
	</div>
	<div class="modal-footer">
		<a href="#!" class="modal-close waves-effect waves-red btn-flat">Cancel</a>
		<a id="mod_group_btn_add" data-token="<?php echo create_token();?>" href="#!" class="waves-effect waves-green btn-flat <?php if(!grant_access([4011])){echo "hide";} ?>">Create</a>
	</div>
</div>