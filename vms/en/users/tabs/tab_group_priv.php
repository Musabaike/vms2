<div class="row">
	<div class="col s12">
		<ul id="col_priv_grp" class="collapsible">
			<li class="active">
				<div class="collapsible-header"><i class="material-icons">home</i>Dashboard</div>
				<div class="collapsible-body">
					<form action="#">
						<div class="row">
							<div class="col s6 m4 l3">
								<h5>Page Access</h5>
								<p>
									<label for="priv_grp_1000">
									<input id="priv_grp_1000" type="checkbox" />
									<span>Dashboard</span>
									</label>
								</p>
							</div>
							<div class="col s6 m4 l3">
								<h5>View</h5>
								<p>
									<label for="priv_grp_1001">
									<input id="priv_grp_1001" type="checkbox" />
									<span>Orders button</span>
									</label>
								</p>
								<p>
									<label for="priv_grp_1002">
									<input id="priv_grp_1002" type="checkbox" />
									<span>Admin button</span>
									</label>
								</p>
								<p>
									<label for="priv_grp_1003">
									<input id="priv_grp_1003" type="checkbox" />
									<span>Users button</span>
									</label>
								</p>
								<p>
									<label for="priv_grp_1004">
									<input id="priv_grp_1004" type="checkbox" />
									<span>Settings button</span>
									</label>
								</p>
								<p>
									<label for="priv_grp_1005">
									<input id="priv_grp_1005" type="checkbox" />
									<span>Orders graph</span>
									</label>
								</p>
								<p>
									<label for="priv_grp_1006">
									<input id="priv_grp_1006" type="checkbox" />
									<span>Cards graph</span>
									</label>
								</p>
							</div>
							<div class="col s6 m4 l3">
								<h5>Functions</h5>
								
							</div>
						</div>
					</form>
				</div>
			</li>
			<li>
				<div class="collapsible-header"><i class="material-icons">date_range</i>Orders</div>
				<div class="collapsible-body">
					<form action="#">
						<div class="row">
							<div class="col s6 m4 l3">
								<h5>Page Access</h5>
								<p>
									<label for="priv_grp_2000">
									<input id="priv_grp_2000" type="checkbox" />
									<span>Orders</span>
									</label>
								</p>
							</div>
							<div class="col s6 m4 l3">
								<h5>View</h5>
								<p>
									<label for="priv_grp_2001">
									<input id="priv_grp_2001" type="checkbox" />
									<span>Orders tab</span>
									</label>
								</p>
							</div>
							<div class="col s6 m4 l3">
								<h5>Functions</h5>
								<p>
									<label for="priv_grp_2002">
									<input id="priv_grp_2002" type="checkbox" />
									<span>Orders list</span>
									</label>
								</p>
								<p>
									<label for="priv_grp_2003">
									<input id="priv_grp_2003" type="checkbox" />
									<span>Orders view</span>
									</label>
								</p>
							</div>
						</div>
						<div class="row">
							<div class="col s6 m4 l3">
							</div>
							<div class="col s6 m4 l3">
								<p>
									<label for="priv_grp_2004">
									<input id="priv_grp_2004" type="checkbox" />
									<span>Vouchers tab</span>
									</label>
								</p>
							</div>
							<div class="col s6 m4 l3">
								<p>
									<label for="priv_grp_2005">
									<input id="priv_grp_2005" type="checkbox" />
									<span>Vouchers list</span>
									</label>
								</p>
							</div>
						</div>
					</form>
				</div>
			</li>
			<li>
				<div class="collapsible-header"><i class="material-icons">build</i>Admin</div>
				<div class="collapsible-body">
					<form action="#">
						<div class="row">
							<div class="col s6 m4 l3">
								<h5>Page Access</h5>
								<p>
									<label for="priv_grp_3000">
									<input id="priv_grp_3000" type="checkbox" />
									<span>Admin</span>
									</label>
								</p>
							</div>
							<div class="col s6 m4 l3">
								<h5>View</h5>
								<p>
									<label for="priv_grp_3001">
									<input id="priv_grp_3001" type="checkbox" />
									<span>Distributors tab</span>
									</label>
								</p>
							</div>
							<div class="col s6 m4 l3">
								<h5>Functions</h5>
								<p>
									<label for="priv_grp_3002">
									<input id="priv_grp_3002" type="checkbox" />
									<span>Distributors list</span>
									</label>
								</p>
								<p>
									<label for="priv_grp_3003">
									<input id="priv_grp_3003" type="checkbox" />
									<span>Distributors view</span>
									</label>
								</p>
								<p>
									<label for="priv_grp_3004">
									<input id="priv_grp_3004" type="checkbox" />
									<span>Distributors add</span>
									</label>
								</p>
								<p>
									<label for="priv_grp_3005">
									<input id="priv_grp_3005" type="checkbox" />
									<span>Distributors edit</span>
									</label>
								</p>
								<p>
									<label for="priv_grp_3006">
									<input id="priv_grp_3006" type="checkbox" />
									<span>Distributors delete</span>
									</label>
								</p>
							</div>
						</div>
					</form>
				</div>
			</li>
			<li>
				<div class="collapsible-header"><i class="material-icons">group</i>Users</div>
				<div class="collapsible-body">
					<form action="#">
						<div class="row">
							<div class="col s6 m4 l3">
								<h5>Page Access</h5>
								<p>
									<label for="priv_grp_4000">
									<input id="priv_grp_4000" type="checkbox" />
									<span>Users</span>
									</label>
								</p>
							</div>
							<div class="col s6 m4 l3">
								<h5>View</h5>
								<p>
									<label for="priv_grp_4001">
									<input id="priv_grp_4001" type="checkbox" />
									<span>Users tab</span>
									</label>
								</p>
							</div>
							<div class="col s6 m4 l3">
								<h5>Functions</h5>
								<p>
									<label for="priv_grp_4002">
									<input id="priv_grp_4002" type="checkbox" />
									<span>Users list</span>
									</label>
								</p>
								<p>
									<label for="priv_grp_4003">
									<input id="priv_grp_4003" type="checkbox" />
									<span>Users view</span>
									</label>
								</p>
								<p>
									<label for="priv_grp_4004">
									<input id="priv_grp_4004" type="checkbox" />
									<span>Users add</span>
									</label>
								</p>
								<p>
									<label for="priv_grp_4005">
									<input id="priv_grp_4005" type="checkbox" />
									<span>Users edit</span>
									</label>
								</p>
								<p>
									<label for="priv_grp_4006">
									<input id="priv_grp_4006" type="checkbox" />
									<span>Users delete</span>
									</label>
								</p>
								<p>
									<label for="priv_grp_4007">
									<input id="priv_grp_4007" type="checkbox" />
									<span>Users logout</span>
									</label>
								</p>
							</div>
						</div>
						<div class="row">
							<div class="col s6 m4 l3">
							</div>
							<div class="col s6 m4 l3">
								<p>
									<label for="priv_grp_4008">
									<input id="priv_grp_4008" type="checkbox" />
									<span>Groups tab</span>
									</label>
								</p>
							</div>
							<div class="col s6 m4 l3">
								<p>
									<label for="priv_grp_4009">
									<input id="priv_grp_4009" type="checkbox" />
									<span>Groups list</span>
									</label>
								</p>
								<p>
									<label for="priv_grp_4010">
									<input id="priv_grp_4010" type="checkbox" />
									<span>Groups view</span>
									</label>
								</p>
								<p>
									<label for="priv_grp_4011">
									<input id="priv_grp_4011" type="checkbox" />
									<span>Groups add</span>
									</label>
								</p>
								<p>
									<label for="priv_grp_4012">
									<input id="priv_grp_4012" type="checkbox" />
									<span>Groups edit</span>
									</label>
								</p>
								<p>
									<label for="priv_grp_4013">
									<input id="priv_grp_4013" type="checkbox" />
									<span>Groups delete</span>
									</label>
								</p>
							</div>
						</div>
						<div class="row">
							<div class="col s6 m4 l3">
							</div>
							<div class="col s6 m4 l3">
								<p>
									<label for="priv_grp_4014">
									<input id="priv_grp_4014" type="checkbox" />
									<span>Requests tab</span>
									</label>
								</p>
							</div>
							<div class="col s6 m4 l3">
								<p>
									<label for="priv_grp_4015">
									<input id="priv_grp_4015" type="checkbox" />
									<span>Requests list</span>
									</label>
								</p>
								<p>
									<label for="priv_grp_4016">
									<input id="priv_grp_4016" type="checkbox" />
									<span>Requests view</span>
									</label>
								</p>
								<p>
									<label for="priv_grp_4017">
									<input id="priv_grp_4017" type="checkbox" />
									<span>Requests Approve</span>
									</label>
								</p>
								<p>
									<label for="priv_grp_4018">
									<input id="priv_grp_4018" type="checkbox" />
									<span>Requests edit</span>
									</label>
								</p>
								<p>
									<label for="priv_grp_4019">
									<input id="priv_grp_4019" type="checkbox" />
									<span>Requests Reject</span>
									</label>
								</p>
							</div>
						</div>
					</form>
				</div>
			</li>
			<li>
				<div class="collapsible-header"><i class="material-icons">settings</i>Settings</div>
				<div class="collapsible-body">
					<form action="#">
						<div class="row">
							<div class="col s6 m4 l3">
								<h5>Page Access</h5>
								<p>
									<label for="priv_grp_5000">
									<input id="priv_grp_5000" type="checkbox" />
									<span>Settings</span>
									</label>
								</p>
							</div>
						</div>
					</form>
				</div>
			</li>
		</ul>
		
	</div>
</div>