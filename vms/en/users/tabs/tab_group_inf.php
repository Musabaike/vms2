<form class="row">
	<div class="input-field col s12">
		<i class="material-icons prefix">group</i>
		<input placeholder="New group name..." id="io_edit_grp_name" type="text" data-length="50" maxlength="50" class="validate">
		<label for="io_edit_grp_name">Group Name</label>
	</div>
	<div class="input-field col s12">
		<i class="material-icons prefix">description</i>
		<input placeholder="New group description..." id="io_edit_grp_description" type="text" data-length="255" maxlength="255" class="validate">
		<label for="io_edit_grp_description">Group Description</label>
	</div>
</form>

<div class="row">
</div>
<div class="row">
	<div class="collection">
		<a href="#!" class="collection-item"><b>System Information</b></a>
		<a href="#!" class="collection-item"><span id="group_inf_group_name" class="badge"></span>Group Name</a>
		<a href="#!" class="collection-item"><span id="group_inf_created_on" class="badge"></span>Created On</a>
		<a href="#!" class="collection-item"><span id="group_inf_created_by" class="badge"></span>Created By</a>
		<a href="#!" class="collection-item"><span id="group_inf_updated_on" class="badge"></span>Updated On</a>
		<a href="#!" class="collection-item"><span id="group_inf_updated_by" class="badge"></span>Updated By</a>
	</div>
</div>