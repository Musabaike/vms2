<?php
//Start session
session_start();

// Prevent caching.
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 01 Jan 1996 00:00:00 GMT');
// The JSON standard MIME header.
header('Content-type: application/json');

//Default Data
$error			= false;
$error_msg		= null;
$error_level	= null;
$error_code		= 0;
$verified		= null;

$request		= null;
$io_uname		= null;
$io_pword		= null;

$db_uid			= null;
$db_uname		= null;
$db_name		= null;
$db_initials	= null;
$db_surname		= null;
$db_email		= null;
$db_hash		= null;
$db_group		= null;
$db_status		= null;
$db_enabled		= 0;	//Default disabled
$db_salref		= null;
$db_created_on	= null;
$db_levels		= null;

//Ensure a previous timeout doesn't impact the new login.
$_SESSION['timeout'] = false;	
$now = time();
$session_duration = 900;// The initial/first timeout interval (900 = 15min) - consecutive intervals is set in cfg_session_timer.php
$_SESSION['discard_after'] = $now + $session_duration;

//include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';

// Empty data received in request.
if(empty($_POST['token']))
{
	error_response_json(
		"Please login before continuing.",
		1,
		__LINE__,
		null,
		null);
}

// Data received in request.
if(empty($_POST['request']))
{
	error_response_json(
		"Request data not available.",
		1,
		__LINE__,
		null,
		null);
}

if(empty($_POST['uname']))
{
	error_response_json(
		"No username was entered.",
		1,
		__LINE__,
		null,
		null);
}

if(empty($_POST['pword']))
{
	error_response_json(
		"No password was entered.",
		1,
		__LINE__,
		null,
		null);
}

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session_token.php';
if (!verify_token($_POST['token']))
{
	error_response_json(
		"Please login before continuing.",
		1,
		__LINE__,
		null,
		null);
}

//Open DB connection
include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/db_vmsx.php';

// Protect from MySQL injection
$io_uname	= mysqli_real_escape_string($mysqli, stripslashes($_POST['uname']));
$io_pword	= mysqli_real_escape_string($mysqli, stripslashes($_POST['pword']));

if($io_uname == "")
{
	error_response_json(
		"No username was specified.",
		1,
		__LINE__,
		null,
		null);
}

if($io_pword == "")
{
	error_response_json(
		"No password was specified.",
		1,
		__LINE__,
		null,
		null);
}

// SQL query to fetch user info
$sqli_user_info = "
	SELECT
		users.*
	FROM
		vmsx_db.users
	WHERE
		(users.username=?)
	";
$stmt = $mysqli->prepare($sqli_user_info);
$stmt->bind_param("s",$io_uname);
$stmt->execute();
if(!($result = $stmt->get_result()))
{
	error_response_json(
		"Could not connect to database.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}
$num_rows = $result->num_rows;
if($num_rows === 0)
{
	error_response_json(
		"The user credentials could not be verified.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($num_rows != 1)
{
	error_response_json(
		"The user credentials could not be verified.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

while($row = $result->fetch_assoc())
{
	$db_uid			= intval($row['id']);
	$db_uname		= $row['username'];
	$db_name		= $row['name'];
	$db_initials	= $row['initials'];
	$db_surname		= $row['surname'];
	$db_email		= $row['email'];
	$db_hash		= $row['password'];
	$db_group		= $row['gid'];
	$db_status		= $row['status'];
	$db_enabled		= $row['enabled'];
	$db_salref		= $row['salref'];
	$db_created_on	= $row['created_on'];
}

if(!$db_hash) //no password set
{
	error_response_json(
		"Your password needs to be reset.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

//Config.
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/secure/gen_password.php';
//include_once 'secure/random_string.php';
$verified = vms_password_verify($io_pword, $db_hash);
if(!$verified)	//incorrect username password combination
{
	error_response_json(
		"Your credentials are incorrect.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if(!$db_enabled)	//User access has been disabled
{
	error_response_json(
		"Your account is disabled.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if(!$db_uid)	//User has no id number
{
	error_response_json(
		"Your account cannot be accessed.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}


//Obtain the user special levels
// SQL query to fetch user access levels
if(isset($stmt)){$stmt->close();};
$sql_user_levels = "
	SELECT
		users_access.level
	FROM
		vmsx_db.users_access
	WHERE
		(users_access.uid=?)
	";

$stmt = $mysqli->prepare($sql_user_levels);
$stmt->bind_param("i",$db_uid);
$stmt->execute();
if(!($result = $stmt->get_result()))
{
	error_response_json(
		"Could not connect to database.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

$num_rows = $result->num_rows;

while($row = $result->fetch_assoc())
{
	$db_levels[]	= intval($row['level']);
}

//Obtain the user special levels
// SQL query to fetch user access levels
if(isset($stmt)){$stmt->close();};
$sql_user_levels = "
	SELECT
		groups_access.level
	FROM
		vmsx_db.groups_access
	WHERE
		(groups_access.gid=?)
	";

$stmt = $mysqli->prepare($sql_user_levels);
$stmt->bind_param("i",$db_group);
$stmt->execute();
if(!($result = $stmt->get_result()))
{
	error_response_json(
		"Could not connect to database.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}


$num_rows = $num_rows + $result->num_rows;
if($num_rows === 0)
{
	error_response_json(
		"No access privileges are assigned to your account.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

while($row = $result->fetch_assoc())
{
	$db_levels[]	= intval($row['level']);
}

//Access granted: Configure session for user.
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/secure/session_setup.php';

//Close statment and db connections
$stmt->close();
$mysqli->close();

//create a JSON data structure.
$data_json = array (	
	'errors' 			=> $error,
	'error_msg'			=> $error_msg,
	'error_level'		=> $error_level,
	'error_code'		=> $error_code,
	'verified'			=> $verified,
	'levels'			=> $db_levels
);

//Respond to request.
echo json_encode($data_json, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
?>