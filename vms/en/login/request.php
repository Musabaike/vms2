<?php
//Start session
session_start();

// Prevent caching.
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 01 Jan 1996 00:00:00 GMT');
// The JSON standard MIME header.
header('Content-type: application/json');

//Default Data
$error			= false;
$error_msg		= null;
$error_level	= null;
$error_code		= 0;
$hash			= null; //Default, no password generated
$verified		= false;

$request		= null;
$io_uname		= null; //Default
$io_name		= null; //Default
$io_initials	= null; //Default
$io_surname		= null; //Default
$io_email		= null; //Default
$io_salref		= null; //Default
$io_group		= null; //Default
$io_pword		= null; //Default

//Access check
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
/*if(!grant_access([0]))
{
	error_response_json(
		"Access denied.",
		1,
		__LINE__,
		null,
		null);
}*/

// Empty data received in request.
if(empty($_POST['token']))
{
	error_response_json(
		null,
		1,
		__LINE__,
		null,
		null);
}

if(empty($_POST['request']))
{
	error_response_json(
		"Request data not available.",
		1,
		__LINE__,
		null,
		null);
}

if(empty($_POST['uname']))
{
	error_response_json(
		"No username was received.",
		1,
		__LINE__,
		null,
		null);
}

if(empty($_POST['fname']))
{
	error_response_json(
		"No first name was received.",
		1,
		__LINE__,
		null,
		null);
}

if(empty($_POST['lname']))
{
	error_response_json(
		"No surname was received.",
		1,
		__LINE__,
		null,
		null);
}

if(empty($_POST['email']))
{
	error_response_json(
		"No email address was received.",
		1,
		__LINE__,
		null,
		null);
}

if(empty($_POST['salref']))
{
	error_response_json(
		"No salary reference number was received.",
		1,
		__LINE__,
		null,
		null);
}

if(empty($_POST['group']))
{
	error_response_json(
		"No access type was received.",
		1,
		__LINE__,
		null,
		null);
}

if(empty($_POST['pword']))
{
	error_response_json(
		"No password was received.",
		1,
		__LINE__,
		null,
		null);
}


include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session_token.php';
if (!verify_token($_POST['token']))
{
	error_response_json(
		"Please login before continuing.",
		1,
		__LINE__,
		null,
		null);
}

//Open DB connection
include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/db_vmsx.php';

// Protect from MySQL injection
$io_req			= mysqli_real_escape_string($mysqli, stripslashes($_POST['request']));
$io_uname		= mysqli_real_escape_string($mysqli, stripslashes($_POST['uname']));
$io_initials	= mysqli_real_escape_string($mysqli, stripslashes($_POST['initials']));
$io_name		= mysqli_real_escape_string($mysqli, stripslashes($_POST['fname']));
$io_surname		= mysqli_real_escape_string($mysqli, stripslashes($_POST['lname']));
$io_email		= mysqli_real_escape_string($mysqli, stripslashes($_POST['email']));
$io_salref		= mysqli_real_escape_string($mysqli, stripslashes($_POST['salref']));
$io_group		= mysqli_real_escape_string($mysqli, stripslashes($_POST['group']));
$io_pword		= mysqli_real_escape_string($mysqli, stripslashes($_POST['pword']));


//Required fields check
if($io_req != "register")
{
	error_response_json(
		"Invalid request type.",
		1,
		__LINE__,
		null,
		null);
}

if($io_uname == "")
{
	error_response_json(
		"No username was specified.",
		1,
		__LINE__,
		null,
		null);
}

if($io_name == "")
{
	error_response_json(
		"No first name was specified.",
		1,
		__LINE__,
		null,
		null);
}

if($io_surname =="")
{
	error_response_json(
		"No surname was specified.",
		1,
		__LINE__,
		null,
		null);
}

if($io_email == "")
{
	error_response_json(
		"No email address was specified.",
		1,
		__LINE__,
		null,
		null);
}

if($io_salref == "")
{
	error_response_json(
		"No salary reference number was specified.",
		1,
		__LINE__,
		null,
		null);
}

if($io_group == "")
{
	error_response_json(
		"No access type was received.",
		1,
		__LINE__,
		null,
		null);
}

if($io_pword == "")
{
	error_response_json(
		"No password was specified.",
		1,
		__LINE__,
		null,
		null);
}

// SQL query: Username already taken?
$sql_uname_unique = "
	SELECT
		users.id
	FROM
		vmsx_db.users
	WHERE
		(users.username=?)
	UNION ALL
	SELECT
		users_req.id
	FROM
		vmsx_db.users_req
	WHERE
		(users_req.username=?)
	UNION ALL
	SELECT
		users_del.id
	FROM
		vmsx_db.users_del
	WHERE
		(users_del.username=?)
";

$stmt = $mysqli->prepare($sql_uname_unique);
$stmt->bind_param("sss",$io_uname, $io_uname, $io_uname);
$stmt->execute();
if(!($result = $stmt->get_result()))
{
	error_response_json(
		"Could not connect to database.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

$num_rows = $result->num_rows;
if($num_rows > 0)
{
	error_response_json(
		"The username is already taken.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

//Generate password for DB
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/secure/gen_password.php';
//include_once 'secure/random_string.php';
$db_pword = vms_password_enc($io_pword);


// SQL query: Add user to user_req table
$sql_uname_unique = "
	INSERT INTO
		vmsx_db.users_req
	(
		username,
		name,
		initials,
		surname,
		email,
		password,
		gid,
		salref
	)
	VALUES
	(
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?
	)
";

try
{
	$stmt = $mysqli->prepare($sql_uname_unique);
	$stmt->bind_param("ssssssis",$io_uname, $io_name, $io_initials, $io_surname, $io_email, $db_pword, $io_group, $io_salref);
	$stmt->execute();
}
catch (Exception $e)
{
	if ($mysqli->errno === 1062)
	{
		error_response_json(
		"The username is already taken.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
	}
	
	error_response_json(
		"Could not connect to database. $e = ".$mysqli->errno,
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

//Send emails to user and admin
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/email/email_req.php';
$emailed_support = email_user_req_support($io_uname, $io_name, $io_surname, $mysqli, $stmt);
$emailed_user = email_user_req($io_uname, $io_name, $io_surname, $io_email);
$submitted = true;

//Close statment and db connections
if(isset($stmt) && $stmt != null){$stmt->close();}
if(isset($mysqli) && $mysqli){$mysqli->close();}

//create a JSON data structure.
$data_json = array (	
	'errors' 			=> $error,
	'error_msg'			=> $error_msg,
	'error_level'		=> $error_level,
	'error_code'		=> $error_code,
	'submitted'			=> $submitted,
	'emailed_support'		=> $emailed_support,
	'emailed_user'			=> $emailed_user
);

//Respond to request.
echo json_encode($data_json, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
?>
