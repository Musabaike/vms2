<div class="row">
</div>

<form class="row">
	<div class="input-field col s12">
		<i class="material-icons prefix">domain</i>
		<input placeholder="ESP Agency name" id="io_new_dist_agency" type="text" data-length="10" maxlength="10" class="validate">
		<label for="io_new_dist_agency">Agency (ESP distributor name)</label>
	</div>
	<div class="input-field col s12">
		<i class="material-icons prefix">store</i>
		<input placeholder="SAP customer name" id="io_new_dist_cust_name" type="text" data-length="255" maxlength="255" class="validate">
		<label for="io_new_dist_cust_name">Customer Name (SAP distributor name)</label>
	</div>
	<div class="input-field col s12">
		<i class="material-icons prefix">fingerprint</i>
		<input placeholder="SAP customer number" id="io_new_dist_cust_no" type="text" data-length="18" maxlength="18" class="validate">
		<label for="io_new_dist_cust_no">Customer Number (SAP distributor number)</label>
	</div>
</form>
