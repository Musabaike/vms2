<div class="row">
</div>

<form class="row">
	<div class="input-field col s12">
		<i class="material-icons prefix">person_pin</i>
		<input placeholder="Contact Persons Name" id="io_new_dist_contact" type="text" data-length="50" maxlength="50" class="validate">
		<label for="io_new_dist_contact">Contact Person</label>
	</div>
	<div class="input-field col s12">
		<i class="material-icons prefix">contact_phone</i>
		<input placeholder="Telephone number" id="io_new_dist_tel" type="text" data-length="10" maxlength="10" class="validate">
		<label for="io_new_dist_tel">Telephone Number</label>
	</div>
	<div class="input-field col s12">
		<i class="material-icons prefix">smartphone</i>
		<input placeholder="Mobile number" id="io_new_dist_cel" type="text" data-length="10" maxlength="10" class="validate">
		<label for="io_new_dist_cel">Mobile Number</label>
	</div>
	<div class="input-field col s12">
		<i class="material-icons prefix">print</i>
		<input placeholder="Fax number" id="io_new_dist_fax" type="text" data-length="10" maxlength="10" class="validate">
		<label for="io_new_dist_fax">Fax Number</label>
	</div>
	<div class="input-field col s12">
		<i class="material-icons prefix">contact_mail</i>
		<input placeholder="Contacts Email Address" id="io_new_dist_email" type="text" data-length="50" maxlength="50" class="validate">
		<label for="io_new_dist_email">Email Address</label>
	</div>
	<div class="input-field col s12">
		<i class="material-icons prefix">my_location</i>
		<input placeholder="City/Town/etc." id="io_new_dist_location" type="text" data-length="50" maxlength="50" class="validate">
		<label for="io_new_dist_location">Location</label>
	</div>
	<div class="input-field col s12">
		<i class="material-icons prefix">room</i>
		<input placeholder="Distributor address" id="io_new_dist_address" type="text" data-length="250" maxlength="250" class="validate">
		<label for="io_new_dist_address">Address</label>
	</div>
</form>