<?php
//Start session
session_start();

// Prevent caching.
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 01 Jan 1996 00:00:00 GMT');
// The JSON standard MIME header.
header('Content-type: application/json');

//Default Data
$error			= false;
$error_msg		= null;
$error_level	= null;
$error_code		= 0;
$hash			= null; //Default, no password generated
$verified		= false;

$request		= null;
$io_agency		= null;
$io_decr		= null;

$data_dist		= null;

//Access check
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
if(!grant_access([3004]))
{
	error_response_json(
		"Access denied - Distributor creation is not allowed.",
		1,
		__LINE__,
		null,
		null);
}

// No required data received in request?
if(!isset($_POST['token']))
{
	error_response_json(
		"Please login before continuing.",
		1,
		__LINE__,
		null,
		null);
}

if(!isset($_POST['request']))
{
	error_response_json(
		"Request type not available.",
		1,
		__LINE__,
		null,
		null);
}

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session_token.php';
if (!verify_token($_POST['token']))
{
	error_response_json(
		"Please login before continuing.",
		1,
		__LINE__,
		null,
		null);
}

if(!isset($_POST['data']))
{
	error_response_json(
		"No request data was received.",
		1,
		__LINE__,
		null,
		null);
}

$data_rec = json_decode($_POST['data'], true);

//Open DB connection
include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/db_vmsx.php';

// Protect from MySQL injection
$io_req			= mysqli_real_escape_string($mysqli, stripslashes($_POST['request']));
$io_agency		= mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['agency']));
$io_cust_name	= mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['cust_name']));
$io_cust_no		= mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['cust_no']));
$io_location	= mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['location']));
$io_contact		= mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['contact']));
$io_tel			= mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['tel']));
$io_cel			= mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['cel']));
$io_fax			= mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['fax']));
$io_address		= mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['address']));
$io_transmit_dir= mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['transmit_dir']));
$io_receive_dir	= mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['receive_dir']));
$io_ftp_server	= mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['ftp_server']));
$io_ftp_user	= mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['ftp_user']));
$io_ftp_password= mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['ftp_password']));
$io_seqno		= mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['seqno']));
$io_batch_size	= mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['batch_size']));
$io_public_key	= mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['public_key']));
$io_email		= mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['email']));
$io_ftp_transmit_dir	= mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['ftp_transmit_dir']));
$io_ftp_receive_dir		= mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['ftp_receive_dir']));
$io_max_cutoff	= mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['max_cutoff']));
$io_starter_size= mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['starter_size']));


//Required fields check
if($io_req != "dist_add")
{
	error_response_json(
		"Invalid request type.",
		1,
		__LINE__,
		null,
		$mysqli);
}

if($io_agency == "")
{
	error_response_json(
		"No agency name was specified.",
		1,
		__LINE__,
		null,
		$mysqli);
}

if($io_cust_name == "")
{
	error_response_json(
		"No Customer Name was specified.",
		1,
		__LINE__,
		null,
		$mysqli);
}

if($io_cust_no == "")
{
	error_response_json(
		"No Customer Number was specified.",
		1,
		__LINE__,
		null,
		$mysqli);
}

if($io_batch_size == "")
{
	error_response_json(
		"No Batch Size was specified.",
		1,
		__LINE__,
		null,
		$mysqli);
}

// SQL query:
$sqli_query = "
	INSERT INTO
		vmsx_db.dists
	(
		agency,
		cust_name,
		cust_no,
		location,
		contact,
		tel,
		cel,
		fax,
		address,
		transmit_dir,
		receive_dir,
		ftp_server,
		ftp_user,
		ftp_password,
		seqno,
		batch_size,
		public_key,
		email,
		ftp_transmit_dir,
		ftp_receive_dir,
		max_cutoff,
		starter_size,
		created_by
	)
	VALUES
	(
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?
	)
";

try
{
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param("ssssssssssssssiissssiii",
		$io_agency,
		$io_cust_name,
		$io_cust_no,
		$io_location,
		$io_contact,
		$io_tel,
		$io_cel,
		$io_fax,
		$io_address,
		$io_transmit_dir,
		$io_receive_dir,
		$io_ftp_server,
		$io_ftp_user,
		$io_ftp_password,
		$io_seqno,
		$io_batch_size,
		$io_public_key,
		$io_email,
		$io_ftp_transmit_dir,
		$io_ftp_receive_dir,
		$io_max_cutoff,
		$io_starter_size,
		$_SESSION['uid']);
	$stmt->execute();
}
catch (Exception $e)
{
	if ($mysqli->errno === 1062)
	{
		error_response_json(
		"The distributor name already exists.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
	}
	
	error_response_json(
		"Could not connect to database.".$e->getMessage(),
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

// SQL query to fetch dists info
$sqli_query = "
		SELECT
			main.id,
			main.agency,
			main.cust_name,
			main.created_on,
			main.updated_on,
			extra.username AS updated_by_uname,
			extra1.username AS created_by_uname
		FROM
			vmsx_db.dists main
		LEFT JOIN
			vmsx_db.users extra
		ON
			main.updated_by=extra.id
		LEFT JOIN
			vmsx_db.users extra1
		ON
			main.created_by=extra1.id
		WHERE
			main.agency=?
	";
$stmt = $mysqli->prepare($sqli_query);
$stmt->bind_param("s",$io_agency);
$stmt->execute();
if(!($result = $stmt->get_result()))
{
	error_response_json(
		"Could not connect to the database.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}
$num_rows = $result->num_rows;
if($num_rows === 0)
{
	error_response_json(
		"The created distributor could not be retrieved.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

while($row = $result->fetch_assoc())
{
	$data_dist[]	= $row;
}


//Close statment and db connections
if(isset($stmt)){$stmt->close();};
if(isset($mysqli)){$mysqli->close();};

//create a JSON data structure.
$data_json = array (	
	'errors' 			=> $error,
	'error_msg'			=> $error_msg,
	'error_level'		=> $error_level,
	'error_code'		=> $error_code,
	'session_timeout'	=> $_SESSION['timeout'],
	'dist'				=> $data_dist
);

//Respond to request.
echo json_encode($data_json, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
?>