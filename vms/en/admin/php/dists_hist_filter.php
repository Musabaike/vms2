<?php
//Filter results
if (isset($_POST['filter']['columns']) && isset($_POST['filter']['keyword']))
{
	if($_POST['filter']['keyword'] != "" && $_POST['filter']['keyword'] != "null" && $_POST['filter']['keyword'] != null)
	{
		$temp_counter = 0;
		$temp_length = count($_POST['filter']['columns']);
		$temp_multi_conditions = false;
		$temp_keyword = $_POST['filter']['keyword'];
		$filter = "WHERE ( ";
		foreach($_POST['filter']['columns'] as $key => $value)
		{
			//error_log($_POST['filter']['columns'][$key],0);
			switch($_POST['filter']['columns'][$key])
			{
				case 0:
					$temp_col = "agency";
					break;
				case 1:
					$temp_col = "cust_name";
					break;
				case 2:
					$temp_col = "cust_no";
					break;
				case 3:
					$temp_col = "location";
					break;
				case 4:
					$temp_col = "contact";
					break;
				case 5:
					$temp_col = "tel";
					break;
				case 6:
					$temp_col = "fax";
					break;
				case 7:
					$temp_col = "ftp_transmit_dir";
					break;
				case 8:
					$temp_col = "ftp_receive_dir";
					break;
				case 9:
					$temp_col = "seqno";
					break;
				case 10:
					$temp_col = "batch_size";
					break;
				case 11:
					$temp_col = "email";
					break;		
				case 12:
					$temp_col = "updated_on";
					break;
				case 13:
					$temp_col = "updated_by_uname";
					break;
				case 14:
					$temp_col = "ftp_server";
					break;
				case 15:
					$temp_col = "ftp_user";
					break;
				case 16:
					$temp_col = "action";
					break;
				case 17:
					$temp_col = "active";
					break;
				default:
					$temp_col = "";
					$temp_counter++;
			}
			
			if($temp_col != "")
			{
				if($temp_multi_conditions)
				{
					$filter = $filter."OR $temp_col LIKE '%$temp_keyword%' ";
				}
				else
				{
					$filter = $filter."$temp_col LIKE '%$temp_keyword%' ";
					$temp_multi_conditions = true;
				}
			}
		}
		$filter = $filter." AND active='1')";
		if ($temp_counter >= $temp_length)
		{	//No columns were selected
			$filter = "WHERE active='1'";
		}
	}
}
//error_log($filter,0);
//error_log("^^^^^^^^^^^ ".$filter, 0);
?>
