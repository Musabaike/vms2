<?php
//Start session
session_start();

// Prevent caching.
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 01 Jan 1996 00:00:00 GMT');
// The JSON standard MIME header.
header('Content-type: application/json');

//Default Data
$error			= false;
$error_msg		= null;
$error_level	= null;
$error_code		= 0;

$io_request		= null;
$io_did			= null;

$data_dist		= null;

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
if(!grant_access([3006]))
{
	error_response_json(
		"Access denied - Distributor deletion is not allowed.",
		1,
		__LINE__,
		null,
		null);
}

// Empty data received in request.
if(!isset($_POST['token']))
{
	error_response_json(
		"Please login before continuing.",
		1,
		__LINE__,
		null,
		null);
}

// Data received in request.
if(!isset($_POST['request']))
{
	error_response_json(
		"Request data not available.",
		1,
		__LINE__,
		null,
		null);
}

if(!isset($_POST['did']))
{
	error_response_json(
		"Request data not available.",
		1,
		__LINE__,
		null,
		null);
}

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session_token.php';
if (!verify_token($_POST['token']))
{
	error_response_json(
		"Please login again before continuing.",
		1,
		__LINE__,
		null,
		null);
}

//Open DB connection
include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/db_vmsx.php';

// Protect from MySQL injection
$io_request	= mysqli_real_escape_string($mysqli, stripslashes($_POST['request']));
$io_did	= mysqli_real_escape_string($mysqli, stripslashes($_POST['did']));

if($io_request == "")
{
	error_response_json(
		"Invalid request.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($io_request != "a_dist_remove")
{
	error_response_json(
		"Invalid request.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($io_did == "")
{
	error_response_json(
		"Distributor selection undefined.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

//Step 1: Copy the table row from users to users_del.
//Step 2: If successful, delete the entry from the users table.
//Step 3: respond to request with actions taken.


// SQL query to copy user info
/*$sqli_dist_info = "
	INSERT INTO
		vmsx_db.dists_del
	SELECT
		dists.*
	FROM
		vmsx_db.dists
	WHERE
		dists.id=?";
$stmt = $mysqli->prepare($sqli_dist_info);
$stmt->bind_param("i", $io_did);

if(!($stmt->execute()))
{
	error_response_json(
		"Could not delete the distributor.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

$sqli_dist_info = "
	UPDATE
		vmsx_db.dists_del
	SET
		dists_del.deleted_by=?,
		dists_del.deleted_on=NOW()
	WHERE
		dists_del.id=?;
	";
$stmt = $mysqli->prepare($sqli_dist_info);
$stmt->bind_param("ii", $_SESSION['uid'], $io_did);

if(!($stmt->execute()))
{
	error_response_json(
		"Could not delete the distributor.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}


// SQL query to copy user info
$sqli_dist_info = "
	DELETE FROM
		vmsx_db.dists
	WHERE
		dists.id=?
	";
$stmt = $mysqli->prepare($sqli_dist_info);
$stmt->bind_param("i", $io_did);

if(!($stmt->execute()))
{
	error_response_json(
		"Could not delete the distributor.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}*/

if($io_did)
{
	// SQL update query
	$sqli_query = "
		UPDATE
			vmsx_db.dists 
		SET 
			active = 0,
			deleted_by = ?,
			deleted_on = NOW()
		WHERE
			dists.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param( "is", $_SESSION['uid'], $io_did);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not delete the distributor.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
}

$data_dist = "The distributor was deleted.";

//Close statment and db connections
if(isset($stmt) && $stmt != null){$stmt->close();}
if(isset($mysqli) && $mysqli){$mysqli->close();}

//create a JSON data structure.
$data = array (	
	'errors' 			=> $error,
	'error_msg'			=> $error_msg,
	'error_level'		=> $error_level,
	'error_code'		=> $error_code,
	'session_timeout'	=> $_SESSION['timeout'],
	'dist'				=> $data_dist
);

//Respond to request.
echo json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
?>