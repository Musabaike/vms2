$(document).ready(function(){

	//Orders page
	$('#dash_orders').click(function(){
		event.preventDefault();
		window.location = "/en/orders/";
	});
	
	//Admin page
	$('#dash_admin').click(function(){
		event.preventDefault();
		window.location = "/en/admin/";
	});
	
	//User page
	$('#dash_users').click(function(){
		event.preventDefault();
		window.location = "/en/users/";
	});
	
	//Settings page
	$('#dash_settings').click(function(){
		event.preventDefault();
		window.location = "/en/settings/";
	});
	//History page
	$('#dash_history').click(function(){
		console.log("Clicked");
		event.preventDefault();
		window.location = "/en/history/";
	});
	// Disable automatic style injection
	Chart.platform.disableCSSInjection = true;

	orders_counts_get($("body").attr('data-token'), 30);
	cards_counts_get($("body").attr('data-token'), 30);
});


function orderHist(labels_array, data_array, color_array)
{	//Display a graph of the order count for the past 31 days
	var ctx = document.getElementById('graph_orders').getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'line',
		data: {
			labels: labels_array,
			datasets: [{
				label: '# of Orders',
				data: data_array,
				backgroundColor: color_array,
				borderWidth: 1
			}]
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero: true
					}
				}]
			}
		}
	});
	//myChart.canvas.parentNode.style.height = '200px';
}

function cardHist(labels_array, dates_array, color_array)
{	//Display a graph of the card count for the past 31 days
	var ctx = document.getElementById('graph_cards').getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'line',
		data: {
			labels: labels_array,
			datasets: [{
				label: '# of Cards',
				data: dates_array,
				backgroundColor: color_array,
				borderWidth: 1
			}]
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero: true
					}
				}]
			}
		}
	});
	//myChart.canvas.parentNode.style.height = '200px';
}

function orders_counts_get(token, days)
{	//Retrieve cards counts for all orders for last x days

	//Retrieve new data
	$.post('/en/home/php/orders_counts.php',
		{
			request: 'all_orders_counts',
			token: token,
			days: days
		},
		function(json)
		{
			
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				if(json['error_msg']!="Access denied."){M.toast({html: toastHTML});}
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			var orders_counts;
			var orders_dates;
			if (json["orders_counts"]!=null)
			{
				orders_counts = json["orders_counts"].map(function(obj){
					var key = Object.keys(obj).sort()[0], rtn = {};    
					//return rtn[key] = obj[key], rtn;
					return obj[key];
				});
				orders_dates = json["orders_counts"].map(function(obj){
					var key = Object.keys(obj).sort()[1], rtn = {};    
					//return rtn[key] = obj[key], rtn;
					return obj[key];
				});
			}
			else
			{
				orders_counts = [];
				orders_dates = [];
			}
			
			let graph_dates = [];
			let graph_count = [];
			let graph_color = [];
			
			for (var i = 30; i >= 0; --i)
			{	//Create and populate the Date and color arrays
				var today = new Date(Date.now() - (i*86400000));
				var date = today.getFullYear()+'-'+(today.getMonth()+1).toString().padStart(2, 0)+'-'+today.getDate().toString().padStart(2, 0);
				//(i == 0) ? graph_dates.push("Today") : graph_dates.push(date);
				graph_dates.push(date);
				(i==0) ? graph_color.push('rgba(218,27,96,0.7)'): graph_color.push('rgba(0,91,155,0.7)');
			}
			
			
			
			if(orders_counts.length==orders_dates.length)
			{	//Insert the received count values into a new array with 0 counts included for dates not in the received array of counts.
				let orders_dates_len = orders_dates.length;
				let orders_dates_index = 0;//cards_dates_len-1;
				
				for (var i = 0; i <= 30; i++)
				{
					
					//graph_count.push();
					if (orders_dates_index<orders_dates_len)
					{
						//console.log(orders_dates[i]);
						//console.log(orders_dates[orders_dates_index]);
						
						//console.log(orders_counts[orders_dates_index]);
						//console.log('index: '+i);
						if(graph_dates[i]==orders_dates[orders_dates_index])
						{
							graph_count.push(orders_counts[orders_dates_index]);
							orders_dates_index++;
						}
						else if(graph_dates[i]>orders_dates[orders_dates_index])
						{
							//graph_count.push(0);
							while((graph_dates[i]>orders_dates[orders_dates_index]) && (orders_dates_index<orders_dates_len))
							{
								orders_dates_index++;
							}
							if(orders_dates_index<orders_dates_len)
							{
								if(graph_dates[i]==orders_dates[orders_dates_index])
								{
									graph_count.push(orders_counts[orders_dates_index]);
									orders_dates_index++;
								}
							}
							else
							{
								graph_count.push(0);
							}
						}
						else
						{
							graph_count.push(0);
						}
					}
					else
					{
						graph_count.push(0);
					}
				}
				graph_dates[30] = "Today";
			}

			//console.log(orders_counts);
			//console.log(orders_dates);
			//console.log(graph_dates);
			//console.log(graph_count);
			//console.log(graph_color);
			
			orderHist(graph_dates, graph_count, graph_color);
		},
		'json'
	).done(function()
	{
		
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	
	return null;
}

function cards_counts_get(token, days)
{	//Retrieve cards counts for all orders for last x days

	//Retrieve new data
	$.post('/en/home/php/cards_counts.php',
		{
			request: 'all_cards_counts',
			token: token,
			days: days
		},
	//console.log('request: all_cards_counts,	token: '+token+',days: '+days);
		function(json)
		{
			
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				if(json['error_msg']!="Access denied."){M.toast({html: toastHTML});}
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			var cards_counts;
			var cards_dates;
			if (json["cards_counts"]!=null)
			{
				cards_counts = json["cards_counts"].map(function(obj){
					var key = Object.keys(obj).sort()[0], rtn = {};    
					//return rtn[key] = obj[key], rtn;
					return obj[key];
				});
				
				cards_dates = json["cards_counts"].map(function(obj){
					var key = Object.keys(obj).sort()[1], rtn = {};    
					//return rtn[key] = obj[key], rtn;
					return obj[key];
				});
			}
			else
			{
				cards_counts = [];
				cards_dates = [];
			}
			//Create a new array 
			/*var cards_counts = json["cards_counts"].map(function(obj){
				var key = Object.keys(obj).sort()[0], rtn = {};    
				//return rtn[key] = obj[key], rtn;
				return obj[key];
			});
			
			var cards_dates = json["cards_counts"].map(function(obj){
				var key = Object.keys(obj).sort()[1], rtn = {};    
				//return rtn[key] = obj[key], rtn;
				return obj[key];
			});*/
			
			
			let graph_dates = [];
			let graph_count = [];
			let graph_color = [];

			for (var i = 30; i >= 0; --i) {
				var today = new Date(Date.now() - (i*86400000));
				var date = today.getFullYear()+'-'+(today.getMonth()+1).toString().padStart(2, 0)+'-'+today.getDate().toString().padStart(2, 0);
				//(i == 0) ? graph_dates.push("Today") : graph_dates.push(date);
				graph_dates.push(date);
				(i==0) ? graph_color.push('rgba(218,27,96,0.7)'): graph_color.push('rgba(0,91,155,0.7)');
			}
			
			
			//Insert the received count values into a new array with 0 counts included for dates not in the received array of counts.
			if(cards_counts.length==cards_dates.length)
			{
				let cards_dates_len = cards_dates.length;
				let cards_dates_index = 0;//cards_dates_len-1;
				
				for (var i = 0; i <= 30; i++)
				{
					//console.log(cards_dates[i]);
					//console.log(cards_dates[cards_dates_index]);
					//graph_count.push();
					if (cards_dates_index<cards_dates_len)
					{
						if(graph_dates[i]==cards_dates[cards_dates_index])
						{
							graph_count.push(cards_counts[cards_dates_index]);
							cards_dates_index++;
						}
						else if(graph_dates[i]>cards_dates[cards_dates_index])
						{
							//graph_count.push(0);
							while((graph_dates[i]>cards_dates[cards_dates_index]) && (cards_dates_index<cards_dates_len))
							{
								cards_dates_index++;
							}
							if(cards_dates_index<cards_dates_len)
							{
								if(graph_dates[i]==cards_dates[cards_dates_index])
								{
									graph_count.push(cards_counts[cards_dates_index]);
									cards_dates_index++;
								}
							}
							else
							{
								graph_count.push(0);
							}
						}
						else
						{
							graph_count.push(0);
						}
					}
					else
					{
						graph_count.push(0);
					}
				}
				graph_dates[30] = "Today";
			}
			
			//console.log(cards_counts);
			//console.log(cards_dates);
			//console.log(graph_dates);
			//console.log(graph_count);
			//console.log(graph_color);
			
			cardHist(graph_dates, graph_count, graph_color);
			//cardHist(json["cards_dates"], json["cards_counts"], json["cards_colors"]);
		},
		'json'
	).done(function()
	{
		
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	
	return null;
}
