<?php
	include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session.php';
	include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
	include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
	if(!grant_access([1000]))
	{
		header('Location: /en/denied/');
		exit();
	}
	
	$page_title = "Dashboard";
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $website_name ; if ($page_title <> ""){echo " - ".$page_title;}?></title>
		<?php
			include_once $_SERVER['DOCUMENT_ROOT'].'/configs/ui/css.php';
		?>
		<link href="/vendors/ChartJS/css/Chart.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
		
		<!--Let browser know website is optimized for mobile-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	</head>

	<body data-token="<?php echo create_token();?>">
		<header class="page-header">
			<?php
				include_once $_SERVER['DOCUMENT_ROOT'].'/configs/ui/nav.php';
			?>
		</header>

		<main class="page-main">
			<div class="container">
				<div class="row">
				</div>
				<div class="row">
					<div class="col s6 m4 l3 xl2 offset-xl2">
						<div id="dash_orders" class="card-panel telkom-blue darken-4 center vms-hand <?php if(!grant_access([1001])){echo "hide";}?>">
							<span class="white-text">
								<i class="material-icons large">date_range</i>
							</span>
							<p class="white-text">Orders</p>
						</div>
					</div>
					<div class="col s6 m4 l3 xl2">
						<div id="dash_admin" class="card-panel telkom-blue center vms-hand <?php if(!grant_access([1002])){echo "hide";}?>">
							<span class="white-text">
								<i class="material-icons large">build</i>
							</span>
							<p class="white-text">Administration</p>
						</div>
					</div>
					<div class="col s6 m4 l3 xl2">
						<div id="dash_users" class="card-panel telkom-red lighten-4 center vms-hand <?php if(!grant_access([1003])){echo "hide";}?>">
							<span class="white-text">
								<i class="material-icons large">group</i>
							</span>
							<p class="white-text">Users</p>
						</div>
					</div>
					<div class="col s6 m4 l3 xl2">
						<div id="dash_settings" class="card-panel telkom-red center vms-hand <?php if(!grant_access([1004])){echo "hide";}?>">
							<span class="white-text">
								<i class="material-icons large">settings</i>
							</span>
							<p class="white-text">Settings</p>
						</div>
					</div>
					<div class="col s6 m4 l3 xl2">
						<div id="dash_history" class="card-panel telkom-red lighten-6 center vms-hand <?php if(!grant_access([1004])){echo "hide";}?>">
							<span class="white-text">
								<i class="material-icons large">history</i>
							</span>
							<p class="white-text">History</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col s12">
						<div id="dash_graph_orders" class="card-panel center vms-hand <?php if(!grant_access([1005])){echo "hide";}?>">
							<canvas id="graph_orders" width="500" height="100"></canvas>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col s12">
						<div id="dash_graph_cards" class="card-panel center vms-hand <?php if(!grant_access([1006])){echo "hide";}?>">
							<canvas id="graph_cards" width="500" height="100"></canvas>
						</div>
					</div>
				</div>
			</div>
		</main>

		<?php
			//Footer:
			include_once $_SERVER['DOCUMENT_ROOT'].'/configs/ui/footer.php';
			//Modals:
			//include_once $_SERVER['DOCUMENT_ROOT'].'/php/mod_login.php';
			//Javascripts:
			include_once $_SERVER['DOCUMENT_ROOT'].'/configs/ui/js.php';
		?>
		<script src="/vendors/ChartJS/js/Chart.min.js"></script>
	</body>
</html>
