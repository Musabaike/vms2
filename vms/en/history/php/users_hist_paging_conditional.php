<?php
//Defaults, otherwise set in JS call
$page_no		= 1;
$page_limit		= 10;

//Get the requested page number and number of rows
// Protect from MySQL injection
$io_page_no	= mysqli_real_escape_string($mysqli, stripslashes($_POST['page_no']));
if($io_page_no == "")
{
	error_response_json(
		"System paging error.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}
else
{
	$page_no = intval($io_page_no);
}

// Protect from MySQL injection
$io_page_limit	= mysqli_real_escape_string($mysqli, stripslashes($_POST['page_limit']));
if($io_page_limit == "")
{
	error_response_json(
		"System paging error.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}
else
{
	$page_limit = intval($io_page_limit);
}

//Calculations
$page_offset	= ($page_no-1) * $page_limit;
$page_result	= null; //mysqli_query($mysqli,$page_total_sql);
$stmt = $mysqli->prepare($page_total_sql);
//$stmt->bind_param( "i", $io_oid);
$stmt->execute();
if(!($page_result = $stmt->get_result()))
{
	error_response_json(
		"Could not connect to the database.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}
$temp_row = $page_result->fetch_assoc();
$page_total_rows= $temp_row["count"];

$page_total		= ceil($page_total_rows / $page_limit);


//error_log('Total Rows = '.$page_total_rows.' Page Total = '.$page_total,0);
?>
