<?php
	include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session.php';
	include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
	include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
	if(!grant_access([5000]))
	{
		header('Location: /en/denied/');
		exit();
	}
	
	$page_title = "Settings";
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $website_name ; if ($page_title <> ""){echo " - ".$page_title;}?></title>
		<?php
			include_once $_SERVER['DOCUMENT_ROOT'].'/configs/ui/css.php';
		?>
		
		<!--Let browser know website is optimized for mobile-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	</head>

	<body>
		<header class="page-header">
			<?php
				include_once $_SERVER['DOCUMENT_ROOT'].'/configs/ui/nav.php';
			?>
		</header>
		
		<main class="page-main">
			<div class="container">
				<div class="row">
					<p>SFTP server settings, maybe per distributor is better as it helps with migration to a new sftp server per distributor instead of globally.</p>
					<p>SFG works with mailboxes, ESP account to connect to SFG using SFTP, directories to separate Distributors. SFG will take files and place them in the customers mailbox for collection.</p>
					<p>Card number ranges</p>
					<p>Distributor encoding method</p>
					<p>SMTP settings</p>
				</div>
				<div class="row">
				<div class="row">
					    	<form enctype="multipart/form-data" action="" name="form" method="post">
    							Select File
    							<input type="file" name="file" id="file" /></td>
    							<input type="submit" name="submit" id="submit" value="Submit" />
    						</form>
				</div>
					
				</div>
			</div>
		</main>

		<?php
			//Footer:
			include_once $_SERVER['DOCUMENT_ROOT'].'/configs/ui/footer.php';
			//Modals:
			//include_once $_SERVER['DOCUMENT_ROOT'].'/php/mod_login.php';
			//Javascripts:
			include_once $_SERVER['DOCUMENT_ROOT'].'/configs/ui/js.php';
		?>
	</body>
</html>
