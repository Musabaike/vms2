<?php
	session_start();	//Start session
	include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
	include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
	include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/common/header.php';
	$page_title = "Denied";
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $website_name ; if ($page_title <> ""){echo " - ".$page_title;}?></title>
		<?php
			include_once $_SERVER['DOCUMENT_ROOT'].'/configs/ui/css.php';
		?>
		
		<!--Let browser know website is optimized for mobile-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	</head>

	<body>
		<header class="page-header">
			<?php
				include_once $_SERVER['DOCUMENT_ROOT'].'/configs/ui/nav.php';
			?>
		</header>
		
		<main class="page-main">
			<div class="section no-pad-bot" id="index-banner">
				<div class="container">
					<div class="row">
						<div class="col s12">
							
						</div>
					</div>
					<div class="row">
						<div class="col s12 m2 l3">
						</div>
						<form class="col s12 m8 l6">
							<div class="card">
								<div class="card-image white-text">
									<img src="images/worldcall.png">
									<span class="card-title jw-text-shadow">Access was denied</span>
								</div>
								<div class="card-content">
									<div class="valign-wrapper">
										<h6>WorldCall Prepaid Voucher Management System</h6>
									</div>
									<br />
									<h4 center>You do not have the required account privileges to view the requested page.</h4>
									<br />
									<a href="/" class="waves-effect waves-light btn">Return to dashboard...</a>
								</div>
								<div class="card-action valign-wrapper">
									
								</div>
							</div>
						</form>
						<div class="col s12 m2 l3">
						</div>
					</div>
					<div class="row">
						<div class="col s12">
						</div>
					</div>
				</div>
			</div>
		</main>

		<?php
			//Footer:
			include_once $_SERVER['DOCUMENT_ROOT'].'/configs/ui/footer.php';
			//Modals:
			include_once $_SERVER['DOCUMENT_ROOT'].'/php/modal_request.php';
			//Javascripts:
			include_once $_SERVER['DOCUMENT_ROOT'].'/configs/ui/js.php';
		?>
	</body>
</html>