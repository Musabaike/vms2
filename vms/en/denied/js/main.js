var mod_request;
var mod_reset;
var order_status_filter;
var elems;
var options;

$(document).ready(function(){
	//Modals
	mod_request = M.Modal.init(document.querySelector('#modal_request'), {});
	mod_reset = M.Modal.init(document.querySelector('#modal_reset'), {});
	
	M.updateTextFields();
	//Select dropdown
	elems = document.querySelectorAll('select');
	options = {};
    order_status_filter = M.FormSelect.init(elems, options);
	
	//Request Button
	
	//Login Button
	$('#btn_login').click(function(){
		event.preventDefault();
		verify_user($(this).attr('data-token'));
	});
	
	//Register Button
	$('#btn_reg').click(function(){
		event.preventDefault();
		request_user($(this).attr('data-token'));
	});
	
	//Reset Button
	$('#btn_reset').click(function(){
		event.preventDefault();
		mod_reset.close();
	});
});


//Functions
function verify_user(token){
	var uname = $('#username').val();
	var pword = $('#password').val();
	
	$.post('/en/login/verify.php',
		{
			request: 'verify',
			token: token,
			uname: uname,
			pword: pword
		},
		function(json)
		{
			if(json['error']){
				var toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				return null;
			}
			
			//Successful response
			window.location = "/en/home/";
		},
		'json'
	).done(function()
	{
		
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	
	return null;
}

function request_user(token){
	var uname	= $('#io_req_uname').val();
	var fname	= $('#io_req_fname').val();
	var initials = $('#io_req_initials').val();
	var lname	= $('#io_req_lname').val();
	var email	= $('#io_req_email').val();
	var salref	= $('#io_req_salref').val();
	var group	= $('#io_req_group').val();
	var pword	= $('#io_req_password').val();
	var pass1	= $('#io_req_password1').val();
	
	if (uname=="")
	{
		M.toast({html: "<span>Please enter a Username.</span>"});
		var elmnt = document.getElementById("io_req_uname");
		elmnt.scrollIntoView();
		$("#io_req_uname").removeClass('valid').focus();
		return null;
	}
	
	if (fname=="")
	{
		M.toast({html: "<span>Please enter a First Name.</span>"});
		var elmnt = document.getElementById("io_req_fname");
		elmnt.scrollIntoView();
		$("#io_req_fname").removeClass('valid').focus();
		return null;
	}
	
	if (lname=="")
	{
		M.toast({html: "<span>Please enter a Surname.</span>"});
		var elmnt = document.getElementById("io_req_lname");
		elmnt.scrollIntoView();
		$("#io_req_lname").removeClass('valid').focus();
		return null;
	}
	
	if (email=="")
	{
		M.toast({html: "<span>Please enter an email address.</span>"});
		var elmnt = document.getElementById("io_req_email");
		elmnt.scrollIntoView();
		$("#io_req_email").removeClass('valid').focus();
		return null;
	}
	
	if (salref=="")
	{
		M.toast({html: "<span>Please enter a Salary Reference Number.</span>"});
		var elmnt = document.getElementById("io_req_salref");
		elmnt.scrollIntoView();
		$("#io_req_salref").removeClass('valid').focus();
		return null;
	}
	
	if (pword=="")
	{
		M.toast({html: "<span>Please enter a Password.</span>"});
		var elmnt = document.getElementById("io_req_password");
		elmnt.scrollIntoView();
		$("#io_req_password").removeClass('valid').focus();
		return null;
	}
	
	if (pass1=="")
	{
		M.toast({html: "<span>Please confirm your password.</span>"});
		var elmnt = document.getElementById("io_req_password1");
		elmnt.scrollIntoView();
		$("#io_req_password1").removeClass('valid').focus();
		return null;
	}
	
	if (pass1!=pword)
	{
		M.toast({html: "<span>The passwords do not match.</span>"});
		var elmnt = document.getElementById("io_req_password");
		elmnt.scrollIntoView();
		$("#io_req_password").removeClass('valid').focus();
		return null;
	}
	
	$.post('/en/login/request.php',
		{
			request: 'register',
			token: token,
			uname: uname,
			fname: fname,
			initials: initials,
			lname: lname,
			email: email,
			salref: salref,
			group: group,
			pword: pword
		},
		function(json)
		{
			if(json['error']){
				var toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				return null;
			}
			
			//Successful response
			mod_request.close();
			var toastHTML = '<span>Your request has been submitted.</span>';
			M.toast({html: toastHTML});
		},
		'json'
	).done(function()
	{
		
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	return null;
}