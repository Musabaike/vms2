<?php
	$to = "wilmanj@telkom.co.za";
	$subject = "VMS Test email";

	$message = "
	<html>
	<head>
	<title>HTML email</title>
	</head>
	<body>
	<p>This is a test email</p>
	<table>
	<tr>
	<th>Firstname</th>
	<th>Lastname</th>
	</tr>
	<tr>
	<td>John</td>
	<td>Doe</td>
	</tr>
	</table>
	</body>
	</html>
	";

	// Always set content-type when sending HTML email
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

	// More headers
	$headers .= 'From: vms@telkom.co.za' . "\r\n";
	$headers .= 'Cc: tomp@openserve.co.za' . "\r\n";

	if(mail($to,$subject,$message,$headers))
	{
		echo "Email Accepted by SMTP server";
	} else {
		echo "Email Not Accepted by SMTP server";
	}
?>
