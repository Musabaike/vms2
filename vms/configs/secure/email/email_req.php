<?php
function email_user_req_support($temp_username, $temp_name, $temp_surname, $mysqli, $stmt)
{	//This function informs all users marked as support notify, via email, of a new user request
	$to = "";
	$subject = "Telkom VMS - New User Request - $temp_username";
	
	if($temp_username == "")	{ return false; }
	if($temp_name == "")		{ return false; }
	if($temp_surname == "")		{ return false; }

	//Get list of support email addresses to notify
	$sqli_users_support = "
		SELECT
				email
		FROM
				vmsx_db.users
		WHERE
				notify_support=1
        ";
	$stmt = $mysqli->prepare($sqli_users_support);
	$stmt->execute();
	if(!($result = $stmt->get_result()))
	{
		return false;
	}

	while($row = $result->fetch_assoc())
	{
			$to.=$row['email'].', ';
	}

	if($to == "")				{ return false; }
	
	$message = "
<html>
	<head>
		<title>Telkom VMS - New User Request - $temp_username</title>
	</head>
	<body>
		<p>Good day,</p>
		<br />
		<p>A new user has requested access to the Telkom VMS website.</p>
		<br />
		<table>
			<tr>
				<th>Username</th>
				<th>Firstname</th>
				<th>Lastname</th>
			</tr>
			<tr>
				<td>$temp_username</td>
				<td>$temp_name</td>
				<td>$temp_surname</td>
			</tr>
		</table>
		<p>Kind regards,</p>
		<p>Telkom VMS (<a href=\"http:\\\\vms.telkom.co.za\\\">Telkom VMS</a>)</p>
		<br>
		<p>Note: This is an automated email. Do NOT reply to this email as it is an unattended email address.</p>
	</body>
</html>
	";
	
	// Always set content-type when sending HTML email
	$headers[] = "MIME-Version: 1.0";
	$headers[] .= "Content-type:text/html;charset=UTF-8";

	// More headers
	$headers[] .= 'From: Telkom VMS <vms@telkom.co.za>';

	return mail($to, $subject, $message, implode("\r\n", $headers));
}

function email_user_req($temp_username, $temp_name, $temp_surname, $temp_email)
{	//This function informs the requesting user, via email, of a new user request submission
	$to = $temp_email;
	
	if($to == "")				{ return false; }
	if($temp_username == "")	{ return false; }
	if($temp_name == "")		{ return false; }
	if($temp_surname == "")		{ return false; }
	
	$subject = "Telkom VMS - New User Request - $temp_username";
	
	$message = "
<html>
	<head>
		<title>Telkom VMS - New User Request - $temp_username</title>
	</head>
	<body>
		<p>Hi $temp_name $temp_surname,</p>
		<br />
		<p>Your account creation request has been submit for approval.</p>
		<p>On Approval, you will receive an email indicating this.</p>
		<p>Once Enabled, you will receive an email indicating this.</p>
		<p>Only once the account has been enabled, after approval, will you be able to login.</p>
		<br />
		<p>The following details were submitted:</p>
		<br />
		<table>
			<tr>
				<th>Username</th>
				<th>Firstname</th>
				<th>Lastname</th>
			</tr>
			<tr>
				<td>$temp_username</td>
				<td>$temp_name</td>
				<td>$temp_surname</td>
			</tr>
		</table>
		<p>Kind regards,</p>
		<p>Telkom VMS (<a href=\"http:\\\\vms.telkom.co.za\\\">Telkom VMS</a>)</p>
		<br>
		<p>Note: This is an automated email. Do NOT reply to this email as it is an unattended email address.</p>
	</body>
</html>
	";
	
	// Always set content-type when sending HTML email
	$headers[] = "MIME-Version: 1.0";
	$headers[] = "Content-type:text/html;charset=UTF-8";

	// More headers
	$headers[] = 'From: Telkom VMS <vms@telkom.co.za>';

	return mail($to, $subject, $message, implode("\r\n", $headers));
}

function email_user_approve($temp_username, $temp_name, $temp_surname, $temp_email)
{	//This function informs the requesting user, via email, of a new user request submission
	$to = $temp_email;
	
	if($to == "")				{ return false; }
	if($temp_username == "")	{ return false; }
	if($temp_name == "")		{ return false; }
	if($temp_surname == "")		{ return false; }
	
	$subject = "Telkom VMS - New User Approved - $temp_username";
	
	$message = "
<html>
	<head>
		<title>Telkom VMS - New User Approved - $temp_username</title>
	</head>
	<body>
		<p>Hi $temp_name $temp_surname,</p>
		<br />
		<p>Your account creation request has been Approved.</p>
		<p>Your Account still needs to be Enabled, you will receive an email indicating when this is done.</p>
		<p>Only once the account has been enabled will you be able to login.</p>
		<br />
		<p>The following details were submitted:</p>
		<br />
		
		<p>Kind regards,</p>
		<p>Telkom VMS (<a href=\"http:\\\\vms.telkom.co.za\\\">Telkom VMS</a>)</p>
		<br>
		<p>Note: This is an automated email. Do NOT reply to this email as it is an unattended email address.</p>
	</body>
</html>
	";
	
	// Always set content-type when sending HTML email
	$headers[] = "MIME-Version: 1.0";
	$headers[] = "Content-type:text/html;charset=UTF-8";

	// More headers
	$headers[] = 'From: Telkom VMS <vms@telkom.co.za>';

	return mail($to, $subject, $message, implode("\r\n", $headers));
}
?>