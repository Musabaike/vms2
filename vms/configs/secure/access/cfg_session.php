<?php
session_start();	//Start session
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/common/header.php';	//Website default values

//Check if session was started already and whether or not the user has successfully logged into their account
if (isset($_SESSION['username'])){
	if(empty($_SESSION['username']) /*|| empty($_SESSION['level'])*/){
		header('Location: /'); //Redirect to the login page
		exit();	//Stop any further code being executed on the page calling this
	}
	//Currently Logged in
	//Timer for session expiry
	include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session_timer.php';
} else {
	header('Location: /'); //Redirect to the login page
	exit();	//Stop any further code being executed on the page calling this
}
?>