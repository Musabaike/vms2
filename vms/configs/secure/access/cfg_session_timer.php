<?php
$now = time();

if (isset($_SESSION['discard_after']))
{
	if($now > $_SESSION['discard_after'])
	{ //Session has timed out.
		//Destroy session:
		include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/secure/session_destroy.php';
		
		//Cannot redirect using header for POST requests!!!
		//Inform the browser via JS.
		$_SESSION['timeout'] = true;
	}
}

//Live at most for an additional from now:
$session_duration = 1800;	//1800=30min
$_SESSION['discard_after'] = $now + $session_duration;
?>