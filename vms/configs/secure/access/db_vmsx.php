<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
$mysqli	=	null;

//Info
$mysql_hostname = "localhost";
$mysql_user		= "worldcall";
$mysql_password = 'VMS_p4$$w0rd2020';
$mysql_database = "worldcall";

mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

try
{
	//Create Connection
	$mysqli = new mysqli(
		$mysql_hostname,
		$mysql_user,
		$mysql_password,
		$mysql_database
	);
	$mysqli->set_charset("utf8mb4");
}
catch (Exception $e)
{
	error_log($e->getMessage());
	error_response_json(
		"Could not connect to the database.",
		1,
		__LINE__,
		null,
		null);
}
?>
