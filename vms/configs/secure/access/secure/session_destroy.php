<?php

//Obtain current session ID
$db_session_id = session_id();

if (!(preg_match('/^[-,a-zA-Z0-9]{1,128}$/', $db_session_id) > 0))
{
	error_response_json(
		true,
		"Could not log out due to a server error. Please try again or contact the site administrator.",
		"Oops",
		1,
		"eid_DES",
		$stmt,
		$mysqli);
}

//Open DB connection
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/db_vmsx.php';

//Add session to database for control and tracking purposes.
if(isset($stmt)){$stmt->close();};
$sql_user_session_del = "
	DELETE FROM
		vmsx_db.users_sessions
	WHERE
		sid=?
	";

$stmt = $mysqli->prepare($sql_user_session_del);
$stmt->bind_param("s", $db_session_id);
if(!$stmt->execute())
{
	error_response_json(
		true,
		"Could not log out due to a server error. Please try again or contact the site administrator.",
		"Oops",
		1,
		"eid_DES",
		$stmt,
		$mysqli);
}

//Destroy session and associated variables
session_unset();
session_destroy();
session_start();
?>