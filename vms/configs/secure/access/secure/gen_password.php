<?php
	function vms_password_enc($text)
	{	
		//Requires PHP 5.5 and higher
		return password_hash($text, PASSWORD_DEFAULT);
	}
	
	function vms_password_verify($text, $encrypted)
	{
		//Requires PHP 5.5 and higher
		return password_verify($text, $encrypted);
	}
?>