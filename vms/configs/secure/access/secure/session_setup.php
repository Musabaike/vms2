<?php

//Obtain current session ID
$db_session_id = session_id();

if (!(preg_match('/^[-,a-zA-Z0-9]{1,128}$/', $db_session_id) > 0))
{
	error_response_json(
		true,
		"Could not log in due to a server error. Please try again or contact the site administrator.",
		"Oops",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}


//Setup the session variables
$_SESSION['username']	= $db_uname;
$_SESSION['name']		= $db_name;
$_SESSION['initials']	= $db_initials;
$_SESSION['surname']	= $db_surname;
$_SESSION['email']		= $db_email;
$_SESSION['group']		= $db_group;
$_SESSION['status']		= $db_status;
$_SESSION['enabled']	= $db_enabled;
$_SESSION['created_on']	= $db_created_on;

$_SESSION['uid']	= $db_uid;
$_SESSION['levels']	= $db_levels;

//$_SESSION['discard_after'] = $now + 60;  //One minute to perform first action, thereafter timer is set by every call to cfg_session_timer.php;
$_SESSION['timeout']	= false;

$_temp_ip = $_SERVER['REMOTE_ADDR'];

//Add session to database for control and tracking purposes.
if(isset($stmt)){$stmt->close();};
$sql_user_session = "
	INSERT INTO
		vmsx_db.users_sessions
	(
		uid,
		gid,
		sid,
		ip_addr
	)
	VALUES
	(
		?,
		?,
		?,
		?
	)
	";

$stmt = $mysqli->prepare($sql_user_session);
$stmt->bind_param("iiss",$db_uid, $db_group, $db_session_id, $_temp_ip);
$stmt->execute();
?>