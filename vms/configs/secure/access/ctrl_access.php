<?php
//Pre-requisites:
// $_SESSION['level'] and $_SESSION['status'] must be defined to grant access to a user
function grant_access($levels)
{
	//Check timer has not expired
	include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session_timer.php';
	
	//Defaults:
	$access_granted	= false;
	//$levels_user_count = count($_SESSION['levels']);
	//$levels_count	= count($levels);
	
	//Ensure the current user is allowed access:
	if (isset($_SESSION['levels']) )//&& isset($_SESSION['status']))
	{
		//if ($_SESSION['status'] != 0)
		//{
			if ((sizeof(array_intersect($_SESSION['levels'],$levels)))>0)
			{
				$access_granted = true;
			}
		//}
	}
	 return $access_granted;
}
?>