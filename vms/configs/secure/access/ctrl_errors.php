<?php
//Pre-requisites:
function error_response_json($error_msg, $error_code, $error_id, $mysqli_stmt, $mysqli_db)
{
	if (isset($mysqli_stmt))
	{
		$mysqli_stmt->close();
	}
	
	if(isset($mysqli_db))
	{
		$mysqli_db->close();
	}
	
	if ($error_msg == "")
	{	//default message
		$error_msg = "Something went wrong.";
	}
	
	switch ($error_code)
	{
		case 0:
			$error_level = "Success";
			break;
		case 1:
			$error_level = "Oops";
			break;
		case 2:
			$error_level = "Warning";
			break;
		case 3:
			$error_level = "Critical";
			break;
		default:
			$error_level = "Unknown";
	}
	
	if ($_SESSION['timeout'])
	{	//Change default errors due to timeout
		$error_code = 99;
		$error_level = "Oops";
		$error_msg = "Your session timed out.";
	}
	
	//Unauthorized access!
	$data_json = array (
		'error'			=> true,
		'error_msg'		=> $error_msg,
		'error_level'	=> $error_level,
		'error_code'	=> $error_code,
		'error_line'	=> $error_id,
		'session_timeout' => $_SESSION['timeout']
	);
	//Respond to request.
	echo json_encode($data_json, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
	exit(); //Stop any further code being executed.
}
?>