<?php
//Defaults, otherwise set in JS call
$page_no		= 1;
$page_limit		= 10;

//Get the requested page number and number of rows
// Protect from MySQL injection
$io_page_no	= mysqli_real_escape_string($mysqli, stripslashes($_POST['page_no']));
if($io_page_no == "")
{
	error_response_json(
		"System paging error.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}
else
{
	$page_no = intval($io_page_no);
}

// Protect from MySQL injection
$io_page_limit	= mysqli_real_escape_string($mysqli, stripslashes($_POST['page_limit']));
if($io_page_limit == "")
{
	error_response_json(
		"System paging error.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}
else
{
	$page_limit = intval($io_page_limit);
}

//Calculations
$page_offset	= ($page_no-1) * $page_limit;
$page_result	= mysqli_query($mysqli,$page_total_sql);
$page_total_rows= mysqli_fetch_array($page_result)[0];
$page_total		= ceil($page_total_rows / $page_limit);
?>