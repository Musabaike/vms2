<?php
	//This file defines the common header information required for the website.
	$website_name = "VMS";
	
	//$login_page	= '/';	//Client side location
	
	//Session info
	//$session_duration = 1800;	//0.5 hour default
	include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session_timer.php';
	include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session_token.php';
	$_SESSION['level'] = null;	//Default: null = Access denied
	
	
	//Remember to set your timezone in php.ini
	//[Date]
	//; Defines the default timezone used by the date functions
	//; http://php.net/date.timezone
	//date.timezone = Africa/Johannesburg
?>