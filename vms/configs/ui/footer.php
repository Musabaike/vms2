<footer class="page-footer">
	<div class="container">
		<div class="row">
			<div class="col l3 m4 s6">
				<h5 class="white-text"></h5>
				<ul>
					<li><a class="white-text" href="#"></a></li>
					<li><a class="white-text" href="#"></a></li>
					<li><a class="white-text" href="#"></a></li>
					<li><a class="white-text" href="#"></a></li>
				</ul>
			</div>
			<div class="col l3 m4 s6">
				<h5 class="white-text"></h5>
				<ul>
					<li><a class="white-text" href="#"></a></li>
					<li><a class="white-text" href="#"></a></li>
					<li><a class="white-text" href="#"></a></li>
					<li><a class="white-text" href="#"></a></li>
				</ul>
			</div>
			
		</div>
	</div>
	<div class="footer-copyright">
		<div class="container">
			<strong>Copyright &copy; 2019<?php if(date('Y') > "2019"){ echo " - ".date('Y');} ?></strong> Telkom SA SOC Limited. All Rights Reserved.
		</div>
	</div>
</footer>