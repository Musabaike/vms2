<?php
//Pre-requisites:
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
?>
<div class="navbar-fixed">
	<!-- Dropdown Structure -->
	<ul id="dd_menu" class="dropdown-content">
		<li><a data-target="modal_request" class="modal-trigger" href="#"><i class="material-icons left">person_add</i>Request Access</a></li>
	</ul>
	
	<ul id="dd_apps" class="dropdown-content">
		<?php
			if (grant_access([1000])){
		?>
			<li><a href="/en/home/" class="waves-effect waves-light"><i class="material-icons left">home</i>Home</a></li>
		<?php
			}
			
			if (grant_access([2000])){
		?>
			<li><a href="/en/orders/"><i class="material-icons left">date_range</i>Orders</a></li>
		<?php
			}
			
			if (grant_access([3000])){
		?>
			<li><a href="/en/admin/"><i class="material-icons left">build</i>Admin</a></li>
		<?php
			}
			
			if (grant_access([4000])){
		?>
			<li><a href="/en/users/"><i class="material-icons left">group</i>Users</a></li>
		<?php
			}
			
			if (grant_access([5000])){
		?>
			<li><a href="/en/settings/"><i class="material-icons left">settings</i>Settings</a></li>
		<?php
			}
			
			if (grant_access([5000])){
		?>
			<li><a href="/en/history/"><i class="material-icons left">history</i>History</a></li>
		<?php
			}
			
			if (grant_access([1000,2000,3000,4000,5000])){
		?>
			<li><a id="btn_logoff" href="#" data-token="<?php echo create_token();?>"><i class="material-icons left">exit_to_app</i>Log off</a></li>		
		<?php
			}
		?>
	</ul>
	<ul id="dd_acc" class="dropdown-content">
		<?php
			if (grant_access([1000,2000,3000,4000,5000])){
		?>
			<li><a href="#" class="waves-effect waves-light"><i class="material-icons left">person</i><?php if(isset($_SESSION['username'])){echo $_SESSION['username'];} else {echo "Unknown";}?></a></li>
		<?php
			}
		?>
	</ul>
	
	<nav>
		<div class="nav-wrapper">
			<a href="#" class="brand-logo center truncate no-select"> <span class="telkom-font-bold">Telkom</span> WorldCall VMS</a>
			<ul class="right">
				<?php			
					if (grant_access([1000,2000,3000,4000,5000])){
				?>
					<li><a class="dropdown-trigger" href="#" data-target="dd_apps"><i class="material-icons right">apps</i></a></li>
				<?php
					} else {
				?>
					<li><a class="dropdown-trigger" href="#" data-target="dd_menu"><i class="material-icons right">menu</i></a></li>
				<?php
					}
				?>
			</ul>
			<ul class="left">
				<?php			
					if (grant_access([1000,2000,3000,4000,5000])){
				?>
					<li><a class="dropdown-trigger" href="#" data-target="dd_acc"><i class="material-icons">account_box</i></a></li>
				<?php
					}
				?>
			</ul>
		</div>
	</nav>
</div>
