var elems;
var options;
var menu_main;

$(document).ready(function(){
	elems = document.querySelectorAll('.dropdown-trigger');
	options = {
		constrainWidth: false
	};
    menu_main = M.Dropdown.init(elems, options);
	
	//Login Button
	$('#btn_logoff').click(function(){
		event.preventDefault();
		logoff($(this).attr('data-token'));
	});
});


//Functions
function logoff(token){
	
	$.post('/en/logout/logoff.php',
		{
			request: 'logoff',
			token: token
		},
		function(json)
		{
			if(json['error']){
				var toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				return null;
			}
			
			if (!json['logoff'])
			{
				var toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				return null;
			}
			
			//Successful response
			//Redirect to login page
			window.location = "/";
		},
		'json'
	).done(function()
	{
		
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	
	return null;
}