<div id="modal_request" class="modal">
	<div class="modal-content">
		<h4>Request access to WorldCall VMS</h4>
		<div class="row">
			<div class="col s12 m2 l3">
			</div>
			<div class="col s12 m8 l6">
				<form class="row">
					<p>Note: All requests submitted are recorded and sent for review before access is granted.</p>
					<div class="input-field col s12">
						<i class="material-icons prefix">domain</i>
						<input placeholder="Telkom username..." id="io_req_uname" type="text" class="validate">
						<label for="io_req_uname">Username</label>
					</div>
					<div class="input-field col s12">
						<i class="material-icons prefix">person</i>
						<input id="io_req_fname" type="text" class="validate">
						<label for="io_req_fname">First Name</label>
					</div>
					<div class="input-field col s12">
						<i class="material-icons prefix"></i>
						<input id="io_req_initials" type="text" class="validate">
						<label for="io_req_initials">Initials</label>
					</div>
					<div class="input-field col s12">
						<i class="material-icons prefix"></i>
						<input id="io_req_lname" type="text" class="validate">
						<label for="io_req_lname">Surname</label>
					</div>
					<div class="input-field col s12">
						<i class="material-icons prefix">email</i>
						<input id="io_req_email" type="text" class="validate">
						<label for="io_req_email">Email Address</label>
					</div>
					<div class="input-field col s12">
						<i class="material-icons prefix">local_offer</i>
						<input id="io_req_salref" type="text" class="validate">
						<label for="io_req_salref">Salary Ref.</label>
					</div>
					<div class="input-field col s12">
						<i class="material-icons prefix">supervisor_account</i>
						<select id="io_req_group">
							<!--<option value="0" disabled selected></option>
							<option value="1">Administrator</option>
							<option value="2">Power User</option>
							<option value="3">VMS Web User</option>-->
<?php
//Open DB connection
include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/db_vmsx.php';
	$sql_query = "
	SELECT
		id,
		name
	FROM
		vmsx_db.groups
	ORDER BY
		id ASC";
		
		
$stmt = $mysqli->prepare($sql_query);
$stmt->execute();
if(!($result = $stmt->get_result()))
{
	echo '<option value="0" disabled selected>Error - Could not get list of options.</option>';
}

while($row = $result->fetch_assoc())
{
	echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
}
if(isset($stmt) && $stmt != null){$stmt->close();}
if(isset($mysqli) && $mysqli){$mysqli->close();}
?>
						</select>
						<label>User Type</label>
					</div>
					<div class="input-field col s12">
						<i class="material-icons prefix">vpn_key</i>
						<input id="io_req_password" type="password" class="validate">
						<label for="io_req_password">New Password</label>
					</div>
					<div class="input-field col s12">
						<i class="material-icons prefix"></i>
						<input id="io_req_password1" type="password" class="validate">
						<label for="io_req_password1">Confirm Password</label>
					</div>
				</form>
			</div>
			<div class="col s12 m2 l3">
			</div>
		</div>

		<div class="row">
			<div class="col s12 m2 l3">
			</div>
			<div class="col s12 m8 l6">
				<p>Information: </p>
				<p>On submission of a valid request, an email will be sent to the VMS administrator for review and approval.</p>
				<p>A second email will be sent to the specified email address as confirmation of the request submission.</p>
				<p>On approval, the specified email address will recieve confirmation, at which point you may proceed by logging into VMS. </p>
			</div>
			<div class="col s12 m2 l3">
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<a href="#" class="modal-close waves-effect waves-red btn-flat">Cancel</a>
		<a id="btn_reg" data-token="<?php echo create_token();?>" href="#" class="waves-effect waves-green btn-flat">Agree</a>
	</div>
</div>