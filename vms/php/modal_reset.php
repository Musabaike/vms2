<div id="modal_reset" class="modal">
	<div class="modal-content">
		<h4>Reset access to WorldCall VMS</h4>
		<div class="row">
			<div class="col s12 m2 l3">
			</div>
			<div class="col s12 m8 l6">
				<form class="row">
					<p>Note: All requests submitted are recorded.</p>
					<div class="input-field col s12">
						<i class="material-icons prefix">email</i>
						<input id="io_reset_email" placeholder="Account email address..." type="text" class="validate" autocomplete="email">
						<label for="io_reset_email">Email</label>
					</div>
				</form>
			</div>
			<div class="col s12 m2 l3">
			</div>
		</div>
		<div class="row">
			<div class="col s12 m2 l3">
			</div>
			<div class="col s12 m8 l6">
				<p>Information: </p>
				<p>On submission of a valid email address, a reset code will be sent to the specified email address.</p>
				<p>To reset your password, follow the instructions within the received email.</p>
			</div>
			<div class="col s12 m2 l3">
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<a href="#" class="modal-close waves-effect waves-red btn-flat">Cancel</a>
		<a id="btn_reset" href="#" data-token="<?php echo create_token();?>" class="waves-effect waves-green btn-flat">Reset</a>
	</div>
</div>