<?php
	session_start();	//Start session
	include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
	include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
	include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/common/header.php';
	
	$page_title = "Login";
	if(isset($_SESSION['username']) && isset($_SESSION['levels']))
	{
		header('Location: /en/home/'); // Already logged in, Redirecting to dashboar.
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $website_name ; if ($page_title <> ""){echo " - ".$page_title;}?></title>
		<?php
			include_once $_SERVER['DOCUMENT_ROOT'].'/configs/ui/css.php';
		?>
		
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="screen">
        	<link rel="stylesheet" type="text/css" href="css/DT_bootstrap.css">
        	
		<!--Let browser know website is optimized for mobile-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	</head>
	<script src="js/jquery.js" type="text/javascript"></script>
	<script src="js/bootstrap.js" type="text/javascript"></script>
	
	<script type="text/javascript" charset="utf-8" language="javascript" src="js/jquery.dataTables.js"></script>
	<script type="text/javascript" charset="utf-8" language="javascript" src="js/DT_bootstrap.js"></script>

	<body data-token="<?php echo create_token();?>">
		<header class="page-header">
			<?php
				include_once $_SERVER['DOCUMENT_ROOT'].'/configs/ui/nav.php';
			?>
		</header>
		
		<main class="page-main">
			<div class="section no-pad-bot" id="index-banner">
				<div class="container">
					<div class="row">
						<div class="col s12">
							
						</div>
					</div>
					<div class="row">
						<div class="col s12 m2 l3">
						</div>
						<form class="col s12 m8 l6">
							<div class="card">
								<div class="card-image white-text">
									<img src="images/worldcall.png">
									<span class="card-title jw-text-shadow">Login</span>
								</div>
								<div class="card-content">
									<div class="valign-wrapper">
										<h6>WorldCall Prepaid Voucher Management System</h6>
									</div>
									<br />
									<div class="input-field col s12">
										<input placeholder="Email or username" id="username" type="text" autocomplete="username" class="validate" autofocus>
										<label for="username">Username</label>
									</div>
									<div class="input-field col s12">
										<input placeholder="Password" id="password" type="password" autocomplete="current-password" class="validate">
										<label for="password">Password</label>
									</div>
								</div>
								<div class="card-action valign-wrapper">
									<div class="col s6">
										<a data-target="modal_reset" class="modal-trigger" href="#">Forgotten my password</a>
									</div>
									<div class="col s6 right-align">
										<button id="btn_login" data-token="<?php echo create_token();?>" class="btn waves-effect waves-light" type="submit" name="action">Login
											<i class="material-icons right">send</i>
										</button>
									</div>
								</div>
							</div>
						</form>
						<div class="col s12 m2 l3">
						</div>
					</div>
					<div class="row">
						<div class="col s12">
							<p>Authorization Notice: ...</p>
						</div>
					</div>
				</div>
			</div>
		</main>

		<?php
			//Footer:
			include_once $_SERVER['DOCUMENT_ROOT'].'/configs/ui/footer.php';
			//Modals:
			include_once $_SERVER['DOCUMENT_ROOT'].'/php/modal_request.php';
			include_once $_SERVER['DOCUMENT_ROOT'].'/php/modal_reset.php';
			//Javascripts:
			include_once $_SERVER['DOCUMENT_ROOT'].'/configs/ui/js.php';
		?>
	</body>
</html>
