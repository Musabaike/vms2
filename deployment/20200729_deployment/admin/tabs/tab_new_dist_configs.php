<div class="row">
	<h6>Distributor Configuration Details:</h6>
</div>

<form class="row">
	<div class="input-field col s12">
		<i class="material-icons prefix">unarchive</i>
		<input placeholder="Transmit directory" id="io_new_dist_transmit_dir" type="text" data-length="250" maxlength="250" class="validate">
		<label for="io_new_dist_transmit_dir">Transmit Directory</label>
	</div>
	<div class="input-field col s12">
		<i class="material-icons prefix">archive</i>
		<input placeholder="Receive directory" id="io_new_dist_receive_dir" type="text" data-length="250" maxlength="250" class="validate">
		<label for="io_new_dist_receive_dir">Receive Directory</label>
	</div>
	<div class="input-field col s12">
		<i class="material-icons prefix">cloud</i>
		<input placeholder="SFG IP address" id="io_new_dist_ftp_server" type="text" data-length="50" maxlength="50" class="validate">
		<label for="io_new_dist_ftp_server">SFG Server</label>
	</div>
	<div class="input-field col s12">
		<i class="material-icons prefix">folder_shared</i>
		<input placeholder="SFG Username" id="io_new_dist_ftp_user" type="text" data-length="50" maxlength="50" class="validate">
		<label for="io_new_dist_ftp_user">SFG User</label>
	</div>
	<div class="input-field col s12">
		<i class="material-icons prefix">fingerprint</i>
		<input placeholder="SFG Account password" id="io_new_dist_ftp_password" type="text" data-length="20" maxlength="20" class="validate">
		<label for="io_new_dist_ftp_password">SFG Account Password</label>
	</div>
	<div class="input-field col s12">
		<i class="material-icons prefix">plus_one</i>
		<input placeholder="Distributor Sequence Number" id="io_new_dist_seqno" type="text" data-length="4" maxlength="4" class="validate">
		<label for="io_new_dist_seqno">Sequence Number</label>
	</div>
	<div class="input-field col s12">
		<i class="material-icons prefix">list</i>
		<input placeholder="Distributor batch size" id="io_new_dist_batch_size" type="text" data-length="10" maxlength="10" class="validate">
		<label for="io_new_dist_batch_size">Batch Size</label>
	</div>
	<div class="input-field col s12">
		<i class="material-icons prefix">vpn_key</i>
		<input placeholder="Transmit directory" id="io_new_dist_public_key" type="text" data-length="128" maxlength="128" class="validate">
		<label for="io_new_dist_public_key">Public Key</label>
	</div>
	<div class="input-field col s12">
		<i class="material-icons prefix">cloud_upload</i>
		<input placeholder="SFG Transmit directory" id="io_new_dist_ftp_transmit_dir" type="text" data-length="50" maxlength="50" class="validate">
		<label for="io_new_dist_transmit_dir">SFG Transmit Directory</label>
	</div>
	<div class="input-field col s12">
		<i class="material-icons prefix">cloud_download</i>
		<input placeholder="SFG Receive directory" id="io_new_dist_ftp_receive_dir" type="text" data-length="50" maxlength="50" class="validate">
		<label for="io_new_dist_ftp_receive_dir">SFG Receive Directory</label>
	</div>
	<div class="input-field col s12">
		<i class="material-icons prefix">vertical_align_top</i>
		<input placeholder="Maximum Cutoff" id="io_new_dist_max_cutoff" type="text" data-length="50" maxlength="50" class="validate">
		<label for="io_new_dist_max_cutoff">Maximum Cutoff</label>
	</div>
	<div class="input-field col s12">
		<i class="material-icons prefix">vertical_align_bottom</i>
		<input placeholder="Starter Size" id="io_new_dist_starter_size" type="text" data-length="50" maxlength="50" class="validate">
		<label for="io_new_dist_starter_size">Starter Size</label>
	</div>
</form>