<div class="row">
</div>

<form class="row">
	<div class="input-field col s12">
		<i class="material-icons prefix">domain</i>
		<input placeholder="ESP Agency name" id="io_dist_agency" type="text" data-length="10" maxlength="10" class="validate">
		<label for="io_dist_agency">Agency (ESP distributor name)</label>
	</div>
	<div class="input-field col s12">
		<i class="material-icons prefix">store</i>
		<input placeholder="SAP customer name" id="io_dist_cust_name" type="text" data-length="255" maxlength="255" class="validate">
		<label for="io_dist_cust_name">Customer Name (SAP distributor name)</label>
	</div>
	<div class="input-field col s12">
		<i class="material-icons prefix">fingerprint</i>
		<input placeholder="SAP customer number" id="io_dist_cust_no" type="text" data-length="18" maxlength="18" class="validate">
		<label for="io_dist_cust_no">Customer Number (SAP distributor number)</label>
	</div>
</form>

<div class="row">
</div>

<div class="row">
	<div class="collection">
		<a href="#!" class="collection-item"><b>System Information</b></a>
		<a href="#!" class="collection-item"><span id="dist_inf_created_on" class="badge"></span>Created On</a>
		<a href="#!" class="collection-item"><span id="dist_inf_created_by" class="badge"></span>Created By</a>
		<a href="#!" class="collection-item"><span id="dist_inf_updated_on" class="badge"></span>Updated On</a>
		<a href="#!" class="collection-item"><span id="dist_inf_updated_by" class="badge"></span>Updated By</a>
	</div>
</div>