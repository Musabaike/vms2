<div id="modal_dist_add" class="modal modal-fixed-footer">
	<div class="modal-content">
		<h4>Create a new Distributor</h4>
		<div class="row">
			<div class="col s12">
				<div class="row">
					<div class="col s12">
						<ul id="tab_order_edit" class="tabs">
							<li class="tab col s4"><a id="new_dist_inf_tab" class="active tooltipped" href="#tab_new_dist_info" data-position="top" data-tooltip="Distributor Info"><b>Information </b></a></li>
							<li class="tab col s4"><a id="new_dist_contacts_tab" class="tooltipped" href="#tab_new_dist_contacts" data-position="top" data-tooltip="Distributor Contact Details"><b>Contacts </b></a></li>
							<li class="tab col s4"><a id="new_dist_configs_tab" class="tooltipped" href="#tab_new_dist_configs" data-position="top" data-tooltip="Distributor System Configurations"><b>Configs </b></a></li>
						</ul>
					</div>
					<div id="tab_new_dist_info" class="col s12"><?php include $_SERVER['DOCUMENT_ROOT'].'/en/admin/tabs/tab_new_dist_inf.php'; ?></div>
					<div id="tab_new_dist_contacts" class="col s12"><?php include $_SERVER['DOCUMENT_ROOT'].'/en/admin/tabs/tab_new_dist_contacts.php'; ?></div>
					<div id="tab_new_dist_configs" class="col s12"><?php include $_SERVER['DOCUMENT_ROOT'].'/en/admin/tabs/tab_new_dist_configs.php'; ?></div>
				</div>
				
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<a href="#!" class="modal-close waves-effect waves-red btn-flat">Cancel</a>
		<a id="mod_dist_btn_add" data-token="<?php echo create_token();?>" href="#!" class="waves-effect waves-green btn-flat">Create</a>
	</div>
</div>