var elems, options;
var tabs;
var tt_sessions;

var users_hist_page_no = 1;
var users_hist_page_limit = 10;
var users_hist_sort_order = "ASC";
var users_hist_sort_column = 0;

var dists_hist_page_no = 1;
var dists_hist_page_limit = 10;
var dists_hist_sort_order = "ASC";
var dists_hist_sort_column = 0;

$(document).ready(function()
{	//Main
	prepareData();
	initialise_Mcss();
	initialise_Btns();
	initialise_Forms();
	
	//Default data: Fetch all users
	users_hist_get($("body").attr('data-token'), null);
});


//INITIALIZATION
function prepareData()
{	//Prepare DOM elements for initialising
	getColumnsToFilterBy('users_hist_tbl',		'users_hist_filter_sel');
	getColumnsToFilterBy('dists_hist_tbl',		'dists_hist_filter_sel');	
}

function initialise_Mcss()
{	//MaterializeCSS initializations

	//Tabs:
	elems  = document.querySelectorAll('.tabs');
	options = {
		duration: 300
	};
	tabs = M.Tabs.init(elems, options);
	
	//Button Tooltips:
	options = {
		enterDelay: 500
	};
	elems = document.querySelectorAll('.tooltipped');
    	M.Tooltip.init(elems, options);
	//Tab Tooltips:
	options = {
		enterDelay: 500
	};
	elems = document.querySelectorAll('.tabs .tooltipped');
    	tabs_tooltips = M.Tooltip.init(elems, options);

	elems = document.querySelectorAll('.btn-floating.tooltipped');
    	btn_add_tooltips = M.Tooltip.init(elems, options);
		
	//Collapsables
	M.Collapsible.init(document.querySelectorAll('.collapsible'), {});

}

function initialise_Btns()
{	//Initialise Button listeners
	//Search
	$('#btn_users_hist_search').click(function(){
		event.preventDefault();
		users_hist_page_no = 1;
		users_hist_get($("body").attr('data-token'), null);
	});	
	$('#btn_dists_hist_search').click(function(){
		event.preventDefault();
		dists_hist_page_no = 1;
		dists_hist_get($("body").attr('data-token'), null);
	});
	

	//Fetch all data on Tab select
	$("#users_hist_tab").click(function (){
		event.preventDefault();
		sort_clear('#users_hist_tbl');
		document.getElementById("users_hist_filter").value="";
		users_hist_get($("body").attr('data-token'), this);
	});
	$("#dists_hist_tab").click(function (){
		event.preventDefault();
		sort_clear('#dists_hist_tbl');
		document.getElementById("dists_hist_filter").value="";
		dists_hist_get($("body").attr('data-token'), this);
	});

	//Sort Button: i.e. Header cell
	$('#users_hist_tbl > thead > tr > th').click(function(){
		event.preventDefault();
		sortByColumn("#users_hist_tbl", this);
	});
	$('#dists_hist_tbl > thead > tr > th').click(function(){
		event.preventDefault();
		sortByColumn("#dists_hist_tbl", this);
	});
	
}

function initialise_Forms()
{	//Initialise Form listeners
	M.updateTextFields();
	
	//Select dropdown
	M.FormSelect.init(document.querySelectorAll('select'), {});

	//Paging
	$("#page_limit_users_hist").change(function (){
		users_hist_page_no = 1;
		users_hist_get($("body").attr('data-token'), null);
	});
	$("#page_limit_dists_hist").change(function (){
		dists_hist_page_no = 1;
		dists_hist_get($("body").attr('data-token'), null);
	});
}

//USERS HIST TAB
function users_hist_get(token, caller)
{	//Fetch users in the database
	filter_data = {
		columns:	$('#users_hist_filter_sel').val(),
		keyword:	document.getElementById("users_hist_filter").value
	};
	if(caller != null)
	{
		if(caller.hasAttribute("data-page"))
		{	//Check if page change requested
			let temp_page_no = caller.getAttribute("data-page");
			if (temp_page_no != null && temp_page_no != "null")
			{	//Set new page by selection
				users_hist_page_no = temp_page_no;
			}
			else
			{
				return;
			}
		}
	}
	//#page_limit
	users_hist_page_limit = $('#page_limit_users_hist').val();

	$('#users_hist_tbl > tbody').text(""); //sort_clear('#users_hist_tbl');
	$('.progress').remove();
	$('#users_hist_tbl').parent().append('<div class="progress"><div class="indeterminate"></div></div>');
	$.post('/en/history/php/users_hist.php',
		{
			request: 'all_users_hist',
			token: token,
			page_no: users_hist_page_no,
			page_limit: users_hist_page_limit,
			sort_order: users_hist_sort_order,
			sort_column: users_hist_sort_column,
			filter: filter_data
		},
		function(json)
		{
			paging_update('page_users_hist', json['page_no'], json['page_total']);
			//Paging click event
			$('#page_users_hist li').on("click", "a", function(){
				event.preventDefault();
				users_hist_get($("body").attr('data-token'), this);
			});	
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			users_hist_append('#users_hist_tbl', json['users_hist']);
		},
		'json'
	).done(function()
	{
		$('.progress').remove();
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	
	return null;
}


function users_hist_append(table_id, users_array)
{	//Add users to a specified table
	if (users_array == null) return false;
	var html = "";
	users_array.forEach(function(item, index, data){
		html = '<tr data-uid="'+	data[index]['id'] + 
			'"><td>'+ 		data[index]['action'] +
			'</td><td>' +		data[index]['username'] + 
			'</td><td>' +		data[index]['name'] + 
			'</td><td>' +		data[index]['initials'] + 
			'</td><td>' +		data[index]['surname'] + 
			'</td><td>' +		data[index]['email'] + 
			'</td><td>' +		data[index]['group_name'] + 
			'</td><td>' +		data[index]['salref'] +
			'</td><td>' +		data[index]['updated_by_uname'] + 
			'</td><td>' +		(data[index]['updated_on']!=null?data[index]['updated_on']:'') + 
			'</td></tr>';
			
			$(table_id + ' > tbody').append(html);
	});
	return true;
}

//DISTS HIST TAB
function dists_hist_get(token, caller)
{	//Fetch all distributors in the database
	filter_data = {
		columns:	$('#dists_hist_filter_sel').val(),
		keyword:	document.getElementById("dists_hist_filter").value
	};

	if(caller != null)
	{
		if(caller.hasAttribute("data-page"))
		{	//Check if page change requested
			let temp_page_no = caller.getAttribute("data-page");
			if (temp_page_no != null && temp_page_no != "null")
			{	//Set new page by selection
				dists_hist_page_no = temp_page_no;
			}
			else
			{
				return;
			}
		}
	}
	//#page_limit
	dists_page_limit = $('#page_limit_dists_hist').val();
		
	$('#dists_hist_tbl > tbody').text(""); //sort_clear('#dists_hist_tbl');
	$('.progress').remove();
	$('#dists_hist_tbl').parent().append('<div class="progress"><div class="indeterminate"></div></div>');
	$.post('/en/history/php/dists_hist.php',
		{
			request: 'all_dists_hist',
			token: token,
			page_no: dists_hist_page_no,
			page_limit: dists_hist_page_limit,
			sort_order: dists_hist_sort_order,
			sort_column: dists_hist_sort_column,
			filter: filter_data
		},
		function(json)
		{
			paging_update('page_dists_hist', json['page_no'], json['page_total']);
			//Paging click event
			$('#page_dists_hist li').on("click", "a", function(){
				event.preventDefault();
				dists_hist_get($("body").attr('data-token'), this);
			});	
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			
			dists_hist_append('#dists_hist_tbl', json['dists_hist']);
		},
		'json'
	).done(function()
	{
		$('.progress').remove();
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	
	return null;
}

function dists_hist_append(table_id, dists_hist_array)
{	//Add distributor to a specified table
	if (dists_hist_array == null) return false;
	var html = "";
	dists_hist_array.forEach(function(item, index, data){
		html = html + 
			'<tr data-did="'+	data[index]['id'] +
			'"><td>'	+ 		(data[index]['action']!=null?data[index]['action']:'') +
			'</td><td>' +		(data[index]['agency']!=null?data[index]['agency']:'') + 
			'</td><td>' +		(data[index]['cust_name']!=null?data[index]['cust_no']:'') + 
			'</td><td>' +		(data[index]['cust_no']!=null?data[index]['cust_name']:'') + 
			'</td><td>' +		(data[index]['location']!=null?data[index]['location']:'') + 
			'</td><td>' +		(data[index]['contact']!=null?data[index]['contact']:'') + 
			'</td><td>' +		(data[index]['tel']!=null?data[index]['tel']:'') + 
			'</td><td>' +		(data[index]['fax']!=null?data[index]['fax']:'') + 
			'</td><td>' +		(data[index]['address']!=null?data[index]['address']:'') + 
			'</td><td>' +		(data[index]['ftp_transmit_dir']!=null?data[index]['ftp_transmit_dir']:'') + 
			'</td><td>' +		(data[index]['ftp_receive_dir']!=null?data[index]['ftp_receive_dir']:'') + 
			'</td><td>' +		(data[index]['ftp_server']!=null?data[index]['ftp_server']:'') + 
			'</td><td>' +		(data[index]['ftp_user']!=null?data[index]['ftp_user']:'') + 
			//'</td><td>' +		(data[index]['seqno']!=null?data[index]['seqno']:'') + 
			'</td><td>' +		(data[index]['batch_size']!=null?data[index]['batch_size']:'') + 
			'</td><td>' +		(data[index]['email']!=null?data[index]['email']:'') +
			'</td><td>' +		(data[index]['updated_on']!=null?data[index]['updated_on']:'') + 
			'</td><td>' +		(data[index]['updated_by_uname']!=null?data[index]['updated_by_uname']:'') + 

			'</td></tr>';
	});
	$(table_id + ' > tbody').append(html);
	return true;
}

//MISC
function getColumnsToFilterBy(table_id, filter_sel_id)
{	//Adds columns' indexes and names to to the drop down filter
	var testThisFunc = document.querySelectorAll('#'+table_id+'> thead > tr > th')
	testThisFunc.forEach(function(value,index,array){
		//console.log('index: '+ index + ', Value: ' + value.innerText);
		var html = '<option value="'+index+'" selected>'+value.innerText+'</option>';
		$('#'+filter_sel_id).append(html);
	});
	
}

function sortTable(table_id, column, order)
{	//Sorting Algorithm used by sortByColumn()
	switch(table_id)
	{
		case "#users_hist_tbl":
			users_hist_sort_order = order;
			users_hist_sort_column = column;
			users_hist_get($("body").attr('data-token'), null);
			break;
		case "#dists_hist_tbl":
			dists_hist_sort_order = order;
			dists_hist_sort_column = column;
			dists_hist_get($("body").attr('data-token'), null);
			break;
		default:
	}
}

function sortByColumn(table_id, table_th)
{	//Sort a table by a specified column
	if($(table_th).find("i").hasClass("vms-sort"))
	{
		sortTable(table_id, $(table_th).index(), "ASC");
		$(table_th).find("i").removeClass("vms-sort");
		$(table_th).find("i").addClass("vms-sort-asc").text("arrow_drop_up");
		$(table_th).siblings().find("i").addClass("vms-sort").text("");
		$(table_th).siblings().find("i").removeClass("vms-sort-asc");
		$(table_th).siblings().find("i").removeClass("vms-sort-desc");
	}
	else if($(table_th).find("i").hasClass("vms-sort-asc"))
	{
		sortTable(table_id, $(table_th).index(), "DESC");
		$(table_th).find("i").removeClass("vms-sort-asc");
		$(table_th).find("i").addClass("vms-sort-desc").text("arrow_drop_down");
		$(table_th).siblings().find("i").addClass("vms-sort").text("");
		$(table_th).siblings().find("i").removeClass("vms-sort-asc");
		$(table_th).siblings().find("i").removeClass("vms-sort-desc");
	}
	else if($(table_th).find("i").hasClass("vms-sort-desc"))
	{
		sortTable(table_id, $(table_th).index(), "ASC");
		$(table_th).find("i").removeClass("vms-sort-desc");
		$(table_th).find("i").addClass("vms-sort-asc").text("arrow_drop_up");
		$(table_th).siblings().find("i").addClass("vms-sort").text("");
		$(table_th).siblings().find("i").removeClass("vms-sort-asc");
		$(table_th).siblings().find("i").removeClass("vms-sort-desc");
	}
}

function sort_clear(table_id)
{	//Clearing current sort of selected tab
	$(table_id + ' > tbody').text("");
	$(table_id).find("i").removeClass("vms-sort-asc");
	$(table_id).find("i").removeClass("vms-sort-desc");
	$(table_id).find("i").addClass("vms-sort").text("");
}

function paging_update(element_id, new_page_no, new_page_total)
{
	(new_page_no==null)?new_page_no=1:(typeof new_page_no=="undefined")?new_page_no=1:false;
	(new_page_total==null)?new_page_total=1:(typeof new_page_total=="undefined")?new_page_total=1:false;
	//var html = "";
	document.getElementById(element_id).innerHTML = "";
	//Prev button
	(
		(new_page_no > 1) ?
			$('#' + element_id).append('<li class="waves-effect tooltipped" data-tooltip="Previous page" data-position="left"><a data-page="' + (new_page_no - 1) + '" href="#!"><i class="material-icons">chevron_left</i></a></li>') :
			$('#' + element_id).append('<li class="disabled"><a data-page="null" href="#!"><i class="material-icons">chevron_left</i></a></li>')
	);
	//First page
	(
		(new_page_no > 4) ?
			$('#' + element_id).append('<li class="waves-effect"><a data-page="1" href="#!">1</a></li>') :
			false
	);
	//Lower More = ...
	(
		(new_page_no > 5) ?
			((new_page_no == 6) ?
				$('#' + element_id).append('<li class="waves-effect"><a data-page="2" href="#!">2</a></li>') :
				$('#' + element_id).append('<li class="disabled"><a data-page="null" href="#!"><i class="material-icons">more_horiz</i></a></li>')
			) :
			false
	);
	//Current page - 3
	(
		(new_page_no > 3) ?
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no - 3) + '" href="#!">' + (new_page_no - 3) + '</a></li>') :
		false
	);
	//Current page - 2
	(
		(new_page_no > 2) ?
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no - 2) + '" href="#!">' + (new_page_no - 2) + '</a></li>') :
		false
	);
	//Current page - 1
	(
		(new_page_no > 1) ?
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no - 1) + '" href="#!">' + (new_page_no - 1) + '</a></li>') :
		false
	);
	//Current page
	(
		(new_page_no > 0) ?
		$('#' + element_id).append('<li class="active"><a data-page="null" href="#!">' + new_page_no + '</a></li>') :
		false
	);
	//Current page + 1
	(
		((new_page_total - new_page_no) > 1) ?
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no + 1) + '" href="#!">' + (new_page_no + 1) + '</a></li>') :
		false
	);
	//Current page + 2
	(
		((new_page_total - new_page_no) > 2) ?
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no + 2) + '" href="#!">' + (new_page_no + 2) + '</a></li>') :
		false
	);
	//Current page + 3
	(
		((new_page_total - new_page_no) > 3) ?
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no + 3) + '" href="#!">' + (new_page_no + 3) + '</a></li>') :
		false
	);
	//Upper More = ...
	(
		((new_page_total - new_page_no) > 4) ?
			(((new_page_total - new_page_no) == 5) ?
				$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_total - 1) + '" href="#!">' + (new_page_total - 1) + '</a></li>') :
				$('#' + element_id).append('<li class="disabled"><a data-page="null" href="#!"><i class="material-icons">more_horiz</i></a></li>')
			) :
			false
	);
	//Last page
	(
		(new_page_no >= new_page_total) ?
		true :
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_total) + '" href="#!">' + (new_page_total) + '</a></li>')
	);
	//Next button
	(
		((new_page_total - new_page_no) > 0) ?
		$('#'+element_id).append('<li class="waves-effect tooltipped right" data-tooltip="Next page" data-position="right"><a data-page="'+(new_page_no+1)+'" href="#!"><i class="material-icons">chevron_right</i></a></li>') :
		$('#'+element_id).append('<li class="disabled right"><a data-page="null" href="#!"><i class="material-icons">chevron_right</i></a></li>')
	);
	//Activate Tooltips:
	options = {
		enterDelay: 500
	};
	elems = document.querySelectorAll('ul .tooltipped');
    M.Tooltip.init(elems, options);
}
