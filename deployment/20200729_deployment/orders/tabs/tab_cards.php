<div class="row">
	<ul class="collapsible popout">
		<li>
			<div class="collapsible-header"><i class="material-icons">search</i>Search</div>
			<div class="collapsible-body">
				<?php include $_SERVER['DOCUMENT_ROOT'].'/en/orders/php/col_cards_search.php'; ?>
			</div>
		</li>
	</ul>
</div>

<div class="divider"></div>		
<div class="row">
	<div class="col s12 center">

		<table id="cards_tbl" class="responsive-table highlight striped">
			<thead>
				<tr>
					<th class="vms-sort">SAP Order No <i class="material-icons tiny vms-sort"></th>
					<th class="vms-sort">Card No <i class="material-icons tiny vms-sort"></th>
					<th class="vms-sort">Serial No <i class="material-icons tiny vms-sort"></th>
					<th class="vms-sort">Distributor <i class="material-icons tiny vms-sort"></th>
					<th class="vms-sort">Voucher Type <i class="material-icons tiny vms-sort"></th>
					<th class="vms-sort">Status <i class="material-icons tiny vms-sort"></th>
					<th class="vms-sort">Last Updated On <i class="material-icons tiny vms-sort"></th>
					<th class="vms-sort">Last Updated By <i class="material-icons tiny vms-sort"></th>
				</tr>
			</thead>

			<tbody>
				
			</tbody>
		</table>
	</div>
</div>
<div class="row">
	<div class="input-field col s12 m10 l10">
		<ul id="page_cards" class="pagination">
			<li class="disabled"><a data-page="null" href="#!"><i class="material-icons">chevron_left</i></a></li>
			<li class="disabled right"><a data-page="null" href="#!"><i class="material-icons">chevron_right</i></a></li>
		</ul>
	</div>
	<div class="input-field col s3 m2 l2 right">
		<select id="page_limit_cards" class="right">
			<option value="2">2</option>
			<option value="5">5</option>
			<option value="10" selected>10</option>
			<option value="20">20</option>
			<option value="50">50</option>
			<option value="100">100</option>
		</select>
		<label>No. of rows</label>
	</div>
</div>
