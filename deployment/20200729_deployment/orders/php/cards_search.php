<?php
//Search by the following conditions
	if (isset($_POST['search']['enabled']))
	{	//Search information is provided
		if($_POST['search']['enabled'] == "true")
		{	//Build the WHERE statement required by MySQL
			$sql_begin = true;
			$sql_condition = "WHERE ( ";
			
			if(isset($_POST['search']['date_from']))
			{	//Date From search
				if($_POST['search']['date_from'] != "" && $_POST['search']['date_from'] != null)
				{
					if ($sql_begin)
					{
						$sql_condition = $sql_condition." o.file_date >= '".$_POST['search']['date_from']."'";
						$sql_begin = false;
					}
					else
					{
						$sql_condition = $sql_condition." AND o.file_date >= '".$_POST['search']['date_from']."'";
					}
				}
			}
			
			if(isset($_POST['search']['date_to']))
			{	//Date To search
				if($_POST['search']['date_to'] != "" && $_POST['search']['date_to'] != null)
				{
					
					if ($sql_begin)
					{
						$sql_condition = $sql_condition." o.file_date <= '".$_POST['search']['date_to']."'";
						$sql_begin = false;
					}
					else
					{
						$sql_condition = $sql_condition." AND o.file_date <= '".$_POST['search']['date_to']."'";
					}
				}
			}
			
			/*if(isset($_POST['search']['status_filter']))
			{	//Status search
				if($_POST['search']['status_filter'] != "" && $_POST['search']['status_filter'] != null)
				{
					if ($sql_begin)
					{	
						$sql_condition = $sql_condition." ( ";
						$sql_begin = false;
					}
					else
					{
						$sql_condition = $sql_condition." AND ( ";
					}
					
					$first_array_item = true;
					foreach($_POST['search']['status_filter'] as $key => $value)
					{
						if ($first_array_item)
						{
							$sql_condition = $sql_condition." e.status = $value";
							$first_array_item = false;
						}
						else
						{
							$sql_condition = $sql_condition." OR e.status = $value";
						}
					}
					$sql_condition = $sql_condition." ) ";
				}
			}*/
			
			if(isset($_POST['search']['voucher_type']))
			{	//Status search
				if($_POST['search']['voucher_type'] != "" && $_POST['search']['voucher_type'] != null)
				{
					if ($sql_begin)
					{	
						$sql_condition = $sql_condition." ( ";
						$sql_begin = false;
					}
					else
					{
						$sql_condition = $sql_condition." AND ( ";
					}
					
					$first_array_item = true;
					foreach($_POST['search']['voucher_type'] as $key => $value)
					{
						if ($first_array_item)
						{
							$sql_condition = $sql_condition." o.ctid = $value";
							$first_array_item = false;
						}
						else
						{
							$sql_condition = $sql_condition." OR o.ctid = $value";
						}
					}
					$sql_condition = $sql_condition." ) ";
				}
			}
			
			if(isset($_POST['search']['order_number']))
			{	//Order search
				if($_POST['search']['order_number'] != "" && $_POST['search']['order_number'] != null)
				{
					if ($sql_begin)
					{
						$sql_condition = $sql_condition." o.sap_order_no LIKE '%".$_POST['search']['order_number']."%'";
						$sql_begin = false;
					}
					else
					{
						$sql_condition = $sql_condition." AND o.sap_order_no LIKE '%".$_POST['search']['order_number']."%'";
					}
				}
			}
			
			if(isset($_POST['search']['card_number']))
			{	//Order search
				if($_POST['search']['card_number'] != "" && $_POST['search']['card_number'] != null)
				{
					if ($sql_begin)
					{
						$sql_condition = $sql_condition." e.card LIKE '%".$_POST['search']['card_number']."%'";
						$sql_begin = false;
					}
					else
					{
						$sql_condition = $sql_condition." AND e.card LIKE '%".$_POST['search']['card_number']."%'";
					}
				}
			}
			
			if(isset($_POST['search']['serial_number']))
			{	//Order search
				if($_POST['search']['serial_number'] != "" && $_POST['search']['serial_number'] != null)
				{
					if ($sql_begin)
					{
						$sql_condition = $sql_condition." e.serial LIKE '%".$_POST['search']['serial_number']."%'";
						$sql_begin = false;
					}
					else
					{
						$sql_condition = $sql_condition." AND e.serial LIKE '%".$_POST['search']['serial_number']."%'";
					}
				}
			}
			
			if(isset($_POST['search']['agency']))
			{	//Agency search
				if($_POST['search']['agency'] != "" && $_POST['search']['agency'] != null)
				{
					if ($sql_begin)
					{	
						$sql_condition = $sql_condition." ( ";
						$sql_begin = false;
					}
					else
					{
						$sql_condition = $sql_condition." AND ( ";
					}
					
					$first_array_item = true;
					foreach($_POST['search']['agency'] as $key => $value)
					{
						if ($first_array_item)
						{
							$sql_condition = $sql_condition." e.did = $value";
							$first_array_item = false;
						}
						else
						{
							$sql_condition = $sql_condition." OR e.did = $value";
						}
					}
					$sql_condition = $sql_condition." ) ";
				}
			}
			
			if($sql_begin)
			{	//No search conditions to use in SQL
				$search = "";
			}
			else
			{
				$search = $sql_condition." )";
			}
		}
		else
		{	//No search conditions to use in SQL
			$search = "";
		}
		
		//error_log("SQL condition:: ".$search, 0);
	}
?>