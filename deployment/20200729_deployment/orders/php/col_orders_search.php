<div class="row">
	<div class="input-field col s6 m4 l3">
		<input id="orders_date_from" placeholder="e.g. YYYY-MM-DD" type="text" class="datepicker">
		<label for="orders_date_from">From Date</label>
	</div>
	<div class="input-field col s6 m4 l3">
		<input id="orders_date_to" placeholder="e.g. YYYY-MM-DD" type="text" class="datepicker">
		<label for="orders_date_to">To Date</label>
	</div>
	
	<div class="input-field col s6 m4 l3">
		<select id="orders_vtype" multiple>
			<?php include $_SERVER['DOCUMENT_ROOT'].'/en/orders/php/cards_types.php'; ?>
		</select>
		<label>Voucher Type</label>
	</div>
	<div class="input-field col s6 m4 l3">
		<input id="orders_order_no" placeholder="e.g. 0000000000" type="text" class="validate">
		<label for="orders_order_no">Order Number</label>
	</div>
</div>
<div class="row">
	<div class="input-field col s6 m4 l3">
		<select id="orders_status" multiple>
			<?php include $_SERVER['DOCUMENT_ROOT'].'/en/orders/php/orders_status.php'; ?>
		</select>
		<label>Status Filter</label>
	</div>
	<div class="input-field col s6 m4 l3 right">
		<select id="orders_agency" multiple>
			<?php include $_SERVER['DOCUMENT_ROOT'].'/en/orders/php/dists.php'; ?>
		</select>
		<label>Distributor</label>
	</div>
</div>

<div class="row">
	<div class="col">
		<a id="btn_orders_search_clr" class="waves-effect waves-light telkom-blue btn tooltipped" data-position="top" data-tooltip="Clear Results & Search Input"><i class="material-icons right">clear_all</i>Clear All</a>
	</div>
	<div class="col right">
		<a id="btn_orders_search" class="waves-effect waves-light btn tooltipped" data-position="top" data-tooltip="Submit Search"><i class="material-icons right">search</i>Search</a>
	</div>
	
</div>