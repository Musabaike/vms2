<?php
	include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session.php';
	include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
	include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
	if(!grant_access([2000]))
	{
		header('Location: /en/denied/');
		exit();
	}

	$page_title = "Orders";
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $website_name ; if ($page_title <> ""){echo " - ".$page_title;} ?></title>
		<?php
			include_once $_SERVER['DOCUMENT_ROOT'].'/configs/ui/css.php';
		?>
		
		<!--Let browser know website is optimized for mobile-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	</head>

	<body data-token="<?php echo create_token(); ?>">
		<header class="page-header">
			<?php
				include_once $_SERVER['DOCUMENT_ROOT'].'/configs/ui/nav.php';
			?>
		</header>

		<main class="page-main">
			<div class="container">
				<div class="row">
					<div class="col s12">
						<ul class="tabs tabs-fixed-width z-depth-1">
							<li class="tab col s3 <?php if(!grant_access([2001])){echo "hide";} ?>"><a id="orders_tab" class="tooltipped active" href="#tab_orders" data-position="top" data-tooltip="Orders"><b>Orders</b></a></li>
							<li class="tab col s3 <?php if(!grant_access([2004])){echo "hide";} ?>"><a id="cards_tab" class="tooltipped active" href="#tab_cards" data-position="top" data-tooltip="Vouchers/Cards"><b>Vouchers</b></a></li>
						</ul>
					</div>
					<div class="col s12">
						<div id="tab_orders" class="col s12 z-depth-1 <?php if(!grant_access([2001])){echo "hide";} ?>"> <?php include $_SERVER['DOCUMENT_ROOT'].'/en/orders/tabs/tab_orders.php'; ?> </div>
						<div id="tab_cards" class="col s12 z-depth-1 <?php if(!grant_access([2004])){echo "hide";} ?>"> <?php include $_SERVER['DOCUMENT_ROOT'].'/en/orders/tabs/tab_cards.php'; ?> </div>
					</div>
				</div>
			</div>
		</main>

		<?php
			//Footer:
			include_once $_SERVER['DOCUMENT_ROOT'].'/configs/ui/footer.php';
			//Modals:
			include_once $_SERVER['DOCUMENT_ROOT'].'/en/orders/modals/modal_order_edit.php';
			include_once $_SERVER['DOCUMENT_ROOT'].'/en/orders/modals/modal_card_edit.php';
			//Javascripts:
			include_once $_SERVER['DOCUMENT_ROOT'].'/configs/ui/js.php';
		?>
	</body>
</html>