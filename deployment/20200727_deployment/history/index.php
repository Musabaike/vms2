<?php
	include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session.php';
	include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
	include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
	if(!grant_access([4000]))
	{
		header('Location: /en/denied/');
		exit();
	}
	$page_title = "History";
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $website_name ; if ($page_title <> ""){echo " - ".$page_title;}?></title>
		<?php
			include_once $_SERVER['DOCUMENT_ROOT'].'/configs/ui/css.php';
		?>
		
		<!--Let browser know website is optimized for mobile-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	</head>

	<body data-token="<?php echo create_token();?>">
		<header class="page-header">
			<?php
				include_once $_SERVER['DOCUMENT_ROOT'].'/configs/ui/nav.php';
			?>
		</header>
		<main class="page-main">

			<div class="container">
				<div class="row">
					<div class="col s12">
						<ul class="tabs tabs-fixed-width z-depth-1">
							<li class="tab col s4">
								<a id="users_hist_tab" class="tooltipped active <?php if(!grant_access([4001])){echo "hide";} ?>" href="#tab_users_hist" data-tooltip="Users change history">
									<b>Users History </b>
								</a>
							</li>
							<li class="tab col s4">
								<a id="dists_hist_tab" class="tooltipped <?php if(!grant_access([4008])){echo "hide";} ?>" href="#tab_dists_hist" data-tooltip="Distributors change history">
									<b>Distributors History </b>
								</a>
							</li>
				
						</ul>
					</div>
					<div class="col s12">
						<div id="tab_users_hist" class="col s12 z-depth-1 <?php if(!grant_access([4001])){echo "hide";} ?>">
							<?php include $_SERVER['DOCUMENT_ROOT'].'/en/history/tabs/tab_users_hist.php'; ?>
						</div>
						<div id="tab_dists_hist" class="col s12 z-depth-1 <?php if(!grant_access([4008])){echo "hide";} ?>">
							<?php include $_SERVER['DOCUMENT_ROOT'].'/en/history/tabs/tab_dist_hist.php'; ?>
						</div>
					</div>
				</div>
			</div>
		</main>

		<?php
			//Action Button:
			//include_once $_SERVER['DOCUMENT_ROOT'].'/en/users/btn_actions.php';
			//Footer:
			include_once $_SERVER['DOCUMENT_ROOT'].'/configs/ui/footer.php';
			//Javascripts:
			include_once $_SERVER['DOCUMENT_ROOT'].'/configs/ui/js.php';
		?>
	</body>
</html>
