<form class="row">
	<div class="input-field col s12">
		<i class="material-icons prefix">person</i>
		<input id="io_edit_user_fname" type="text" class="validate">
		<label for="io_edit_user_fname">First Name</label>
	</div>
	<div class="input-field col s12">
		<i class="material-icons prefix"></i>
		<input id="io_edit_user_initials" type="text" class="validate">
		<label for="io_edit_user_initials">Initials</label>
	</div>
	<div class="input-field col s12">
		<i class="material-icons prefix"></i>
		<input id="io_edit_user_lname" type="text" class="validate">
		<label for="io_edit_user_lname">Surname</label>
	</div>
	<div class="input-field col s12">
		<i class="material-icons prefix">email</i>
		<input id="io_edit_user_email" type="text" class="validate">
		<label for="io_edit_user_email">Email Address</label>
	</div>
	<div class="input-field col s12">
		<i class="material-icons prefix">local_offer</i>
		<input id="io_edit_user_salref" type="text" class="validate">
		<label for="io_edit_user_salref">Salary Ref.</label>
	</div>
	<div class="input-field col s12">
		<i class="material-icons prefix">supervisor_account</i>
		<select id="io_edit_user_group">
		</select>
		<label>User Type</label>
	</div>
	<!--<div class="input-field col s12">
		<i class="material-icons prefix">verified_user</i>
		<select id="io_edit_user_status">
			<option value="-1" disabled selected></option>
			<option value="0">No Status</option>
			<option value="1">Locked</option>
		</select>
		<label>User Status</label>
	</div>-->
	<div class="input-field col s12">
		<i class="material-icons prefix">lock</i>
		<select id="io_edit_user_enabled">
			<option value="-1" disabled selected></option>
			<option value="0">Disabled</option>
			<option value="1">Enabled</option>
		</select>
		<label>User Enabled</label>
	</div>
	<div class="input-field col s12">
		<i class="material-icons prefix">email</i>
		<select id="io_edit_user_notify">
			<option value="-1" disabled selected></option>
			<option value="0">Disabled</option>
			<option value="1">Enabled</option>
		</select>
		<label>Receive Support Emails</label>
	</div>
	<div class="input-field col s12">
		<i class="material-icons prefix">vpn_key</i>
		<input id="io_edit_user_password" type="password" class="validate">
		<label for="io_edit_user_password">New Password</label>
	</div>
	<div class="input-field col s12">
		<i class="material-icons prefix"></i>
		<input id="io_edit_user_password1" type="password" class="validate">
		<label for="io_edit_user_password1">Confirm Password</label>
	</div>
</form>

<div class="row">
</div>
<div class="row">
	<div class="collection">
		<a href="#!" class="collection-item"><b>System Information</b></a>
		<a href="#!" class="collection-item"><span id="user_inf_username" class="badge"></span>Username</a>
		<a href="#!" class="collection-item"><span id="user_inf_created_on" class="badge"></span>Created On</a>
		<a href="#!" class="collection-item"><span id="user_inf_created_by" class="badge"></span>Created By</a>
		<a href="#!" class="collection-item"><span id="user_inf_updated_on" class="badge"></span>Updated On</a>
		<a href="#!" class="collection-item"><span id="user_inf_updated_by" class="badge"></span>Updated By</a>
	</div>
</div>