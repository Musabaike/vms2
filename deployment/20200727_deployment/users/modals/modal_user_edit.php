<div id="modal_user_edit" class="modal modal-fixed-footer">
	<div class="modal-content">
		<h4>User: <span id="io_edit_user_uname"></span></h4>
		
		<div class="row">
			<div class="col s12">
				<div class="row">
					<div class="col s12">
						<ul id="tab_users_edit" class="tabs">
							<li class="tab col s4"><a id="user_inf_tab" class="active" href="#tab_users_info"><b>Information </b></a></li>
							<li class="tab col s4"><a id="user_priv_tab" href="#tab_users_priv"><b>Privileges </b></a></li>
							<li class="tab col s4"><a id="user_ses_tab" href="#tab_users_ses"><b>Sessions </b></a></li>
						</ul>
					</div>
					<div id="tab_users_info" class="col s12"><?php include $_SERVER['DOCUMENT_ROOT'].'/en/users/tabs/tab_users_inf.php'; ?></div>
					<div id="tab_users_priv" class="col s12"><?php include $_SERVER['DOCUMENT_ROOT'].'/en/users/tabs/tab_users_priv.php'; ?></div>
					<div id="tab_users_ses" class="col s12"><?php include $_SERVER['DOCUMENT_ROOT'].'/en/users/tabs/tab_users_ses.php'; ?></div>
				</div>
				
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<a id="btn_users_modal_rem" class="btn-flat waves-effect waves-light left tooltipped <?php if(!grant_access([4006])){echo "hide";} ?>" data-position="top" data-tooltip="Delete this user"><i class="material-icons red-text">delete</i></a>
		<!--<a class="btn-flat waves-effect waves-light left tooltipped" data-position="top" data-tooltip="Disable this user"><i class="material-icons red-text">block</i></a>-->
		<a id="btn_users_edit_ses_rem" class="btn-flat waves-effect waves-light left tooltipped <?php if(!grant_access([4007])){echo "hide";} ?>" data-position="top" data-tooltip="Logout all of this user's sessions"><i class="material-icons red-text">clear_all</i></a>
		<a href="#!" class="modal-close waves-effect waves-red btn-flat">Cancel</a>
		<a id="btn_users_edit_update" data-token="<?php echo create_token();?>" href="#!" class="waves-effect waves-green btn-flat <?php if(!grant_access([4005])){echo "hide";} ?>">Update</a>
	</div>
</div>