<div id="modal_group_edit" class="modal modal-fixed-footer">
	<div class="modal-content">
		<h4>Edit a Group</h4>
		
		<div class="row">
			<div class="col s12">
				<div class="row">
					<div class="col s12">
						<ul id="tab_group_edit" class="tabs">
							<li class="tab col s4"><a class="active" href="#tab_group_info"><b>Information </b></a></li>
							<li class="tab col s4"><a id="user_ses_tab" href="#tab_group_priv"><b>Privileges</b></a></li>
						</ul>
					</div>
					<div id="tab_group_info" class="col s12"><?php include $_SERVER['DOCUMENT_ROOT'].'/en/users/tabs/tab_group_inf.php'; ?></div>
					<div id="tab_group_priv" class="col s12"><?php include $_SERVER['DOCUMENT_ROOT'].'/en/users/tabs/tab_group_priv.php'; ?></div>
				</div>
				
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<a id="btn_groups_modal_rem" class="btn-flat waves-effect waves-light left tooltipped <?php if(!grant_access([4013])){echo "hide";} ?>" data-position="top" data-tooltip="Delete this group"><i class="material-icons red-text">delete</i></a>
		<!--<a class="btn-flat waves-effect waves-light left tooltipped" data-position="top" data-tooltip="Disable this group"><i class="material-icons red-text">block</i></a>-->
		<a href="#!" class="modal-close waves-effect waves-red btn-flat">Cancel</a>
		<a id="btn_groups_edit_update" href="#!" class="waves-effect waves-green btn-flat <?php if(!grant_access([4012])){echo "hide";} ?>">Update</a>
	</div>
</div>