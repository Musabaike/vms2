<div id="modal_users_req_edit" class="modal modal-fixed-footer">
	<div class="modal-content">
		<h4>User Request: <span id="io_edit_users_req_uname"></span></h4>
		<div class="row">
			<p><b>Note:</b> The request can only be <b>Approved</b> or <b>Rejected</b>. Once Approved, the administrator <b>must</b> navigate to the <b>Users tab</b> and <b>enable</b> the user. The user information may also be edited there, if required, by the administrator. If the request is <b>Rejected</b>, it is deleted from the system.
		</div>
		<div class="row">
			<div class="col s12 m2 l3">
			</div>
			<div class="col s12 m8 l6">
				<form class="row">
					<!--<div class="input-field col s12">
						<i class="material-icons prefix">domain</i>
						<input disabled placeholder="Telkom username..." id="io_edit_users_req_uname" type="text" class="validate">
						<label for="io_edit_users_req_uname">Username</label>
					</div>-->
					<div class="input-field col s12">
						<i class="material-icons prefix">person</i>
						<input disabled id="io_edit_users_req_fname" type="text" class="validate">
						<label for="io_edit_users_req_fname">First Name</label>
					</div>
					<div class="input-field col s12">
						<i class="material-icons prefix"></i>
						<input disabled id="io_edit_users_req_initials" type="text" class="validate">
						<label for="io_edit_users_req_initials">Initials</label>
					</div>
					<div class="input-field col s12">
						<i class="material-icons prefix"></i>
						<input disabled id="io_edit_users_req_lname" type="text" class="validate">
						<label for="io_edit_users_req_lname">Surname</label>
					</div>
					<div class="input-field col s12">
						<i class="material-icons prefix">email</i>
						<input disabled id="io_edit_users_req_email" type="text" class="validate">
						<label for="io_edit_users_req_email">Email Address</label>
					</div>
					<div class="input-field col s12">
						<i class="material-icons prefix">local_offer</i>
						<input disabled id="io_edit_users_req_salref" type="text" class="validate">
						<label for="io_edit_users_req_salref">Salary Ref.</label>
					</div>
					<div class="input-field col s12">
						<i class="material-icons prefix">supervisor_account</i>
						<select disabled id="io_edit_users_req_group">
						</select>
						<label>User Type</label>
					</div>
					<!--<div class="input-field col s12">
						<i class="material-icons prefix">verified_user</i>
						<select id="io_edit_users_req_status">
							<option value="0" disabled selected></option>
							<option value="0">No Status</option>
							<option value="1">Locked</option>
						</select>
						<label>User Status</label>
					</div>-->
					<!--<div class="input-field col s12">
						<i class="material-icons prefix">lock</i>
						<select id="io_edit_users_req_enabled">
							<option value="0" disabled selected></option>
							<option value="0">Disabled</option>
							<option value="1">Enabled</option>
						</select>
						<label>User Enabled</label>
					</div>-->
				</form>
			</div>
			<div class="col s12 m2 l3">
			</div>
		</div>
		
		<div class="row">
		</div>
		<div class="row">
			<div class="collection">
				<a href="#!" class="collection-item"><b>System Information</b></a>
				<a href="#!" class="collection-item"><span id="users_req_inf_username" class="badge"></span>Username</a>
				<a href="#!" class="collection-item"><span id="users_req_inf_created_on" class="badge"></span>Created On</a>
				<a href="#!" class="collection-item"><span id="users_req_inf_created_by" class="badge"></span>Created By</a>
				<a href="#!" class="collection-item"><span id="users_req_inf_updated_on" class="badge"></span>Updated On</a>
				<a href="#!" class="collection-item"><span id="users_req_inf_updated_by" class="badge"></span>Updated By</a>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<!--<a class="btn-flat waves-effect waves-light left tooltipped" data-position="top" data-tooltip="Delete this user request"><i class="material-icons red-text">delete</i></a>-->
		<a href="#!" class="modal-close waves-effect waves-red btn-flat">Cancel</a>
		<a id="btn_users_req_reject" href="#!" class="modal-close waves-effect waves-red btn-flat <?php if(!grant_access([4017])){echo "hide";} ?>">Reject</a>
		<a id="btn_users_req_approve" data-token="<?php echo create_token();?>" href="#!" class="waves-effect waves-green btn-flat <?php if(!grant_access([4019])){echo "hide";} ?>">Approve</a>
	</div>
	
	
</div>