<?php
//Start session
session_start();

// Prevent caching.
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 01 Jan 1996 00:00:00 GMT');
// The JSON standard MIME header.
header('Content-type: application/json');

//Default Data
$error			= false;
$error_msg		= null;
$error_level	= null;
$error_code		= 0;

$stmt			= null;
$mysqli			= null;

$request		= null;
$io_temp		= null;
$data_user		= null;

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
if(!grant_access([4005]))
{
	error_response_json(
		"Access denied.",
		1,
		__LINE__,
		null,
		null);
}

// Empty data received in request.
if(!isset($_POST['token']))
{
	error_response_json(
		"Please login before continuing.",
		1,
		__LINE__,
		null,
		null);
}

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session_token.php';
if (!verify_token($_POST['token']))
{
	error_response_json(
		"Please login again before continuing.",
		1,
		__LINE__,
		null,
		null);
}

// Data received in request.
if(!isset($_POST['request']))
{
	error_response_json(
		"Request type not available.",
		1,
		__LINE__,
		null,
		null);
}

if(!isset($_POST['json']))
{
	error_response_json(
		"Request data not available.",
		1,
		__LINE__,
		null,
		null);
}
$data_rec = json_decode($_POST['json'], true);

//Open DB connection
include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/db_vmsx.php';

$io_request	= mysqli_real_escape_string($mysqli, stripslashes($_POST['request']));
if($io_request != "a_user_priv_update")
{
	error_response_json(
		"Invalid request.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

$io_uid	= mysqli_real_escape_string($mysqli, stripslashes($_POST['uid']));
if($io_uid == "")
{
	error_response_json(
		"User identity was undefined.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if (!(count($data_rec['user_priv'])>0))
{
	error_response_json(
		"No changes were made.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}


$sqli_priv_add = "
	INSERT INTO
		vmsx_db.users_access
	(
		uid,
		level
	)
	VALUES
	(
		?,
		?
	)
";

$sqli_priv_rem = "
	DELETE FROM
		vmsx_db.users_access
	WHERE
		users_access.uid=?
	AND
		users_access.level=?
	";

$stmt_add = $mysqli->prepare($sqli_priv_add);
$stmt_rem = $mysqli->prepare($sqli_priv_rem);


foreach($data_rec['user_priv'] as $key => $value)
{
	//$data_rec['group_priv'][$key]
	$io_temp = intval($key);
	
	if($value[0]==true && $value[1]==true)
	{
		$stmt_add->bind_param("ii", $io_uid, $io_temp);
		if(!($stmt_add->execute()))
		{
			if(isset($stmt_add) && $stmt_add != null){$stmt_add->close();}
			if(isset($stmt_rem) && $stmt_rem != null){$stmt_rem->close();}
			error_response_json(
				"Could not complete updating the user privileges.",
				1,
				__LINE__,
				null,
				$mysqli);
		}
		$data_user[] = array("level" => $io_temp, "checked" => $value[0]);
	}
	else if($value[0]==false && $value[1]==true)
	{
		$stmt_rem->bind_param("ii", $io_uid, $io_temp);
		if(!($stmt_rem->execute()))
		{
			if(isset($stmt_add) && $stmt_add != null){$stmt_add->close();}
			if(isset($stmt_rem) && $stmt_rem != null){$stmt_rem->close();}
			error_response_json(
				"Could not complete updating the user privileges.",
				1,
				__LINE__,
				null,
				$mysqli);
		}
		$data_user[] = array("level" => $io_temp, "checked" => $value[0]);
	}
}


//Close db connections
if(isset($stmt_add) && $stmt_add != null){$stmt_add->close();}
if(isset($stmt_rem) && $stmt_rem != null){$stmt_rem->close();}
if(isset($stmt) && $stmt != null){$stmt->close();}
if(isset($mysqli) && $mysqli){$mysqli->close();}

//create a JSON data structure.
$data = array (	
	'errors' 			=> $error,
	'error_msg'			=> $error_msg,
	'error_level'		=> $error_level,
	'error_code'		=> $error_code,
	'session_timeout'	=> $_SESSION['timeout'],
	'user'				=> $data_user
);

//Respond to request.
echo json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
?>