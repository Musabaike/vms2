<?php
//Start session
session_start();

// Prevent caching.
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 01 Jan 1996 00:00:00 GMT');
// The JSON standard MIME header.
header('Content-type: application/json');

//Default Data
$error			= false;
$error_msg		= null;
$error_level	= null;
$error_code		= 0;
$verified		= null;

$request		= null;
$io_uname		= null;
$io_pword		= null;

$data_user_levels	= null;
$data_group_levels	= null;
$data_user_gid		= null;


include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
if(!grant_access([4003]))
{
	error_response_json(
		"Access denied.",
		1,
		__LINE__,
		null,
		null);
}

// Empty data received in request.
if(!isset($_POST['token']))
{
	error_response_json(
		"Please login before continuing.",
		1,
		__LINE__,
		null,
		null);
}

// Data received in request.
if(!isset($_POST['request']))
{
	error_response_json(
		"Request data not available.",
		1,
		__LINE__,
		null,
		null);
}

if(!isset($_POST['uid']))
{
	error_response_json(
		"Request data not available.",
		1,
		__LINE__,
		null,
		null);
}

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session_token.php';
if (!verify_token($_POST['token']))
{
	error_response_json(
		"Please login again before continuing.",
		1,
		__LINE__,
		null,
		null);
}

//Open DB connection
include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/db_vmsx.php';

// Protect from MySQL injection
$io_request	= mysqli_real_escape_string($mysqli, stripslashes($_POST['request']));
$io_uid	= mysqli_real_escape_string($mysqli, stripslashes($_POST['uid']));

if($io_request == "")
{
	error_response_json(
		"Invalid request.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($io_request != "user_priv")
{
	error_response_json(
		"Invalid request.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($io_uid == "")
{
	error_response_json(
		"User selection undefined.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}


// SQL query to fetch user privileges
$sqli_user_info = "
	SELECT
		level
	FROM
		vmsx_db.users_access
	WHERE
		users_access.uid=?
	";
$stmt = $mysqli->prepare($sqli_user_info);
$stmt->bind_param("i", $io_uid);
$stmt->execute();
if(!($result = $stmt->get_result()))
{
	error_response_json(
		"Could not connect to the database.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

while($row = $result->fetch_assoc())
{
	$data_user_levels[]	= $row;
}

//Get the group ID that the user belongs to
$sqli_user_info = "
	SELECT
		gid
	FROM
		vmsx_db.users
	WHERE
		users.id=?
	";
$stmt = $mysqli->prepare($sqli_user_info);
$stmt->bind_param("i", $io_uid);
$stmt->execute();
if(!($result = $stmt->get_result()))
{
	error_response_json(
		"Could not connect to the database.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}
$num_rows = $result->num_rows;
if($num_rows === 1)
{
	while($row = $result->fetch_assoc())
	{
		$data_user_gid	= intval($row['gid']);
	}

	// SQL query to fetch user privileges
	$sqli_user_info = "
		SELECT
			level
		FROM
			vmsx_db.groups_access
		WHERE
			groups_access.gid=?
		";
	$stmt = $mysqli->prepare($sqli_user_info);
	$stmt->bind_param("i", $data_user_gid);
	$stmt->execute();
	if(!($result = $stmt->get_result()))
	{
		error_response_json(
			"Could not connect to the database.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}

	while($row = $result->fetch_assoc())
	{
		$data_group_levels[]	= $row;
	}

}

//Close statment and db connections
if(isset($stmt) && $stmt != null){$stmt->close();}
if(isset($mysqli) && $mysqli){$mysqli->close();}

//create a JSON data structure.
$data = array (	
	'errors' 			=> $error,
	'error_msg'			=> $error_msg,
	'error_level'		=> $error_level,
	'error_code'		=> $error_code,
	'session_timeout'	=> $_SESSION['timeout'],
	'user_levels'		=> $data_user_levels,
	'user_gid'			=> $data_user_gid,
	'group_levels'		=> $data_group_levels
);

//Respond to request.
echo json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
?>