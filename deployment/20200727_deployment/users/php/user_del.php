<?php
//Start session
session_start();

// Prevent caching.
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 01 Jan 1996 00:00:00 GMT');
// The JSON standard MIME header.
header('Content-type: application/json');

//Default Data
$error			= false;
$error_msg		= null;
$error_level	= null;
$error_code		= 0;

$io_request		= null;
$io_uid			= null;

$data_user		= null;

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
if(!grant_access([4006]))
{
	error_response_json(
		"Access denied.",
		1,
		__LINE__,
		null,
		null);
}

// Empty data received in request.
if(!isset($_POST['token']))
{
	error_response_json(
		"Please login before continuing.",
		1,
		__LINE__,
		null,
		null);
}

// Data received in request.
if(!isset($_POST['request']))
{
	error_response_json(
		"Request data not available.",
		1,
		__LINE__,
		null,
		null);
}

if(!isset($_POST['uid']))
{
	error_response_json(
		"Request data not available.",
		1,
		__LINE__,
		null,
		null);
}

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session_token.php';
if (!verify_token($_POST['token']))
{
	error_response_json(
		"Please login again before continuing.",
		1,
		__LINE__,
		null,
		null);
}

//Open DB connection
include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/db_vmsx.php';

// Protect from MySQL injection
$io_request	= mysqli_real_escape_string($mysqli, stripslashes($_POST['request']));
$io_uid	= mysqli_real_escape_string($mysqli, stripslashes($_POST['uid']));

if($io_request == "")
{
	error_response_json(
		"Invalid request.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($io_request != "a_user_remove")
{
	error_response_json(
		"Invalid request.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($io_uid == "")
{
	error_response_json(
		"User selection undefined.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($io_uid == 1)
{	//SYSTEM user account cannot be deleted.
	error_response_json(
		"The SYSTEM user cannot be deleted.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($io_uid == $_SESSION['uid'])
{	//Own user account cannot be deleted.
	error_response_json(
		"You cannot delete your own account.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

//Step 1: Copy the table row from users to users_del.
//Step 2: If successful, delete the entry from the users table.
//Step 3: respond to request with actions taken.


// SQL query to copy user info
$sqli_user_info = "
	INSERT INTO
		vmsx_db.users_del
	SELECT
		users.*
	FROM
		vmsx_db.users
	WHERE
		users.id=?
	";
$stmt = $mysqli->prepare($sqli_user_info);
$stmt->bind_param("i", $io_uid);

if(!($stmt->execute()))
{
	error_response_json(
		"Could not delete the user account.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

$sqli_user_info = "
	UPDATE
		vmsx_db.users_del
	SET
		users_del.deleted_by=?,
		users_del.deleted_on=NOW()
	WHERE
		users_del.id=?;
	";
$stmt = $mysqli->prepare($sqli_user_info);
$stmt->bind_param("ii", $_SESSION['uid'], $io_uid);

if(!($stmt->execute()))
{
	error_response_json(
		"Could not delete the distributor.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

// SQL query to copy user info
$sqli_user_info = "
	DELETE FROM
		vmsx_db.users
	WHERE
		users.id=?
	";
$stmt = $mysqli->prepare($sqli_user_info);
$stmt->bind_param("i", $io_uid);

if(!($stmt->execute()))
{
	error_response_json(
		"Could not delete the user account.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

$data_user = "The user account was deleted";

//Close statment and db connections
if(isset($stmt) && $stmt != null){$stmt->close();}
if(isset($mysqli) && $mysqli){$mysqli->close();}

//create a JSON data structure.
$data = array (	
	'errors' 			=> $error,
	'error_msg'			=> $error_msg,
	'error_level'		=> $error_level,
	'error_code'		=> $error_code,
	'session_timeout'	=> $_SESSION['timeout'],
	'user'				=> $data_user
);

//Respond to request.
echo json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
?>