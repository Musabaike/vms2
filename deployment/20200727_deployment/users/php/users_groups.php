<?php
//Start session
session_start();

// Prevent caching.
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 01 Jan 1996 00:00:00 GMT');
// The JSON standard MIME header.
header('Content-type: application/json');

//Default Data
$error			= false;
$error_msg		= null;
$error_level	= null;
$error_code		= 0;
$verified		= null;

$request		= null;
$io_uname		= null;
$io_pword		= null;

$data_users_groups		= null;


include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
if(!grant_access([4003]))
{
	error_response_json(
		true,
		"Access denied.",
		"Oops",
		1,
		"eid_DEN",
		null,
		null);
}

// Empty data received in request.
if(empty($_POST['token']))
{
	error_response_json(
		true,
		"Please login before continuing.",
		"Oops",
		1,
		"eid_000",
		null,
		null);
}

// Data received in request.
if(empty($_POST['request']))
{
	error_response_json(
		true,
		"Request data not available.",
		"Oops",
		1,
		"eid_010",
		null,
		null);
}

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session_token.php';
if (!verify_token($_POST['token']))
{
	error_response_json(
		true,
		"Please login again before continuing.",
		"Oops",
		1,
		"eid_TOK",
		null,
		null);
}

//Open DB connection
include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/db_vmsx.php';

// Protect from MySQL injection
$io_request	= mysqli_real_escape_string($mysqli, stripslashes($_POST['request']));

if($io_request == "")
{
	error_response_json(
		true,
		"Invalid request.",
		"Oops",
		1,
		"eid_020",
		$stmt,
		$mysqli);
}

if($io_request != "all_users_groups")
{
	error_response_json(
		true,
		"Invalid request.",
		"Oops",
		1,
		"eid_020",
		$stmt,
		$mysqli);
}


// SQL query to fetch user info
$sqli_group_info = "
	SELECT
		main.*,
		extra.username AS updated_by_uname
	FROM
		vmsx_db.users_groups main
	LEFT JOIN
		vmsx_db.users extra
	ON
		main.updated_by=extra.id
	ORDER BY
		main.description
	ASC
	";
$stmt = $mysqli->prepare($sqli_group_info);
$stmt->execute();
if(!($result = $stmt->get_result()))
{
	error_response_json(
		true,
		"Could not connect to the database.",
		"Oops",
		1,
		"eid_030",
		$stmt,
		$mysqli);
}
$num_rows = $result->num_rows;
if($num_rows === 0)
{
	error_response_json(
		true,
		"There were no groups to retrieve.",
		"Oops",
		1,
		"eid_040",
		$stmt,
		$mysqli);
}

while($row = $result->fetch_assoc())
{
	$data_users_groups[]	= $row;
}

//Close statment and db connections
if(isset($stmt) && $stmt != null){$stmt->close();}
if(isset($mysqli) && $mysqli){$mysqli->close();}

//create a JSON data structure.
$data_json = array (	
	'errors' 			=> $error,
	'error_msg'			=> $error_msg,
	'error_level'		=> $error_level,
	'error_code'		=> $error_code,
	'users_groups'		=> $data_users_groups
);

//Respond to request.
echo json_encode($data_json, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
?>