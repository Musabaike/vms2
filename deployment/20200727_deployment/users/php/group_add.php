<?php
//Start session
session_start();

// Prevent caching.
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 01 Jan 1996 00:00:00 GMT');
// The JSON standard MIME header.
header('Content-type: application/json');

//Default Data
$error			= false;
$error_msg		= null;
$error_level	= null;
$error_code		= 0;
$hash			= null; //Default, no password generated
$verified		= false;

$request		= null;
$io_uname		= null;
$io_name		= null;
$io_initials	= null;
$io_surname		= null;
$io_email		= null;
$io_salref		= null;
$io_pword		= null;
$io_group		= null;
$io_status		= null;
$io_enabled		= null;

$data_group		= null;

//Access check
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
if(!grant_access([4011]))
{
	error_response_json(
		"Access denied.",
		1,
		__LINE__,
		null,
		null);
}

// No required data received in request?
if(!isset($_POST['token']))
{
	error_response_json(
		"Please login before continuing.",
		1,
		__LINE__,
		null,
		null);
}

if(!isset($_POST['request']))
{
	error_response_json(
		"Request type not available.",
		1,
		__LINE__,
		null,
		null);
}

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session_token.php';
if (!verify_token($_POST['token']))
{
	error_response_json(
		"Please login before continuing.",
		1,
		__LINE__,
		null,
		null);
}

if(!isset($_POST['data']))
{
	error_response_json(
		"No request data was received.",
		1,
		__LINE__,
		null,
		null);
}

$data_rec = json_decode($_POST['data'], true);

//Open DB connection
include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/db_vmsx.php';

// Protect from MySQL injection
$io_req		= mysqli_real_escape_string($mysqli, stripslashes($_POST['request']));
$io_name	= mysqli_real_escape_string($mysqli, stripslashes($data_rec['group']['name']));
$io_descr	= mysqli_real_escape_string($mysqli, stripslashes($data_rec['group']['description']));


//Required fields check
if($io_req != "group_add")
{
	error_response_json(
		"Invalid request type.",
		1,
		__LINE__,
		null,
		$mysqli);
}

if($io_name == "")
{
	error_response_json(
		"No group name was specified.",
		1,
		__LINE__,
		null,
		$mysqli);
}

// SQL query: Add user to user_req table
$sqli_query = "
	INSERT INTO
		vmsx_db.groups
	(
		name,
		description,
		created_by
	)
	VALUES
	(
		?,
		?,
		?
	)
";

try
{
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param("ssi",
		$io_name,
		$io_descr,
		$_SESSION['uid']);
	$stmt->execute();
}
catch (Exception $e)
{
	if ($mysqli->errno === 1062)
	{
		error_response_json(
		"The group name already exists.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
	}
	
	error_response_json(
		"Could not connect to database.".$e->getMessage(),
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

// SQL query to fetch user info
$sqli_query = "
	SELECT
		main.*,
		extra.username AS updated_by_uname,
		extra1.username AS created_by_uname
	FROM
		vmsx_db.groups main
	LEFT JOIN
		vmsx_db.users extra
	ON
		main.updated_by=extra.id
	LEFT JOIN
		vmsx_db.users extra1
	ON
		main.created_by=extra1.id
	
	WHERE
		(main.name=?)
	ORDER BY
		main.name
	ASC
	";
$stmt = $mysqli->prepare($sqli_query);
$stmt->bind_param("s",$io_name);
$stmt->execute();
if(!($result = $stmt->get_result()))
{
	error_response_json(
		"Could not connect to the database.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}
$num_rows = $result->num_rows;
if($num_rows === 0)
{
	error_response_json(
		"The created group could not be retrieved.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

while($row = $result->fetch_assoc())
{
	$data_group[]	= $row;
}


//Close statment and db connections
if(isset($stmt)){$stmt->close();};
if(isset($mysqli)){$mysqli->close();};

//create a JSON data structure.
$data_json = array (	
	'errors' 			=> $error,
	'error_msg'			=> $error_msg,
	'error_level'		=> $error_level,
	'error_code'		=> $error_code,
	'session_timeout'	=> $_SESSION['timeout'],
	'group'				=> $data_group
);

//Respond to request.
echo json_encode($data_json, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
?>