<?php
//Start session
session_start();

// Prevent caching.
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 01 Jan 1996 00:00:00 GMT');
// The JSON standard MIME header.
header('Content-type: application/json');

//Default Data
$error			= false;
$error_msg		= null;
$error_level	= null;
$error_code		= 0;

$io_request		= null;
$io_urid			= null;

$data_user		= null;

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
if(!grant_access([4017]))
{
	error_response_json(
		"Access denied.",
		1,
		__LINE__,
		null,
		null);
}

// Empty data received in request.
if(!isset($_POST['token']))
{
	error_response_json(
		"Please login before continuing.",
		1,
		__LINE__,
		null,
		null);
}

// Data received in request.
if(!isset($_POST['request']))
{
	error_response_json(
		"Request data not available.",
		1,
		__LINE__,
		null,
		null);
}

if(!isset($_POST['urid']))
{
	error_response_json(
		"Request data not available.",
		1,
		__LINE__,
		null,
		null);
}

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session_token.php';
if (!verify_token($_POST['token']))
{
	error_response_json(
		"Please login again before continuing.",
		1,
		__LINE__,
		null,
		null);
}

//Open DB connection
include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/db_vmsx.php';

// Protect from MySQL injection
$io_request	= mysqli_real_escape_string($mysqli, stripslashes($_POST['request']));
$io_urid	= mysqli_real_escape_string($mysqli, stripslashes($_POST['urid']));

if($io_request == "")
{
	error_response_json(
		"Invalid request.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($io_request != "a_users_req_added")
{
	error_response_json(
		"Invalid request.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($io_urid == "")
{
	error_response_json(
		"User selection undefined.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}


// SQL query to copy user info
$sqli_user_info = "
	INSERT INTO
		vmsx_db.users
		(
			username,
			name,
			initials,
			surname,
			email,
			gid,
			password,
			enabled,
			salref,
			requested_on,
			created_by
		)
	SELECT
		username,
		name,
		initials,
		surname,
		email,
		gid,
		password,
		enabled,
		salref,
		requested_on,
		?
	FROM
		vmsx_db.users_req
	WHERE
		users_req.id=?
	";
$stmt = $mysqli->prepare($sqli_user_info);
$stmt->bind_param("ii", $_SESSION['uid'], $io_urid);

if(!($stmt->execute()))
{
	error_response_json(
		"Could not create the user account. Please try again.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

//need to get the new ID from the user table

/*$sqli_user_info = "
	UPDATE
		vmsx_db.users
	SET
		users.created_by=?
	WHERE
		users.id=?;
	";
$stmt = $mysqli->prepare($sqli_user_info);
$stmt->bind_param("ii", $_SESSION['uid'], $io_uid);

if(!($stmt->execute()))
{
	error_response_json(
		"Could not delete the distributor.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}*/

// SQL query to copy user info
$sqli_user_info = "
	DELETE FROM
		vmsx_db.users_req
	WHERE
		users_req.id=?
	";
$stmt = $mysqli->prepare($sqli_user_info);
$stmt->bind_param("i", $io_urid);

if(!($stmt->execute()))
{
	error_response_json(
		"Could not delete the user request. Please manually reject it after confirming the user exists.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

$data_user = "The user account was approved. The user account must still be enabled .";

//Send emails to user
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/email/email_req.php';
#$emailed_user = email_user_approve($data_user_info[], $io_name, $io_surname, $io_email);
$submitted = true;

//Close statment and db connections
if(isset($stmt) && $stmt != null){$stmt->close();}
if(isset($mysqli) && $mysqli){$mysqli->close();}

//create a JSON data structure.
$data = array (	
	'errors' 			=> $error,
	'error_msg'			=> $error_msg,
	'error_level'		=> $error_level,
	'error_code'		=> $error_code,
	'session_timeout'	=> $_SESSION['timeout'],
	'emailed_user'                  => $emailed_user,
	'user'				=> $data_user
);

//Respond to request.
echo json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
?>
