<?php
//Start session
session_start();

// Prevent caching.
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 01 Jan 1996 00:00:00 GMT');
// The JSON standard MIME header.
header('Content-type: application/json');

//Default Data
$error			= false;
$error_msg		= null;
$error_level	= null;
$error_code		= 0;
$verified		= null;

$request		= null;
$io_uname		= null;
$io_pword		= null;

$data_user_ses	= null;


include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
if(!grant_access([4007]))
{
	error_response_json(
		"Access denied.",
		1,
		__LINE__,
		null,
		null);
}

// Empty data received in request.
if(!isset($_POST['token']))
{
	error_response_json(
		"Please login before continuing.",
		1,
		__LINE__,
		null,
		null);
}

// Data received in request.
if(!isset($_POST['request']))
{
	error_response_json(
		"Request data not available.",
		1,
		__LINE__,
		null,
		null);
}

if(!isset($_POST['id']))
{
	error_response_json(
		"Request data not available.",
		1,
		__LINE__,
		null,
		null);
}

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session_token.php';
if (!verify_token($_POST['token']))
{
	error_response_json(
		"Please login again before continuing.",
		1,
		__LINE__,
		null,
		null);
}

//Open DB connection
include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/db_vmsx.php';

// Protect from MySQL injection
$io_request	= mysqli_real_escape_string($mysqli, stripslashes($_POST['request']));
$io_id	= mysqli_real_escape_string($mysqli, stripslashes($_POST['id']));

if($io_request == "")
{
	error_response_json(
		"Invalid request.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($io_request != "user_ses_rem")
{
	error_response_json(
		"Invalid request.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($io_id == "")
{
	error_response_json(
		"Session selection undefined.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}


// SQL query to fetch user sessions info
$sqli_ses_info = "
	SELECT
		*
	FROM
		vmsx_db.users_sessions
	WHERE
		users_sessions.id=?
	";
$stmt = $mysqli->prepare($sqli_ses_info);
$stmt->bind_param("i", $io_id);
$stmt->execute();
if(!($result = $stmt->get_result()))
{
	error_response_json(
		"Could not connect to the database.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

while($row = $result->fetch_assoc())
{
	$data_user_ses[]	= $row;
}

//Save my session ID:
$my_session_id = session_id();




if(isset($stmt)){$stmt->close();};
foreach ($data_user_ses as $key => $value) {
    //Destroy each session recorded in the database, if it still exists
	
	if ($data_user_ses[$key]["sid"] != $my_session_id)
	{
		//Set a session ID
		session_id($data_user_ses[$key]["sid"]);
		$db_session_id = session_id();
		
		//Destroy session:
		if (!(preg_match('/^[-,a-zA-Z0-9]{1,128}$/', $db_session_id) > 0))
		{
			error_response_json(
				true,
				"Could not log out due to a server error. Please try again or contact the site administrator.",
						1,
				"eid_DES",
				$stmt,
				$mysqli);
		}
		
		//Destroy session and associated variables
		session_unset();
		session_destroy();
		session_start();

		//Remove session from database for control and tracking purposes.
		$sql_user_session_del = "
			DELETE FROM
				vmsx_db.users_sessions
			WHERE
				id=?
			";

		$stmt = $mysqli->prepare($sql_user_session_del);
		$stmt->bind_param("s", $io_id);
		if(!$stmt->execute())
		{
			error_response_json(
				true,
				"Could not remove record from the database, however, the session was logged out. Please try again or contact the site administrator.",
						1,
				"eid_DES",
				$stmt,
				$mysqli);
		}
		
	}
}





//Restore my session ID:
session_id($my_session_id);

//Close statment and db connections
if(isset($stmt) && $stmt != null){$stmt->close();}
if(isset($mysqli) && $mysqli){$mysqli->close();}

//create a JSON data structure.
$data = array (	
	'errors' 			=> $error,
	'error_msg'			=> $error_msg,
	'error_level'		=> $error_level,
	'error_code'		=> $error_code,
	'session_timeout'	=> $_SESSION['timeout'],
	'user_sessions'		=> $data_user_ses
);

//Respond to request.
echo json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
?>