<?php
//Start session
session_start();

// Prevent caching.
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 01 Jan 1996 00:00:00 GMT');
// The JSON standard MIME header.
header('Content-type: application/json');

//Default Data
$error			= false;
$error_msg		= null;
$error_level	= null;
$error_code		= 0;

$mysqli			= null;
$stmt			= null;

$io_request		= null;
$io_name		= null;
$io_description	= null;
$io_temp		= null;

$data_group		= null;


include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
if(!grant_access([4012]))
{
	error_response_json(
		"Access denied.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

// Empty data received in request.
if(!isset($_POST['token']))
{
	error_response_json(
		"Please login before continuing.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session_token.php';
if (!verify_token($_POST['token']))
{
	error_response_json(
		"Please login again before continuing.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

// Data received in request.
if(!isset($_POST['request']))
{
	error_response_json(
		"Request type not available.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if(!isset($_POST['data']))
{
	error_response_json(
		"Request data not available.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}
$data_rec = json_decode($_POST['data'], true);

//Open DB connection
include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/db_vmsx.php';

$io_request	= mysqli_real_escape_string($mysqli, stripslashes($_POST['request']));
if($io_request == "" || $io_request != "group_update")
{
	error_response_json(
		"Invalid request.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

$io_gid	= mysqli_real_escape_string($mysqli, stripslashes($data_rec['group']['gid'][0]));
if($io_gid == "" || $io_gid == "")
{
	error_response_json(
		"Group ID unknown.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($io_gid == 1)
{	//Administrators group cannot be edited.
	error_response_json(
		"You cannot make changes to the Administrators group information.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($io_gid == 2)
{	//Power Users group cannot be edited.
	error_response_json(
		"You cannot make changes to the Power Users group information.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($io_gid == 3)
{	//VMS Web Users group cannot be edited.
	error_response_json(
		"You cannot make changes to the VMS Web Users group information.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

$io_name = mysqli_real_escape_string($mysqli, stripslashes($data_rec['group']['name'][0]));
if($io_name == "" || $io_name == null)
{
	error_response_json(
		"Please provide a group name.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

$io_description = mysqli_real_escape_string($mysqli, stripslashes($data_rec['group']['description'][0]));

if($data_rec['group']['name'][1])
{
	
	// SQL update query
	$sqli_query = "
		UPDATE
			vmsx_db.groups 
		SET 
			name = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			groups.id = ?
	";
	try
	{
		$stmt = $mysqli->prepare($sqli_query);
		$stmt->bind_param( "sis", $io_name, $_SESSION['uid'], $io_gid);
		$exe_result = $stmt->execute();
	}
	catch (Exception $e)
	{
		if ($mysqli->errno === 1062)
		{
			error_response_json(
			"The group name already exists.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
		}
		
		error_response_json(
			"Could not connect to database. ".$e->getMessage(),
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
	if(isset($stmt) && $stmt != null){$stmt->close();}
}

if($data_rec['group']['description'][1])
{
	
	// SQL update query
	$sqli_query = "
		UPDATE
			vmsx_db.groups 
		SET 
			description = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			groups.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param( "sis", $io_description, $_SESSION['uid'], $io_gid);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update Group Description.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
	if(isset($stmt) && $stmt != null){$stmt->close();}
}



// SQL query to fetch user info
$sqli_query = "
	SELECT
		main.*,
		extra.username AS updated_by_uname,
		extra1.username AS created_by_uname
	FROM
		vmsx_db.groups main
	LEFT JOIN
		vmsx_db.users extra
	ON
		main.updated_by=extra.id
	LEFT JOIN
		vmsx_db.users extra1
	ON
		main.created_by=extra1.id
	WHERE
		main.id=?
	";
$stmt = $mysqli->prepare($sqli_query);
$stmt->bind_param( "i", $io_gid);
$exe_result = $stmt->execute();
if(!$exe_result)
{
	error_response_json(
		"Could not update the password.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}
	
if(!($result = $stmt->get_result()))
{
	error_response_json(
		"Could not connect to the database.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}
$num_rows = $result->num_rows;
if($num_rows === 0)
{
	error_response_json(
		"Could not retrieve user information.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

while($row = $result->fetch_assoc())
{
	$data_group		= $row;
}

//Close db connections
if(isset($stmt) && $stmt != null){$stmt->close();}
if(isset($mysqli) && $mysqli){$mysqli->close();}

//create a JSON data structure.
$data = array (	
	'errors' 			=> $error,
	'error_msg'			=> $error_msg,
	'error_level'		=> $error_level,
	'error_code'		=> $error_code,
	'session_timeout'	=> $_SESSION['timeout'],
	'group'				=> $data_group
);

//Respond to request.
echo json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
?>