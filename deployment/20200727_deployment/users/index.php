<?php
	include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session.php';
	include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
	include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
	if(!grant_access([4000]))
	{
		header('Location: /en/denied/');
		exit();
	}
	$page_title = "Users";
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $website_name ; if ($page_title <> ""){echo " - ".$page_title;}?></title>
		<?php
			include_once $_SERVER['DOCUMENT_ROOT'].'/configs/ui/css.php';
		?>
		
		<!--Let browser know website is optimized for mobile-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	</head>

	<body data-token="<?php echo create_token();?>">
		<header class="page-header">
			<?php
				include_once $_SERVER['DOCUMENT_ROOT'].'/configs/ui/nav.php';
			?>
		</header>
		<main class="page-main">

			<div class="container">
				<div class="row">
					<div class="col s12">
						<ul class="tabs tabs-fixed-width z-depth-1">
							<li class="tab col s4"><a id="users_tab" class="tooltipped active <?php if(!grant_access([4001])){echo "hide";} ?>" href="#tab_users" data-tooltip="Users"><b>Users </b></a></li>
							<li class="tab col s4"><a id="groups_tab" class="tooltipped <?php if(!grant_access([4008])){echo "hide";} ?>" href="#tab_groups" data-tooltip="Groups"><b>Groups </b></a></li>
							<li class="tab col s4"><a id="users_reqs_tab" class="tooltipped <?php if(!grant_access([4014])){echo "hide";} ?>" href="#tab_requests" data-tooltip="User Requests"><b>Requests </b><?php //include $_SERVER['DOCUMENT_ROOT'].'/en/users/php/users_reqs_count.php'; ?></a></li>
							
						</ul>
					</div>
					<div class="col s12">
						<div id="tab_users" class="col s12 z-depth-1 <?php if(!grant_access([4001])){echo "hide";} ?>"><?php include $_SERVER['DOCUMENT_ROOT'].'/en/users/tabs/tab_users.php'; ?></div>
						<div id="tab_groups" class="col s12 z-depth-1 <?php if(!grant_access([4008])){echo "hide";} ?>"><?php include $_SERVER['DOCUMENT_ROOT'].'/en/users/tabs/tab_groups.php'; ?></div>
						<div id="tab_requests" class="col s12 z-depth-1 <?php if(!grant_access([4014])){echo "hide";} ?>"><?php include $_SERVER['DOCUMENT_ROOT'].'/en/users/tabs/tab_requests.php'; ?></div>
						
					</div>
				</div>

			</div>
		</main>

		<?php
			//Action Button:
			//include_once $_SERVER['DOCUMENT_ROOT'].'/en/users/btn_actions.php';
			//Footer:
			include_once $_SERVER['DOCUMENT_ROOT'].'/configs/ui/footer.php';
			//Modals:
			include_once $_SERVER['DOCUMENT_ROOT'].'/en/users/modals/modal_user_add.php';
			include_once $_SERVER['DOCUMENT_ROOT'].'/en/users/modals/modal_group_add.php';
			include_once $_SERVER['DOCUMENT_ROOT'].'/en/users/modals/modal_user_edit.php';
			include_once $_SERVER['DOCUMENT_ROOT'].'/en/users/modals/modal_users_req_edit.php';
			include_once $_SERVER['DOCUMENT_ROOT'].'/en/users/modals/modal_group_edit.php';
			//Javascripts:
			include_once $_SERVER['DOCUMENT_ROOT'].'/configs/ui/js.php';
		?>
	</body>
</html>
