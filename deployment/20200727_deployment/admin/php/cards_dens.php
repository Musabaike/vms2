<?php
//Start session
session_start();

// Prevent caching.
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 01 Jan 1996 00:00:00 GMT');
// The JSON standard MIME header.
header('Content-type: application/json');

//Default Data
$error			= false;
$error_msg		= null;
$error_level	= null;
$error_code		= 0;
$verified		= null;

$request		= null;
$io_uname		= null;
$io_pword		= null;

$data_cards_den	= null;
$sort_column_name = "name";
$sort_order_sql	= "ASC";

$page_no = 1;
$page_limit = 100;
$page_total = 1;
$page_offset = 0;

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
if(!grant_access([3002]))
{
	error_response_json(
		"Access denied - Card Types list view is not allowed.",
		1,
		__LINE__,
		null,
		null);
}

// Empty data received in request.
if(empty($_POST['token']))
{
	error_response_json(
		"Please login before continuing.",
		1,
		__LINE__,
		null,
		null);
}

// Data received in request.
if(empty($_POST['request']))
{
	error_response_json(
		"Request data not available.",
		1,
		__LINE__,
		null,
		null);
}

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session_token.php';
if (!verify_token($_POST['token']))
{
	error_response_json(
		"Please login again before continuing.",
		1,
		__LINE__,
		null,
		null);
}

//Open DB connection
include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/db_vmsx.php';

// Protect from MySQL injection
$io_request	= mysqli_real_escape_string($mysqli, stripslashes($_POST['request']));

if($io_request == "")
{
	error_response_json(
		"Invalid request.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($io_request != "all_cards_den")
{
	error_response_json(
		"Invalid request.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

//Map the column to sort, by HTML Dom index
if(isset($_POST['sort_column']))
{
	$sort_column = intval($_POST['sort_column']);
	switch($sort_column)
	{
		case 0:
			$sort_column_name = "name";
			break;
		case 1:
			$sort_column_name = "code";
			break;
		case 2:
			$sort_column_name = "next_batch_serial_no";
			break;
		case 3:
			$sort_column_name = "next_batch_man_no";
			break;
		case 4:
			$sort_column_name = "start_serial_no";
			break;
		case 5:
			$sort_column_name = "end_serial_no";
			break;
		case 6:
			$sort_column_name = "start_man_no";
			break;
		case 7:
			$sort_column_name = "end_man_no";
			break;
		case 8:
			$sort_column_name = "updated_on";
			break;
		case 9:
			$sort_column_name = "updated_by_uname";
			break;
		default:
			$sort_column_name = "name";
	}
}

if(isset($_POST['sort_order']))
{
	$sort_order = $_POST['sort_order'];
	switch($sort_order)
	{
		case "ASC":
			$sort_order_sql = "ASC";
			break;
		case "DESC":
			$sort_order_sql = "DESC";
			break;
		default:
			$sort_order_sql = "ASC";
	}
}

$filter = "";
if(isset($_POST['filter']))
{
	//error_log("Filter received, SET",0);
	include_once $_SERVER['DOCUMENT_ROOT'].'/en/admin/php/cards_den_filter.php';
}

if(isset($_POST['page_no']) && isset($_POST['page_limit']))
{	//Enable paging
	$page_total_sql = "SELECT
		COUNT(*) AS count
	FROM
	(
		SELECT
			main.name,
			main.code,
			main.next_batch_serial_no,
			main.next_batch_man_no,
			main.start_serial_no,
			main.end_serial_no,
			main.start_man_no,
			main.end_man_no,
			main.updated_on,
			extra.username AS updated_by_uname
		FROM
			vmsx_db.cards_den main
		LEFT JOIN
			vmsx_db.users extra
		ON
			main.updated_by=extra.id
	) filter_tbl
	$filter";
	//error_log('_#_#_#_#_#_'.$page_total_sql, 0);
	include_once $_SERVER['DOCUMENT_ROOT'].'/en/admin/php/cards_paging_conditional.php';
}

// SQL query to fetch user info
$sqli_cards_den_info = "SELECT
		filter_tbl.*
	FROM
	(
		SELECT
			main.name,
			main.code,
			main.next_batch_serial_no,
			main.next_batch_man_no,
			main.start_serial_no,
			main.end_serial_no,
			main.start_man_no,
			main.end_man_no,
			main.updated_on,
			extra.username AS updated_by_uname
		FROM
			vmsx_db.cards_den main
		LEFT JOIN
			vmsx_db.users extra
		ON
			main.updated_by=extra.id
	) filter_tbl
	$filter
	ORDER BY
		$sort_column_name
	$sort_order_sql
	LIMIT ?, ?
	";
$stmt = $mysqli->prepare($sqli_cards_den_info);
$stmt->bind_param( "ii", $page_offset, $page_limit);
$stmt->execute();
if(!($result = $stmt->get_result()))
{
	error_response_json(
		"Could not connect to the database.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

while($row = $result->fetch_assoc())
{
	$data_cards_den[]	= $row;
}

//Close statment and db connections
if(isset($stmt) && $stmt != null){$stmt->close();}
if(isset($mysqli) && $mysqli){$mysqli->close();}

//create a JSON data structure.
$data_json = array (	
	'errors' 			=> $error,
	'error_msg'			=> $error_msg,
	'error_level'		=> $error_level,
	'error_code'		=> $error_code,
	'session_timeout'	=> $_SESSION['timeout'],
	'page_total'		=> $page_total,
	'page_no'			=> $page_no,
	'cards_dens'		=> $data_cards_den
);

//Respond to request.
echo json_encode($data_json, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
?>
