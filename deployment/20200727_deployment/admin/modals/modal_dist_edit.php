<div id="modal_dist_edit" class="modal modal-fixed-footer">
	<div class="modal-content">
		<h4>Distributor:  <span class="right" id="dist_edit_agency">Unknown</span></h4>
		<div class="row">
			<div class="col s12">
				<div class="row">
					<div class="col s12">
						<ul id="tab_order_edit" class="tabs">
							<li class="tab col s4"><a id="dist_inf_tab" class="active tooltipped" href="#tab_dist_info" data-position="top" data-tooltip="Distributor Info"><b>Information </b></a></li>
							<li class="tab col s4"><a id="dist_contacts_tab" class="tooltipped" href="#tab_dist_contacts" data-position="top" data-tooltip="Distributor Contact Details"><b>Contacts </b></a></li>
							<li class="tab col s4"><a id="dist_configs_tab" class="tooltipped" href="#tab_dist_configs" data-position="top" data-tooltip="Distributor System Configurations"><b>Configs </b></a></li>
						</ul>
					</div>
					<div id="tab_dist_info" class="col s12"><?php include $_SERVER['DOCUMENT_ROOT'].'/en/admin/tabs/tab_dist_inf.php'; ?></div>
					<div id="tab_dist_contacts" class="col s12"><?php include $_SERVER['DOCUMENT_ROOT'].'/en/admin/tabs/tab_dist_contacts.php'; ?></div>
					<div id="tab_dist_configs" class="col s12"><?php include $_SERVER['DOCUMENT_ROOT'].'/en/admin/tabs/tab_dist_configs.php'; ?></div>
				</div>
				
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<a id="btn_dists_modal_rem" class="btn-flat waves-effect waves-light left tooltipped <?php if(!grant_access([3006])){echo "hide";}?>" data-position="top" data-tooltip="Delete this distributor"><i class="material-icons red-text">delete</i></a>
		<a href="#!" class="modal-close waves-effect waves-red btn-flat">Cancel</a>
		<a id="btn_dist_update" data-token="<?php echo create_token();?>" href="#!" class="waves-effect waves-green btn-flat <?php if(!grant_access([3005])){echo "hide";}?>">Update</a>
	</div>
</div>