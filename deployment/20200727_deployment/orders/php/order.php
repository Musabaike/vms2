<?php
//Start session
session_start();

// Prevent caching.
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 01 Jan 1996 00:00:00 GMT');
// The JSON standard MIME header.
header('Content-type: application/json');

//Default Data
$error			= false;
$error_msg		= null;
$error_level	= null;
$error_code		= 0;
$verified		= null;

$request		= null;

$data_order		= null;

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
if(!grant_access([2003]))
{
	error_response_json(
		"Access denied.",
		1,
		__LINE__,
		null,
		null);
}

// Empty data received in request.
if(!isset($_POST['token']))
{
	error_response_json(
		"Please login before continuing.",
		1,
		__LINE__,
		null,
		null);
}

// Data received in request.
if(!isset($_POST['request']))
{
	error_response_json(
		"Request data not available.",
		1,
		__LINE__,
		null,
		null);
}

if(!isset($_POST['oid']))
{
	error_response_json(
		"Request data not available.",
		1,
		__LINE__,
		null,
		null);
}

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session_token.php';
if (!verify_token($_POST['token']))
{
	error_response_json(
		"Please login again before continuing.",
		1,
		__LINE__,
		null,
		null);
}

//Open DB connection
include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/db_vmsx.php';

// Protect from MySQL injection
$io_request	= mysqli_real_escape_string($mysqli, stripslashes($_POST['request']));
$io_oid	= mysqli_real_escape_string($mysqli, stripslashes($_POST['oid']));

if($io_request == "")
{
	error_response_json(
		"Invalid request.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($io_request != "a_order")
{
	error_response_json(
		"Invalid request.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($io_oid == "")
{
	error_response_json(
		"Order selection undefined.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}


// SQL query to fetch user info
$sqli_order_info = "
	SELECT
		o.id,
		o.did,
		d.agency,
		o.file_name,
		o.file_date,
		o.file_seq_no,
		o.sap_order_no,
		o.osid,
		s.status,
		o.created_by,
		c.username AS created_by_uname,
		o.created_on,
		o.updated_by,
		u.username AS updated_by_uname,
		o.updated_on,
		o.espfileinput,
		o.espfileoutput,
		o.cancelreason
	FROM
		vmsx_db.orders o
	LEFT JOIN
		(
			SELECT
				id,
				agency
			FROM vmsx_db.dists
			UNION ALL
			SELECT
				id,
				agency
			FROM vmsx_db.dists_del
		) d
	ON
		o.did=d.id
	LEFT JOIN
		(
			SELECT
				id,
				username
			FROM vmsx_db.users
			UNION ALL
			SELECT
				id,
				username
			FROM vmsx_db.users_del
		) c
	ON
		o.created_by=c.id
	LEFT JOIN
		vmsx_db.orders_status s
	ON
		o.osid=s.id
	LEFT JOIN
		(
			SELECT
				id,
				username
			FROM vmsx_db.users
			UNION ALL
			SELECT
				id,
				username
			FROM vmsx_db.users_del
		) u
	ON
		o.updated_by=u.id
	WHERE
		o.id=?
	";

	
$stmt = $mysqli->prepare($sqli_order_info);
$stmt->bind_param("i", $io_oid);
$stmt->execute();
if(!($result = $stmt->get_result()))
{
	error_response_json(
		"Could not connect to the database.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}
$num_rows = $result->num_rows;
if($num_rows === 0)
{
	error_response_json(
		"Could not retrieve order information.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

while($row = $result->fetch_assoc())
{
	$data_order		= $row;
}

//Close statment and db connections
if(isset($stmt) && $stmt != null){$stmt->close();}
if(isset($mysqli) && $mysqli){$mysqli->close();}

//create a JSON data structure.
$data = array (	
	'errors' 			=> $error,
	'error_msg'			=> $error_msg,
	'error_level'		=> $error_level,
	'error_code'		=> $error_code,
	'session_timeout'	=> $_SESSION['timeout'],
	'order'				=> $data_order
);

//Respond to request.
echo json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
?>
