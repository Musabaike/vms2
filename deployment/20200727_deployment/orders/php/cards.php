<?php
//Start session
session_start();

// Prevent caching.
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 01 Jan 1996 00:00:00 GMT');
// The JSON standard MIME header.
header('Content-type: application/json');

//Default Data
$error			= false;
$error_msg		= null;
$error_level	= null;
$error_code		= 0;

$request		= null;

$data_cards		= null;
$sort_column_name = "sap_order_no";
$sort_order_sql	= "ASC";

$page_no		= 1;
$page_limit		= 100;
$page_total		= 1;
$page_offset	= 0;

$search			= "";

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
if(!grant_access([2005]))
{
	error_response_json(
		"Access denied.",
		1,
		__LINE__,
		null,
		null);
}

// Empty data received in request.
if(empty($_POST['token']))
{
	error_response_json(
		"Please login before continuing.",
		1,
		__LINE__,
		null,
		null);
}

// Data received in request.
if(empty($_POST['request']))
{
	error_response_json(
		"Request data not available.",
		1,
		__LINE__,
		null,
		null);
}

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session_token.php';
if (!verify_token($_POST['token']))
{
	error_response_json(
		"Please login again before continuing.",
		1,
		__LINE__,
		null,
		null);
}

//Open DB connection
include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/db_vmsx.php';

// Protect from MySQL injection
$io_request	= mysqli_real_escape_string($mysqli, stripslashes($_POST['request']));

if($io_request == "")
{
	error_response_json(
		"Invalid request.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($io_request != "all_cards")
{
	error_response_json(
		"Invalid request.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

//Map the column to sort, by HTML Dom index
if(isset($_POST['sort_column']))
{
	$sort_column = intval($_POST['sort_column']);
	switch($sort_column)
	{
		case 0:
			$sort_column_name = "sap_order_no";
			break;
		case 1:
			$sort_column_name = "serial";
			break;
		case 2:
			$sort_column_name = "card";
			break;
		case 3:
			$sort_column_name = "agency";
			break;
		case 4:
			$sort_column_name = "effdt";
			break;
		case 5:
			$sort_column_name = "vtype";
			break;
		case 5:
			$sort_column_name = "updated_on";
			break;
		case 6:
			$sort_column_name = "updated_by_uname";
			break;
		default:
			$sort_column_name = "sap_order_no";
	}
}

if(isset($_POST['sort_order']))
{
	$sort_order = $_POST['sort_order'];
	switch($sort_order)
	{
		case "ASC":
			$sort_order_sql = "ASC";
			break;
		case "DESC":
			$sort_order_sql = "DESC";
			break;
		default:
			$sort_order_sql = "DESC";
	}
}

if(isset($_POST['search']))
{	//Enable Searching
	include_once $_SERVER['DOCUMENT_ROOT'].'/en/orders/php/cards_search.php';
}

if(isset($_POST['page_no']) && isset($_POST['page_limit']))
{	//Enable paging
	$page_total_sql	= "SELECT COUNT(*) FROM vmsx_db.cards e LEFT JOIN
		vmsx_db.orders o
	ON
		e.oid=o.id $search";
	include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/common/paging_conditional.php';
}

// SQL query to fetch user info
$sqli_order_info = "
	SELECT
		e.id,
		e.card,
		e.serial,
		e.oid,
		vt.vtype,
		o.file_date,
		o.sap_order_no,
		e.did,
		d.agency,
		e.effdt,
		e.rechgallow,
		e.updated_by,
		u.username AS updated_by_uname,
		e.updated_on
	FROM
		vmsx_db.cards e
	LEFT JOIN
		vmsx_db.users u
	ON
		e.updated_by=u.id
	LEFT JOIN
		vmsx_db.orders o
	ON
		e.oid=o.id
	LEFT JOIN
		vmsx_db.dists d
	ON
		e.did=d.id
	LEFT JOIN
		vmsx_db.cards_types vt
	ON
		o.ctid=vt.id
	$search
	ORDER BY
		$sort_column_name
	$sort_order_sql
	LIMIT ?, ?
	";
$stmt = $mysqli->prepare($sqli_order_info);
$stmt->bind_param( "ii", $page_offset, $page_limit);
$stmt->execute();
if(!($result = $stmt->get_result()))
{
	error_response_json(
		"Could not connect to the database.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

while($row = $result->fetch_assoc())
{
	$data_cards[]		= $row;
}

//Close statment and db connections
if(isset($stmt) && $stmt != null){$stmt->close();}
if(isset($mysqli) && $mysqli){$mysqli->close();}

//create a JSON data structure.
$data = array (	
	'errors' 			=> $error,
	'error_msg'			=> $error_msg,
	'error_level'		=> $error_level,
	'error_code'		=> $error_code,
	'session_timeout'	=> $_SESSION['timeout'],
	'page_total'		=> $page_total,
	'page_no'			=> $page_no,
	'cards'				=> $data_cards
);

//Respond to request.
echo json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
?>