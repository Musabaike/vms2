<?php
$temp_error = false;
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
if(!grant_access([2000,2001,2002,2003,2004,2005]))
{
	error_response_json(
		"Access denied.",
		1,
		__LINE__,
		null,
		null);
}

//Open DB connection
include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/db_vmsx.php';

// SQL query to fetch user info
$sqli_orders_status = "
	SELECT
		cards_types.id,
		cards_types.vtype
	FROM
		vmsx_db.cards_types
	ORDER BY
		cards_types.id
	ASC
	";
$stmt = $mysqli->prepare($sqli_orders_status);
$stmt->execute();
if(!($result = $stmt->get_result()))
{
	$temp_error = true;
	echo '<option value="-1">Error1</option>';
}

if(!$temp_error)
{
	$num_rows = $result->num_rows;
	if($num_rows === 0)
	{
		$error = true;
		echo '<option value="-1">Error2</option>';
	}
}

if(!$temp_error)
{
	while($row = $result->fetch_assoc())
	{
		echo '<option value="'.$row['id'].'">'.$row['vtype'].'</option>';
	}
}
//Close statment and db connections
if($stmt)$stmt->close();
if($mysqli)$mysqli->close();

?>