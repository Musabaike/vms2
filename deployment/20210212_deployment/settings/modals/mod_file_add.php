<div id="modal_file_add" class="modal modal-fixed-footer">
	<div class="modal-content">
		<h4>Upload a new file</h4>
		<div class="row">
			<div class="col s12">
				<div class="row">
		
					<div id="tab_new_file_info" class="col s12"><?php include $_SERVER['DOCUMENT_ROOT'].'/en/settings/tabs/tab_new_file_inf.php'; ?></div>
				</div>
				
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<a href="#!" class="modal-close waves-effect waves-red btn-flat">Cancel </a>
		<a id="mod_file_btn_add" data-token="<?php echo create_token();?>" href="#!" class="waves-effect waves-green btn-flat">Upload </a>
	</div>
</div>
