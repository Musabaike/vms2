<div id="modal_order_edit" class="modal modal-fixed-footer">
	<div class="modal-content">
		<h4>SAP Order No.: <span id="order_sap_no">Unknown</span> <span class="right" id="order_edit_agency">Unknown</span></h4>
		<div class="row">
			<div class="col s12">
				<div class="row">
					<div class="col s12">
						<ul id="tab_order_edit" class="tabs">
							<li class="tab col s3"><a id="order_inf_tab" class="active tooltipped" href="#tab_order_info" data-position="top" data-tooltip="Order Info"><b>Information </b></a></li>
							<li class="tab col s3"><a id="order_items_tab" class="tooltipped" href="#tab_order_items" data-position="top" data-tooltip="Order Items"><b>Items </b></a></li>
							<li class="tab col s3"><a id="order_batches_tab" class="tooltipped" href="#tab_order_batches" data-position="top" data-tooltip="Item Batches"><b>Batches </b></a></li>
							<li class="tab col s2"><a id="order_cards_tab" class="tooltipped" href="#tab_order_cards" data-position="top" data-tooltip="Vouchers/Cards"><b><i class="material-icons inline-icon">credit_card</i> </b></a></li>
							<li class="tab col s1"><a id="order_errors_tab" class="tooltipped"href="#tab_order_errors" data-position="top" data-tooltip="Order Errors"><b><i class="material-icons inline-icon">error</i> </b></a></li>
						</ul>
					</div>
					<div id="tab_order_info" class="col s12"><?php include $_SERVER['DOCUMENT_ROOT'].'/en/orders/tabs/tab_order_inf.php'; ?></div>
					<div id="tab_order_items" class="col s12"><?php include $_SERVER['DOCUMENT_ROOT'].'/en/orders/tabs/tab_order_items.php'; ?></div>
					<div id="tab_order_batches" class="col s12"><?php include $_SERVER['DOCUMENT_ROOT'].'/en/orders/tabs/tab_order_batches.php'; ?></div>
					<div id="tab_order_cards" class="col s12"><?php include $_SERVER['DOCUMENT_ROOT'].'/en/orders/tabs/tab_order_cards.php'; ?></div>
					<div id="tab_order_errors" class="col s12"><?php include $_SERVER['DOCUMENT_ROOT'].'/en/orders/tabs/tab_order_errors.php'; ?></div>
				</div>
				
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<a href="#!" class="modal-close waves-effect waves-red btn-flat">Close</a>
	</div>
</div>