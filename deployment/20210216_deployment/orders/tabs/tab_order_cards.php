<div class="row">
	<div class="input-field col s1 m1 right">
		<a id="btn_order_cards_search" class="btn-floating waves-effect waves-light telkom-blue tooltipped right" data-position="right" data-tooltip="Search the list of cards"><i class="material-icons">search</i></a>
	</div>
	<div class="input-field col s5 m2 l3 xl2 right">
		<select multiple id="order_cards_filter_sel">
			<option value="" disabled selected>Choose Columns</option>
		</select>
		<label>Filter by columns</label>
	</div>
	<div class="input-field col  s6 m3 right">
		<i class="material-icons prefix">filter_list</i>
		<input id="order_cards_filter" type="text" class="validate">
		<label for="order_cards_filter">Filter</label>
	</div>
</div>

<div class="divider"></div>		
<div class="row">
	<div class="col s12 center">
		<table id="order_cards_tbl" class="responsive-table highlight striped">
			<thead>
				<tr>
					<th class="vms-sort">Card # <i class="material-icons tiny vms-sort"></th>
					<th class="vms-sort">Serial # <i class="material-icons tiny vms-sort"></th>
					<th class="vms-sort">Voucher <i class="material-icons tiny vms-sort"></th>
					<th class="vms-sort">Rechargeable <i class="material-icons tiny vms-sort"></th>
					<th class="vms-sort">Status <i class="material-icons tiny vms-sort"></th>
					<th class="vms-sort">Last Updated On <i class="material-icons tiny vms-sort"></th>
					<th class="vms-sort">Last Updated By <i class="material-icons tiny vms-sort"></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
<div class="row">
	<div class="input-field col s12 m10 l10">
		<ul id="page_order_cards" class="pagination">
			<li class="disabled"><a data-page="null" href="#!"><i class="material-icons">chevron_left</i></a></li>
			<li class="disabled right"><a data-page="null" href="#!"><i class="material-icons">chevron_right</i></a></li>
		</ul>
	</div>
	<div class="input-field col s3 m2 l2 right">
		<select id="page_limit_order_cards" class="right">
			<option value="2">2</option>
			<option value="5">5</option>
			<option value="10" selected>10</option>
			<option value="20">20</option>
			<option value="50">50</option>
			<option value="100">100</option>
		</select>
		<label>No. of rows</label>
	</div>
</div>
