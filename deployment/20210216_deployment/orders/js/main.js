var elems, options;
var mod_order_edit;
var orders_tabs_tooltips;
var order_status_filter;
var orders_page_no = 1;
var orders_page_limit = 10;
var orders_sort_order = "DESC";
var orders_sort_column = 0;
var order_cards_page_no = 1;
var order_cards_page_limit = 10;
var order_cards_sort_order = "DESC";
var order_cards_sort_column = 0;
var order_items_page_no = 1;
var order_items_page_limit = 10;
var order_items_sort_order = "ASC";
var order_items_sort_column = 0;
var order_batches_page_no = 1;
var order_batches_page_limit = 10;
var order_batches_sort_order = "ASC";
var order_batches_sort_column = 0;
var order_errors_page_no = 1;
var order_errors_page_limit = 10;
var order_errors_sort_order = "DESC";
var order_errors_sort_column = 0;
var cards_page_no = 1;
var cards_page_limit = 10;
var cards_sort_order = "DESC";
var cards_sort_column = 0;

var cards_search = false;
var orders_search = false;

$(document).ready(function(){
	prepareData();
	initialise_Mcss_all();
	initialise_Btns();
	initialise_Forms();
	
	//if($('orders_tab').hasClass("active")){orders_get($('body').attr('data-token'), null);}
	orders_get($('body').attr('data-token'), null);
});

//Functions
function prepareData()
{	//Prepare DOM elements for initialising
	getColumnsToFilterBy('orders_tbl',		'orders_filter_sel');
	getColumnsToFilterBy('order_items_tbl',	'order_items_filter_sel');
	getColumnsToFilterBy('order_cards_tbl',	'order_cards_filter_sel');
	getColumnsToFilterBy('order_batches_tbl',	'order_batches_filter_sel');
	getColumnsToFilterBy('order_errors_tbl','order_errors_filter_sel');
	getColumnsToFilterBy('cards_tbl',		'cards_filter_sel');
}

function initialise_Mcss_all()
{	//MaterializeCSS initializations
	//Tabs:
	elems  = document.querySelectorAll('.tabs');
	options = {
		duration: 300
	};
	tabs = M.Tabs.init(elems, options);
	
	//Button Tooltips:
	options = {
		enterDelay: 500
	};
	elems = document.querySelectorAll('.tooltipped');
    M.Tooltip.init(elems, options);
	//Tab Tooltips:
	options = {
		enterDelay: 500
	};
	elems = document.querySelectorAll('.tabs .tooltipped');
    tabs_tooltips = M.Tooltip.init(elems, options);
	
	M.updateTextFields();
	
	//Select dropdown
	elems = document.querySelectorAll('select');
	options = {};
    order_status_filter = M.FormSelect.init(elems, options);
	
	//Modals
	options = {};
	elems = document.querySelector('#modal_order_edit');
	mod_order_edit = M.Modal.init(elems, options);
}

function initialise_Btns()
{	//Initialise Button listeners
	//Search
	$('#btn_orders_search').click(function(){
		event.preventDefault();
		orders_search = true;
		orders_page_no = 1;
		orders_get($("body").attr('data-token'), null);
	});
	$('#btn_orders_search_clr').click(function(){
		event.preventDefault();
		orders_search = false;
		
		//Clear input fields
		document.getElementById("orders_date_from").value = "";
		document.getElementById("orders_date_to").value = "";
		$('#orders_status').val([]);
		$('#orders_vtype').val([]);
		document.getElementById("orders_order_no").value = "";
		$('#orders_agency').val([]);
		
		elems = document.querySelectorAll('select');
		options = {};
		M.FormSelect.init(elems, options);
		
		orders_get($("body").attr('data-token'), null);
	});
	
	$('#btn_cards_search').click(function(){
		event.preventDefault();
		cards_search = true;
		cards_page_no = 1;
		cards_get($("body").attr('data-token'), null);
	});
	$('#btn_cards_search_clr').click(function(){
		event.preventDefault();
		cards_search = false;
		
		//Clear input fields
		document.getElementById("cards_date_from").value = "";
		document.getElementById("cards_date_to").value = "";
		$('#cards_status').val([]),
		$('#cards_vtype').val([]),
		document.getElementById("cards_order_no").value = "";
		document.getElementById("cards_no").value = "";
		document.getElementById("cards_serial_no").value = "";
		$('#cards_agency').val([]);
		
		elems = document.querySelectorAll('select');
		options = {};
		M.FormSelect.init(elems, options);
		
		cards_get($("body").attr('data-token'), null);
	});
	
	//TABS refresh
	$('#orders_tab').click(function(){
		event.preventDefault();
		orders_get($("body").attr('data-token'), null);
	});
	$('#order_inf_tab').click(function(){
		event.preventDefault();
		order_get($("body").attr('data-token'), $("#modal_order_edit").attr("data-oid"));
		order_errors_get($("body").attr('data-token'), $("#modal_order_edit").attr("data-oid"),null);
	});
	$('#order_items_tab').click(function(){
		event.preventDefault();
		order_items_get($("body").attr('data-token'), $("#modal_order_edit").attr("data-oid"),null);
	});
	$('#order_batches_tab').click(function(){
		event.preventDefault();
		order_batches_get($("body").attr('data-token'), $("#modal_order_edit").attr("data-oid"),null);
	});
	$('#order_cards_tab').click(function(){
		event.preventDefault();
		order_cards_get($("body").attr('data-token'), $("#modal_order_edit").attr("data-oid"),null);
	});
	$('#order_errors_tab').click(function(){
		event.preventDefault();
		order_errors_get($("body").attr('data-token'), $('#modal_order_edit').attr('data-oid'), null);
	});
	$('#cards_tab').click(function(){
		event.preventDefault();
		cards_get($("body").attr('data-token'), null);
	});
	
	
	//Search buttons with column filter select
	$('#btn_order_cards_search').click(function(){
		event.preventDefault();
		order_cards_page_no = 1;
		order_cards_get($("body").attr('data-token'), $("#modal_order_edit").attr("data-oid"),null);
	});
	$('#btn_order_batches_search').click(function(){
		event.preventDefault();
		order_batches_page_no = 1;
		order_batches_get($("body").attr('data-token'), $("#modal_order_edit").attr("data-oid"),null);
	});
	$('#btn_order_items_search').click(function(){
		event.preventDefault();
		order_items_page_no = 1;
		order_items_get($("body").attr('data-token'), $("#modal_order_edit").attr("data-oid"),null);
	});
	$('#btn_order_errors_search').click(function(){
		event.preventDefault();
		order_errors_page_no = 1;
		order_errors_get($("body").attr('data-token'), $("#modal_order_edit").attr("data-oid"),null);
	});
	
	//Row Double Click => EDIT
	$('#orders_tbl > tbody').on("dblclick", "tr", function(){
		event.preventDefault();
		$('#modal_order_edit').attr('data-oid', $(this).attr("data-oid"));
		order_get($('body').attr('data-token'), $(this).attr("data-oid"));
		order_items_page_no = 1;
		order_items_get($('body').attr('data-token'), $(this).attr("data-oid"), null);
		order_cards_page_no = 1;
		order_cards_get($('body').attr('data-token'), $(this).attr("data-oid"), null);
		order_batches_page_no = 1;
		order_batches_get($('body').attr('data-token'), $(this).attr("data-oid"), null);
		order_errors_page_no = 1;
		order_errors_get($('body').attr('data-token'), $(this).attr("data-oid"), null);
		$(this).siblings().removeClass('vms-user-editing');
		$(this).addClass('vms-user-editing');
		mod_order_edit.open();
	});
	
	//Sort Button: i.e. Header cell
	$('#orders_tbl > thead > tr > th').click(function(){
		event.preventDefault();
		sortByColumn("#orders_tbl", this);
	});
	$('#cards_tbl > thead > tr > th').click(function(){
		event.preventDefault();
		sortByColumn("#cards_tbl", this);
	});
	$('#order_cards_tbl > thead > tr > th').click(function(){
		event.preventDefault();
		sortByColumn("#order_cards_tbl", this);
	});
	$('#order_batches_tbl > thead > tr > th').click(function(){
		event.preventDefault();
		sortByColumn("#order_batches_tbl", this);
	});
	$('#order_items_tbl > thead > tr > th').click(function(){
		event.preventDefault();
		sortByColumn("#order_items_tbl", this);
	});
	$('#order_errors_tbl > thead > tr > th').click(function(){
		event.preventDefault();
		sortByColumn("#order_errors_tbl", this);
	});

	//Cancel Order button
	$('#mod_order_edit_btn_cancel_order').click(function(){
		event.preventDefault();
		$('#modal_order_edit').attr('data-oid', $(this).attr("data-oid"));
		order_update_cancel($('body').attr('data-token'), $('#modal_order_edit').attr("data-oid"));
	
	});
}

function initialise_Forms()
{	//Initialise Form listeners
	
	M.updateTextFields();
	//Select dropdown
    order_status_filter = M.FormSelect.init(document.querySelectorAll('select'), {});
	
	//Datepicker
	if (typeof(document.getElementById('orders_date_from')) != 'undefined' && document.getElementById('orders_date_from') != null)
	{
		options = {
			format: 'yyyy-mm-dd',
			onOpen: ordersMaxFromDate
		};
		M.Datepicker.init(document.querySelector('#orders_date_from'), options);
	}
	if (typeof(document.getElementById('orders_date_to')) != 'undefined' && document.getElementById('orders_date_to') != null)
	{
		options = {
			format: 'yyyy-mm-dd',
			onOpen: ordersMinToDate
		};
		M.Datepicker.init(document.querySelector('#orders_date_to'), options);
	}
	if (typeof(document.getElementById('cards_date_from')) != 'undefined' && document.getElementById('cards_date_from') != null)
	{
		options = {
			format: 'yyyy-mm-dd',
			onOpen: cardsMaxFromDate
		};
		M.Datepicker.init(document.querySelector('#cards_date_from'), options);
	}
	if (typeof(document.getElementById('cards_date_to')) != 'undefined' && document.getElementById('cards_date_to') != null)
	{
		options = {
			format: 'yyyy-mm-dd',
			onOpen: cardsMinToDate
		};
		M.Datepicker.init(document.querySelector('#cards_date_to'), options);
	}
	
    M.Collapsible.init(document.querySelectorAll('.collapsible'), {});
	
	//Paging
	$("#page_limit_orders").change(function (){
		orders_page_no = 1;
		orders_get($("body").attr('data-token'), null);
	});
	$("#page_limit_order_cards").change(function (){
		order_cards_page_no = 1;
		order_cards_get($("body").attr('data-token'), $("#order_sap_no").attr('data-oid'), null);
	});
	$("#page_limit_order_batches").change(function (){
		order_batches_page_no = 1;
		order_batches_get($("body").attr('data-token'), $("#order_sap_no").attr('data-oid'), null);
	});
	$("#page_limit_order_items").change(function (){
		order_items_page_no = 1;
		order_items_get($("body").attr('data-token'), $("#order_sap_no").attr('data-oid'), null);
	});
	$("#page_limit_order_errors").change(function (){
		order_errors_page_no = 1;
		order_errors_get($("body").attr('data-token'), $("#order_sap_no").attr('data-oid'), null);
	});
	$("#page_limit_cards").change(function (){
		cards_page_no = 1;
		cards_get($("body").attr('data-token'), null);
	});
}


//ORDERS TAB
function orders_get(token, caller)
{	//Fetch orders in the database
	search_data = {
		enabled:		orders_search,
		date_from:		document.getElementById("orders_date_from").value,
		date_to:		document.getElementById("orders_date_to").value,
		status_filter:	$('#orders_status').val(),
		voucher_type:	$('#orders_vtype').val(),
		order_number:	document.getElementById("orders_order_no").value,
		agency:			$('#orders_agency').val()
	};
	
	//Paging
	if(caller != null)
	{
		if(caller.hasAttribute("data-page"))
		{	//Check if page change requested
			let temp_page_no = caller.getAttribute("data-page");
			if (temp_page_no != null && temp_page_no != "null")
			{	//Set new page by selection
				orders_page_no = temp_page_no;
			}
			else
			{
				return;
			}
		}
	}
	//#page_limit
	orders_page_limit = $('#page_limit_orders').val();
	
	$('#orders_tbl > tbody').text("");
	$('.progress').remove();
	$('#orders_tbl').parent().append('<div class="progress"><div class="indeterminate"></div></div>');
	$.post('/en/orders/php/orders.php',
		{
			request: 'all_orders',
			token: token,
			page_no: orders_page_no,
			page_limit: orders_page_limit,
			sort_order: orders_sort_order,
			sort_column: orders_sort_column,
			search: search_data
		},
		function(json)
		{
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				if(json['error_msg']!="Access denied."){M.toast({html: toastHTML});}
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			paging_update('page_orders', json['page_no'], json['page_total']);
			//Paging click event
			$('#page_orders li').on("click", "a", function(){
				event.preventDefault();
				orders_get($("body").attr('data-token'), this);
			});	
			orders_append('#orders_tbl', json['orders']);
		},
		'json'
	).done(function()
	{
		$('.progress').remove();
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	
	return null;
}

function orders_append(table_id, orders_array)
{	//Add users to a specified table
	if (orders_array == null) return false;
	var html = "";
	orders_array.forEach(function(item, index, data){
		html = '<tr data-oid="'+	data[index]['id'] + 
			'"><td>'+ 			(data[index]['sap_order_no']!=null?data[index]['sap_order_no']:'') +
			'</td><td>' +		(data[index]['agency']!=null?data[index]['agency']:'') + 
			'</td><td>' +		(data[index]['vtype']!=null?data[index]['vtype']:'') + 
			'</td><td>' +		(data[index]['file_date']!=null?data[index]['file_date']:'') + 
			/*'</td><td>' +		data[index]['voucher_type'] + */
			'</td><td>' +		(data[index]['status']!=null?data[index]['status']:'') + 
			'</td><td>' +		(data[index]['updated_on']!=null?data[index]['updated_on']:'') + 
			'</td><td>' +		(data[index]['updated_by_uname']!=null?data[index]['updated_by_uname']:'') + 
			'</td></tr>';
			
			$(table_id + ' > tbody').append(html);
	});
	return true;
}

function order_get(token, oid)
{	//Fetch an order in the database
	
	//Clear previous data:
	//Tab: Info
	//$('#order_sap_no').html("Unknown");
	document.getElementById('order_sap_no').innerText = "Unknown";
	document.getElementById('order_edit_agency').innerText = "Unknown";
	document.getElementById('order_inf_sap_order_no').innerText = "";
	document.getElementById('order_inf_sap_file_name').innerText = "";
	document.getElementById('order_inf_sap_date_rec').innerText = "";
	document.getElementById('order_inf_created_on').innerText = "";
	document.getElementById('order_inf_created_by').innerText = "";
	document.getElementById('order_inf_updated_on').innerText = "";
	document.getElementById('order_inf_updated_by').innerText = "";
	document.getElementById('order_inf_status').innerText = "";
	document.getElementById('order_inf_esp_file_input').innerText = "";
	document.getElementById('order_inf_esp_file_output').innerText = "";
	document.getElementById('io_cancel_reason').value = "";

	//Order progress checkboxes:
	document.getElementById("order_prog_1") != null ? document.getElementById("order_prog_1").checked = false: false;
	document.getElementById("order_prog_2") != null ? document.getElementById("order_prog_2").checked = false: false;
	document.getElementById("order_prog_3") != null ? document.getElementById("order_prog_3").checked = false: false;
	document.getElementById("order_prog_4") != null ? document.getElementById("order_prog_4").checked = false: false;
	document.getElementById("order_prog_5") != null ? document.getElementById("order_prog_5").checked = false: false;
	document.getElementById("order_prog_6") != null ? document.getElementById("order_prog_6").checked = false: false;
	document.getElementById("order_prog_7") != null ? document.getElementById("order_prog_7").checked = false: false;
	document.getElementById("order_prog_8") != null ? document.getElementById("order_prog_8").checked = false: false;
	document.getElementById("order_prog_9") != null ? document.getElementById("order_prog_9").checked = false: false;
	
	$("#order_prog_1_sp").removeClass('red-text');
	$("#order_prog_2_sp").removeClass('red-text');
	$("#order_prog_3_sp").removeClass('red-text');
	$("#order_prog_4_sp").removeClass('red-text');
	$("#order_prog_5_sp").removeClass('red-text');
	$("#order_prog_6_sp").removeClass('red-text');
	$("#order_prog_7_sp").removeClass('red-text');
	$("#order_prog_8_sp").removeClass('red-text');
	$("#order_prog_9_sp").removeClass('red-text');
	
	//Tabs:
	$('#order_items_tbl > tbody').html("");
	$('#order_batches_tbl > tbody').html("");
	$('#order_errors_tbl > tbody').html("");
	$('#order_errors_tab').removeClass("red-text");
	
	M.updateTextFields();
	
	//Retrieve new data
	$.post('/en/orders/php/order.php',
		{
			request: 'a_order',
			token: token,
			oid: oid
		},
		function(json)
		{
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
				
			//Vanilla
			document.getElementById('order_sap_no').setAttribute('data-oid', json['order']['id']);
			//document.getElementById('io_edit_order_sap').innerText = json['order']['order_number'];
			document.getElementById('order_sap_no').innerText = json['order']['sap_order_no'];
			document.getElementById('order_edit_agency').innerText = json['order']['agency'];
			//$('#order_sap_no').html(json['order']['order_number']);
			document.getElementById('order_inf_sap_order_no').innerText = json['order']['sap_order_no'];
			document.getElementById('order_inf_sap_file_name').innerText = json['order']['file_name'];
			document.getElementById('order_inf_sap_date_rec').innerText = json['order']['file_date'];
			document.getElementById('order_inf_sap_seq_no').innerText = json['order']['file_seq_no'];
			document.getElementById('order_inf_created_on').innerText = json['order']['created_on'];
			document.getElementById('order_inf_created_by').innerText = json['order']['created_by_uname'];
			document.getElementById('order_inf_updated_on').innerText = json['order']['updated_on'];
			document.getElementById('order_inf_updated_by').innerText = json['order']['updated_by_uname'];	
			document.getElementById('order_inf_status').innerText = json['order']['status'];
			document.getElementById('order_inf_esp_file_input').innerText = json['order']['espfileinput'];
			document.getElementById('order_inf_esp_file_output').innerText = json['order']['espfileoutput'];
			document.getElementById('io_cancel_reason').value = (json['order']['cancelreason']!=null?json['order']['cancelreason']:'');			
		
			order_progress_get(token, oid, document.getElementById('mod_order_edit_btn_cancel_order'));
			order_cancel_btn(token, json['order']['osid']);			
			M.updateTextFields();
			
		},
		'json'
	).done(function(json)
	{
		$('.progress').remove();
	}).fail(function(jqXHR,status,err)
	{
		$('#io_edit_user_uname').html("Error");
	}).always(function()
	{
		
	});
	
	return null;
}

function order_cancel_btn(token, osid)
{
	if (osid === 9){
		document.getElementById('mod_order_edit_btn_cancel_order').disabled =true;
		document.getElementById("order_inf_status").style.display = "block";
		document.getElementById('io_cancel_reason').disabled = true;
	}else{
		document.getElementById('mod_order_edit_btn_cancel_order').disabled =false;
		document.getElementById("order_inf_status").style.display = "none";
		document.getElementById('io_cancel_reason').disabled = false;
	}

}

function order_update_cancel(token, oid){
	
	var cancel_reason = document.getElementById('io_cancel_reason').value;

	if ( cancel_reason == ""){
		M.toast({html: "<span>Please enter the cancellation reason.</span>"});

		var elmnt = document.getElementById("io_cancel_reason");
		elmnt.scrollIntoView();
		$("#io_cancel_reason").removeClass('valid').focus();
		return null;
	}

	$.post('/en/orders/php/order_cancel.php',
		{
			request: 'cancel_order',
			token: token,
			oid: oid,
			cancel_reason: cancel_reason
		},
		function(json)
		{
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}

			//Successful response
			order_get(token, oid);
			orders_get(token, null);			
			var toastHTML = '<span>Order Cancelled.</span>';
			M.toast({html: toastHTML});
		},
		'json'
	).done(function(json)
	{
		$('.progress').remove();
	}).fail(function(jqXHR,status,err)
	{
		$('#io_edit_user_uname').html("Error");
	}).always(function()
	{
		
	});
	
	return null;

}

function order_progress_get(token, oid)
{	//Fetch an order's progress in the database
	$.post('/en/orders/php/order_progress.php',
		{
			request: 'a_order_progress',
			token: token,
			oid: oid
		},
		function(json)
		{
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			json['progress'].forEach(function(item, index, data){
				switch(data[index]['osid'])
				{
					case 1:
						document.getElementById("order_prog_1") != null ? document.getElementById("order_prog_1").checked = true: false;
						break;
					case 2:
						document.getElementById("order_prog_2") != null ? document.getElementById("order_prog_2").checked = true: false;
						break;
					case 3:
						document.getElementById("order_prog_3") != null ? document.getElementById("order_prog_3").checked = true: false;
						break;
					case 4:
						document.getElementById("order_prog_4") != null ? document.getElementById("order_prog_4").checked = true: false;
						break;
					case 5:
						document.getElementById("order_prog_5") != null ? document.getElementById("order_prog_5").checked = true: false;
						break;
					case 6:
						document.getElementById("order_prog_6") != null ? document.getElementById("order_prog_6").checked = true: false;
						break;
					case 7:
						document.getElementById("order_prog_7") != null ? document.getElementById("order_prog_7").checked = true: false;
						break;
					case 8:
						document.getElementById("order_prog_8") != null ? document.getElementById("order_prog_8").checked = true: false;
						break;
					case 9:
						document.getElementById("order_prog_9") != null ? document.getElementById("order_prog_9").checked = true: false;
						break;
					default:
					
				}
				//console.log(data[index]['status']);
			});
			
		},
		'json'
	).done(function(json)
	{
		
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	
	return null;
}

function order_cards_get(token, oid, caller)
{	//Retrieve cards for a specified order
	filter_data = {
		columns:	$('#order_cards_filter_sel').val(),
		keyword:	document.getElementById("order_cards_filter").value
	};
	if(caller != null)
	{
		if(caller.hasAttribute("data-page"))
		{	//Check if page change requested
			let temp_page_no = caller.getAttribute("data-page");
			if (temp_page_no != null && temp_page_no != "null")
			{	//Set new page by selection
				order_cards_page_no = temp_page_no;
			}
			else
			{
				return;
			}
		}
	}
	//#page_limit
	order_cards_page_limit = $('#page_limit_order_cards').val();
	
	//Clear previous data:
	//Tab: Vouchers
	//document.getElementById('order_sap_no').innerText = "Unknown";
	$('#order_cards_tbl > tbody').html("");
	
	M.updateTextFields();

console.log("order_cards_get");
console.log("request: all_order_cards, token: " +token+ ", oid: " + oid + ", page_no: " + order_cards_page_no + ", page_limit: " + order_cards_page_limit + ", sort_order: " + order_cards_sort_order + ", sort_column: " + order_cards_sort_column + ", filter: ");
console.log(filter_data);	
	//Retrieve new data
	$.post('/en/orders/php/order_cards.php',
		{
			request: 'all_order_cards',
			token: token,
			oid: oid,
			page_no: order_cards_page_no,
			page_limit: order_cards_page_limit,
			sort_order: order_cards_sort_order,
			sort_column: order_cards_sort_column,
			filter: filter_data
		},
		function(json)
		{
			paging_update('page_order_cards', json['page_no'], json['page_total']);
			//Paging click event
			$('#page_order_cards li').on("click", "a", function(){
				event.preventDefault();
				order_cards_get($("body").attr('data-token'), $("#order_sap_no").attr('data-oid'), this);
			});	
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			
			order_cards_append("#order_cards_tbl", json['cards']);
			M.updateTextFields();
//console.log(json['cards']);
		},
		'json'
	).done(function()
	{
		
		$('.progress').remove();
	}).fail(function(jqXHR,status,err)
	{
		//$('#io_edit_user_uname').html("Error");
	}).always(function()
	{
		
	});
	
	return null;
}

function order_cards_append(table_id, cards_array)
{	//Append the cards to the selected table
	if (cards_array == null) return false;
	var html = "";
	cards_array.forEach(function(item, index, data){
		html = '<tr data-cid="'+	data[index]['id'] + 
			'"><td>'+ 			(data[index]['card']!=null?data[index]['card'] + '&bull;&bull;&bull;&bull;':'') +
			'</td><td>' +		(data[index]['serial']!=null?data[index]['serial']:'') + 
			'</td><td>' +		(data[index]['denom']!=null?data[index]['denom']:'') + 
			/*'</td><td>' +		data[index]['order_number'] + 
			//'</td><td>' +		data[index]['agency_name'] + */
			//'</td><td>' +		(data[index]['effdt']!=null?data[index]['effdt']:'') + 
			'</td><td>' +		(data[index]['rechgallow']!=null?data[index]['rechgallow']:'') + 
			'</td><td>' +		(data[index]['cards_status']!=null?data[index]['cards_status']:'') + 
			'</td><td>' +		(data[index]['updated_on']!=null?data[index]['updated_on']:'') + 
			'</td><td>' +		(data[index]['updated_by_uname']!=null?data[index]['updated_by_uname']:'') + 
			'</td></tr>';
			
			$(table_id + ' > tbody').append(html);
	});
	return true;
}

function order_batches_get(token, oid, caller)
{	//Retrieve cards for a specified order
	filter_data = {
		columns:	$('#order_batches_filter_sel').val(),
		keyword:	document.getElementById("order_batches_filter").value
	};
	if(caller != null)
	{
		if(caller.hasAttribute("data-page"))
		{	//Check if page change requested
			let temp_page_no = caller.getAttribute("data-page");
			if (temp_page_no != null && temp_page_no != "null")
			{	//Set new page by selection
				order_batches_page_no = temp_page_no;
			}
			else
			{
				return;
			}
		}
	}
	//#page_limit
	order_batches_page_limit = $('#page_limit_order_batches').val();
	
	//Clear previous data:
	//Tab: Vouchers
	//document.getElementById('order_sap_no').innerText = "Unknown";
	$('#order_batches_tbl > tbody').html("");
	
	M.updateTextFields();
	
	//Retrieve new data
	$.post('/en/orders/php/order_batches.php',
		{
			request: 'all_order_batches',
			token: token,
			oid: oid,
			page_no: order_batches_page_no,
			page_limit: order_batches_page_limit,
			sort_order: order_batches_sort_order,
			sort_column: order_batches_sort_column,
			filter: filter_data
		},
		function(json)
		{
			paging_update('page_order_batches', json['page_no'], json['page_total']);
			//Paging click event
			$('#page_order_batches li').on("click", "a", function(){
				event.preventDefault();
				order_batches_get($("body").attr('data-token'), $("#order_sap_no").attr('data-oid'), this);
			});	
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			order_batches_append("#order_batches_tbl", json['batches']);
			M.updateTextFields();
		},
		'json'
	).done(function()
	{
		
		$('.progress').remove();
	}).fail(function(jqXHR,status,err)
	{
		//$('#io_edit_user_uname').html("Error");
	}).always(function()
	{
		
	});
	
	return null;
}

function order_batches_append(table_id, batches_array)
{	//Append the cards to the selected table
	if (batches_array == null) return false;
	var html = "";
	batches_array.forEach(function(item, index, data){
		html = '<tr data-obid="'+	data[index]['id'] + 
			'"><td>'+ 			(data[index]['line_item_no']!=null?data[index]['line_item_no']:'') + 
			'</td><td>' +		(data[index]['seqno']!=null?(String(data[index]['seqno'])).padStart(4, '0'):'') + 
			'</td><td>' +		(data[index]['qty']!=null?data[index]['qty']:'') + 
			'</td><td>' +		(data[index]['serial_start']!=null?data[index]['serial_start']:'') + 
			'</td><td>' +		(data[index]['serial_end']!=null?data[index]['serial_end']:'') + 
			'</td><td>' +		(data[index]['man_start']!=null?data[index]['man_start']:'') + 
			'</td><td>' +		(data[index]['man_end']!=null?data[index]['man_end']:'') + 
			'</td><td>' +		(data[index]['status']!=null?data[index]['status']:'') + 
			'</td><td>' +		(data[index]['updated_on']!=null?data[index]['updated_on']:'') + 
			'</td><td>' +		(data[index]['updated_by']!=null?data[index]['updated_by']:'') + 
			'</td></tr>';
			
			$(table_id + ' > tbody').append(html);
	});
	return true;
}

function order_items_get(token, oid, caller)
{	//Retrieve cards for a specified order
	filter_data = {
		columns:	$('#order_items_filter_sel').val(),
		keyword:	document.getElementById("order_items_filter").value
	};
	if(caller != null)
	{
		if(caller.hasAttribute("data-page"))
		{	//Check if page change requested
			let temp_page_no = caller.getAttribute("data-page");
			if (temp_page_no != null && temp_page_no != "null")
			{	//Set new page by selection
				order_items_page_no = temp_page_no;
			}
			else
			{
				return;
			}
		}
	}
	//#page_limit
	order_items_page_limit = $('#page_limit_order_items').val();
	
	//Clear previous data:
	//Tab: Vouchers
	//document.getElementById('order_sap_no').innerText = "Unknown";
	$('#order_items_tbl > tbody').html("");
	
	M.updateTextFields();
	
	//Retrieve new data
	$.post('/en/orders/php/order_items.php',
		{
			request: 'all_order_items',
			token: token,
			oid: oid,
			page_no: order_items_page_no,
			page_limit: order_items_page_limit,
			sort_order: order_items_sort_order,
			sort_column: order_items_sort_column,
			filter: filter_data
		},
		function(json)
		{
			paging_update('page_order_items', json['page_no'], json['page_total']);
			//Paging click event
			$('#page_order_items li').on("click", "a", function(){
				event.preventDefault();
				order_items_get($("body").attr('data-token'), $("#order_sap_no").attr('data-oid'), this);
			});	
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			
			order_items_append("#order_items_tbl", json['items']);
			M.updateTextFields();
		},
		'json'
	).done(function(json)
	{
		$('.progress').remove();
	}).fail(function(jqXHR,status,err)
	{
		//$('#io_edit_user_uname').html("Error");
	}).always(function()
	{
		
	});
	
	return null;
}

function order_items_append(table_id, items_array)
{	//Append the cards to the selected table
	if (items_array == null) return false;
	var html = "";
	items_array.forEach(function(item, index, data){
		html = '<tr data-cdid="'+	data[index]['id'] + 
			'"><td>'+ 			(data[index]['line_item_no']!=null?data[index]['line_item_no']:'') + 
			'</td><td>' +		(data[index]['denom']!=null?data[index]['denom']:'') + 
			'</td><td>' +		(data[index]['qty']!=null?data[index]['qty']:'') + 
			'</td><td>' +		(data[index]['serial_start']!=null?data[index]['serial_start']:'') + 
			'</td><td>' +		(data[index]['serial_end']!=null?data[index]['serial_end']:'') + 
			'</td><td>' +		(data[index]['status']!=null?data[index]['status']:'') + 
			'</td><td>' +		(data[index]['updated_on']!=null?data[index]['updated_on']:'') + 
			'</td><td>' +		(data[index]['updated_by_uname']!=null?data[index]['updated_by_uname']:'') + 
			'</td></tr>';
			
			$(table_id + ' > tbody').append(html);
	});
	return true;
}

function order_errors_get(token, oid, caller)
{	//Retrieve errors for a specified order
	filter_data = {
		columns:	$('#order_errors_filter_sel').val(),
		keyword:	document.getElementById("order_errors_filter").value
	};
	if(caller != null)
	{
		if(caller.hasAttribute("data-page"))
		{	//Check if page change requested
			let temp_page_no = caller.getAttribute("data-page");
			if (temp_page_no != null && temp_page_no != "null")
			{	//Set new page by selection
				order_errors_page_no = temp_page_no;
			}
			else
			{
				return;
			}
		}
	}
	//#page_limit
	order_errors_page_limit = $('#page_limit_order_errors').val();
	
	//Clear previous data:
	//Tab: Vouchers
	$('#order_errors_tbl > tbody').html("");
	
	M.updateTextFields();
	
	//Retrieve new data
	$.post('/en/orders/php/order_errors.php',
		{
			request: 'all_order_errors',
			token: token,
			oid: oid,
			page_no: order_errors_page_no,
			page_limit: order_errors_page_limit,
			sort_order: order_errors_sort_order,
			sort_column: order_errors_sort_column,
			filter: filter_data
		},
		function(json)
		{
			paging_update('page_order_errors', json['page_no'], json['page_total']);
			//Paging click event
			$('#page_order_errors li').on("click", "a", function(){
				event.preventDefault();
				order_errors_get($("body").attr('data-token'), $("#order_sap_no").attr('data-oid'), this);
			});
			
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			order_errors_append("#order_errors_tbl", json['order_errors']);
			
			M.updateTextFields();
		},
		'json'
	).done(function(json)
	{
		$('.progress').remove();
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	
	return null;
}

function order_errors_append(table_id, errors_array)
{	//Append the cards to the selected table
	if (errors_array == null) return false;
	var html = "";
	errors_array.forEach(function(item, index, data){
		html = '<tr data-cid="'+	data[index]['id'] + 
			'"><td>'+ 			(data[index]['created_on']!=null?data[index]['created_on']:'') + 
			'</td><td>' +		(data[index]['description']!=null?data[index]['description']:'') + 
			'</td><td>' +		(data[index]['actions']!=null?data[index]['actions']:'') + 
			'</td><td>' +		(data[index]['cleared']!=null?((data[index]['cleared'])?"Yes":"No"):'') + 
			'</td><td>' +		(data[index]['created_by_uname']!=null?data[index]['created_by_uname']:'') + 
			'</td></tr>';
			
			$(table_id + ' > tbody').append(html);
			if(!data[index]['cleared'])
			{
				$('#order_errors_tab').addClass("red-text");
				switch(data[index]['osid'])
				{
					case 1:
						document.getElementById("order_prog_1_sp") != null ? $("#order_prog_1_sp").addClass('red-text'): false;
						break;
					case 2:
						document.getElementById("order_prog_2_sp") != null ? $("#order_prog_2_sp").addClass('red-text'): false;
						break;
					case 3:
						document.getElementById("order_prog_3_sp") != null ? $("#order_prog_3_sp").addClass('red-text'): false;
						break;
					case 4:
						document.getElementById("order_prog_4_sp") != null ? $("#order_prog_4_sp").addClass('red-text'): false;
						break;
					case 5:
						document.getElementById("order_prog_5_sp") != null ? $("#order_prog_5_sp").addClass('red-text'): false;
						break;
					case 6:
						document.getElementById("order_prog_6_sp") != null ? $("#order_prog_6_sp").addClass('red-text'): false;
						break;
					case 7:
						document.getElementById("order_prog_7_sp") != null ? $("#order_prog_7_sp").addClass('red-text'): false;
						break;
					case 8:
						document.getElementById("order_prog_8_sp") != null ? $("#order_prog_8_sp").addClass('red-text'): false;
						break;
					case 9:
						document.getElementById("order_prog_9_sp") != null ? $("#order_prog_9_sp").addClass('red-text'): false;
						break;
					default:
					
				}
			}
	});
	return true;
}


//CARDS TAB
function cards_get(token, caller)
{	//Retrieve cards for all orders
	search_data = {
		enabled:		cards_search,
		date_from:		document.getElementById("cards_date_from").value,
		date_to:		document.getElementById("cards_date_to").value,
		status_filter:	$('#cards_orders_status').val(),
		voucher_type:	$('#cards_vtype').val(),
		order_number:	document.getElementById("cards_order_no").value,
		card_number:	document.getElementById("cards_no").value,
		serial_number:	document.getElementById("cards_serial_no").value,
		agency:			$('#cards_agency').val()
	};
	
	if(caller != null)
	{
		if(caller.hasAttribute("data-page"))
		{	//Check if page change requested
			let temp_page_no = caller.getAttribute("data-page");
			if (temp_page_no != null && temp_page_no != "null")
			{	//Set new page by selection
				cards_page_no = temp_page_no;
			}
			else
			{
				return;
			}
		}
	}
	//#page_limit
	cards_page_limit = $('#page_limit_cards').val();
	
	//Clear previous data:
	$('#cards_tbl > tbody').html("");
	
	M.updateTextFields();
	
//console.log("cards_get");
//console.log("request: all_cards, token: " +token+ ", page_no: " + cards_page_no + ", page_limit: " + cards_page_limit + ", sort_order: " + cards_sort_order + ", sort_column: " + cards_sort_column + ", search: ");
//console.log(search_data);

	//Retrieve new data
	$.post('/en/orders/php/cards.php',
		{
			request: 'all_cards',
			token: token,
			page_no: cards_page_no,
			page_limit: cards_page_limit,
			sort_order: cards_sort_order,
			sort_column: cards_sort_column,
			search: search_data
		},
		function(json)
		{
			paging_update('page_cards', json['page_no'], json['page_total']);
			//Paging click event
			$('#page_cards li').on("click", "a", function(){
				event.preventDefault();
				cards_get($("body").attr('data-token'), this);
			});	
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				if(json['error_msg']!="Access denied."){M.toast({html: toastHTML});}
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			
			cards_append("#cards_tbl", json['cards']);
			M.updateTextFields();
console.log( json['cards']);
		},
		'json'
	).done(function()
	{
		$('.progress').remove();
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	
	return null;
}

function cards_append(table_id, cards_array)
{	//Append the cards to the selected table
	if (cards_array == null) return false;
	var html = "";
	cards_array.forEach(function(item, index, data){
		html = '<tr data-cid="'+	data[index]['id'] + 
			'"><td>' +		(data[index]['sap_order_no']!=null?data[index]['sap_order_no']:'') + 
			'</td><td>' +		(data[index]['card']!=null?data[index]['card'] + '&bull;&bull;&bull;&bull;':'') + 
			'</td><td>' +		(data[index]['serial']!=null?data[index]['serial']:'') + 
			'</td><td>' +		(data[index]['agency']!=null?data[index]['agency']:'') + 
			//'</td><td>' +		(data[index]['file_date']!=null?data[index]['file_date']:'') + 
			'</td><td>' +		(data[index]['vtype']!=null?data[index]['vtype']:'') + 
			'</td><td>' +		(data[index]['cards_status']!=null?data[index]['cards_status']:'') +
			'</td><td>' +		(data[index]['updated_on']!=null?data[index]['updated_on']:'') + 
			'</td><td>' +		(data[index]['updated_by_uname']!=null?data[index]['updated_by_uname']:'') + 
			'</td></tr>';
			
			$(table_id + ' > tbody').append(html);
	});
	return true;
}


//MISC
/*function filterByColumns(table_id, filter_input_id, columns)
{	//Filter table contents by text input field
	var table = $(table_id + " > tbody").find("tr");
	var filter = $(filter_input_id);
	var keywords = filter.val().toUpperCase().split(" ");
	let num_cols = columns.length;

	if (keywords == "" || num_cols == 0)	//If empty search show all rows
	{
		table.show();
		return;
	}
	
	table.hide();	//Clear table
	
	table.filter(function(index){
		var row = $(this); //This table row
		var row_cells = $(this).find("td"); //Cells in this row
		var num_cell_match = 0;
		var keyword_len = keywords.length;
		
		columns.forEach(function(value, index, array)
		{
			for (var index=0; index<keyword_len; index++)
			{
				if(!(row_cells[value].innerText.toUpperCase().indexOf(keywords[index]) > -1))
				{
					
				}
				else
				{
					num_cell_match++;
				}
			}
			
		});
		
		if(num_cell_match == 0)
		{
			return false;
		}
		return true;
	}).show();//show the rows that match.
}
*/

function getColumnsToFilterBy(table_id, filter_sel_id)
{	//Adds columns' indexes and names to to the drop down filter
	var testThisFunc = document.querySelectorAll('#'+table_id+'> thead > tr > th')
	testThisFunc.forEach(function(value,index,array){
		//console.log('index: '+ index + ', Value: ' + value.innerText);
		var html = '<option value="'+index+'" selected>'+value.innerText+'</option>';
		$('#'+filter_sel_id).append(html);
	});
	
}

function sortTable(table_id, column, order)
{	//Sorting Algorithm used by sortByColumn()
	switch(table_id)
	{
		case "#orders_tbl":
			orders_sort_order = order;
			orders_sort_column = column;
			orders_get($("body").attr('data-token'), null);
			break;
		case "#order_cards_tbl":
			order_cards_sort_order = order;
			order_cards_sort_column = column;
			order_cards_get($("body").attr('data-token'), $("#order_sap_no").attr('data-oid'), null);
			break;
		case "#order_batches_tbl":
			order_batches_sort_order = order;
			order_batches_sort_column = column;
			order_batches_get($("body").attr('data-token'), $("#order_sap_no").attr('data-oid'), null);
			break;
		case "#order_items_tbl":
			order_items_sort_order = order;
			order_items_sort_column = column;
			order_items_get($("body").attr('data-token'), $("#order_sap_no").attr('data-oid'), null);
			break;
		case "#order_errors_tbl":
			order_errors_sort_order = order;
			order_errors_sort_column = column;
			order_errors_get($("body").attr('data-token'), $("#order_sap_no").attr('data-oid'), null);
			break;
		case "#cards_tbl":
			cards_sort_order = order;
			cards_sort_column = column;
			cards_get($("body").attr('data-token'), null);
			break;
		default:
	}
}

function sortByColumn(table_id, table_th)
{	//Sort a table a a specified column
	if($(table_th).find("i").hasClass("vms-sort"))
	{
		sortTable(table_id, $(table_th).index(), "ASC");
		$(table_th).find("i").removeClass("vms-sort");
		$(table_th).find("i").addClass("vms-sort-asc").text("arrow_drop_up");
		$(table_th).siblings().find("i").addClass("vms-sort").text("");
		$(table_th).siblings().find("i").removeClass("vms-sort-asc");
		$(table_th).siblings().find("i").removeClass("vms-sort-desc");
	}
	else if($(table_th).find("i").hasClass("vms-sort-asc"))
	{
		sortTable(table_id, $(table_th).index(), "DESC");
		$(table_th).find("i").removeClass("vms-sort-asc");
		$(table_th).find("i").addClass("vms-sort-desc").text("arrow_drop_down");
		$(table_th).siblings().find("i").addClass("vms-sort").text("");
		$(table_th).siblings().find("i").removeClass("vms-sort-asc");
		$(table_th).siblings().find("i").removeClass("vms-sort-desc");
	}
	else if($(table_th).find("i").hasClass("vms-sort-desc"))
	{
		sortTable(table_id, $(table_th).index(), "ASC");
		$(table_th).find("i").removeClass("vms-sort-desc");
		$(table_th).find("i").addClass("vms-sort-asc").text("arrow_drop_up");
		$(table_th).siblings().find("i").addClass("vms-sort").text("");
		$(table_th).siblings().find("i").removeClass("vms-sort-asc");
		$(table_th).siblings().find("i").removeClass("vms-sort-desc");
	}
}

function sort_clear(table_id)
{	//Clearing current sort of selected tab
	$(table_id + ' > tbody').text("");
	$(table_id).find("i").removeClass("vms-sort-asc");
	$(table_id).find("i").removeClass("vms-sort-desc");
	$(table_id).find("i").addClass("vms-sort").text("");
}

function ordersMinToDate()
{	//Calendar date picker minimum allowed date
	let minDate =  document.getElementById("orders_date_from").value;
	let instance = M.Datepicker.getInstance(document.getElementById("orders_date_to"));

	if (minDate != "")
	{
        instance.options.minDate = new Date(minDate);
	}
	else
	{
		instance.options.minDate = null;
	}
}

function ordersMaxFromDate()
{	//Calendar date picker maximum allowed date
	let maxDate =  document.getElementById("orders_date_to").value;
	let instance = M.Datepicker.getInstance(document.getElementById("orders_date_from"));

	if (maxDate != "")
	{
		var yesterday = new Date(maxDate);
		yesterday.setDate(yesterday.getDate() - 1);
        instance.options.maxDate = yesterday;
	}
	else
	{
		instance.options.maxDate = null;
	}
}

function cardsMinToDate()
{	//Calendar date picker minimum allowed date
	let minDate =  document.getElementById("cards_date_from").value;
	let instance = M.Datepicker.getInstance(document.getElementById("cards_date_to"));

	if (minDate != "")
	{
        instance.options.minDate = new Date(minDate);
	}
	else
	{
		instance.options.minDate = null;
	}
}

function cardsMaxFromDate()
{	//Calendar date picker maximum allowed date
	let maxDate =  document.getElementById("cards_date_to").value;
	let instance = M.Datepicker.getInstance(document.getElementById("cards_date_from"));

	if (maxDate != "")
	{
		var yesterday = new Date(maxDate);
		yesterday.setDate(yesterday.getDate() - 1);
        instance.options.maxDate = yesterday;
	}
	else
	{
		instance.options.maxDate = null;
	}
}

function paging_update(element_id, new_page_no, new_page_total)
{
	(new_page_no==null)?new_page_no=1:(typeof new_page_no=="undefined")?new_page_no=1:false;
	(new_page_total==null)?new_page_total=1:(typeof new_page_total=="undefined")?new_page_total=1:false;
	//var html = "";
	document.getElementById(element_id).innerHTML = "";
	//Prev button
	(
		(new_page_no > 1) ?
			$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no - 1) + '" href="#!"><i class="material-icons">chevron_left</i></a></li>') :
			$('#' + element_id).append('<li class="disabled"><a data-page="null" href="#!"><i class="material-icons">chevron_left</i></a></li>')
	);
	//First page
	(
		(new_page_no > 4) ?
			$('#' + element_id).append('<li class="waves-effect"><a data-page="1" href="#!">1</a></li>') :
			false
	);
	//Lower More = ...
	(
		(new_page_no > 5) ?
			((new_page_no == 6) ?
				$('#' + element_id).append('<li class="waves-effect"><a data-page="2" href="#!">2</a></li>') :
				$('#' + element_id).append('<li class="disabled"><a data-page="null" href="#!"><i class="material-icons">more_horiz</i></a></li>')
			) :
			false
	);
	//Current page - 3
	(
		(new_page_no > 3) ?
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no - 3) + '" href="#!">' + (new_page_no - 3) + '</a></li>') :
		false
	);
	//Current page - 2
	(
		(new_page_no > 2) ?
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no - 2) + '" href="#!">' + (new_page_no - 2) + '</a></li>') :
		false
	);
	//Current page - 1
	(
		(new_page_no > 1) ?
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no - 1) + '" href="#!">' + (new_page_no - 1) + '</a></li>') :
		false
	);
	//Current page
	(
		(new_page_no > 0) ?
		$('#' + element_id).append('<li class="active"><a data-page="null" href="#!">' + new_page_no + '</a></li>') :
		false
	);
	//Current page + 1
	(
		((new_page_total - new_page_no) > 1) ?
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no + 1) + '" href="#!">' + (new_page_no + 1) + '</a></li>') :
		false
	);
	//Current page + 2
	(
		((new_page_total - new_page_no) > 2) ?
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no + 2) + '" href="#!">' + (new_page_no + 2) + '</a></li>') :
		false
	);
	//Current page + 3
	(
		((new_page_total - new_page_no) > 3) ?
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no + 3) + '" href="#!">' + (new_page_no + 3) + '</a></li>') :
		false
	);
	//Upper More = ...
	(
		((new_page_total - new_page_no) > 4) ?
			(((new_page_total - new_page_no) == 5) ?
				$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_total - 1) + '" href="#!">' + (new_page_total - 1) + '</a></li>') :
				$('#' + element_id).append('<li class="disabled"><a data-page="null" href="#!"><i class="material-icons">more_horiz</i></a></li>')
			) :
			false
	);
	//Last page
	(
		(new_page_no >= new_page_total) ?
		true :
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_total) + '" href="#!">' + (new_page_total) + '</a></li>')
	);
	//Next button
	(
		((new_page_total - new_page_no) > 0) ?
		$('#'+element_id).append('<li class="waves-effect right"><a data-page="'+(new_page_no+1)+'" href="#!"><i class="material-icons">chevron_right</i></a></li>') :
		$('#'+element_id).append('<li class="disabled right"><a data-page="null" href="#!"><i class="material-icons">chevron_right</i></a></li>')
	);
}


