<?php
//Start session
session_start();

// Prevent caching.
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 01 Jan 1996 00:00:00 GMT');
// The JSON standard MIME header.
header('Content-type: application/json');

//Default Data
$error			= false;
$error_msg		= null;
$error_level	= null;
$error_code		= 0;
$stmt			= null;
$mysqli			= null;

$request		= null;
$io_oid			= null;

$data_batches	= null;
$sort_column_name = "line_item_no";
$sort_order_sql	= "ASC";

$page_no = 1;
$page_limit = 10;
$page_total = 1;
$page_offset = 0;

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
if(!grant_access([2003]))
{
	error_response_json(
		"Access denied.",
		1,
		__LINE__,
		null,
		null);
}

// Empty data received in request.
if(empty($_POST['token']))
{
	error_response_json(
		"Please login before continuing.",
		1,
		__LINE__,
		null,
		null);
}

// Data received in request.
if(empty($_POST['request']))
{
	error_response_json(
		"Request data not available.",
		1,
		__LINE__,
		null,
		null);
}

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session_token.php';
if (!verify_token($_POST['token']))
{
	error_response_json(
		"Please login again before continuing.",
		1,
		__LINE__,
		null,
		null);
}

//Open DB connection
include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/db_vmsx.php';

// Protect from MySQL injection
$io_request	= mysqli_real_escape_string($mysqli, stripslashes($_POST['request']));

if($io_request == "")
{
	error_response_json(
		"Invalid request.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($io_request != "all_order_batches")
{
	error_response_json(
		"Invalid request.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

$io_oid	= mysqli_real_escape_string($mysqli, stripslashes($_POST['oid']));
if($io_oid == "")
{
	error_response_json(
		"Invalid order ID.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

//Map the column to sort, by HTML Dom index
if(isset($_POST['sort_column']))
{
	$sort_column = intval($_POST['sort_column']);
	switch($sort_column)
	{
		case 0:
			$sort_column_name = "line_item_no";
			break;
		case 1:
			$sort_column_name = "seqno";
			break;
		case 2:
			$sort_column_name = "qty";
			break;
		case 3:
			$sort_column_name = "serial_start";
			break;
		case 4:
			$sort_column_name = "serial_end";
			break;
		case 5:
			$sort_column_name = "man_start";
			break;
		case 6:
			$sort_column_name = "man_end";
			break;
		case 7:
			$sort_column_name = "updated_on";
			break;
		case 8:
			$sort_column_name = "updated_by_uname";
			break;
		default:
			$sort_column_name = "line_item_no";
	}
}

if(isset($_POST['sort_order']))
{
	$sort_order = $_POST['sort_order'];
	switch($sort_order)
	{
		case "ASC":
			$sort_order_sql = "ASC";
			break;
		case "DESC":
			$sort_order_sql = "DESC";
			break;
		default:
			$sort_order_sql = "ASC";
	}
}

$filter = "";
if(isset($_POST['filter']))
{
	//error_log("Filter received, SET",0);
	include_once $_SERVER['DOCUMENT_ROOT'].'/en/orders/php/order_batches_filter.php';
}

if(isset($_POST['page_no']) && isset($_POST['page_limit']))
{	//Enable paging
	$page_total_sql = "SELECT
		COUNT(*) AS count
	FROM
	(
		SELECT
			i.line_item_no,
			b.oid,
			b.seqno,
			b.qty,
			b.serial_start,
			b.serial_end,
			b.man_start,
			b.man_end,
			b.updated_on,
			u.username AS updated_by_uname
		FROM
			vmsx_db.orders_batches b
		LEFT JOIN
			vmsx_db.orders_items i
		ON
			b.oiid=i.id
		LEFT JOIN
			vmsx_db.users u
		ON
			b.updated_by=u.id
		LEFT JOIN 
			vmsx_db.orders_status os 
		ON 
			os.id = b.osid
		WHERE
			b.oid=?
	) filter_tbl
	$filter";
	//error_log('_#_#_#_#_#_'.$page_total_sql, 0);
	include_once $_SERVER['DOCUMENT_ROOT'].'/en/orders/php/order_batches_paging_conditional.php';
}

// SQL query to fetch user info
$sqli_order_batches_info = "
	SELECT
		res_batches.*
	FROM
	(
		SELECT
			i.line_item_no,
			b.oid,
			b.seqno,
			b.qty,
			b.serial_start,
			b.serial_end,
			b.man_start,
			b.man_end,
			b.updated_on,
			u.username AS updated_by,
			os.status
		FROM
			vmsx_db.orders_batches b
		LEFT JOIN
			vmsx_db.orders_items i
		ON
			b.oiid=i.id
		LEFT JOIN
			vmsx_db.users u
		ON
			b.updated_by=u.id
		LEFT JOIN 
			vmsx_db.orders_status os 
		ON 
			os.id = b.osid
		WHERE
			b.oid=?
	) res_batches
	$filter
	ORDER BY
		$sort_column_name
	$sort_order_sql
	LIMIT ?, ?
	";
//error_log("########## ".$sqli_order_info, 0);
$stmt = $mysqli->prepare($sqli_order_batches_info);
$stmt->bind_param( "iii", $io_oid, $page_offset, $page_limit);
$stmt->execute();
if(!($result = $stmt->get_result()))
{
	error_response_json(
		"Could not connect to the database.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

while($row = $result->fetch_assoc())
{
	$data_batches[]		= $row;
}

//Close statment and db connections
if(isset($stmt) && $stmt != null){$stmt->close();}
if(isset($mysqli) && $mysqli){$mysqli->close();}

//create a JSON data structure.
$data = array (	
	'errors' 			=> $error,
	'error_msg'			=> $error_msg,
	'error_level'		=> $error_level,
	'error_code'		=> $error_code,
	'session_timeout'	=> $_SESSION['timeout'],
	'page_total'		=> $page_total,
	'page_no'			=> $page_no,
	'batches'			=> $data_batches
);

//Respond to request.
echo json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
?>
