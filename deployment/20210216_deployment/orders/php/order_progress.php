<?php
//Start session
session_start();

// Prevent caching.
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 01 Jan 1996 00:00:00 GMT');
// The JSON standard MIME header.
header('Content-type: application/json');

//Default Data
$error			= false;
$error_msg		= null;
$error_level	= null;
$error_code		= 0;
$verified		= null;

$request		= null;

$data_order_progress= null;

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
if(!grant_access([2003]))
{
	error_response_json(
		"Access denied.",
		1,
		__LINE__,
		null,
		null);
}

// Empty data received in request.
if(!isset($_POST['token']))
{
	error_response_json(
		"Please login before continuing.",
		1,
		__LINE__,
		null,
		null);
}

// Data received in request.
if(!isset($_POST['request']))
{
	error_response_json(
		"Request data not available.",
		1,
		__LINE__,
		null,
		null);
}

if(!isset($_POST['oid']))
{
	error_response_json(
		"Request data not available.",
		1,
		__LINE__,
		null,
		null);
}

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session_token.php';
if (!verify_token($_POST['token']))
{
	error_response_json(
		"Please login again before continuing.",
		1,
		__LINE__,
		null,
		null);
}

//Open DB connection
include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/db_vmsx.php';

// Protect from MySQL injection
$io_request	= mysqli_real_escape_string($mysqli, stripslashes($_POST['request']));
$io_oid	= mysqli_real_escape_string($mysqli, stripslashes($_POST['oid']));

if($io_request == "")
{
	error_response_json(
		"Invalid request.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($io_request != "a_order_progress")
{
	error_response_json(
		"Invalid request.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($io_oid == "")
{
	error_response_json(
		"Order selection undefined.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}


// SQL query to fetch user info
$sqli_order_prog = "
	SELECT
		o.id,
		o.oid,
		o.osid,
		s.status,
		o.created_by,
		u.username AS created_by_uname,
		o.created_on
	FROM
		vmsx_db.orders_progress o
	LEFT JOIN
		vmsx_db.orders_status s
	ON
		o.osid=s.id
	LEFT JOIN
		vmsx_db.users u
	ON
		o.created_by=u.id
	WHERE
		o.oid=?
	";
$stmt = $mysqli->prepare($sqli_order_prog);
$stmt->bind_param("i", $io_oid);
$stmt->execute();
if(!($result = $stmt->get_result()))
{
	error_response_json(
		"No progress as yet.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}
$num_rows = $result->num_rows;
if($num_rows === 0)
{
	error_response_json(
		"Order has not been processed yet.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

while($row = $result->fetch_assoc())
{
	$data_order_progress[]= $row;
}

//Close statment and db connections
if(isset($stmt) && $stmt != null){$stmt->close();}
if(isset($mysqli) && $mysqli){$mysqli->close();}

//create a JSON data structure.
$data = array (	
	'errors' 			=> $error,
	'error_msg'			=> $error_msg,
	'error_level'		=> $error_level,
	'error_code'		=> $error_code,
	'session_timeout'	=> $_SESSION['timeout'],
	'progress'			=> $data_order_progress
);

//Respond to request.
echo json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
?>