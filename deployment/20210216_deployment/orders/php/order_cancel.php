<?php
//Start session
session_start();

// Prevent caching.
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 01 Jan 1996 00:00:00 GMT');
// The JSON standard MIME header.
header('Content-type: application/json');

//Default Data
$error			= false;
$error_msg		= null;
$error_level	= null;
$error_code		= 0;
$verified		= null;

$request		= null;

$data_order		= null;

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
if(!grant_access([2003]))
{
	error_response_json(
		"Access denied.",
		1,
		__LINE__,
		null,
		null);
}

// Empty data received in request.
if(!isset($_POST['token']))
{
	error_response_json(
		"Please login before continuing.",
		1,
		__LINE__,
		null,
		null);
}

// Data received in request.
if(!isset($_POST['request']))
{
	error_response_json(
		"Request data not available.",
		1,
		__LINE__,
		null,
		null);
}

if(!isset($_POST['oid']))
{
	error_response_json(
		"Request data not available.",
		1,
		__LINE__,
		null,
		null);
}

if(!isset($_POST['cancel_reason']))
{
	error_response_json(
		"Request cancel data not available.",
		1,
		__LINE__,
		null,
		null);
}

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session_token.php';
if (!verify_token($_POST['token']))
{
	error_response_json(
		"Please login again before continuing.",
		1,
		__LINE__,
		null,
		null);
}

//Open DB connection
include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/db_vmsx.php';

// Protect from MySQL injection
$io_request	= mysqli_real_escape_string($mysqli, stripslashes($_POST['request']));
$io_oid	= mysqli_real_escape_string($mysqli, stripslashes($_POST['oid']));
$io_cancelreason = mysqli_real_escape_string($mysqli, stripslashes($_POST['cancel_reason']));
$mysqli->autocommit(FALSE);

if($io_request == "")
{
	error_response_json(
		"Invalid request.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($io_request != "cancel_order")
{
	error_response_json(
		"Invalid request.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($io_oid == "")
{
	error_response_json(
		"Order selection undefined.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($io_cancelreason == "")
{
	error_response_json(
		"Order cancel reason undefined.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

	// SQL update cards query
	$sqli_query = "
		UPDATE
			vmsx_db.cards
		SET 
			esp_status = 'C', 
			reason='Order cancelled',
			updated_by = ?,
			updated_on = NOW()
		WHERE
			cards.oid = ?
	";
	
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param( "is", $_SESSION['uid'], $io_oid);
	$exe_result_card = $stmt->execute();

	// SQL update Order query
	$sqli_query = "
		UPDATE
			vmsx_db.orders 
		SET 
			osid = 9,
			updated_by = ?,
			cancelreason = ?,
			updated_on = NOW()
		WHERE
			orders.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param( "iss", $_SESSION['uid'], $io_cancelreason, $io_oid);
	$exe_result_order = $stmt->execute();

	// SQL update items query
	$sqli_query = "
		UPDATE
			vmsx_db.orders_items 
		SET 
			osid = 9,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			orders_items.oid = ?
	";
	
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param( "is", $_SESSION['uid'], $io_oid);
	$exe_result_item = $stmt->execute();

	// SQL update batches query
	$sqli_query = "
		UPDATE
			vmsx_db.orders_batches
		SET 
			osid = 9,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			orders_batches.oid = ?
	";
	
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param( "is", $_SESSION['uid'], $io_oid);
	$exe_result_batch = $stmt->execute();

if($exe_result_batch && $exe_result_item && $exe_result_order && exe_result_card)
{
	$sqli_query = "
		INSERT INTO 
			vmsx_db.orders_progress (
				oid,
				osid,
				created_by,
				created_on
			) values 
			(?,9,?,NOW())";

	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param( "si", $io_oid,$_SESSION['uid']);
	$exe_result_batch = $stmt->execute();

	$mysqli->commit();

} else {$mysqli->rollback();}

// SQL query to fetch user info
$sqli_order_info = "
	SELECT
		o.id,
		o.did,
		d.agency,
		o.file_name,
		o.file_date,
		o.file_seq_no,
		o.sap_order_no,
		o.osid,
		s.status,
		o.created_by,
		c.username AS created_by_uname,
		o.created_on,
		o.updated_by,
		u.username AS updated_by_uname,
		o.updated_on,
		o.espfileinput,
		o.espfileoutput,
		o.cancelreason
	FROM
		vmsx_db.orders o
	LEFT JOIN
		(
			SELECT
				id,
				agency
			FROM vmsx_db.dists
			UNION ALL
			SELECT
				id,
				agency
			FROM vmsx_db.dists_del
		) d
	ON
		o.did=d.id
	LEFT JOIN
		(
			SELECT
				id,
				username
			FROM vmsx_db.users
			UNION ALL
			SELECT
				id,
				username
			FROM vmsx_db.users_del
		) c
	ON
		o.created_by=c.id
	LEFT JOIN
		vmsx_db.orders_status s
	ON
		o.osid=s.id
	LEFT JOIN
		(
			SELECT
				id,
				username
			FROM vmsx_db.users
			UNION ALL
			SELECT
				id,
				username
			FROM vmsx_db.users_del
		) u
	ON
		o.updated_by=u.id
	WHERE
		o.id=?
	";

	
$stmt = $mysqli->prepare($sqli_order_info);
$stmt->bind_param("i", $io_oid);
$stmt->execute();
if(!($result = $stmt->get_result()))
{
	error_response_json(
		"Could not connect to the database.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}
$num_rows = $result->num_rows;
if($num_rows === 0)
{
	error_response_json(
		"Could not retrieve order information.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

while($row = $result->fetch_assoc())
{
	$data_order		= $row;
}

//Close statment and db connections
if(isset($stmt) && $stmt != null){$stmt->close();}
if(isset($mysqli) && $mysqli){$mysqli->close();}

//create a JSON data structure.
$data = array (	
	'errors' 			=> $error,
	'error_msg'			=> $error_msg,
	'error_level'		=> $error_level,
	'error_code'		=> $error_code,
	'session_timeout'	=> $_SESSION['timeout'],
	'order'				=> $data_order
);

//Respond to request.
echo json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
?>
