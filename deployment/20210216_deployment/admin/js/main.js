var elems, options;
var mod_dist_add;
var mod_dist_edit;
var tt_sessions;

var dists_page_no = 1;
var dists_page_limit = 10;
var dists_sort_order = "ASC";
var dists_sort_column = 0;

var cards_page_no = 1;
var cards_page_limit = 10;
var cards_sort_order = "ASC";
var cards_sort_column = 0;

var dists_hist_page_no = 1;
var dists_hist_page_limit = 10;
var dists_hist_sort_order = "ASC";
var dists_hist_sort_column = 0;

$(document).ready(function(){	//Main
	prepareData();
	initialise_Mcss_all();
	initialise_Btns();
	initialise_Forms();
	
	//Default data: Fetch distributors
	dists_get($("body").attr('data-token'), null);
});

//INITIALIZATION
function prepareData()
{	//Prepare DOM elements for initialising
	getColumnsToFilterBy('dists_tbl',		'dists_filter_sel');
	getColumnsToFilterBy('cards_tbl',		'cards_filter_sel');
	getColumnsToFilterBy('dists_hist_tbl',		'dists_hist_filter_sel');
}

function initialise_Mcss_all()
{	//MaterializeCSS initializations
	//Modals
	mod_dist_add = M.Modal.init(document.querySelector('#modal_dist_add'), {});
	mod_dist_edit = M.Modal.init(document.querySelector('#modal_dist_edit'), {});
	
	//Tabs:
	options = {
		duration: 300
	};
	M.Tabs.init(document.querySelectorAll('.tabs'), options);
	
	//Tooltips:
	options = {
		enterDelay: 500
	};
    M.Tooltip.init(document.querySelectorAll('.tooltipped'), options);
	
	//Btn Add
	//options = {hoverEnabled: false};
    //M.FloatingActionButton.init(document.querySelectorAll('.fixed-action-btn'), options);
	
	
	//Character Counters
	//$('input#io_dist_agency').characterCounter();
}

function initialise_Btns()
{	//Initialise Button listeners
	//Search buttons with column filter select
	$('#btn_dists_search').click(function(){
		event.preventDefault();
		dists_page_no = 1;
		dists_get($("body").attr('data-token'), null);
	});
	$('#btn_cards_search').click(function(){
		event.preventDefault();
		cards_page_no = 1;
		cards_dens_get($("body").attr('data-token'), null);
	});
	$('#btn_dists_hist_search').click(function(){
		event.preventDefault();
		dists_hist_page_no = 1;
		dists_hist_get($("body").attr('data-token'), null);
	});
	
	//Edit
	$('#btn_dists_add').click(function(){
		event.preventDefault();
		mod_dist_add.open();
	});
	$('#btn_dist_update').click(function(){
		event.preventDefault();
		dist_set($('body').attr('data-token'), $('.vms-dist-editing').attr("data-did"));
	});
	
	//Add
	$('#mod_dist_btn_add').click(function(){
		event.preventDefault();
		dist_add($("body").attr('data-token'));
	});
	
	//Delete
	$('#btn_dists_modal_rem').click(function(){
		event.preventDefault();
		dist_rem($("body").attr('data-token'), $('.vms-dist-editing').attr('data-did'));
		
	});

	//Fetch all data on Tab select
	$("#dists_tab").click(function (){
		event.preventDefault();
		sort_clear('#dists_tbl');
		document.getElementById("dists_filter").value="";
		dists_get($("body").attr('data-token'));
	});
	$('#cards_tab').click(function(){
		event.preventDefault();
		//sort_clear('#cards_tbl');
		//document.getElementById("cards_filter").value="";
		cards_dens_get($("body").attr('data-token'), null);
	});
	$("#dists_hist_tab").click(function (){
		event.preventDefault();
		sort_clear('#dists_hist_tbl');
		document.getElementById("dists_hist_filter").value="";
		dists_hist_get($("body").attr('data-token'));
	});

	//Sort Button: i.e. Header cell
	$('#dists_tbl > thead > tr > th').click(function(){
		event.preventDefault();
		sortByColumn("#dists_tbl", this);
	});
	$('#cards_tbl > thead > tr > th').click(function(){
		event.preventDefault();
		sortByColumn("#cards_tbl", this);
	});
	$('#dists_hist_tbl > thead > tr > th').click(function(){
		event.preventDefault();
		sortByColumn("#dists_hist_tbl", this);
	});

	//Row Double Click => EDIT
	$('#dists_tbl > tbody').on("dblclick", "tr", function(){
		event.preventDefault();
		$('#modal_dist_edit').attr('data-did', $(this).attr("data-did"));
		dist_get($('body').attr('data-token'), $(this).attr("data-did"));
		$(this).siblings().removeClass('vms-dist-editing');
		$(this).addClass('vms-dist-editing');
		mod_dist_edit.open();
	});
	
	
}

function initialise_Forms()
{	//Initialise Form listeners
	
	//Select dropdown
    M.FormSelect.init(document.querySelectorAll('select'), {});
	
	//Paging
	$("#page_limit_dists").change(function (){
		dists_page_no = 1;
		dists_get($("body").attr('data-token'), null);
	});
	$("#page_limit_cards").change(function (){
		cards_page_no = 1;
		cards_dens_get($("body").attr('data-token'), null);
	});
	$("#page_limit_dists_hist").change(function (){
		dists_hist_page_no = 1;
		dists_hist_get($("body").attr('data-token'), null);
	});
	
	M.updateTextFields();
}

//DISTS TAB
function dists_get(token, caller)
{	//Fetch all distributors in the database
	filter_data = {
		columns:	$('#dists_filter_sel').val(),
		keyword:	document.getElementById("dists_filter").value
	};
	//console.log(filter_data);

	if(caller != null)
	{
		if(caller.hasAttribute("data-page"))
		{	//Check if page change requested
			let temp_page_no = caller.getAttribute("data-page");
			if (temp_page_no != null && temp_page_no != "null")
			{	//Set new page by selection
				dists_page_no = temp_page_no;
			}
			else
			{
				return;
			}
		}
	}
	//#page_limit
	dists_page_limit = $('#page_limit_dists').val();

	//console.log('request: all_dists, token: ' +token+ ', page_no: ' +dists_page_no+', page_limit: ' +dists_page_limit+', sort_order: '+dists_sort_order+', sort_column: ' +dists_sort_column+', filter: '+filter_data);	
	
	$('#dists_tbl > tbody').text(""); //sort_clear('#dists_tbl');
	$('.progress').remove();
	$('#dists_tbl').parent().append('<div class="progress"><div class="indeterminate"></div></div>');
	$.post('/en/admin/php/dists.php',
		{
			request: 'all_dists',
			token: token,
			page_no: dists_page_no,
			page_limit: dists_page_limit,
			sort_order: dists_sort_order,
			sort_column: dists_sort_column,
			filter: filter_data
		},
		function(json)
		{
			paging_update('page_dists', json['page_no'], json['page_total']);
			//Paging click event
			$('#page_dists li').on("click", "a", function(){
				event.preventDefault();
				dists_get($("body").attr('data-token'), this);
			});	
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			
			dists_append('#dists_tbl', json['dists']);
		},
		'json'
	).done(function()
	{
		$('.progress').remove();
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	
	return null;
}

function dists_append(table_id, dists_array)
{	//Add distributor to a specified table
	if (dists_array == null) return false;
	var html = "";
	dists_array.forEach(function(item, index, data){
		html = html + 
			'<tr data-did="'+	data[index]['id'] +
			'"><td>'	+ 		(data[index]['agency']!=null?data[index]['agency']:'') +
			'</td><td>' +		(data[index]['cust_name']!=null?data[index]['cust_name']:'') + 
			'</td><td>' +		(data[index]['created_on']!=null?data[index]['created_on']:'') + 
			'</td><td>' +		(data[index]['created_by_uname']!=null?data[index]['created_by_uname']:'') + 
			'</td><td>' +		(data[index]['updated_on']!=null?data[index]['updated_on']:'') + 
			'</td><td>' +		(data[index]['updated_by_uname']!=null?data[index]['updated_by_uname']:'') + 
			'</td></tr>';
	});
	$(table_id + ' > tbody').append(html);
	return true;
}

function dist_get(token, did)
{	//Fetch distributors in the database
	document.getElementById('dist_edit_agency').innerText = "Unknown";
	
	document.getElementById('io_dist_agency').value = "";
	document.getElementById('io_dist_cust_name').value = "";
	document.getElementById('io_dist_cust_no').value = ""
	
	document.getElementById('io_dist_contact').value = "";
	document.getElementById('io_dist_tel').value = "";
	document.getElementById('io_dist_cel').value = "";
	document.getElementById('io_dist_fax').value = "";
	document.getElementById('io_dist_email').value = "";
	document.getElementById('io_dist_location').value = "";
	document.getElementById('io_dist_address').value = "";
	
	document.getElementById('dist_inf_created_on').innerText = "";
	document.getElementById('dist_inf_created_by').innerText = "";
	document.getElementById('dist_inf_updated_on').innerText = "";
	document.getElementById('dist_inf_updated_by').innerText = "";
	
	$.post('/en/admin/php/dist.php',
		{
			request: 'a_dist',
			token: token,
			did: did
		},
		function(json)
		{
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			document.getElementById('dist_edit_agency').innerText = (json['dist']['agency']!=null?json['dist']['agency']:'Unknown');
	
			document.getElementById('io_dist_agency').value = (json['dist']['agency']!=null?json['dist']['agency']:'');
			document.getElementById('io_dist_cust_name').value = (json['dist']['cust_name']!=null?json['dist']['cust_name']:'');
			document.getElementById('io_dist_cust_no').value = (json['dist']['cust_no']!=null?json['dist']['cust_no']:'');
			
			document.getElementById('io_dist_contact').value = (json['dist']['contact']!=null?json['dist']['contact']:'');
			document.getElementById('io_dist_tel').value = (json['dist']['tel']!=null?json['dist']['tel']:'');
			document.getElementById('io_dist_cel').value = (json['dist']['cel']!=null?json['dist']['cel']:'');
			document.getElementById('io_dist_fax').value = (json['dist']['fax']!=null?json['dist']['fax']:'');
			document.getElementById('io_dist_email').value = (json['dist']['email']!=null?json['dist']['email']:'');
			document.getElementById('io_dist_location').value = (json['dist']['location']!=null?json['dist']['location']:'');
			document.getElementById('io_dist_address').value = (json['dist']['address']!=null?json['dist']['address']:'');
			
			document.getElementById('io_dist_transmit_dir').value = (json['dist']['transmit_dir']!=null?json['dist']['transmit_dir']:'');
			document.getElementById('io_dist_receive_dir').value = (json['dist']['receive_dir']!=null?json['dist']['receive_dir']:'');
			document.getElementById('io_dist_ftp_server').value = (json['dist']['ftp_server']!=null?json['dist']['ftp_server']:'');
			document.getElementById('io_dist_ftp_user').value = (json['dist']['ftp_user']!=null?json['dist']['ftp_user']:'');
			document.getElementById('io_dist_ftp_password').value = (json['dist']['ftp_password']!=null?json['dist']['ftp_password']:'');
			document.getElementById('io_dist_seqno').value = (json['dist']['seqno']!=null?json['dist']['seqno']:'');
			document.getElementById('io_dist_batch_size').value = (json['dist']['batch_size']!=null?json['dist']['batch_size']:'');
			document.getElementById('io_dist_public_key').value = (json['dist']['public_key']!=null?json['dist']['public_key']:'');
			document.getElementById('io_dist_ftp_transmit_dir').value = (json['dist']['ftp_transmit_dir']!=null?json['dist']['ftp_transmit_dir']:'');
			document.getElementById('io_dist_ftp_receive_dir').value = (json['dist']['ftp_receive_dir']!=null?json['dist']['ftp_receive_dir']:'');
			document.getElementById('io_dist_max_cutoff').value = (json['dist']['max_cutoff']!=null?json['dist']['max_cutoff']:'');
			document.getElementById('io_dist_starter_size').value = (json['dist']['starter_size']!=null?json['dist']['starter_size']:'');
			document.getElementById('io_dist_contact').value = (json['dist']['contact']!=null?json['dist']['contact']:'');
			
			
			document.getElementById('io_dist_agency').setAttribute('data-value',(json['dist']['agency']!=null?json['dist']['agency']:''));
			document.getElementById('io_dist_cust_name').setAttribute('data-value',(json['dist']['cust_name']!=null?json['dist']['cust_name']:''));
			document.getElementById('io_dist_cust_no').setAttribute('data-value',(json['dist']['cust_no']!=null?json['dist']['cust_no']:''));
			
			document.getElementById('io_dist_contact').setAttribute('data-value',(json['dist']['contact']!=null?json['dist']['contact']:''));
			document.getElementById('io_dist_tel').setAttribute('data-value',(json['dist']['tel']!=null?json['dist']['tel']:''));
			document.getElementById('io_dist_cel').setAttribute('data-value',(json['dist']['cel']!=null?json['dist']['cel']:''));
			document.getElementById('io_dist_fax').setAttribute('data-value',(json['dist']['fax']!=null?json['dist']['fax']:''));
			document.getElementById('io_dist_email').setAttribute('data-value',(json['dist']['email']!=null?json['dist']['email']:''));
			document.getElementById('io_dist_location').setAttribute('data-value',(json['dist']['location']!=null?json['dist']['location']:''));
			document.getElementById('io_dist_address').setAttribute('data-value',(json['dist']['address']!=null?json['dist']['address']:''));
			
			document.getElementById('io_dist_transmit_dir').setAttribute('data-value',(json['dist']['transmit_dir']!=null?json['dist']['transmit_dir']:''));
			document.getElementById('io_dist_receive_dir').setAttribute('data-value',(json['dist']['receive_dir']!=null?json['dist']['receive_dir']:''));
			document.getElementById('io_dist_ftp_server').setAttribute('data-value',(json['dist']['ftp_server']!=null?json['dist']['ftp_server']:''));
			document.getElementById('io_dist_ftp_user').setAttribute('data-value',(json['dist']['ftp_user']!=null?json['dist']['ftp_user']:''));
			document.getElementById('io_dist_ftp_password').setAttribute('data-value',(json['dist']['ftp_password']!=null?json['dist']['ftp_password']:''));
			document.getElementById('io_dist_seqno').setAttribute('data-value',(json['dist']['seqno']!=null?json['dist']['seqno']:''));
			document.getElementById('io_dist_batch_size').setAttribute('data-value',(json['dist']['batch_size']!=null?json['dist']['batch_size']:''));
			document.getElementById('io_dist_public_key').setAttribute('data-value',(json['dist']['public_key']!=null?json['dist']['public_key']:''));
			document.getElementById('io_dist_ftp_transmit_dir').setAttribute('data-value',(json['dist']['ftp_transmit_dir']!=null?json['dist']['ftp_transmit_dir']:''));
			document.getElementById('io_dist_ftp_receive_dir').setAttribute('data-value',(json['dist']['ftp_receive_dir']!=null?json['dist']['ftp_receive_dir']:''));
			document.getElementById('io_dist_max_cutoff').setAttribute('data-value',(json['dist']['max_cutoff']!=null?json['dist']['max_cutoff']:''));
			document.getElementById('io_dist_starter_size').setAttribute('data-value',(json['dist']['starter_size']!=null?json['dist']['starter_size']:''));
			document.getElementById('io_dist_contact').setAttribute('data-value',(json['dist']['contact']!=null?json['dist']['contact']:''));
			
			
			document.getElementById('dist_inf_created_on').innerText = (json['dist']['created_on']!=null?json['dist']['created_on']:'');
			document.getElementById('dist_inf_created_by').innerText = (json['dist']['created_by_uname']!=null?json['dist']['created_by_uname']:'');
			document.getElementById('dist_inf_updated_on').innerText = (json['dist']['updated_on']!=null?json['dist']['updated_on']:'');
			document.getElementById('dist_inf_updated_by').innerText = (json['dist']['updated_by_uname']!=null?json['dist']['updated_by_uname']:'');
		},
		'json'
	).done(function()
	{
		$('.progress').remove();
	}).fail(function(jqXHR,status,err)
	{
		//$('#io_edit_user_uname').html("Error");
	}).always(function()
	{
		
	});
	
	return null;
}

function dist_add(token)
{	//Add a new distributor
	let json_data = {
		"dist":{
			"agency":			document.getElementById("io_new_dist_agency").value,
			"cust_name":		document.getElementById("io_new_dist_cust_name").value,
			"cust_no":			document.getElementById("io_new_dist_cust_no").value,
			"location":			document.getElementById("io_new_dist_location").value,
			"contact":			document.getElementById("io_new_dist_contact").value,
			"tel":				document.getElementById("io_new_dist_tel").value,
			"cel":				document.getElementById("io_new_dist_cel").value,
			"fax":				document.getElementById("io_new_dist_fax").value,
			"address":			document.getElementById("io_new_dist_address").value,
			"transmit_dir":		document.getElementById("io_new_dist_transmit_dir").value,
			"receive_dir":		document.getElementById("io_new_dist_receive_dir").value,
			"ftp_server":		document.getElementById("io_new_dist_ftp_server").value,
			"ftp_user":			document.getElementById("io_new_dist_ftp_user").value,
			"ftp_password":		document.getElementById("io_new_dist_ftp_password").value,
			"seqno":			document.getElementById("io_new_dist_seqno").value,
			"batch_size":		document.getElementById("io_new_dist_batch_size").value,
			"public_key":		document.getElementById("io_new_dist_public_key").value,
			"email":			document.getElementById("io_new_dist_email").value,
			"ftp_transmit_dir":	document.getElementById("io_new_dist_ftp_transmit_dir").value,
			"ftp_receive_dir":	document.getElementById("io_new_dist_ftp_receive_dir").value,
			"max_cutoff":		document.getElementById("io_new_dist_max_cutoff").value,
			"starter_size":		document.getElementById("io_new_dist_starter_size").value
		}
	};
	
	if (json_data['dist']['agency']=="")
	{
		M.toast({html: "<span>Please enter an agency name.</span>"});
		var elmnt = document.getElementById("io_dist_agency");
		elmnt.scrollIntoView();
		$("#io_new_dist_agency").removeClass('valid').focus();
		return null;
	}
	
	if (json_data['dist']['cust_name']=="")
	{
		M.toast({html: "<span>Please enter an agency description.</span>"});
		var elmnt = document.getElementById("io_dist_descr");
		elmnt.scrollIntoView();
		$("#io_new_dist_cust_name").removeClass('valid').focus();
		return null;
	}
	
	if (json_data['dist']['cust_no']=="")
	{
		M.toast({html: "<span>Please enter a SAP Customer Number.</span>"});
		var elmnt = document.getElementById("io_new_dist_cust_no");
		elmnt.scrollIntoView();
		$("#io_new_dist_cust_no").removeClass('valid').focus();
		return null;
	}
	
	$.post('/en/admin/php/dist_add.php',
		{
			request: 'dist_add',
			token: token,
			data: JSON.stringify(json_data)
		},
		function(json)
		{
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			dists_append('#dists_tbl', json['dist']);
			
			mod_dist_add.close();
			var toastHTML = '<span>The distributor has been created.</span>';
			M.toast({html: toastHTML});
		},
		'json'
	).done(function()
	{
		
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	return null;
}

function dist_set(token, did)
{	//Update a distributor
	let io_agency = 			document.getElementById("io_dist_agency").value;
	let io_cust_name = 			document.getElementById("io_dist_cust_name").value;
	let io_cust_no = 			document.getElementById("io_dist_cust_no").value;
	let io_location = 			document.getElementById("io_dist_location").value;
	let io_contact = 			document.getElementById("io_dist_contact").value;
	let io_tel = 				document.getElementById("io_dist_tel").value;
	let io_cel = 				document.getElementById("io_dist_cel").value;
	let io_fax = 				document.getElementById("io_dist_fax").value;
	let io_address = 			document.getElementById("io_dist_address").value;
	let io_transmit_dir = 		document.getElementById("io_dist_transmit_dir").value;
	let io_receive_dir = 		document.getElementById("io_dist_receive_dir").value;
	let io_ftp_server = 		document.getElementById("io_dist_ftp_server").value;
	let io_ftp_user = 			document.getElementById("io_dist_ftp_user").value;
	let io_ftp_password = 		document.getElementById("io_dist_ftp_password").value;
	let io_seqno = 				document.getElementById("io_dist_seqno").value;
	let io_batch_size = 		document.getElementById("io_dist_batch_size").value;
	let io_public_key = 		document.getElementById("io_dist_public_key").value;
	let io_email = 				document.getElementById("io_dist_email").value;
	let io_ftp_transmit_dir = 	document.getElementById("io_dist_ftp_transmit_dir").value;
	let io_ftp_receive_dir = 	document.getElementById("io_dist_ftp_receive_dir").value;
	let io_max_cutoff = 		document.getElementById("io_dist_max_cutoff").value;
	let io_starter_size = 		document.getElementById("io_dist_starter_size").value;
	
	let json_data = {
		"dist":{
			"agency":			[io_agency, (io_agency==$('#io_dist_agency').attr('data-value')?false:true)],
			"cust_name":		[io_cust_name, (io_cust_name==$('#io_dist_cust_name').attr('data-value')?false:true)],
			"cust_no":			[io_cust_no, (io_cust_no==$('#io_dist_cust_no').attr('data-value')?false:true)],
			"location":			[io_location, (io_location==$('#io_dist_location').attr('data-value')?false:true)],
			"contact":			[io_contact, (io_contact==$('#io_dist_contact').attr('data-value')?false:true)],
			"tel":				[io_tel, (io_tel==$('#io_dist_tel').attr('data-value')?false:true)],
			"cel":				[io_cel, (io_cel==$('#io_dist_cel').attr('data-value')?false:true)],
			"fax":				[io_fax, (io_fax==$('#io_dist_fax').attr('data-value')?false:true)],
			"address":			[io_address, (io_address==$('#io_dist_address').attr('data-value')?false:true)],
			"transmit_dir":		[io_transmit_dir, (io_transmit_dir==$('#io_dist_transmit_dir').attr('data-value')?false:true)],
			"receive_dir":		[io_receive_dir, (io_receive_dir==$('#io_dist_receive_dir').attr('data-value')?false:true)],
			"ftp_server":		[io_ftp_server, (io_ftp_server==$('#io_dist_ftp_server').attr('data-value')?false:true)],
			"ftp_user":			[io_ftp_user, (io_ftp_user==$('#io_dist_ftp_user').attr('data-value')?false:true)],
			"ftp_password":		[io_ftp_password, (io_ftp_password==$('#io_dist_ftp_password').attr('data-value')?false:true)],
			"seqno":			[io_seqno, (io_seqno==$('#io_dist_seqno').attr('data-value')?false:true)],
			"batch_size":		[io_batch_size, (io_batch_size==$('#io_dist_batch_size').attr('data-value')?false:true)],
			"public_key":		[io_public_key, (io_public_key==$('#io_dist_public_key').attr('data-value')?false:true)],
			"email":			[io_email, (io_email==$('#io_dist_email').attr('data-value')?false:true)],
			"ftp_transmit_dir":	[io_ftp_transmit_dir, (io_ftp_transmit_dir==$('#io_dist_ftp_transmit_dir').attr('data-value')?false:true)],
			"ftp_receive_dir":	[io_ftp_receive_dir, (io_ftp_receive_dir==$('#io_dist_ftp_receive_dir').attr('data-value')?false:true)],
			"max_cutoff":		[io_max_cutoff, (io_max_cutoff==$('#io_dist_max_cutoff').attr('data-value')?false:true)],
			"starter_size":		[io_starter_size, (io_starter_size==$('#io_dist_starter_size').attr('data-value')?false:true)],

		}
	};
	
	let hasChange = false;
	
	for (var key in json_data["dist"]) {
		if (json_data["dist"].hasOwnProperty(key))
		{
			if (json_data["dist"][key][1])
			{
				hasChange = true;
			}
		}
	}
	
	if(!hasChange) //Nothing has changed
	{
		M.toast({html: "<span>No changes to save.</span>"});
		return null;
	}
	
	if (json_data['dist']['agency'][0]=="")
	{
		M.toast({html: "<span>Please enter an agency name.</span>"});
		var elmnt = document.getElementById("io_dist_agency");
		elmnt.scrollIntoView();
		$("#io_dist_agency").removeClass('valid').focus();
		return null;
	}
	
	if (json_data['dist']['cust_name'][0]=="")
	{
		M.toast({html: "<span>Please enter a Customer Name.</span>"});
		var elmnt = document.getElementById("io_dist_cust_name");
		elmnt.scrollIntoView();
		$("#io_dist_cust_name").removeClass('valid').focus();
		return null;
	}
	
	if (json_data['dist']['cust_no'][0]=="")
	{
		M.toast({html: "<span>Please enter a SAP Customer Number.</span>"});
		var elmnt = document.getElementById("io_dist_cust_no");
		elmnt.scrollIntoView();
		$("#io_dist_cust_no").removeClass('valid').focus();
		return null;
	}
	
	$.post('/en/admin/php/dist_update.php',
		{
			request: 'a_dist_update',
			token: token,
			did: did,
			data: JSON.stringify(json_data)
		},
		function(json)
		{
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			dist_get(token, did);
			
			//mod_dist_add.close();
			var toastHTML = '<span>The distributor has been updated.</span>';
			M.toast({html: toastHTML});
		},
		'json'
	).done(function()
	{
		
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	return null;
}

function dist_rem(token, did)
{	//Remove distributor information from database
	
	var result = confirm("Are you sure you want to delete the distributor?");
	if (result)
	{	//Continue to delete the user account

		$.post('/en/admin/php/dist_del.php',
			{
				"request": 'a_dist_remove',
				"token": token,
				"did": did
			},
			function(json)
			{
				if(json['error']){
					let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
					M.toast({html: toastHTML});
					if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
					}
					return null;
				}
				
				//Successful response
				//Update table row
				$('.vms-dist-editing').remove();
				mod_dist_edit.close();
				M.toast({html: json['dist']});
			},
			'json'
		).done(function()
		{
			
		}).fail(function(jqXHR,status,err)
		{
			
		}).always(function()
		{
			
		});
	}
	
	return null;
}


//CARD DENOMINATIONS TAB
function cards_dens_get(token, caller)
{	//Retrieve cards for all orders
	filter_data = {
		columns:	$('#cards_filter_sel').val(),
		keyword:	document.getElementById("cards_filter").value
	};
	if(caller != null)
	{
		if(caller.hasAttribute("data-page"))
		{	//Check if page change requested
			let temp_page_no = caller.getAttribute("data-page");
			if (temp_page_no != null && temp_page_no != "null")
			{	//Set new page by selection
				cards_page_no = temp_page_no;
			}
			else
			{
				return;
			}
		}
	}
	//#page_limit
	cards_page_limit = $('#page_limit_cards').val();
	
	$('#cards_tbl > tbody').text("");
	$('.progress').remove();
	$('#cards_tbl').parent().append('<div class="progress"><div class="indeterminate"></div></div>');
	
	//Retrieve new data
	$.post('/en/admin/php/cards_dens.php',
		{
			request: 'all_cards_den',
			token: token,
			page_no: cards_page_no,
			page_limit: cards_page_limit,
			sort_order: cards_sort_order,
			sort_column: cards_sort_column,
			filter: filter_data
		},
		function(json)
		{
			paging_update('page_cards', json['page_no'], json['page_total']);
			//Paging click event
			$('#page_cards li').on("click", "a", function(){
				event.preventDefault();
				cards_dens_get($("body").attr('data-token'), this);
			});	
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				if(json['error_msg']!="Access denied."){M.toast({html: toastHTML});}
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			
			cards_den_append("#cards_tbl", json['cards_dens']);
			M.updateTextFields();
		},
		'json'
	).done(function()
	{
		$('.progress').remove();
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	
	return null;
}

function cards_den_append(table_id, cards_array)
{	//Append the cards to the selected table
	if (cards_array == null) return false;
	var html = "";
	cards_array.forEach(function(item, index, data){
		html = '<tr data-cid="'+	data[index]['id'] + 
			'"><td>'+ 			(data[index]['name']!=null?data[index]['name']:'') + 
			'</td><td>' +		(data[index]['code']!=null?data[index]['code']:'') + 
			'</td><td>' +		(data[index]['next_batch_serial_no']!=null?(data[index]['next_batch_serial_no']).padLeft(10, '0'):'') + 
			'</td><td>' +		(data[index]['next_batch_man_no']!=null?(data[index]['next_batch_man_no']).padLeft(12, '0'):'') + 		
			'</td><td>' +		(data[index]['updated_on']!=null?data[index]['updated_on']:'') +  
			'</td><td>' +		(data[index]['updated_by_uname']!=null?data[index]['updated_by_uname']:'') +  
			'</td></tr>';
			
			$(table_id + ' > tbody').append(html);
	});
	return true;
}

//DISTS HIST TAB
function dists_hist_get(token, caller)
{	//Fetch all distributors in the database
	filter_data = {
		columns:	$('#dists_hist_filter_sel').val(),
		keyword:	document.getElementById("dists_hist_filter").value
	};

	console.log(filter_data);
	if(caller != null)
	{
		if(caller.hasAttribute("data-page"))
		{	//Check if page change requested
			let temp_page_no = caller.getAttribute("data-page");
			if (temp_page_no != null && temp_page_no != "null")
			{	//Set new page by selection
				dists_page_no = temp_page_no;
			}
			else
			{
				return;
			}
		}
	}
	//#page_limit
	dists_page_limit = $('#page_limit_dists_hist').val();
		
	console.log('request: all_dists_hist, token: ' +token+ ', page_no: ' +dists_hist_page_no+',	page_limit: ' +dists_hist_page_limit+', sort_order: '+dists_hist_sort_order+', sort_column: ' +dists_hist_sort_column+', filter: '+filter_data);	

	$('#dists_hist_tbl > tbody').text(""); //sort_clear('#dists_hist_tbl');
	$('.progress').remove();
	$('#dists_hist_tbl').parent().append('<div class="progress"><div class="indeterminate"></div></div>');
	$.post('/en/admin/php/dists_hist.php',
		{
			request: 'all_dists_hist',
			token: token,
			page_no: dists_hist_page_no,
			page_limit: dists_hist_page_limit,
			sort_order: dists_hist_sort_order,
			sort_column: dists_hist_sort_column,
			filter: filter_data
		},
		function(json)
		{
			paging_update('page_dists_hist', json['page_no'], json['page_total']);
			//Paging click event
			$('#page_dists_hist li').on("click", "a", function(){
				event.preventDefault();
				dists_hist_get($("body").attr('data-token'), this);
			});	
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			
			dists_hist_append('#dists_hist_tbl', json['dists_hist']);
		},
		'json'
	).done(function()
	{
		$('.progress').remove();
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	
	return null;
}

function dists_hist_append(table_id, dists_hist_array)
{	//Add distributor to a specified table
	if (dists_hist_array == null) return false;
	var html = "";
	dists_hist_array.forEach(function(item, index, data){
		html = html + 
			'<tr data-did="'+	data[index]['id'] +
			'"><td>'	+ 		(data[index]['action']!=null?data[index]['action']:'') +
			'</td><td>' +		(data[index]['agency']!=null?data[index]['agency']:'') + 
			'</td><td>' +		(data[index]['cust_name']!=null?data[index]['cust_no']:'') + 
			'</td><td>' +		(data[index]['cust_no']!=null?data[index]['cust_name']:'') + 
			'</td><td>' +		(data[index]['location']!=null?data[index]['location']:'') + 
			'</td><td>' +		(data[index]['contact']!=null?data[index]['contact']:'') + 
			'</td><td>' +		(data[index]['tel']!=null?data[index]['tel']:'') + 
			'</td><td>' +		(data[index]['fax']!=null?data[index]['fax']:'') + 
			'</td><td>' +		(data[index]['ftp_transmit_dir']!=null?data[index]['ftp_transmit_dir']:'') + 
			'</td><td>' +		(data[index]['ftp_receive_dir']!=null?data[index]['ftp_receive_dir']:'') + 
			'</td><td>' +		(data[index]['ftp_server']!=null?data[index]['ftp_server']:'') + 
			'</td><td>' +		(data[index]['ftp_user']!=null?data[index]['ftp_user']:'') + 
			'</td><td>' +		(data[index]['seqno']!=null?data[index]['seqno']:'') + 
			'</td><td>' +		(data[index]['batch_size']!=null?data[index]['batch_size']:'') + 
			'</td><td>' +		(data[index]['email']!=null?data[index]['email']:'') +
			'</td><td>' +		(data[index]['active']!=null?data[index]['active']:'') +  
			'</td><td>' +		(data[index]['updated_on']!=null?data[index]['updated_on']:'') + 
			'</td><td>' +		(data[index]['updated_by_uname']!=null?data[index]['updated_by_uname']:'') + 

			'</td></tr>';
	});
	$(table_id + ' > tbody').append(html);
	return true;
}


//MISC
function getColumnsToFilterBy(table_id, filter_sel_id)
{	//Adds columns' indexes and names to to the drop down filter
	var testThisFunc = document.querySelectorAll('#'+table_id+'> thead > tr > th')
	testThisFunc.forEach(function(value,index,array){
		//console.log('index: '+ index + ', Value: ' + value.innerText);
		var html = '<option value="'+index+'" selected>'+value.innerText+'</option>';
		$('#'+filter_sel_id).append(html);
	});
	
}

function sortTable(table_id, column, order)
{	//Sorting Algorithm used by sortByColumn()
	switch(table_id)
	{
		case "#dists_tbl":
			dists_sort_order = order;
			dists_sort_column = column;
			dists_get($("body").attr('data-token'), null);
			break;
		case "#cards_tbl":
			cards_sort_order = order;
			cards_sort_column = column;
			cards_dens_get($("body").attr('data-token'), null);
			break;
		case "#dists_hist_tbl":
			dists_hist_sort_order = order;
			dists_hist_sort_column = column;
			dists_hist_get($("body").attr('data-token'), null);
			break;
		default:
	}
}

function sortByColumn(table_id, table_th)
{	//Sort a table a a specified column
	if($(table_th).find("i").hasClass("vms-sort"))
	{
		sortTable(table_id, $(table_th).index(), "ASC");
		$(table_th).find("i").removeClass("vms-sort");
		$(table_th).find("i").addClass("vms-sort-asc").text("arrow_drop_up");
		$(table_th).siblings().find("i").addClass("vms-sort").text("");
		$(table_th).siblings().find("i").removeClass("vms-sort-asc");
		$(table_th).siblings().find("i").removeClass("vms-sort-desc");
	}
	else if($(table_th).find("i").hasClass("vms-sort-asc"))
	{
		sortTable(table_id, $(table_th).index(), "DESC");
		$(table_th).find("i").removeClass("vms-sort-asc");
		$(table_th).find("i").addClass("vms-sort-desc").text("arrow_drop_down");
		$(table_th).siblings().find("i").addClass("vms-sort").text("");
		$(table_th).siblings().find("i").removeClass("vms-sort-asc");
		$(table_th).siblings().find("i").removeClass("vms-sort-desc");
	}
	else if($(table_th).find("i").hasClass("vms-sort-desc"))
	{
		sortTable(table_id, $(table_th).index(), "ASC");
		$(table_th).find("i").removeClass("vms-sort-desc");
		$(table_th).find("i").addClass("vms-sort-asc").text("arrow_drop_up");
		$(table_th).siblings().find("i").addClass("vms-sort").text("");
		$(table_th).siblings().find("i").removeClass("vms-sort-asc");
		$(table_th).siblings().find("i").removeClass("vms-sort-desc");
	}
}

function sort_clear(table_id)
{	//Clearing current sort of selected tab
	$(table_id + ' > tbody').text("");
	$(table_id).find("i").removeClass("vms-sort-asc");
	$(table_id).find("i").removeClass("vms-sort-desc");
	$(table_id).find("i").addClass("vms-sort").text("");
}

function paging_update(element_id, new_page_no, new_page_total)
{
	console.log("Element_id: " + element_id + " New page no: " + new_page_no + " New page total: " + new_page_total);
	(new_page_no==null)?new_page_no=1:(typeof new_page_no=="undefined")?new_page_no=1:false;
	(new_page_total==null)?new_page_total=1:(typeof new_page_total=="undefined")?new_page_total=1:false;
	//var html = "";
	document.getElementById(element_id).innerHTML = "";
	//Prev button
	(
		(new_page_no > 1) ?
			$('#' + element_id).append('<li class="waves-effect tooltipped" data-tooltip="Previous page" data-position="left"><a data-page="' + (new_page_no - 1) + '" href="#"><i class="material-icons">chevron_left</i></a></li>') :
			$('#' + element_id).append('<li class="disabled"><a data-page="null" href="#"><i class="material-icons">chevron_left</i></a></li>')
	);
	//First page
	(
		(new_page_no > 4) ?
			$('#' + element_id).append('<li class="waves-effect"><a data-page="1" href="#">1</a></li>') :
			false
	);
	//Lower More = ...
	(
		(new_page_no > 5) ?
			((new_page_no == 6) ?
				$('#' + element_id).append('<li class="waves-effect"><a data-page="2" href="#">2</a></li>') :
				$('#' + element_id).append('<li class="disabled"><a data-page="null" href="#"><i class="material-icons">more_horiz</i></a></li>')
			) :
			false
	);
	//Current page - 3
	(
		(new_page_no > 3) ?
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no - 3) + '" href="#">' + (new_page_no - 3) + '</a></li>') :
		false
	);
	//Current page - 2
	(
		(new_page_no > 2) ?
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no - 2) + '" href="#">' + (new_page_no - 2) + '</a></li>') :
		false
	);
	//Current page - 1
	(
		(new_page_no > 1) ?
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no - 1) + '" href="#">' + (new_page_no - 1) + '</a></li>') :
		false
	);
	//Current page
	(
		(new_page_no > 0) ?
		$('#' + element_id).append('<li class="active"><a data-page="null" href="#">' + new_page_no + '</a></li>') :
		false
	);
	//Current page + 1
	(
		((new_page_total - new_page_no) > 1) ?
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no + 1) + '" href="#">' + (new_page_no + 1) + '</a></li>') :
		false
	);
	//Current page + 2
	(
		((new_page_total - new_page_no) > 2) ?
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no + 2) + '" href="#">' + (new_page_no + 2) + '</a></li>') :
		false
	);
	//Current page + 3
	(
		((new_page_total - new_page_no) > 3) ?
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no + 3) + '" href="#">' + (new_page_no + 3) + '</a></li>') :
		false
	);
	//Upper More = ...
	(
		((new_page_total - new_page_no) > 4) ?
			(((new_page_total - new_page_no) == 5) ?
				$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_total - 1) + '" href="#">' + (new_page_total - 1) + '</a></li>') :
				$('#' + element_id).append('<li class="disabled"><a data-page="null" href="#"><i class="material-icons">more_horiz</i></a></li>')
			) :
			false
	);
	//Last page
	(
		(new_page_no >= new_page_total) ?
		true :
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_total) + '" href="#">' + (new_page_total) + '</a></li>')
	);
	//Next button
	(
		((new_page_total - new_page_no) > 0) ?
		$('#'+element_id).append('<li class="waves-effect tooltipped right" data-tooltip="Next page" data-position="right"><a data-page="'+(new_page_no+1)+'" href="#"><i class="material-icons">chevron_right</i></a></li>') :
		$('#'+element_id).append('<li class="disabled right"><a data-page="null" href="#"><i class="material-icons">chevron_right</i></a></li>')
	);
	//Activate Tooltips:
	options = {
		enterDelay: 500
	};
	elems = document.querySelectorAll('ul .tooltipped');
    M.Tooltip.init(elems, options);
}

Number.prototype.padLeft = function (n,str) {
    return (this < 0 ? '-' : '') + 
            Array(n-String(Math.abs(this)).length+1)
             .join(str||'0') + 
           (Math.abs(this));
}

function myDistHistScroll() {
  var elmnt = document.getElementById("dist_hist_scroll");
  var x = elmnt.scrollLeft;
}
