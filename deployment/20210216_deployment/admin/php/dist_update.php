<?php
//Start session
session_start();

// Prevent caching.
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 01 Jan 1996 00:00:00 GMT');
// The JSON standard MIME header.
header('Content-type: application/json');

//Default Data
$error			= false;
$error_msg		= null;
$error_level	= null;
$error_code		= 0;
$stmt			= null;
$mysqli			= null;

$request		= null;
$io_did			= null;

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
if(!grant_access([3003]))
{
	error_response_json(
		"Access denied- Distributor details view is not allowed.",
		1,
		__LINE__,
		null,
		null);
}

// Empty data received in request.
if(!isset($_POST['token']))
{
	error_response_json(
		"Please login before continuing.",
		1,
		__LINE__,
		null,
		null);
}

// Data received in request.
if(!isset($_POST['request']))
{
	error_response_json(
		"Request data not available.",
		1,
		__LINE__,
		null,
		null);
}

if(!isset($_POST['did']))
{
	error_response_json(
		"Request data not available.",
		1,
		__LINE__,
		null,
		null);
}

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session_token.php';
if (!verify_token($_POST['token']))
{
	error_response_json(
		"Please login again before continuing.",
		1,
		__LINE__,
		null,
		null);
}

//Open DB connection
include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/db_vmsx.php';

// Protect from MySQL injection
$io_request	= mysqli_real_escape_string($mysqli, stripslashes($_POST['request']));
$io_did		= mysqli_real_escape_string($mysqli, stripslashes($_POST['did']));
$data_rec	= json_decode($_POST['data'], true);

if($io_request == "")
{
	error_response_json(
		"Invalid request.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($io_request != "a_dist_update")
{
	error_response_json(
		"Invalid request.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($io_did == "")
{
	error_response_json(
		"Distributor selection undefined.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}




if($data_rec['dist']['agency'][1])
{
	
	$io_temp = mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['agency'][0]));
	// SQL update query
	$sqli_query = "
		UPDATE
			vmsx_db.dists 
		SET 
			agency = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			dists.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param( "sis", $io_temp, $_SESSION['uid'], $io_did);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update the Agency name.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
}

if($data_rec['dist']['cust_name'][1])
{
	
	$io_temp = mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['cust_name'][0]));
	// SQL update query
	$sqli_query = "
		UPDATE
			vmsx_db.dists 
		SET 
			cust_name = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			dists.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param( "sis", $io_temp, $_SESSION['uid'], $io_did);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update the Customer name.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
}

if($data_rec['dist']['cust_no'][1])
{
	
	$io_temp = mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['cust_no'][0]));
	// SQL update query
	$sqli_query = "
		UPDATE
			vmsx_db.dists 
		SET 
			cust_no = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			dists.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param( "sis", $io_temp, $_SESSION['uid'], $io_did);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update the Customer Number.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
}

if($data_rec['dist']['location'][1])
{
	
	$io_temp = mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['location'][0]));
	// SQL update query
	$sqli_query = "
		UPDATE
			vmsx_db.dists 
		SET 
			location = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			dists.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param( "sis", $io_temp, $_SESSION['uid'], $io_did);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update the Location.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
}

if($data_rec['dist']['contact'][1])
{
	
	$io_temp = mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['contact'][0]));
	// SQL update query
	$sqli_query = "
		UPDATE
			vmsx_db.dists 
		SET 
			contact = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			dists.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param( "sis", $io_temp, $_SESSION['uid'], $io_did);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update the Contact Person.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
}

if($data_rec['dist']['tel'][1])
{
	
	$io_temp = mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['tel'][0]));
	// SQL update query
	$sqli_query = "
		UPDATE
			vmsx_db.dists 
		SET 
			tel = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			dists.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param( "sis", $io_temp, $_SESSION['uid'], $io_did);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update the Telephone Number.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
}

if($data_rec['dist']['cel'][1])
{
	
	$io_temp = mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['cel'][0]));
	// SQL update query
	$sqli_query = "
		UPDATE
			vmsx_db.dists 
		SET 
			cel = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			dists.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param( "sis", $io_temp, $_SESSION['uid'], $io_did);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update the Mobile Number.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
}

if($data_rec['dist']['fax'][1])
{
	
	$io_temp = mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['fax'][0]));
	// SQL update query
	$sqli_query = "
		UPDATE
			vmsx_db.dists 
		SET 
			fax = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			dists.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param( "sis", $io_temp, $_SESSION['uid'], $io_did);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update the Fax Number.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
}

if($data_rec['dist']['address'][1])
{
	
	$io_temp = mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['address'][0]));
	// SQL update query
	$sqli_query = "
		UPDATE
			vmsx_db.dists 
		SET 
			address = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			dists.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param( "sis", $io_temp, $_SESSION['uid'], $io_did);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update the Address.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
}

if($data_rec['dist']['email'][1])
{
	
	$io_temp = mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['email'][0]));
	// SQL update query
	$sqli_query = "
		UPDATE
			vmsx_db.dists 
		SET 
			email = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			dists.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param( "sis", $io_temp, $_SESSION['uid'], $io_did);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update the Email Address.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
}

if($data_rec['dist']['transmit_dir'][1])
{
	
	$io_temp = mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['transmit_dir'][0]));
	// SQL update query
	$sqli_query = "
		UPDATE
			vmsx_db.dists 
		SET 
			transmit_dir = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			dists.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param( "sis", $io_temp, $_SESSION['uid'], $io_did);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update the Transmit Directory.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
}

if($data_rec['dist']['receive_dir'][1])
{
	
	$io_temp = mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['receive_dir'][0]));
	// SQL update query
	$sqli_query = "
		UPDATE
			vmsx_db.dists 
		SET 
			receive_dir = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			dists.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param( "sis", $io_temp, $_SESSION['uid'], $io_did);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update the Receive Directory.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
}

if($data_rec['dist']['ftp_server'][1])
{
	
	$io_temp = mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['ftp_server'][0]));
	// SQL update query
	$sqli_query = "
		UPDATE
			vmsx_db.dists 
		SET 
			ftp_server = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			dists.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param( "sis", $io_temp, $_SESSION['uid'], $io_did);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update the SFTP Server.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
}

if($data_rec['dist']['ftp_user'][1])
{
	
	$io_temp = mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['ftp_user'][0]));
	// SQL update query
	$sqli_query = "
		UPDATE
			vmsx_db.dists 
		SET 
			ftp_user = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			dists.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param( "sis", $io_temp, $_SESSION['uid'], $io_did);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update the SFTP User.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
}

if($data_rec['dist']['ftp_password'][1])
{
	
	$io_temp = mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['ftp_password'][0]));
	// SQL update query
	$sqli_query = "
		UPDATE
			vmsx_db.dists 
		SET 
			ftp_password = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			dists.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param( "sis", $io_temp, $_SESSION['uid'], $io_did);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update the SFTP Password.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
}

if($data_rec['dist']['seqno'][1])
{
	
	$io_temp = intval(mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['seqno'][0])));
	
	if(!$io_temp)
	{
		error_response_json(
			"Could not update the Sequence Number. Invalid Integer",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
	
	// SQL update query
	$sqli_query = "
		UPDATE
			vmsx_db.dists 
		SET 
			seqno = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			dists.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param( "iis", $io_temp, $_SESSION['uid'], $io_did);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update the Sequence Number.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
}

if($data_rec['dist']['batch_size'][1])
{
	
	$io_temp = intval(mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['batch_size'][0])));
	
	if(!$io_temp)
	{
		error_response_json(
			"Could not update the Batch Size. Invalid Integer",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
	
	// SQL update query
	$sqli_query = "
		UPDATE
			vmsx_db.dists 
		SET 
			batch_size = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			dists.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param( "iis", $io_temp, $_SESSION['uid'], $io_did);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update the Batch Size.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
}

if($data_rec['dist']['public_key'][1])
{
	
	$io_temp = mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['public_key'][0]));
	// SQL update query
	$sqli_query = "
		UPDATE
			vmsx_db.dists 
		SET 
			public_key = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			dists.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param( "sis", $io_temp, $_SESSION['uid'], $io_did);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update the Public Key.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
}

if($data_rec['dist']['ftp_transmit_dir'][1])
{
	
	$io_temp = mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['ftp_transmit_dir'][0]));
	// SQL update query
	$sqli_query = "
		UPDATE
			vmsx_db.dists 
		SET 
			ftp_transmit_dir = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			dists.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param( "sis", $io_temp, $_SESSION['uid'], $io_did);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update the SFTP Transmit Directory.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
}

if($data_rec['dist']['ftp_receive_dir'][1])
{
	
	$io_temp = mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['ftp_receive_dir'][0]));
	// SQL update query
	$sqli_query = "
		UPDATE
			vmsx_db.dists 
		SET 
			ftp_receive_dir = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			dists.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param( "sis", $io_temp, $_SESSION['uid'], $io_did);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update the SFTP Receive Directory.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
}

if($data_rec['dist']['max_cutoff'][1])
{
	
	$io_temp = intval(mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['max_cutoff'][0])));
	
	if(!$io_temp)
	{
		error_response_json(
			"Could not update the Max Cutoff. Invalid Integer",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
	
	// SQL update query
	$sqli_query = "
		UPDATE
			vmsx_db.dists 
		SET 
			max_cutoff = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			dists.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param( "iis", $io_temp, $_SESSION['uid'], $io_did);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update the Max Cutoff.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
}

if($data_rec['dist']['starter_size'][1])
{
	
	$io_temp = intval(mysqli_real_escape_string($mysqli, stripslashes($data_rec['dist']['starter_size'][0])));
	
	if(!$io_temp)
	{
		error_response_json(
			"Could not update the Starter Size. Invalid Integer",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
	
	// SQL update query
	$sqli_query = "
		UPDATE
			vmsx_db.dists 
		SET 
			starter_size = ?,
			updated_by = ?,
			updated_on = NOW()
		WHERE
			dists.id = ?
	";
	
	$stmt = $mysqli->prepare($sqli_query);
	$stmt->bind_param( "iis", $io_temp, $_SESSION['uid'], $io_did);
	$exe_result = $stmt->execute();
	if(!$exe_result)
	{
		error_response_json(
			"Could not update the Starter Size.",
			1,
			__LINE__,
			$stmt,
			$mysqli);
	}
}

//Close statment and db connections
if(isset($stmt) && $stmt != null){$stmt->close();}
if(isset($mysqli) && $mysqli){$mysqli->close();}

//create a JSON data structure.
$data = array (	
	'errors' 			=> $error,
	'error_msg'			=> $error_msg,
	'error_level'		=> $error_level,
	'error_code'		=> $error_code,
	'session_timeout'	=> $_SESSION['timeout']
);

//Respond to request.
echo json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
?>