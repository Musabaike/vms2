<?php
//Start session
session_start();

// Prevent caching.
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 01 Jan 1996 00:00:00 GMT');
// The JSON standard MIME header.
header('Content-type: application/json');

//Default Data
$error			= false;
$error_msg		= null;
$error_level	= null;
$error_code		= 0;
$stmt			= null;
$mysqli			= null;

$request		= null;
$io_uname		= null;
$io_pword		= null;

$data_dist		= null;

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
if(!grant_access([3003]))
{
	error_response_json(
		"Access denied- Distributor details view is not allowed.",
		1,
		__LINE__,
		null,
		null);
}

// Empty data received in request.
if(!isset($_POST['token']))
{
	error_response_json(
		"Please login before continuing.",
		1,
		__LINE__,
		null,
		null);
}

// Data received in request.
if(!isset($_POST['request']))
{
	error_response_json(
		"Request data not available.",
		1,
		__LINE__,
		null,
		null);
}

if(!isset($_POST['did']))
{
	error_response_json(
		"Request data not available.",
		1,
		__LINE__,
		null,
		null);
}

include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session_token.php';
if (!verify_token($_POST['token']))
{
	error_response_json(
		"Please login again before continuing.",
		1,
		__LINE__,
		null,
		null);
}

//Open DB connection
include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/db_vmsx.php';

// Protect from MySQL injection
$io_request	= mysqli_real_escape_string($mysqli, stripslashes($_POST['request']));
$io_did	= mysqli_real_escape_string($mysqli, stripslashes($_POST['did']));

if($io_request == "")
{
	error_response_json(
		"Invalid request.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($io_request != "a_dist")
{
	error_response_json(
		"Invalid request.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($io_did == "")
{
	error_response_json(
		"Distributor selection undefined.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}


// SQL query to fetch user info
$sqli_user_info = "
	SELECT
		d.id,
		d.agency,
		d.cust_name,
		d.cust_no,
		d.contact,
		d.tel,
		d.cel,
		d.fax,
		d.email,
		d.location,
		d.address,
		d.transmit_dir,
		d.receive_dir,
		d.ftp_server,
		d.ftp_user,
		d.ftp_password,
		d.seqno,
		d.batch_size,
		d.public_key,
		d.email,
		d.ftp_transmit_dir,
		d.ftp_receive_dir,
		d.max_cutoff,
		d.starter_size,
		c.username AS created_by_uname,
		d.created_on,
		u.username AS updated_by_uname,
		d.updated_on
	FROM
		vmsx_db.dists d
	LEFT JOIN
		(
			SELECT
				id,
				username
			FROM vmsx_db.users
			UNION ALL
			SELECT
				id,
				username
			FROM vmsx_db.users_del
		) c
	ON
		d.created_by=c.id
	LEFT JOIN
		(
			SELECT
				id,
				username
			FROM vmsx_db.users
			UNION ALL
			SELECT
				id,
				username
			FROM vmsx_db.users_del
		) u
	ON
		d.updated_by=u.id
	WHERE
		d.id=?
	";
$stmt = $mysqli->prepare($sqli_user_info);
$stmt->bind_param("i", $io_did);
$stmt->execute();
if(!($result = $stmt->get_result()))
{
	error_response_json(
		"Could not connect to the database.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}
$num_rows = $result->num_rows;
if($num_rows === 0)
{
	error_response_json(
		"Could not retrieve distributor information.",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

if($num_rows > 1)
{
	error_response_json(
		"Conflicting distributor information",
		1,
		__LINE__,
		$stmt,
		$mysqli);
}

while($row = $result->fetch_assoc())
{
	$data_dist		= $row;
}

//Close statment and db connections
if(isset($stmt) && $stmt != null){$stmt->close();}
if(isset($mysqli) && $mysqli){$mysqli->close();}

//create a JSON data structure.
$data = array (	
	'errors' 			=> $error,
	'error_msg'			=> $error_msg,
	'error_level'		=> $error_level,
	'error_code'		=> $error_code,
	'session_timeout'	=> $_SESSION['timeout'],
	'dist'				=> $data_dist
);

//Respond to request.
echo json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
?>