<div id="modal_dist_add" class="modal modal-fixed-footer">
	<div class="modal-content">
		<h4>Add a Distributor</h4>
		<div class="row">
			<div class="col s12 m2 l3">
			</div>
			<div class="col s12 m8 l6">
				<form class="row">
					<div class="input-field col s12">
						<i class="material-icons prefix">domain</i>
						<input placeholder="New Distributor/Agency name..." id="io_dist_agency" type="text" data-length="10" maxlength="10" class="validate">
						<label for="io_dist_agency">Distributor</label>
					</div>
					<div class="input-field col s12">
						<i class="material-icons prefix">description</i>
						<input placeholder="New distributor description..." id="io_dist_descr" type="text" data-length="50" maxlength="50" class="validate">
						<label for="io_dist_description">Description</label>
					</div>
				</form>
			</div>
			<div class="col s12 m2 l3">
			</div>
		</div>

		<div class="row">
			<div class="col s12 m2 l3">
			</div>
			<div class="col s12 m8 l6">
				<p>Information: </p>
			</div>
			<div class="col s12 m2 l3">
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<a href="#!" class="modal-close waves-effect waves-red btn-flat">Cancel</a>
		<a id="mod_dist_btn_add" data-token="<?php echo create_token();?>" href="#!" class="waves-effect waves-green btn-flat">Create</a>
	</div>
</div>