var elems, options;
var tt_sessions;
var mod_file_add;

var files_page_no = 1;
var files_page_limit = 10;
var files_sort_order = "DESC";
var files_sort_column = 0;

$(document).ready(function(){

	prepareData();
	initialise_Mcss_all();
	initialise_Btns();
	initialise_Forms();
	
	//Default data: Fetch files
	files_get($("body").attr('data-token'), null);
	
});

//INITIALIZATION
function prepareData()
{	//Prepare DOM elements for initialising
	getColumnsToFilterBy('upload_tbl',		'files_filter_sel');

}

function initialise_Mcss_all()
{	//MaterializeCSS initializations
	//Modals
	mod_file_add = M.Modal.init(document.querySelector('#modal_file_add'), {});
	
	//Tabs:
	options = {
		duration: 300
	};
	M.Tabs.init(document.querySelectorAll('.tabs'), options);
	
	//Tooltips:
	options = {
		enterDelay: 500
	};
    M.Tooltip.init(document.querySelectorAll('.tooltipped'), options);
	
}

function initialise_Btns()
{	//Initialise Button listeners
	//Search buttons with column filter select
	$('#btn_files_search').click(function(){
		event.preventDefault();
		files_page_no = 1;
		files_get($("body").attr('data-token'), null);
	});
	
	//Edit
	$('#btn_file_add').click(function(){
		event.preventDefault();
		mod_file_add.open();
	});
	
		//Add
	$('#mod_file_btn_add').click(function(){
		event.preventDefault();
		file_add($("body").attr('data-token'));
	});

	//Fetch all data on Tab select
	$("#files_tab").click(function (){
		event.preventDefault();
		sort_clear('#upload_tbl');
		document.getElementById("files_filter").value="";
		files_get($("body").attr('data-token'));
	});

	//Sort Button: i.e. Header cell
	$('#upload_tbl > thead > tr > th').click(function(){
		event.preventDefault();
		sortByColumn("#upload_tbl", this);
	});
		
}

function initialise_Forms()
{	//Initialise Form listeners	
	//Select dropdown
    	M.FormSelect.init(document.querySelectorAll('select'), {});
	
	//Paging
	$("#page_limit_files").change(function (){
		files_page_no = 1;
		files_get($("body").attr('data-token'), null);
	});

	M.updateTextFields();
}

function files_get(token, caller)
{	//Fetch all files in the database
	filter_data = {
		columns:	$('#files_filter_sel').val(),
		keyword:	document.getElementById("files_filter").value
	};

	if(caller != null)
	{
		if(caller.hasAttribute("data-page"))
		{	//Check if page change requested
			let temp_page_no = caller.getAttribute("data-page");
			if (temp_page_no != null && temp_page_no != "null")
			{	//Set new page by selection
				files_page_no = temp_page_no;
			}
			else
			{
				return;
			}
		}
	}
	//#page_limit
	files_page_limit = $('#page_limit_files').val();
	
	//console.log("request: all_files, token: " +token+ ", page_no: " + files_page_no + ", page_limit: " + files_page_limit + ", sort_order: " + files_sort_order + ", sort_column: " + files_sort_column + ", search: ");

	$('#upload_tbl > tbody').text(""); 
	$('.progress').remove();
	$('#upload_tbl').parent().append('<div class="progress"><div class="indeterminate"></div></div>');
	$.post('/en/settings/php/files.php',
		{
			request: 'all_files',
			token: token,
			page_no: files_page_no,
			page_limit: files_page_limit,
			sort_order: files_sort_order,
			sort_column: files_sort_column,
			filter: filter_data
		},
		function(json)
		{
			paging_update('page_files', json['page_no'], json['page_total']);
			//Paging click event
			$('#page_files li').on("click", "a", function(){
				event.preventDefault();
				files_get($("body").attr('data-token'), this);
			});	
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			files_append('#upload_tbl', json['files']);
		},
		'json'
	).done(function()
	{
		$('.progress').remove();
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	
	return null;
}
//<button class="alert-success"><a href="download.php?filename=<?php echo $name;?>&f=<?php echo $row['fname'] ?>">Download</a></button>
function files_append(table_id, files_array)
{	//Add file to a specified table
	if (files_array == null) return false;
	var html = "";
	files_array.forEach(function(item, index, data){	
		html = html + 
			'<tr data-fid="'+	data[index]['id'] +
			'"><td>' +		(data[index]['name']!=null?data[index]['name']:'') + 
			'</td><td>' +		(data[index]['created_on']!=null?data[index]['created_on']:'') + 
			'</td><td>' +		(data[index]['created_by_uname']!=null?data[index]['created_by_uname']:'') + 
			'</td><td><button class="alert-success"><a href="php/download.php?filename=' + data[index]['name'] + '&f=' + data[index]['fname'] + '"><i class="material-icons">' + "file_download" +
			'</i></a></button></td></tr>';
			//console.log(html);			
	});
	$(table_id + ' > tbody').append(html);
	return true;
}

function file_add(token)
{	
	var x = document.getElementById("io_file");
	var file = x.files[0];
	
	console.log("File");
	console.log(file);
	
	if ('name' in file) {
          console.log(file.name);
        }
        if ('size' in file) {
           console.log(file.size);
        }
	 
	
	 
	let json_data = {
		"file":{
			"fname":			document.getElementById("io_file").value,
			
		}
	};
	
	//File { name: "ExampleInvoice.csv", lastModified: 1607029956508, webkitRelativePath: "", size: 78, type: "text/csv" }
	var oldpath = file.path;
		console.log(oldpath);
      	var newpath = 'C:/Users/Your Name/' + file.name;
      	console.log(newpath);      	
	console.log(x);
		
	$.post('/en/settings/php/file_add.php',
		{
			request: 'file_add',
			token: token,
			data: JSON.stringify(json_data),
			file: file.source
			
		},
		function(json)
		{
			if(json['error']){
				let toastHTML = '<span>' + json['error_level'] + '! - ' + json['error_msg'] + '</span>';
				M.toast({html: toastHTML});
				if (json['session_timeout']){ setTimeout( function () {window.location = "/";}, 3000); //Delayed redirect after timeout
				}
				return null;
			}
			
			//Successful response
			files_append('#upload_tbl', json['files']);
			
			mod_file_add.close();
			var toastHTML = '<span>The file has been uploaded.</span>';
			M.toast({html: toastHTML});
		},
		'json'
	).done(function()
	{
		
	}).fail(function(jqXHR,status,err)
	{
		
	}).always(function()
	{
		
	});
	return null;
}


//MISC
function getColumnsToFilterBy(table_id, filter_sel_id)
{	//Adds columns' indexes and names to to the drop down filter
	var testThisFunc = document.querySelectorAll('#'+table_id+'> thead > tr > th')
	testThisFunc.forEach(function(value,index,array){
		//console.log('index: '+ index + ', Value: ' + value.innerText);
		var html = '<option value="'+index+'" selected>'+value.innerText+'</option>';
		$('#'+filter_sel_id).append(html);
	});
	
}

function sortTable(table_id, column, order)
{	//Sorting Algorithm used by sortByColumn()
	switch(table_id)
	{
		case "#upload_tbl":
			files_sort_order = order;
			files_sort_column = column;
			files_get($("body").attr('data-token'), null);
			break;
		default:
	}
}

function sortByColumn(table_id, table_th)
{	//Sort a table a a specified column
	if($(table_th).find("i").hasClass("vms-sort"))
	{
		sortTable(table_id, $(table_th).index(), "ASC");
		$(table_th).find("i").removeClass("vms-sort");
		$(table_th).find("i").addClass("vms-sort-asc").text("arrow_drop_up");
		$(table_th).siblings().find("i").addClass("vms-sort").text("");
		$(table_th).siblings().find("i").removeClass("vms-sort-asc");
		$(table_th).siblings().find("i").removeClass("vms-sort-desc");
	}
	else if($(table_th).find("i").hasClass("vms-sort-asc"))
	{
		sortTable(table_id, $(table_th).index(), "DESC");
		$(table_th).find("i").removeClass("vms-sort-asc");
		$(table_th).find("i").addClass("vms-sort-desc").text("arrow_drop_down");
		$(table_th).siblings().find("i").addClass("vms-sort").text("");
		$(table_th).siblings().find("i").removeClass("vms-sort-asc");
		$(table_th).siblings().find("i").removeClass("vms-sort-desc");
	}
	else if($(table_th).find("i").hasClass("vms-sort-desc"))
	{
		sortTable(table_id, $(table_th).index(), "ASC");
		$(table_th).find("i").removeClass("vms-sort-desc");
		$(table_th).find("i").addClass("vms-sort-asc").text("arrow_drop_up");
		$(table_th).siblings().find("i").addClass("vms-sort").text("");
		$(table_th).siblings().find("i").removeClass("vms-sort-asc");
		$(table_th).siblings().find("i").removeClass("vms-sort-desc");
	}
}

function sort_clear(table_id)
{	//Clearing current sort of selected tab
	$(table_id + ' > tbody').text("");
	$(table_id).find("i").removeClass("vms-sort-asc");
	$(table_id).find("i").removeClass("vms-sort-desc");
	$(table_id).find("i").addClass("vms-sort").text("");
}

function paging_update(element_id, new_page_no, new_page_total)
{
//console.log("Element_id: " + element_id + " New page no: " + new_page_no + " New page total: " + new_page_total);
	(new_page_no==null)?new_page_no=1:(typeof new_page_no=="undefined")?new_page_no=1:false;
	(new_page_total==null)?new_page_total=1:(typeof new_page_total=="undefined")?new_page_total=1:false;
	//var html = "";
	document.getElementById(element_id).innerHTML = "";
	//Prev button
	(
		(new_page_no > 1) ?
			$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no - 1) + '" href="#!"><i class="material-icons">chevron_left</i></a></li>') :
			$('#' + element_id).append('<li class="disabled"><a data-page="null" href="#!"><i class="material-icons">chevron_left</i></a></li>')
	);
	//First page
	(
		(new_page_no > 4) ?
			$('#' + element_id).append('<li class="waves-effect"><a data-page="1" href="#!">1</a></li>') :
			false
	);
	//Lower More = ...
	(
		(new_page_no > 5) ?
			((new_page_no == 6) ?
				$('#' + element_id).append('<li class="waves-effect"><a data-page="2" href="#!">2</a></li>') :
				$('#' + element_id).append('<li class="disabled"><a data-page="null" href="#!"><i class="material-icons">more_horiz</i></a></li>')
			) :
			false
	);
	//Current page - 3
	(
		(new_page_no > 3) ?
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no - 3) + '" href="#!">' + (new_page_no - 3) + '</a></li>') :
		false
	);
	//Current page - 2
	(
		(new_page_no > 2) ?
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no - 2) + '" href="#!">' + (new_page_no - 2) + '</a></li>') :
		false
	);
	//Current page - 1
	(
		(new_page_no > 1) ?
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no - 1) + '" href="#!">' + (new_page_no - 1) + '</a></li>') :
		false
	);
	//Current page
	(
		(new_page_no > 0) ?
		$('#' + element_id).append('<li class="active"><a data-page="null" href="#!">' + new_page_no + '</a></li>') :
		false
	);
	//Current page + 1
	(
		((new_page_total - new_page_no) > 1) ?
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no + 1) + '" href="#!">' + (new_page_no + 1) + '</a></li>') :
		false
	);
	//Current page + 2
	(
		((new_page_total - new_page_no) > 2) ?
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no + 2) + '" href="#!">' + (new_page_no + 2) + '</a></li>') :
		false
	);
	//Current page + 3
	(
		((new_page_total - new_page_no) > 3) ?
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_no + 3) + '" href="#!">' + (new_page_no + 3) + '</a></li>') :
		false
	);
	//Upper More = ...
	(
		((new_page_total - new_page_no) > 4) ?
			(((new_page_total - new_page_no) == 5) ?
				$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_total - 1) + '" href="#!">' + (new_page_total - 1) + '</a></li>') :
				$('#' + element_id).append('<li class="disabled"><a data-page="null" href="#!"><i class="material-icons">more_horiz</i></a></li>')
			) :
			false
	);
	//Last page
	(
		(new_page_no >= new_page_total) ?
		true :
		$('#' + element_id).append('<li class="waves-effect"><a data-page="' + (new_page_total) + '" href="#!">' + (new_page_total) + '</a></li>')
	);
	//Next button
	(
		((new_page_total - new_page_no) > 0) ?
		$('#'+element_id).append('<li class="waves-effect right"><a data-page="'+(new_page_no+1)+'" href="#!"><i class="material-icons">chevron_right</i></a></li>') :
		$('#'+element_id).append('<li class="disabled right"><a data-page="null" href="#!"><i class="material-icons">chevron_right</i></a></li>')
	);
}



