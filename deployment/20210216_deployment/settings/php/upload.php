<?php
	//$conn=new PDO('mysql:host=localhost; dbname=demo', 'root', '') or die(mysql_error());
	//Start session
	session_start();
	
	$error			= false;
	$error_msg		= null;
	$error_level	= null;
	$error_code		= 0;
	
	include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_errors.php';
	include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/ctrl_access.php';
	include_once $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/cfg_session_token.php';
	if (!verify_token($_POST['token']))
	{
		error_response_json(
			"Please login before continuing.",
			1,
			__LINE__,
			null,
			null);
	}
	
	//Open DB connection
	include $_SERVER['DOCUMENT_ROOT'].'/configs/secure/access/db_vmsx.php';
	 
	if(isset($_POST['submit'])!=""){
		  $name=$_FILES['file']['name'];
		  $size=$_FILES['file']['size'];
		  $type=$_FILES['file']['type'];
		  $temp=$_FILES['file']['tmp_name'];
		  // $caption1=$_POST['caption'];
		  // $link=$_POST['link'];
		  $fname = date("YmdHis").'_'.$name;
		 // $chk = $conn->query("SELECT * FROM  upload where name = '$name' ")->rowCount();
		 
		 $sqli_query =  "SELECT * FROM vmsx_db.upload where name = ? ";
		 $stmt = $mysqli->prepare($sqli_query);
		 $stmt->bind_param("s",$name);
		 $stmt->execute();
		 
		 if(!($result = $stmt->get_result()))
		{
			error_response_json(
				"Could not connect to the database.",
				1,
				__LINE__,
				$stmt,
				$mysqli);
		}

		$num_rows = $result->num_rows;

		  if($num_rows > 0){
			    $i = 1;
			    $c = 0;
				while($c == 0){
				    	$i++;
				    	$reversedParts = explode('.', strrev($name), 2);
				    	$tname = (strrev($reversedParts[1]))."_".($i).'.'.(strrev($reversedParts[0]));
				    // var_dump($tname);exit;
				    
				    	try{
					    	$stmt = $mysqli->prepare($sqli_query);
						$stmt->bind_param("s",$tname);
						$stmt->execute();
						if(!($result = $stmt->get_result()))
						{
							error_response_json(
								"Could not connect to the database.",
								1,
								__LINE__,
								$stmt,
								$mysqli);
						}
						$num_rows = $result->num_rows;
					    	//$chk2 = $conn->query("SELECT * FROM  upload where name = '$tname' ")->rowCount();
					    	if($num_rows === 0){
					    		$c = 1;
					    		$name = $tname;
					    	}
					    }catch(Exception $e) {
					    
							if ($mysqli->errno === 1062)
							{
								error_response_json(
								"Cannot load the file.",
								1,
								__LINE__,
								$stmt,
								$mysqli);
							}
							
							error_response_json(
								"SELECT Could not connect to database.".$e->getMessage(),
								1,
								__LINE__,
								$stmt,
								$mysqli);
						}
					    }

			    }
		}
		 $move =  move_uploaded_file($temp,"upload/".$fname);
		 if($move){
		 	try
			{
			 	$sqli_query="insert into vmsx_db.upload(name,fname,created_by) values(?,?,?)";
			 	$stmt = $mysqli->prepare($sqli_query);
				$stmt->bind_param("ssi",$name, $fname, $_SESSION['uid']);
				$stmt->execute();
			 	
			 	$num_rows = $result->num_rows;
			}
			catch (Exception $e)
			{
				if ($mysqli->errno === 1062)
				{
					error_response_json(
					"Cannot load the file.",
					1,
					__LINE__,
					$stmt,
					$mysqli);
				}
				
				error_response_json(
					"INSERT Could not connect to database.".$sqli_query,
					1,
					__LINE__,
					$stmt,
					$mysqli);
			}
		
	}
	
	//Close statment and db connections
	if(isset($stmt)){$stmt->close();};
	if(isset($mysqli)){$mysqli->close();};	

	//Respond to request.
	//echo json_encode($data_json, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
	$referer = $_SERVER['HTTP_REFERER'];
	header("Location: $referer");
?>

